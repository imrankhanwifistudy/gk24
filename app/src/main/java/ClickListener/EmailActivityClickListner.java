package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.AppTour;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivityEmailBinding;
import com.daily.currentaffairs.databinding.ForgotpasswordpopupBinding;

import org.json.JSONObject;

import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import ui.AppController;

public class EmailActivityClickListner {
    private Animation animMove, animMove1, animMoveback, animMoveback1;
    private ActivityEmailBinding binding;
    private Activity activity;
    private String email = "", pass = "", status = "1";
    private int resstatus = 1;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public EmailActivityClickListner(ActivityEmailBinding binding, Activity activity) {
        this.binding = binding;
        this.activity = activity;
        animMove = AnimationUtils.loadAnimation(activity, R.anim.move);
        animMove1 = AnimationUtils.loadAnimation(activity, R.anim.move1);
        animMoveback = AnimationUtils.loadAnimation(activity, R.anim.moveback);
        animMoveback1 = AnimationUtils.loadAnimation(activity, R.anim.moveback1);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = sharedPreferences.edit();
    }

    public void onContuine(View view) {
        email = binding.etEmail.getText().toString().trim();
        status = "1";
        pass = "";
        if (Utils.isEmailValid(email)) {
            LoginTask(email, pass);
        }
    }

    private void LoginTask(final String email, final String pass) {
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK + "user-login.php")
                .addBodyParameter("password", pass)
                .addBodyParameter("email", email)
                .addBodyParameter("type", "" + resstatus)// type = 1 for login and type= 0 for register
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Email Activity", String.valueOf(response));

                        if (dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            resstatus = jsonObject.getInt("status");
                            int resstatusfb = jsonObject.optInt("fbstatus");
                            String msg = jsonObject.getString("msg");
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                            if (resstatus == 1) {
                                binding.forgot.setText(String.format("Forgot Password?"));
                                binding.forgot.setTextSize(14);
                                binding.text1.setText(String.format("Enter Password to login into your account"));
                                binding.passContinue.setText(String.format("Login"));
                                binding.etEmail.setHint("Enter password minimum 6 characters");
                                if (resstatusfb == 1) {
                                    binding.text1.setText(String.format("Choose password "));
                                    binding.forgot.setVisibility(View.GONE);
                                }
                                binding.rlLayout1.startAnimation(animMove);
                                binding.rlLayout2.startAnimation(animMove1);
                                binding.rlLayout1.setVisibility(View.GONE);
                                binding.rlLayout2.setVisibility(View.VISIBLE);
                                binding.contuine.setVisibility(View.GONE);
                                binding.passContinue.setVisibility(View.VISIBLE);
                                binding.password.requestFocus();
                                binding.ivBack1.setVisibility(View.VISIBLE);
                                binding.ivBack.setVisibility(View.GONE);
                                binding.etEmail.setVisibility(View.GONE);
                                binding.password.setVisibility(View.VISIBLE);
                            } else if (resstatus == 2) {
                                binding.forgot.setText("2/2");
                                binding.forgot.setTextSize(16);
                                binding.text1.setText(String.format("Choose password to continue registration process"));
                                binding.passContinue.setText(String.format("Continue"));
                                binding.etEmail.setHint("Choose password");
                                binding.rlLayout1.startAnimation(animMove);
                                binding.rlLayout2.startAnimation(animMove1);
                                binding.rlLayout1.setVisibility(View.GONE);
                                binding.rlLayout2.setVisibility(View.VISIBLE);
                                binding.contuine.setVisibility(View.GONE);
                                binding.passContinue.setVisibility(View.VISIBLE);
                                binding.password.requestFocus();
                                binding.ivBack1.setVisibility(View.VISIBLE);
                                binding.ivBack.setVisibility(View.GONE);
                                binding.etEmail.setVisibility(View.GONE);
                                binding.password.setVisibility(View.VISIBLE);
                            } else if (resstatus == 3) {
                                SharePrefrence.getInstance(activity).putBoolean("firsts_home", true);
                                SharePrefrence.getInstance(activity).putBoolean("firsts_vocab", true);
                                SharePrefrence.getInstance(activity).putBoolean("firsts_quiz", true);
                                SharePrefrence.getInstance(activity).putBoolean("firsts_word", true);
                                editor.putString("email", email);
                                AppController.Email = email;
                                editor.putString("mobile", "" + jsonObject.getString("mobile").trim().replace("null", ""));
                                editor.putString("LoginStatus", "Success");
                                editor.putString("uid", "" + jsonObject.getString("uid"));
                                editor.putString("name", "" + jsonObject.getString("name"));
                                editor.commit();
                                SharePrefrence.getInstance(activity).putString(Utills.USERID, jsonObject.getString("uid"));
                                Intent intent = new Intent(activity, AppTour.class);
                                activity.startActivity(intent);
                                activity.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                        anError.printStackTrace();
                    }
                });
    }

    public void onPassContinue(View view) {
        pass = binding.password.getText().toString().trim();
        if (pass.length() > 5) {
            LoginTask(email, pass);
        }
    }

    public void onIvBack(View view) {
        activity.finish();
    }

    public void onIvBack1(View view) {
        resstatus = 1;
        Log.d("layout", "back1");
        binding.rlLayout1.startAnimation(animMoveback1);
        binding.rlLayout2.startAnimation(animMoveback);
        binding.rlLayout1.setVisibility(View.VISIBLE);
        binding.rlLayout2.setVisibility(View.GONE);
        binding.ivBack1.setVisibility(View.GONE);
        binding.ivBack.setVisibility(View.VISIBLE);
        binding.contuine.setVisibility(View.VISIBLE);
        binding.passContinue.setVisibility(View.GONE);
        binding.etEmail.requestFocus();
        binding.etEmail.setVisibility(View.VISIBLE);
        binding.password.setVisibility(View.GONE);
    }

    public void onForgot(View view) {
        if (binding.forgot.getText().toString().trim().equalsIgnoreCase("Forgot Password?")) {
            initiatepopupwindow();
        }
    }

    private void initiatepopupwindow() {
        // TODO Auto-generated method stub
        try {
            LayoutInflater inflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final ForgotpasswordpopupBinding Forgotbinding = DataBindingUtil.inflate(inflater, R.layout.forgotpasswordpopup, null, false);
            final PopupWindow popup = new PopupWindow(Forgotbinding.getRoot(), android.widget.Toolbar.LayoutParams.FILL_PARENT,
                    android.widget.Toolbar.LayoutParams.FILL_PARENT);
            Forgotbinding.mailid.setText(binding.etEmail.getText().toString());
            Forgotbinding.mailid.setEnabled(false);
            Forgotbinding.mailid.setClickable(false);
            Forgotbinding.backto.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (popup != null && popup.isShowing() && activity != null)
                        popup.dismiss();
                }
            });
            Forgotbinding.cancellay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    String femail = Forgotbinding.mailid.getText().toString();
                    if (femail.length() > 0 && Utils.isEmailValid(femail)) {
                        ForgetPassTask(femail);
                        if (popup != null && popup.isShowing() && activity != null)
                            popup.dismiss();
                    } else {
                        Toast.makeText(activity, "please enter your Email", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            popup.setFocusable(true);
            popup.update();
            popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            popup.setOutsideTouchable(true);
            popup.setTouchInterceptor(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View arg0, MotionEvent event) {
                    // TODO Auto-generated method stub
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        if (popup != null && popup.isShowing() && activity != null)
                            popup.dismiss();
                        return true;
                    }
                    return false;
                }
            });
            popup.showAtLocation(Forgotbinding.getRoot(), Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void ForgetPassTask(final String email) {
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK + "forgot-password.php")
                .addBodyParameter("email", email)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                        Log.e("FORGOT", String.valueOf(response));
                        try {
                            JSONObject object = new JSONObject(String.valueOf(response));
                            Toast.makeText(activity, "" + object.getString("msg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                        anError.printStackTrace();
                    }
                });
    }
}