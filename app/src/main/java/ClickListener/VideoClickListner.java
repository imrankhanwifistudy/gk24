package ClickListener;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.YoutubeActivity;
import com.daily.currentaffairs.databinding.ItemVideoBinding;

import java.util.ArrayList;

import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DataObject;
import fragment.BookMarkVideoFragment;

public class VideoClickListner {
    private Activity activity;
    private ItemVideoBinding binding;
    private DatabaseHandler db;
    private SharedPreferences sharedPreferences;
    private String bookmark="";
    private DownloadManager.Request request;
    private DownloadManager manager;
    private final int PERMISSION_REQUEST_CODE = 112;
    private ArrayList<DataObject> list;
    private int postion;
    private TextView noTest;
    public VideoClickListner(Activity activity, ItemVideoBinding binding, int postion, String bookmark, ArrayList<DataObject> list, TextView noTest) {
      this.activity=activity;
      this.binding=binding;
      this.postion=postion;
      this.bookmark=bookmark;
      this.noTest=noTest;
      this.list=list;
      db=new DatabaseHandler(activity);
      sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }
    public void onMainLayout(View view) {
        if( binding.bolt.getVisibility() == View.VISIBLE)
            binding.bolt.setVisibility(View.GONE);
        try {
            db.addreadUnread(list.get(postion).getId()+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE), "VideoRead");
            if(!list.get(postion).getUrl().equalsIgnoreCase("")) {
                Intent intent = new Intent(activity, YoutubeActivity.class);
                intent.putExtra("url", list.get(postion).getUrl());
                activity.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onOption(View view) {
        final PopupMenu popup = new PopupMenu(activity, binding.options);
        popup.getMenuInflater().inflate(R.menu.video_menu, popup.getMenu());
        try {
            Boolean check=db.checkVideoPara(list.get(postion).getId()+ SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            if(check){
                popup.getMenu().findItem(R.id.bookmark).setTitle("Unbookmark");
            }else{
                popup.getMenu().findItem(R.id.bookmark).setTitle("Bookmark");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String pdf = list.get(postion).getPpt_url();
        if(pdf.equalsIgnoreCase("")){
            popup.getMenu().findItem(R.id.download).setVisible(false);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @SuppressLint("SetTextI18n")
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.share:
                        if (checkPermission())
                            shareIntent(list.get(postion).getUrl(),list.get(postion).getName());
                        else
                            requestPermission();
                        break;
                    case R.id.bookmark:
                        try {
                            if(popup.getMenu().findItem(R.id.bookmark).getTitle().equals("Bookmark")){
                                Utils.bookmark_para(list.get(postion).getId(),"2",activity,sharedPreferences.getString("uid",""));
                                db.addVideoBook(list.get(postion).getPackageName(),list.get(postion).getUrl(),list.get(postion).getId()+ SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                popup.getMenu().findItem(R.id.bookmark).setTitle("Unbookmark");
                            }else{
                                Utils.unbookmark(list.get(postion).getId(),"2",activity,sharedPreferences.getString("uid",""));
                                db.deleteVideoBook(list.get(postion).getId()+ SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                popup.getMenu().findItem(R.id.bookmark).setTitle("Bookmark");
                                if(bookmark.equalsIgnoreCase("bookmark")){
                                    list.remove(postion);
                                    if(list.size() == 0){
                                        noTest.setVisibility(View.VISIBLE);
                                        noTest.setText("No Bookmark Found");
                                    }
                                    if(BookMarkVideoFragment.adapter != null ){
                                        BookMarkVideoFragment.adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.download:
                           String pdf = list.get(postion).getPpt_url();
                        try {
                            if(!pdf.trim().equalsIgnoreCase("")){
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(pdf));
                                activity.startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                           /*if (!pdf.equals("")) {
                               String fileName = pdf.substring(pdf.lastIndexOf('/') + 1);
                               request = new DownloadManager.Request(Uri.parse(pdf.replace(" ","%20")));
                               request.setDescription("Some descrition");
                               request.setTitle(fileName);
                               request.allowScanningByMediaScanner();
                               request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                               request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
                               manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                               String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
                               int res = activity.checkCallingOrSelfPermission(permission);
                               if (res == PackageManager.PERMISSION_GRANTED) {
                                   manager.enqueue(request);
                               } else {
                                   ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                               }
                           } else {
                               Toast.makeText(activity, "Please check your internet", Toast.LENGTH_SHORT).show();
                           }*/
                        break;
                    case R.id.play:
                        if( binding.bolt.getVisibility() == View.VISIBLE)
                            binding.bolt.setVisibility(View.GONE);
                        try {
                            db.addreadUnread(list.get(postion).getId()+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE), "VideoRead");
                            if(!list.get(postion).getUrl().equalsIgnoreCase("")){
                                Intent intent = new Intent(activity, YoutubeActivity.class);
                                intent.putExtra("url", list.get(postion).getUrl());
                                activity.startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
                return true;
            }
        });
        popup.show(); //showing popup menu
    }
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }
    private void shareIntent(String video_url, String name) {
        Intent Intent1 = new Intent(Intent.ACTION_SEND);
        Intent1.setType("*/*");
        Intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Intent1.putExtra(Intent.EXTRA_SUBJECT, name);
        Intent1.setType("image/jpeg");
        Intent1.putExtra(Intent.EXTRA_TEXT, video_url);
        activity.startActivity(Intent.createChooser(Intent1, "Share GK & Current Affairs"));

    }
    private void requestPermission() {
        int PERMISSION_REQUEST_CODE = 5;
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }
}
