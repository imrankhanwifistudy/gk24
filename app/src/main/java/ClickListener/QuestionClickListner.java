package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import ui.AppController;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.QuizViewActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivityQuestionQuizBinding;
import com.daily.currentaffairs.databinding.DialogBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import Adapter.BookmarkQuizAdapter;
import Adapter.Start_Test_AdapterEnglish;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

public class QuestionClickListner {
    private Activity questionQuiz;
    private DatabaseHandler db;
    private String TestTitleName,Test_name,correct_mark,wrong_mark,total_question,total_time,attempt,title,rankID,wrongM,
            type,week,correctM,RANKUID,totalTime, PractiseTestName,dateQuiz,UID,theme2,activity_Name="";
    private ActivityQuestionQuizBinding binding;
    private ViewPager pager;
    private BookmarkQuizAdapter AdapterEnglish;
    private ArrayList<FreeTestItem> testitem,testitem2,testitem3;
    private Start_Test_AdapterEnglish adapterEnglish;
    private long millisecondss;
    private Drawable drawBack,drawPlay,drawBookMark;
    private SharedPreferences sharedPreferences;
    public QuestionClickListner(Activity questionQuiz, ViewPager pager, ActivityQuestionQuizBinding binding,
                                Start_Test_AdapterEnglish adapterEnglish, long millisecondss, String TestTitleName, String Test_name,
                                String correctM, String RANKUID, String totalTime, String PractiseTestName, ArrayList<FreeTestItem> testitem,
                                String UID, String wrongM) {
        this.questionQuiz=questionQuiz;
        this.pager=pager;
        this.binding=binding;
        this.adapterEnglish=adapterEnglish;
        this.millisecondss=millisecondss;
        this.TestTitleName=TestTitleName;
        this.Test_name=Test_name;
        this.correctM=correctM;
        this.RANKUID=RANKUID;
        this.totalTime=totalTime;
        this.PractiseTestName=PractiseTestName;
        this.testitem=testitem;
        this.UID=UID;
        this.wrongM=wrongM;
        activity_Name="Question";
        theme2=SharePrefrence.getInstance(questionQuiz).getString("Themes");
        switch (theme2) {
            case "night":
                drawBack= Utils.DrawableChange(questionQuiz,R.drawable.ic_pause_24dp,"#ffffff");
                drawPlay= Utils.DrawableChange(questionQuiz,R.drawable.ic_play_black_24dp,"#ffffff");
                break;
            default:
                drawBack= Utils.DrawableChange(questionQuiz, R.drawable.ic_pause_24dp,"#000000");
                drawPlay= Utils.DrawableChange(questionQuiz,R.drawable.ic_play_black_24dp,"#000000");
                break;
        }
        db=new DatabaseHandler(questionQuiz);
    }
    public QuestionClickListner(Activity quizViewActivity, ViewPager pager, ActivityQuestionQuizBinding binding, String TestTitleName,
                                String Test_name, String rankID, ArrayList<FreeTestItem> testitem) {
        questionQuiz=quizViewActivity;
        this.pager=pager;
        this.binding=binding;
        this.Test_name=Test_name;
        this.rankID=rankID;
        this.testitem=testitem;
        db=new DatabaseHandler(questionQuiz);
        this.TestTitleName=TestTitleName;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(questionQuiz);
        theme2=SharePrefrence.getInstance(questionQuiz).getString("Themes");
        switch (theme2) {
            case "night":
                drawBookMark=Utils.DrawableChange(questionQuiz,R.drawable.ic_bookmark_white,"#ffffff");
                break;
            default:
                drawBookMark=Utils.DrawableChange(questionQuiz,R.drawable.ic_bookmark_white,"#000000");
                break;
        }
        activity_Name="soulation";
    }
    public QuestionClickListner(Activity activity, ViewPager pager, ActivityQuestionQuizBinding binding, ArrayList<FreeTestItem> testitem,
                                BookmarkQuizAdapter AdapterEnglish) {
        questionQuiz=activity;
        this.pager=pager;
        this.binding=binding;
        this.testitem=testitem;
        this.AdapterEnglish=AdapterEnglish;
        db=new DatabaseHandler(questionQuiz);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(questionQuiz);
    }

    @SuppressLint("SetTextI18n")
    public void onPauseTimer(View view) {
        QuestionQuiz.count.cancel();
        if (binding.pauseTimer.getText().toString().equals("PAUSE")) {
            SharePrefrence.getInstance(questionQuiz).putLong(SharePrefrence.PAUSEBUTTON, millisecondss);
            binding.pauseTimer.setText("RESUME");
            binding.pauseTimer.setCompoundDrawablesWithIntrinsicBounds(drawPlay, null, null, null);
            adapterEnglish.ConfirmDialogeQuizExit("Are you sure you want to Exit ", false);
        } else {
            long millis = SharePrefrence.getInstance(questionQuiz).getLong(SharePrefrence.PAUSEBUTTON);
            timerTask(millis);
            binding.pauseTimer.setText("PAUSE");
            binding.pauseTimer.setCompoundDrawablesWithIntrinsicBounds(drawBack, null, null, null);
        }
    }
    private void timerTask(long millis) {
        QuestionQuiz.count = new CountDownTimer(millis, 1000) { // adjust the milli seconds here
            @SuppressLint("DefaultLocale")
            public void onTick(long millisUntilFinished) {
                SharePrefrence.getInstance(questionQuiz).putLong(SharePrefrence.PAUSEBUTTON, millisUntilFinished);
                millisecondss = millisUntilFinished;
                binding.timer.setText(String.format("%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                binding.timertest.setText(String.format("%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }
            public void onFinish() {
                QuestionQuiz.count.cancel();
                if (!questionQuiz.isFinishing()) {
                    adapterEnglish.ConfirmDialogeQuizExit("Are you sure you want to Exit ", true);
                }
            }
        }.start();
    }
    public void onbackImage(View view) {
        if(activity_Name.equalsIgnoreCase("Question")){
            QuestionQuiz.count.cancel();
            adapterEnglish.ConfirmDialogeQuizExit("Are you sure you want to Exit ", false);
        }else {
            questionQuiz.finish();
        }
    }
    public void onLang_pref(View view) {
        String lang_type;
        if(activity_Name.equalsIgnoreCase("Question")){
            if (SharePrefrence.getInstance(questionQuiz).getString("quiz_lang").equalsIgnoreCase("ENGLISH")) {
                lang_type = "hindi";
                SharePrefrence.getInstance(questionQuiz).putString("quiz_lang", "HINDI");
            } else {
                lang_type = "english";
                SharePrefrence.getInstance(questionQuiz).putString("quiz_lang", "ENGLISH");
            }
            if (!TestTitleName.equals("")) {
                try {
                    boolean isData = db.CheckInQuiz(Test_name, lang_type);
                    if (isData) {
                        Log.d("offline", "offline");
                        String response = db.getQuizRes(Test_name, lang_type);
                        gktestdata(response, correctM, correctM, RANKUID,  Test_name);
                    } else {
                        getTestDetails(RANKUID,lang_type, correctM, correctM, TestTitleName);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    boolean isData = db.CheckInQuiz(Test_name, lang_type);
                    if (isData) {
                        String response = db.getQuizRes(Test_name, lang_type);
                        ParseData(response, dateQuiz,Test_name);
                    } else {
                        switch (MainActivity.quiz_type) {
                            case "Monthly":
                                dateQuiz = testitem.get(pager.getCurrentItem()).getId();
                                type = "2";
                                week = "";
                                Get_Catagery(RANKUID, Test_name, type, week, lang_type, "1");
                                break;
                            case "Weekly":
                                dateQuiz = testitem.get(pager.getCurrentItem()).getId();
                                type = "1";
                                week = "w-" + MainActivity.months.get(pager.getCurrentItem());
                                Log.d("Week_no", MainActivity.months.get(pager.getCurrentItem()));
                                Log.d("date_quiz", week);
                                Get_Catagery(RANKUID, Test_name, type, week, lang_type, "1");
                                break;
                            default:
                                dateQuiz = testitem.get(pager.getCurrentItem()).getId();
                                type = "0";
                                week = "";
                                //    Log.e("sweta",""+Test_name.charAt(Test_name.length() - 1));
                                String s = String.valueOf(Test_name.charAt(Test_name.length() - 1));
                                if (s.equalsIgnoreCase("1")) {
                                    Get_Catagery(RANKUID, Test_name, type, week, lang_type, "0");
                                } else {
                                    Get_Catagery(RANKUID, Test_name, type, week, lang_type, "1");
                                }
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else{
            if (SharePrefrence.getInstance(questionQuiz).getString("quiz_lang").equalsIgnoreCase("ENGLISH")) {
                lang_type = "HINDI";
                SharePrefrence.getInstance(questionQuiz).putString("quiz_lang", "HINDI");
            } else {
                lang_type = "ENGLISH";
                SharePrefrence.getInstance(questionQuiz).putString("quiz_lang", "ENGLISH");
            }
            Log.d("lang_type","hindi"+" , "+TestTitleName);
            if(!TestTitleName.equals("") && !TestTitleName.equalsIgnoreCase("Quiz Report")){
                try{
                    boolean isData = db.CheckInQuiz(Test_name, lang_type);
                    if (isData) {
                        Log.d("offline","offline");
                        String response = db.getQuizRes(Test_name ,lang_type);
                        ParseData(response,"","");
                    }else{
                        Log.d("offline","online");
                        getTestDetails(rankID,lang_type,"","","");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                try{
                    boolean isData = db.CheckInQuiz(Test_name , lang_type);
                    if (isData) {
                        String response = db.getQuizRes(Test_name ,lang_type);
                        Log.e("respon",response);
                        ParseData(response,"","");
                    }
                    switch (MainActivity.quiz_type) {
                        case "Monthly":
                            type = "2";
                            week = "";
                            Get_Catagery(rankID, Test_name, type, week, lang_type, "1");
                            break;
                        case "Weekly":
                            type = "1";
                            week = "w-" + MainActivity.months.get(pager.getCurrentItem());
                            Log.d("Week_no", MainActivity.months.get(pager.getCurrentItem()));
                            Log.d("date_quiz", week);
                            Get_Catagery(rankID, Test_name, type, week, lang_type, "1");
                            break;
                        default:
                            type = "0";
                            week = "";
                            String s = String.valueOf(Test_name.charAt(Test_name.length() - 1));
                            if (s.equalsIgnoreCase("1")) {
                                Get_Catagery(rankID, Test_name, type, week, lang_type, "0");
                            } else {
                                Get_Catagery(rankID, Test_name, type, week, lang_type, "1");
                            }
                            break;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
    private void gktestdata(String response, String positive, String nagative, String id, String test_name) {
        testitem3 = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response);
            int success = obj.getInt("status");
            correct_mark = obj.getString("right_mark");
            wrong_mark = obj.getString("rowsqsmar");
            total_question = obj.getString("total_question");
            total_time = obj.getString("total_time");
            attempt = obj.getString("attemp");
            if (success == 1) {
                JSONArray array = obj.getJSONArray("response");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsobj = array.getJSONObject(i);
                    FreeTestItem item = new FreeTestItem();
                    item.setId(jsobj.getString("id"));
                    item.setDirection(jsobj.getString("direction"));
                    item.setQuestionenglish(jsobj.getString("question"));
                    item.setOpt_en_1(jsobj.getString("optiona"));
                    item.setOp_en_2(jsobj.getString("optionb"));
                    item.setOp_en_3(jsobj.getString("optionc"));
                    item.setOp_en_4(jsobj.getString("optiond"));
                    if (jsobj.has("optione")) {
                        item.setOp_en_5(jsobj.getString("optione"));
                    } else {
                        item.setOp_en_5("");
                    }
                    item.setAnswer(jsobj.getString("correct_answer"));
                    item.setSolutionenglish(jsobj.getString("explaination"));
                    item.setNoofoption(jsobj.getString("noofoption"));
                    item.setTime(totalTime);
                    item.setTotalquestio(total_question);
                    item.setCorrectmarks(positive);
                    item.setWrongmarks(nagative);
                    item.setNo_of_attempt(attempt);
                    testitem3.add(item);
                }
            }
            if (positive.trim().length() > 0 && nagative.trim().length() > 0) {
                try {
                    db.addreadUnread(test_name, "TopicTest");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(questionQuiz, QuestionQuiz.class);
                intent.putExtra("TopicWiseTest", "TopicWiseTest");
                intent.putExtra("language", "english");
                intent.putExtra("testitem", testitem3);
                intent.putExtra("catname", "");
                intent.putExtra("testname", test_name);
                intent.putExtra("correctmark", positive);
                intent.putExtra("wrongmark", nagative);
                intent.putExtra("totalque", total_question);
                intent.putExtra("time", totalTime);
                intent.putExtra("topictest", true);
                intent.putExtra("UID", UID);
                intent.putExtra("RANKUID", id);
                intent.putExtra("PractiseTestName", PractiseTestName);
                intent.putExtra("TestTitleName", TestTitleName);
                questionQuiz.startActivityForResult(intent, 1000);
                questionQuiz.finish();
            } else {
                db.addreadUnread(test_name, "TopicTest");
                Intent intent = new Intent(questionQuiz, QuestionQuiz.class);
                intent.putExtra("TopicWiseTest", "TopicWiseTest");
                intent.putExtra("language", "english");
                intent.putExtra("testitem", testitem3);
                intent.putExtra("catname", "");
                intent.putExtra("testname", test_name);
                intent.putExtra("correctmark", positive);
                intent.putExtra("wrongmark", nagative);
                intent.putExtra("totalque", total_question);
                intent.putExtra("time", totalTime);
                intent.putExtra("topictest", true);
                intent.putExtra("UID", UID);
                intent.putExtra("RANKUID", id);
                intent.putExtra("PractiseTestName", PractiseTestName);
                intent.putExtra("TestTitleName", TestTitleName);
                questionQuiz.startActivityForResult(intent, 1000);
                questionQuiz.finish();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void getTestDetails(final String id,final String lang_type, final String positive, final String nagative, final String TestTitleName) {
        final ProgressDialog progressDialog = new ProgressDialog(questionQuiz);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        String uniq = year + id + "TopicTest" + SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE);
        final String test_name = uniq;
        AndroidNetworking.post(Utills.BASE_URLGK + "gk-questions.php")
                .addBodyParameter("testid", id)
                .addBodyParameter("lang", lang_type)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter", String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if(activity_Name.equalsIgnoreCase("Question")){
                                correct_mark = obj.optString("right_mark");
                                wrong_mark = obj.optString("rowsqsmar");
                                total_question = obj.optString("total_question");
                                total_time = obj.optString("total_time");
                                attempt = obj.optString("attemp");
                                if (success == 1) {
                                    gktestdata(String.valueOf(response), positive, nagative, id,  test_name);
                                    try {
                                        boolean isData = db.CheckInQuiz(test_name, lang_type);
                                        if (!isData) {
                                            db.addQuizRes(test_name, String.valueOf(response), lang_type);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (success == 0) {
                                    if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                                        progressDialog.dismiss();
                                    Toast.makeText(questionQuiz, "Internal error ocured", Toast.LENGTH_LONG).show();
                                }
                            }else {
                                if (success == 1) {
                                    ParseData(String.valueOf(response),"","");
                                    try{
                                        boolean isData = db.CheckInQuiz(test_name, lang_type);
                                        if (!isData) {
                                            db.addQuizRes(test_name, String.valueOf(response),lang_type);
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                } else if (success == 0) {
                                    if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                                        progressDialog.dismiss();
                                    Toast.makeText(questionQuiz, "Internal error ocured", Toast.LENGTH_LONG).show();
                                }
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                            progressDialog.dismiss();
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                            progressDialog.dismiss();

                    }
                });
    }
    private void ParseData(String response, String idQuiz, String TestName) {
        testitem2 = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response);
            int success = obj.getInt("status");
            correct_mark = obj.getString("right_mark");
            wrong_mark = obj.getString("rowsqsmar");
            total_question = obj.getString("total_question");
            total_time = obj.getString("total_time");
            attempt = obj.getString("attemp");
            title = obj.optString("title");
            if (success == 1) {
                JSONArray array = obj.getJSONArray("response");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsobj = array.getJSONObject(i);
                    FreeTestItem item = new FreeTestItem();
                    if(jsobj.has("id"))
                        item.setId(jsobj.getString("id"));
                    else
                        item.setId("");
                    item.setDirection(jsobj.getString("direction"));
                    item.setQuestionenglish(jsobj.getString("question"));
                    item.setOpt_en_1(jsobj.getString("optiona"));
                    item.setOp_en_2(jsobj.getString("optionb"));
                    item.setOp_en_3(jsobj.getString("optionc"));
                    item.setOp_en_4(jsobj.getString("optiond"));
                    if (jsobj.has("optione")) {
                        item.setOp_en_5(jsobj.getString("optione"));
                    } else {
                        item.setOp_en_5("");
                    }
                    item.setAnswer(jsobj.getString("correct_answer"));
                    item.setSolutionenglish(jsobj.getString("explaination"));
                    item.setNoofoption(jsobj.getString("noofoption"));
                    item.setTime(total_time);
                    item.setTotalquestio(total_question);
                    item.setCorrectmarks(correct_mark);
                    item.setWrongmarks(wrong_mark);
                    item.setNo_of_attempt(attempt);
                    testitem2.add(item);
                }
                if(activity_Name.equalsIgnoreCase("Question")){
                    Intent i = new Intent(questionQuiz, QuestionQuiz.class);
                    i.putExtra("language", "english");
                    i.putExtra("testitem", testitem2);
                    i.putExtra("catname", "");
                    i.putExtra("testname", TestName);
                    i.putExtra("correctmark", correct_mark);
                    i.putExtra("wrongmark", wrongM);
                    i.putExtra("totalque", total_question);
                    i.putExtra("UID", TestName);
                    i.putExtra("RANKUID", idQuiz + "");
                    i.putExtra("time", "");
                    questionQuiz.startActivityForResult(i, 100);
                    questionQuiz.overridePendingTransition(0, 0);
                    questionQuiz.finish();
                }else{
                    Intent i = new Intent(questionQuiz, QuizViewActivity.class);
                    i.putExtra("language", SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE));
                    i.putExtra("testitem", testitem2);
                    i.putExtra("catname", "");
                    i.putExtra("testname", Test_name);
                    i.putExtra("correctmark", correct_mark);
                    i.putExtra("wrongmark", wrong_mark);
                    i.putExtra("totalque", total_question);
                    i.putExtra("RankID", rankID);
                    i.putExtra("TestTitleName", TestTitleName);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    questionQuiz.startActivity(i);
                    questionQuiz.finish();
                }
            } else if (success == 0) {
                String Message = "Data Not Found";
                Toast.makeText(questionQuiz, "Internal error ocured", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void Get_Catagery(final String idQuiz, final String test_name, final String type, final String week_no, final String testButton,
                              final String quiz_type) {
        final ProgressDialog progressDialog = new ProgressDialog(questionQuiz);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK + "current_quiz.php")
                .addBodyParameter("lang", testButton)
                .addBodyParameter("type", type)
                .addBodyParameter("dttime", idQuiz)
                .addBodyParameter("week", week_no)
                .addBodyParameter("quiz_type", quiz_type)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter", String.valueOf(response));
                        if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                            progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));

                            int success = obj.getInt("status");
                            if (success == 1) {
                                try {
                                    boolean isData = db.CheckInQuiz(test_name, testButton);
                                    if (!isData) {
                                        db.addQuizRes(test_name, String.valueOf(response), testButton);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ParseData(String.valueOf(response), idQuiz, test_name);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(progressDialog != null && progressDialog.isShowing() && questionQuiz != null)
                            progressDialog.dismiss();
                    }
                });
    }
    public void onShareQuiz(View view){
        Utils.ShareData(questionQuiz);
    }
    public void onReportQuiz(View view){
        if(activity_Name.equalsIgnoreCase("soulation"))
            Utils.ReportDialog(questionQuiz,testitem.get(binding.pager.getCurrentItem()).getId(),"2",String.valueOf(AppController.Qtype));
        else{
            Dialog();
        }
    }
    public void onBookmark(View view){
        boolean isAlready;
        try{
            isAlready = db.CheckIsDataAlreadyInBookMarkQuiz(Test_name + testitem.get(pager.getCurrentItem()).getId(),
                    SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE));
            if (isAlready) {
                Toast.makeText(questionQuiz, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                db.deleteBookQuiz(Test_name + testitem.get(pager.getCurrentItem()).getId(), SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE));
                Utils.unbookmark(testitem.get(pager.getCurrentItem()).getId(),"1",questionQuiz,sharedPreferences.getString("uid",""));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    binding.icBookmark.setImageDrawable(drawBookMark);
                 else
                    binding.icBookmark.setImageDrawable(drawBookMark);

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    binding.icBookmark.setImageDrawable(questionQuiz.getResources().getDrawable(R.drawable.ic_bookmark_done, questionQuiz.getApplicationContext().getTheme()));
                } else {
                    binding.icBookmark.setImageDrawable(questionQuiz.getResources().getDrawable(R.drawable.ic_bookmark_done));
                }
                Toast.makeText(questionQuiz, "Bookmark Added", Toast.LENGTH_SHORT).show();
                Log.d("uniq_question2",""+Test_name +testitem.get(pager.getCurrentItem()).getId());
                db.addQuizBook(testitem.get(pager.getCurrentItem()).getQuestionenglish(),
                        testitem.get(pager.getCurrentItem()).getAnswer(), testitem.get(pager.getCurrentItem()).getOpt_en_1(),
                        testitem.get(pager.getCurrentItem()).getOp_en_2(), testitem.get(pager.getCurrentItem()).getOp_en_3(),
                        testitem.get(pager.getCurrentItem()).getOp_en_4(), testitem.get(pager.getCurrentItem()).getOp_en_5(),
                        testitem.get(pager.getCurrentItem()).getSolutionenglish(),
                        testitem.get(pager.getCurrentItem()).getDirection(), Test_name +testitem.get(pager.getCurrentItem()).getId(),
                        SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE), testitem.get(pager.getCurrentItem()).getId());
                Utils.bookmark_para(testitem.get(pager.getCurrentItem()).getId(),"1",questionQuiz,sharedPreferences.getString("uid",""));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @SuppressLint("SetTextI18n")
    public void Dialog() {
        final Dialog dialog = new Dialog(questionQuiz);
        DialogBinding dialogBinding= DataBindingUtil.inflate(LayoutInflater.from(questionQuiz),R.layout.dialog_,null,false);
        dialog.setContentView(dialogBinding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialogBinding.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog != null && dialog.isShowing() && questionQuiz != null)
                    dialog.dismiss();
                Utils.ShareData(questionQuiz);
            }
        });
        dialogBinding.bookmark.setText("Remove Bookmark");
        dialogBinding.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(dialog != null && dialog.isShowing() && questionQuiz != null)
                        dialog.dismiss();
                    boolean isAlready = db.CheckIsDataAlreadyInBookMarkQuiz(testitem.get(binding.pager.getCurrentItem()).getUniq(),
                            SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE));
                    if (isAlready) {
                        db.deleteBookQuiz(testitem.get(binding.pager.getCurrentItem()).getUniq(), SharePrefrence.getInstance(questionQuiz).getString(Utills.DEFAULT_LANGUAGE));
                        Utils.unbookmark(testitem.get(binding.pager.getCurrentItem()).getId(),"1",questionQuiz,sharedPreferences.getString("uid",""));
                        Toast.makeText(questionQuiz, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                        testitem.remove(binding.pager.getCurrentItem());
                        if(testitem.size()==0){
                            binding.pager.setVisibility(View.GONE);
                        }
                        BookmarkActivity.pos = 1;
                        questionQuiz.startActivity(new Intent(questionQuiz, BookmarkActivity.class));
                        questionQuiz.finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }
}