package ClickListener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.daily.currentaffairs.CategoriesActivity;
import com.daily.currentaffairs.GkActivity;
import com.daily.currentaffairs.PracticeTestActivity;

import java.util.List;

public class CategoriesClickListner {
    private List<CategoriesActivity.ExpandableListAdapter.Item> data;
    private String title, catID;
    private int clickPos, pos;
    private Activity mContext;

    public CategoriesClickListner(List<CategoriesActivity.ExpandableListAdapter.Item> data, String title, int pos, int clickPos, String catID,
                                  Activity mContext) {
        this.data = data;
        this.title = title;
        this.clickPos = clickPos;
        this.pos = pos;
        this.catID = catID;
        this.mContext = mContext;
    }

    public void onClickButton(View view) {
        if (clickPos != 6) {
            switch (title) {
                case "Indian History":
                    switch (pos) {
                        case 0:
                        case 11:
                        case 18:
                            break;
                        default:
                            Intent intent = new Intent(mContext, GkActivity.class);
                            intent.putExtra("CatID", catID);
                            intent.putExtra("SubCatID", data.get(pos).Subcatid);
                            intent.putExtra("Title", data.get(pos).text);
                            intent.putExtra("position", clickPos);
                            mContext.startActivity(intent);
                            break;
                    }
                    break;
                case "Geography":
                    switch (pos) {
                        case 0:
                        case 13:
                        case 28:
                            break;
                        default:
                            Intent intent = new Intent(mContext, GkActivity.class);
                            intent.putExtra("CatID", catID);
                            intent.putExtra("SubCatID", data.get(pos).Subcatid);
                            intent.putExtra("Title", data.get(pos).text);
                            intent.putExtra("position", clickPos);
                            mContext.startActivity(intent);
                            break;
                    }
                    break;
                case "General Science":
                    switch (pos) {
                        case 0:
                        case 15:
                        case 28:
                            break;
                        default:
                            Intent intent = new Intent(mContext, GkActivity.class);
                            intent.putExtra("CatID", catID);
                            intent.putExtra("SubCatID", data.get(pos).Subcatid);
                            intent.putExtra("Title", data.get(pos).text);
                            intent.putExtra("position", clickPos);
                            mContext.startActivity(intent);
                            break;
                    }
                    break;
                default:
                    Intent intent = new Intent(mContext, GkActivity.class);
                    intent.putExtra("CatID", catID);
                    intent.putExtra("SubCatID", data.get(pos).Subcatid);
                    intent.putExtra("Title", data.get(pos).text);
                    intent.putExtra("position", clickPos);
                    mContext.startActivity(intent);
                    break;
            }
        } else {
            Intent intent = new Intent(mContext, PracticeTestActivity.class);
            intent.putExtra("CatID", data.get(pos).Subcatid);
            intent.putExtra("SubCatID", "");
            intent.putExtra("Title", data.get(pos).text);
            intent.putExtra("position", clickPos);
            mContext.startActivity(intent);
        }
    }
}
