package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupMenu;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.R;
import ui.activity.SplashActivity;
import com.daily.currentaffairs.databinding.LogoutPopupBinding;
import com.daily.currentaffairs.databinding.ProfileFragmentBinding;
import org.json.JSONObject;
import DB.SharePrefrence;
import DB.Utills;

public class ProfileClickListner {
    private ProfileFragmentBinding binding;
    private Activity activity;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    @SuppressLint("CommitPrefEdits")
    public ProfileClickListner(ProfileFragmentBinding binding, Activity activity) {
        this.binding=binding;
        this.activity=activity;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = sharedPreferences.edit();
    }
    public void onUpdate(View view) {
        PopupMenu popup = new PopupMenu(activity,  binding.update);
        popup.getMenuInflater().inflate(R.menu.edit_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.Edit:
                        if (binding.mobile.getText().length() == 0) {
                            binding.mobile.requestFocus();
                            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        } else {
                            binding.name.requestFocus();
                            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                        binding.name.setFocusableInTouchMode(true);
                        binding.name.setFocusable(true);
                        binding.name.setCursorVisible(true);
                        binding.name.setTextColor(Color.parseColor("#88000000"));
                        binding.name.requestFocus();
                        binding.mobile.setFocusable(true);
                        binding.mobile.setCursorVisible(true);
                        binding.mobile.setFocusableInTouchMode(true);
                        binding.mobile.setTextColor(Color.parseColor("#88000000"));
                        binding.tvNext.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });
        popup.show(); //showing popup menu
    }
    public void onSignINOUT(View view) {
        final Dialog fb_dialog = new Dialog(activity,R.style.MyActivityDialogTheme);
        LogoutPopupBinding logoutbinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.logout_popup, null, false);
        fb_dialog.setContentView(logoutbinding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(fb_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        fb_dialog.getWindow().setAttributes(lp);
        fb_dialog.show();
        logoutbinding.langPopup.setVisibility(View.GONE);
        logoutbinding.logoutPopup.setVisibility(View.VISIBLE);
        logoutbinding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().clear().apply();
                sharedPreferences.edit().remove("uid").apply();
                sharedPreferences.edit().remove("LoginStatus").apply();
                sharedPreferences.edit().remove("razorpayPaymentID").apply();
                if(fb_dialog != null && fb_dialog.isShowing() && activity != null)
                    fb_dialog.dismiss();
                SharePrefrence.getInstance(activity).putString("social_image", "");
                binding.logouttxt.setImageResource(R.drawable.ic_dp_settings);
                Intent intent = new Intent(activity, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                ActivityCompat.finishAffinity(activity);
                activity.finish();

            }
        });
        logoutbinding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fb_dialog != null && fb_dialog.isShowing() && activity != null)
                    fb_dialog.dismiss();
            }
        });
    }
    public void onTvNext(View view) {
        String user_name = binding.name.getText().toString().trim();
        String user_mobile = binding.mobile.getText().toString().trim();
        if (user_name.length() == 0) {
            binding.name.setError("Please enter name");
        } else if (user_mobile.length() < 10) {
            binding.mobile.setError("Please Enter valid mobile nmber");
        } else {
            update(user_name, user_mobile);
        }
    }
    private void update(final String user_name, final String user_mobile) {
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK + "update_profile.php")
                .addBodyParameter(" name", "" + "" + user_name)
                .addBodyParameter(" uid", "" + "" + sharedPreferences.getString("uid", ""))
                .addBodyParameter(" mobile", "" + "" + user_mobile)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                        try {
                            JSONObject err = new JSONObject(String.valueOf(response));
                            String msg = err.getString("msg");
                            int status = err.getInt("status");
                            if (status == 1) {
                                binding.mobile.setFocusable(false);
                                binding.mobile.setCursorVisible(false);
                                binding.mobile.setFocusableInTouchMode(false);
                                binding.mobile.setTextColor(Color.parseColor("#363636"));
                                binding.name.setFocusable(false);
                                binding.name.setCursorVisible(false);
                                binding.name.setFocusableInTouchMode(false);
                                binding.name.setTextColor(Color.parseColor("#363636"));
                                editor.putString("mobile", user_mobile);
                                editor.putString("name", user_name);
                                editor.commit();
                                binding.tvNext.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(dialog != null && dialog.isShowing() && activity != null)
                            dialog.dismiss();
                    }
                });
    }
}