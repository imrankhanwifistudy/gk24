package ClickListener;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivityMainBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import Adapter.DateAdapterMain2;
import Adapter.QuizAdapter;
import Custom.MainUtils;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;
import fragment.LearnFragment;
import fragment.Paragraph;
import fragment.QuizFragment;
import fragment.SettingFRagment;
import fragment.VideoFragment1;

public class MainClickListner {
    private ActivityMainBinding activityMainBinding;
    private ArrayList<DateItem> arrDateItem, arrDateItemwithBlank;
    MainActivity activity;

    public MainClickListner(ActivityMainBinding activityMainBinding, ArrayList<DateItem> arrDateItem, ArrayList<DateItem> arrDateItemwithBlank,
                            MainActivity activity) {
        this.activityMainBinding = activityMainBinding;
        this.arrDateItem = arrDateItem;
        this.arrDateItemwithBlank = arrDateItemwithBlank;
        this.activity = activity;
    }

    public void onQuiz(View view) {
        if (MainActivity.flag) {
            MainActivity.flag = false;
            MainActivity.activityMainBinding.llQuizType.setVisibility(View.VISIBLE);
        } else {
            MainActivity.flag = true;
            MainActivity.activityMainBinding.llQuizType.setVisibility(View.GONE);
        }
    }

    public void onWeekly(View view) {
        week();
        QuizAdapter adapterr = new QuizAdapter(activity, QuizFragment.pagerMain, MainActivity.size_date, MainActivity.actualdate);
        QuizFragment.pagerMain.setAdapter(adapterr);
    }

    public void onMonthly(View view) {
        month();
        QuizAdapter adapterr = new QuizAdapter(activity, QuizFragment.pagerMain, MainActivity.size_date, MainActivity.actualdate);
        QuizFragment.pagerMain.setAdapter(adapterr);
    }

    public void ontxteditorial(View view) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MainUtils.date(activity, arrDateItem, arrDateItemwithBlank);
                activityMainBinding.llQuizType.setVisibility(View.GONE);
                MainActivity.flag = true;
                MainUtils.setZero(activity);
                SharePrefrence.getInstance(activity).putString(Utills.WHICH_SELECTED, "1");
                if (MainActivity.paragraph == null) {
                    MainActivity.paragraph = new Paragraph();
                    activity.getSupportFragmentManager().beginTransaction().add(R.id.continar, MainActivity.paragraph)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commitAllowingStateLoss();
                }
                MainUtils.replaceFragmentFun(MainActivity.paragraph, activity);
                activityMainBinding.txteditorial.setAlpha(1);
                activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.topLayout.setVisibility(View.VISIBLE);
                try {
                    if (MainActivity.adapter2 != null) {
                        MainActivity.adapter2.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ontxtquiz(View view) {
        switch (MainActivity.quiz_type) {
            case "Monthly":
                month();
                break;
            case "Weekly":
                week();
                break;
        }
        activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainActivity.flag = true;
        activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
        activityMainBinding.calendarOpen.setVisibility(View.GONE);
        MainUtils.setZero(activity);
        SharePrefrence.getInstance(activity).putString(Utills.WHICH_SELECTED, "3");
        if (MainActivity.quizFragment == null) {
            MainActivity.quizFragment = new QuizFragment();
            activity.getSupportFragmentManager().beginTransaction().add(R.id.continar, MainActivity.quizFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commitAllowingStateLoss();
        }
        MainUtils.replaceFragmentFun(MainActivity.quizFragment, activity);
        activityMainBinding.txtquiz.setAlpha(1);
        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.topLayout.setVisibility(View.VISIBLE);
        try {
            if (MainActivity.adapter2 != null) {
                MainActivity.adapter2.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ontxtlearn(View view) {
        activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainActivity.flag = true;
        activityMainBinding.progressView.setVisibility(View.GONE);
        MainUtils.setZero(activity);
        if (MainActivity.learnFragment == null) {
            MainActivity.learnFragment = new LearnFragment();
            activity.getSupportFragmentManager().beginTransaction().add(R.id.continar, MainActivity.learnFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
        }
        MainUtils.replaceFragmentFun(MainActivity.learnFragment, activity);
        activityMainBinding.txtlearn.setAlpha(1);
        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.topLayout.setVisibility(View.GONE);
    }

    public void ontxtsetting(View view) {
        activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainActivity.flag = true;
        MainUtils.setZero(activity);
        if (MainActivity.settingFRagment == null) {
            MainActivity.settingFRagment = new SettingFRagment();
            activity.getSupportFragmentManager().beginTransaction().add(R.id.continar, MainActivity.settingFRagment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
        }
        MainUtils.replaceFragmentFun(MainActivity.settingFRagment, activity);
        activityMainBinding.txtsetting.setAlpha(1);
        activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.topLayout.setVisibility(View.GONE);
    }

    public void ontxtvideo(View view) {
        if (activityMainBinding.ivNewVideo.getVisibility() == View.VISIBLE) {
            SharePrefrence.getInstance(activity).putInteger("count", 0);
            activityMainBinding.ivNewVideo.setVisibility(View.GONE);
        }
        activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainActivity.flag = true;
        MainUtils.setZero(activity);
        if (MainActivity.videoFragment1 == null) {
            MainActivity.videoFragment1 = new VideoFragment1();
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.continar, MainActivity.videoFragment1)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
        }
        MainUtils.replaceFragmentFun(MainActivity.videoFragment1, activity);
        activityMainBinding.txtvideo.setAlpha(1);
        activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.topLayout.setVisibility(View.GONE);
    }

    public void onDaily(View view) {
        MainActivity.quiz_type = "Daily";
        MainActivity.flag = true;
        MainActivity.activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainUtils.date(activity, arrDateItem, arrDateItemwithBlank);
        activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
        activityMainBinding.tvDaily.setTextColor(Color.parseColor("#37C076"));
        activityMainBinding.monthly.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.weekly.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.tvSearch.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.tvSearch.setVisibility(View.VISIBLE);
        MainUtils.setZero(activity);
        QuizAdapter adapterr = new QuizAdapter(activity, QuizFragment.pagerMain, arrDateItem.size(), MainActivity.actualdate);
        QuizFragment.pagerMain.setAdapter(adapterr);
    }

    private void week() {
        activityMainBinding.tvSearch.setVisibility(View.GONE);
        activityMainBinding.weekly.setTextColor(Color.parseColor("#37C076"));
        activityMainBinding.monthly.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.tvDaily.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.tvSearch.setTextColor(Color.parseColor("#000000"));
        MainActivity.quiz_type = "Weekly";
        MainActivity.flag = true;
        MainActivity.activityMainBinding.llQuizType.setVisibility(View.GONE);
        MainUtils.weekcount();
        MainActivity.size_date = MainActivity.weeks.size();
        MainUtils.blankSpace(arrDateItem, arrDateItemwithBlank);
        MainActivity.adapter2 = new DateAdapterMain2(activity, MainActivity.weeks, MainActivity.months, MainActivity.quiz_type, activityMainBinding.pagerdate, activityMainBinding.pagerdate.getCurrentItem());
        activityMainBinding.pagerdate.setAdapter(MainActivity.adapter2);
        try {
            if (MainActivity.adapter2 != null)
                MainActivity.adapter2.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
        activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
        MainUtils.setZero(activity);
    }

    private void month() {
        activityMainBinding.tvSearch.setVisibility(View.GONE);
        activityMainBinding.monthly.setTextColor(Color.parseColor("#37C076"));
        activityMainBinding.tvDaily.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.weekly.setTextColor(Color.parseColor("#000000"));
        activityMainBinding.tvSearch.setTextColor(Color.parseColor("#000000"));
        MainActivity.quiz_type = "Monthly";
        MainActivity.flag = true;
        MainActivity.months = new ArrayList<>();
        MainActivity.monthsyear = new ArrayList<>();
        MainActivity.actualdate = new ArrayList<>();
        MainActivity.activityMainBinding.llQuizType.setVisibility(View.GONE);
        Calendar cal = Calendar.getInstance();
        Date end = cal.getTime();
        Calendar calc = Calendar.getInstance();
       /* calc.set(Calendar.YEAR, 2017);
        calc.set(Calendar.MONTH, 01);
        calc.set(Calendar.DAY_OF_MONTH, 20);*/

        calc.set(Calendar.YEAR, 2019);
        calc.set(Calendar.MONTH, 11);
        calc.set(Calendar.DAY_OF_MONTH, 20);


        Date start = calc.getTime();
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(start);
        while (gcal.getTime().before(end)) {
            gcal.add(Calendar.MONTH, 1);
            cal.add(Calendar.MONTH, -1);
            System.out.println("After " + cal.getTime());
            @SuppressLint("SimpleDateFormat") SimpleDateFormat ftMonth = new SimpleDateFormat("MMM");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat ftyear = new SimpleDateFormat("yyyy");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            String date = format.format(cal.getTime());
            String Month = ftMonth.format(cal.getTime());
            String year = ftyear.format(cal.getTime());
            MainActivity.monthsyear.add(year);
            MainActivity.months.add(Month);
            MainActivity.actualdate.add(date);
        }
        MainActivity.size_date = MainActivity.months.size();
        MainUtils.blankSpace(arrDateItem, arrDateItemwithBlank);
        MainActivity.adapter2 = new DateAdapterMain2(activity, MainActivity.months, MainActivity.monthsyear, MainActivity.quiz_type, activityMainBinding.pagerdate, activityMainBinding.pagerdate.getCurrentItem());
        activityMainBinding.pagerdate.setAdapter(MainActivity.adapter2);
        activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
        MainUtils.setZero(activity);
    }
}