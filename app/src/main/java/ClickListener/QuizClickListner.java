package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.StartTestAdapterBinding;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import java.util.List;
import Adapter.Start_Test_AdapterEnglish;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

public class QuizClickListner {
    private Activity activity;
    private StartTestAdapterBinding binding;
    private ViewPager pager;
    private String posi,Test_name;
    private List<FreeTestItem> testitem;
    private DatabaseHandler db;
    private boolean IsAtouched = false, IsBtouched = false,IsCtouched = false,IsDtouched = false,IsEtouched = false;
    private int attemptCount=0,Marks,CorrectQuestion, InCorrectQuestion;
    private String theme;
    private CircularProgressBar cpb;
    private String Answers,Questions,explation,Total_Marks,Attempt_Question,activity_name="";
    public QuizClickListner(Activity activity, StartTestAdapterBinding binding, ViewPager pager, String posi, String Test_name,
                            List<FreeTestItem> testitem, CircularProgressBar cpb) {
        this.activity=activity;
        this.binding=binding;
        db=new DatabaseHandler(activity);
        this.pager=pager;
        this.posi=posi;
        this.Test_name=Test_name;
        this.testitem=testitem;
        this.cpb=cpb;
        theme = SharePrefrence.getInstance(activity).getString("Themes");
    }
    public QuizClickListner(Activity activity, StartTestAdapterBinding binding, ViewPager pager, List<FreeTestItem> testitem,
                            String posi, String Test_name) {
        activity_name="Bookmark";
        this.testitem=testitem;
        this.pager=pager;
        this.activity=activity;
        this.binding=binding;
        this.posi=posi;
        this.Test_name=Test_name;
        db=new DatabaseHandler(activity);
    }
    public void onBtnClear(View view) {
        try {
            String Answer_attempt = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            String optiona = Html.fromHtml(testitem.get(pager.getCurrentItem()).getAnswer()).toString();
            Answers = Html.fromHtml(testitem.get(pager.getCurrentItem()).getAnswer()).toString();
            boolean DAtaAlready = db.CheckIsDataAlreadyInDBorNotReadyStock(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            if (DAtaAlready) {
                final int Attempts = Start_Test_AdapterEnglish.CorrectQuestion + Start_Test_AdapterEnglish.InCorrectQuestion;
                if (Attempts == 1) {
                    Start_Test_AdapterEnglish.attemp_count = 0;
                }
                db.ClearAttamptedOption(posi, Test_name);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.BtnClear.setVisibility(View.GONE);
                    }
                });
                Normal(view);
                if (Answer_attempt.equals(Answers)) {
                    Marks = Marks - 4;
                    Start_Test_AdapterEnglish.CorrectQuestion = Start_Test_AdapterEnglish.CorrectQuestion - 1;
                } else {
                    Marks = Marks - 4;
                    Start_Test_AdapterEnglish.InCorrectQuestion = Start_Test_AdapterEnglish.InCorrectQuestion - 1;
                }
                setprogress(-1);
            } else {
                Toast.makeText(activity, "Sorry Question not attempted", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onQueALayout(View view) {
        String option = "optiona";
        IsAtouched=true;
        if(activity_name.equalsIgnoreCase("Bookmark"))
            BookmarkQuesListner(option);
            else         QuesListner(option,binding.IDA,view,binding.QueALayout, binding.tvOptionA);
    }
    public void onQueBLayout(View view) {
        String option = "optionb";
        IsBtouched=true;
        if(activity_name.equalsIgnoreCase("Bookmark"))
            BookmarkQuesListner(option);
        else         QuesListner(option,binding.IDB,view,binding.QueBLayout, binding.tvOptionB);
    }
    public void onQueCLayout(View view) {
        String option = "optionc";
        IsCtouched=true;
        if(activity_name.equalsIgnoreCase("Bookmark"))
            BookmarkQuesListner(option);
        else           QuesListner(option,binding.IDC,view,binding.QueCLayout, binding.tvOptionC);
    }
    public void onQueDLayout(View view) {
        String option = "optiond";
        IsDtouched=true;
        if(activity_name.equalsIgnoreCase("Bookmark"))
            BookmarkQuesListner(option);
        else    QuesListner(option,binding.IDD,view,binding.QueDLayout, binding.tvOptionD);
    }
    public void onQueELayout(View view) {
        String option = "optione";
        IsEtouched=true;
        if(activity_name.equalsIgnoreCase("Bookmark"))
            BookmarkQuesListner(option);
        else       QuesListner(option,binding.IDE,view,binding.QueELayout, binding.tvOptionE);
    }
    @SuppressLint("SetTextI18n")
    private void QuesListner(String option, TextView ID, View view, LinearLayout queLayout, TextView tvOption){
        Start_Test_AdapterEnglish.attemp_count = 1;
        try {
            Boolean DAtaAlready = db.CheckIsDataAlreadyInDBorNotReadyStock(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {binding.BtnClear.setVisibility(View.VISIBLE);
                }
            });
            if (!DAtaAlready || DAtaAlready) {
                Answers = Html.fromHtml(testitem.get(pager.getCurrentItem()).getAnswer()).toString();
                Normal(view);
                ID.setTextColor(Color.parseColor("#ffffff"));
                ID.setBackgroundResource(R.drawable.skipp_correctt);
                queLayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                tvOption.setTextColor(Color.parseColor("#2CB8E1"));
                swipe_time(Integer.parseInt(posi));
                Questions = Html.fromHtml(testitem.get(pager.getCurrentItem()).getQuestionenglish()).toString();
                explation = Html.fromHtml(testitem.get(pager.getCurrentItem()).getSolutionenglish()).toString();
                binding.Explation.setText("" + explation);
                if (option.equals(Answers)) {
                    Marks = Marks + 4;
                    Start_Test_AdapterEnglish.CorrectQuestion = Start_Test_AdapterEnglish.CorrectQuestion + 1;
                } else {
                    Marks = Marks - 1;
                    Start_Test_AdapterEnglish.InCorrectQuestion = Start_Test_AdapterEnglish.InCorrectQuestion + 1;
                }
                Total_Marks = String.valueOf(Marks);
                String Answer_attempt = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (!DAtaAlready) {
                    db.addtesttable(Questions, Answers, option, testitem.get(pager.getCurrentItem()).getCorrectmarks(), testitem.get(pager.getCurrentItem()).getWrongmarks(),
                            testitem.get(pager.getCurrentItem()).getTotalquestio(), Total_Marks, Attempt_Question, posi, Test_name, explation,
                            SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }
                if (DAtaAlready) {
                    if (Answer_attempt.equals(Answers)) {
                        Marks = Marks - 4;
                        Start_Test_AdapterEnglish.CorrectQuestion = Start_Test_AdapterEnglish.CorrectQuestion - 1;
                    } else {
                        Marks = Marks + 1;
                        Start_Test_AdapterEnglish.InCorrectQuestion = Start_Test_AdapterEnglish.InCorrectQuestion - 1;
                    }
                    Total_Marks = String.valueOf(Marks);
                    db.updatetesttable(Questions, Answers, option, testitem.get(pager.getCurrentItem()).getCorrectmarks(), testitem.get(pager.getCurrentItem()).getWrongmarks(),
                            testitem.get(pager.getCurrentItem()).getTotalquestio(), Total_Marks, "yes", posi, Test_name, explation, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }
                setprogress(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void Normal(View view) {
        binding.tvOptionA.setBackgroundColor(view.getResources().getColor(R.color.trance));
        binding.tvOptionB.setBackgroundColor(view.getResources().getColor(R.color.trance));
        binding.tvOptionC.setBackgroundColor(view.getResources().getColor(R.color.trance));
        binding.tvOptionD.setBackgroundColor(view.getResources().getColor(R.color.trance));
        binding.tvOptionE.setBackgroundColor(view.getResources().getColor(R.color.trance));
        theme = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme) {
            case "night":
                binding.tvOptionA.setTextColor(Color.WHITE);
                binding.tvOptionB.setTextColor(Color.WHITE);
                binding.tvOptionC.setTextColor(Color.WHITE);
                binding.tvOptionD.setTextColor(Color.WHITE);
                binding.tvOptionE.setTextColor(Color.WHITE);
                binding.IDA.setTextColor(Color.WHITE);
                binding.IDB.setTextColor(Color.WHITE);
                binding.IDC.setTextColor(Color.WHITE);
                binding.IDD.setTextColor(Color.WHITE);
                binding.IDE.setTextColor(Color.WHITE);
                break;
            case "sepia":
                binding.tvOptionA.setTextColor(Color.BLACK);
                binding.tvOptionB.setTextColor(Color.BLACK);
                binding.tvOptionC.setTextColor(Color.BLACK);
                binding.tvOptionD.setTextColor(Color.BLACK);
                binding.tvOptionE.setTextColor(Color.BLACK);
                binding.IDA.setTextColor(Color.BLACK);
                binding.IDB.setTextColor(Color.BLACK);
                binding.IDC.setTextColor(Color.BLACK);
                binding.IDD.setTextColor(Color.BLACK);
                binding.IDE.setTextColor(Color.BLACK);
                break;
            default:
                binding.tvOptionA.setTextColor(Color.BLACK);
                binding.tvOptionB.setTextColor(Color.BLACK);
                binding.tvOptionC.setTextColor(Color.BLACK);
                binding.tvOptionD.setTextColor(Color.BLACK);
                binding.tvOptionE.setTextColor(Color.BLACK);
                binding.IDA.setTextColor(Color.BLACK);
                binding.IDB.setTextColor(Color.BLACK);
                binding.IDC.setTextColor(Color.BLACK);
                binding.IDD.setTextColor(Color.BLACK);
                binding.IDE.setTextColor(Color.BLACK);
                break;
        }
        binding.QueALayout.setBackgroundColor(Color.parseColor("#00000000"));
        binding.QueBLayout.setBackgroundColor(Color.parseColor("#00000000"));
        binding.QueCLayout.setBackgroundColor(Color.parseColor("#00000000"));
        binding.QueDLayout.setBackgroundColor(Color.parseColor("#00000000"));
        binding.QueELayout.setBackgroundColor(Color.parseColor("#00000000"));
        binding.IDA.setBackgroundResource(R.drawable.opt_unchkd);
        binding.IDB.setBackgroundResource(R.drawable.opt_unchkd);
        binding.IDC.setBackgroundResource(R.drawable.opt_unchkd);
        binding.IDD.setBackgroundResource(R.drawable.opt_unchkd);
        binding.IDE.setBackgroundResource(R.drawable.opt_unchkd);
        IsAtouched = false;
        IsBtouched = false;
        IsCtouched = false;
        IsDtouched = false;
        IsEtouched = false;
    }
    private void setprogress(int count) {
        attemptCount = Start_Test_AdapterEnglish.CorrectQuestion + Start_Test_AdapterEnglish.InCorrectQuestion; //attemptCount + count;
        final int total = testitem.size();
        int attempHun = attemptCount * 100;
        int progress = attempHun / total;
        cpb.setProgress(progress);
    }
    private void swipe_time(final int i) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pager.setCurrentItem(i, true);
            }
        }, 300);
    }
    @SuppressLint("SetTextI18n")
    private void BookmarkQuesListner(String option){
        boolean DAtaAlready=false;
        try {
            DAtaAlready = db.CheckIsDataAlreadyInDBorNotReadyStock(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
        }catch (Exception e){
            e.printStackTrace();
        }
        if (!DAtaAlready) {
            Answers = Html.fromHtml(testitem.get(pager.getCurrentItem()).getAnswer()).toString();
            if (Answers.equals("optiona")) {
                binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                binding.IDA.setBackgroundResource(R.drawable.correct);
                binding.QueALayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
            }
            if (Answers.equals("optionb")) {
                binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                binding.IDB.setBackgroundResource(R.drawable.correct);
                binding.IDA.setBackgroundResource(R.drawable.wrong);
                binding.QueBLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                binding.QueALayout.setBackgroundColor(Color.parseColor("#FFEBED"));
            }
            if (Answers.equals("optionc")) {
                binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                binding.IDC.setBackgroundResource(R.drawable.correct);
                binding.IDA.setBackgroundResource(R.drawable.wrong);
                binding.QueCLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                binding.QueALayout.setBackgroundColor(Color.parseColor("#FFEBED"));
            }
            if (Answers.equals("optiond")) {
                binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                binding.IDD.setBackgroundResource(R.drawable.correct);
                binding.IDA.setBackgroundResource(R.drawable.wrong);
                binding.QueDLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                binding.QueALayout.setBackgroundColor(Color.parseColor("#FFEBED"));
            }
            if (Answers.equals("optione")) {
                binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                binding.IDE.setBackgroundResource(R.drawable.correct);
                binding.IDA.setBackgroundResource(R.drawable.wrong);
                binding.QueELayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                binding.QueALayout.setBackgroundColor(Color.parseColor("#FFEBED"));
            }
            Questions = Html.fromHtml(testitem.get(pager.getCurrentItem()).getQuestionenglish()).toString();
            explation = Html.fromHtml(testitem.get(pager.getCurrentItem()).getSolutionenglish()).toString();
            binding.Explation.setText("" + explation);
            if (option.equals(Answers)) {
                Marks = Marks + 4;
                CorrectQuestion = CorrectQuestion + 1;
            } else {
                Marks = Marks - 1;
                InCorrectQuestion = InCorrectQuestion + 1;
            }
            Total_Marks = String.valueOf(Marks);
            try {
                String Answer_attempt = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (!DAtaAlready) {
                    db.addtesttable(Questions, Answers, option, testitem.get(pager.getCurrentItem()).getCorrectmarks(), testitem.get(pager.getCurrentItem()).getWrongmarks(),
                            testitem.get(pager.getCurrentItem()).getTotalquestio(), Total_Marks, Attempt_Question, posi, Test_name, explation, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }
                if (DAtaAlready) {
                    if (Answer_attempt.equals(Answers)) {
                        Marks = Marks - 4;
                        CorrectQuestion = CorrectQuestion - 1;
                    } else {
                        Marks = Marks + 1;
                        InCorrectQuestion = InCorrectQuestion - 1;
                    }
                    Total_Marks = String.valueOf(Marks);
                    db.updatetesttable(Questions, Answers, option, testitem.get(pager.getCurrentItem()).getCorrectmarks(), testitem.get(pager.getCurrentItem()).getWrongmarks(),
                            testitem.get(pager.getCurrentItem()).getTotalquestio(), Total_Marks, Attempt_Question, posi, Test_name, explation, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}