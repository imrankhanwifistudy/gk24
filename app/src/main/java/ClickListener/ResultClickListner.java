package ClickListener;

import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ShareCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.QuizViewActivity;
import com.daily.currentaffairs.TopicResultActivity;
import com.daily.currentaffairs.databinding.StartTestActivityBinding;
import java.util.ArrayList;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

public class ResultClickListner {
    private TopicResultActivity topicResultActivity;
    private StartTestActivityBinding binding;
    private DatabaseHandler db;
    private String LanguageOption = "english",cat_name,test_name,correct_mark,wrong_mark,total_question,TestTitleName,RankID,week,CorrectM,
            correctanswerstr,WromgM,time,TestName;
    private ArrayList<FreeTestItem> testitem;
    public ResultClickListner(TopicResultActivity topicResultActivity, StartTestActivityBinding binding, ArrayList<FreeTestItem> testitem,
                              String test_name, String correct_mark, String wrong_mark, String total_question, String TestTitleName,
                              String RankID, String correctanswerstr, String week, String CorrectM, String WromgM, String time,
                              String TestName) {
        this.topicResultActivity=topicResultActivity;
        this.binding=binding;
        this.testitem=testitem;
        this.test_name=test_name;
        this.correct_mark=correct_mark;
        this.wrong_mark=wrong_mark;
        this.total_question=total_question;
        this.TestTitleName=TestTitleName;
        this.RankID=RankID;
        this.correctanswerstr=correctanswerstr;
        this.CorrectM=CorrectM;
        this.WromgM=WromgM;
        this.week=week;
        this.time=time;
        this.TestName=TestName;
        db=new DatabaseHandler(topicResultActivity);
    }
    public void onStartQuiz(View view) {
        LanguageOption = SharePrefrence.getInstance(topicResultActivity).getString(Utills.DEFAULT_LANGUAGE);
        if (binding.startQuiz.getText().toString().equalsIgnoreCase("START QUIZ")) {
            if (testitem.size() == 0) {
                Toast.makeText(topicResultActivity, "please wait for starting quiz", Toast.LENGTH_LONG).show();
            } else {
                Intent i = new Intent(topicResultActivity, QuestionQuiz.class);
                i.putExtra("language", LanguageOption);
                i.putExtra("testitem", testitem);
                i.putExtra("catname", cat_name);
                i.putExtra("testname", test_name);
                i.putExtra("correctmark", correct_mark);
                i.putExtra("wrongmark", wrong_mark);
                i.putExtra("totalque", total_question);
                i.putExtra("TestTitleName", TestTitleName);
                topicResultActivity.startActivityForResult(i, 100);
            }
        }
    }
    public void onViewQuiz(View view) {
        if (testitem.size() == 0) {
            Toast.makeText(topicResultActivity, "please wait for starting quiz", Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(topicResultActivity, QuizViewActivity.class);
            i.putExtra("language", LanguageOption);
            i.putExtra("testitem", testitem);
            i.putExtra("catname", cat_name);
            i.putExtra("testname", test_name);
            i.putExtra("correctmark", correct_mark);
            i.putExtra("wrongmark", wrong_mark);
            i.putExtra("totalque", total_question);
            i.putExtra("RankID", RankID);
            i.putExtra("TestTitleName", TestTitleName);
            topicResultActivity.startActivityForResult(i, 300);
        }
    }
    public void onShareImage(View view) {
        ShareCompat.IntentBuilder
                .from(topicResultActivity) // getActivity() or activity field if within Fragment
                .setText("Hey, \n" +
                        "I scored " + String.valueOf(correctanswerstr) + "/" + total_question +
                        " and " +
                        "my Rank : " + binding.RankTextViwe.getText().toString()
                        + " in the GK Quiz. Can you beat my score? \n" +
                        "Download app here:" +
                        "https://play.google.com/store/apps/details?id=com.daily.currentaffairs"
                )
                .setType("text/plain") // most general text sharing MIME type
                .setChooserTitle("Choose Option")
                .startChooser();
    }
    public void onResetQuiz(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(topicResultActivity);
        builder.setMessage("Your All India Rank will remain unchanged, only score will be updated. Do you want to Retake?");
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    db.removeTestTable(test_name, SharePrefrence.getInstance(topicResultActivity).getString(Utills.DEFAULT_LANGUAGE));
                    db.removeResultTable(test_name, SharePrefrence.getInstance(topicResultActivity).getString(Utills.DEFAULT_LANGUAGE));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                binding.quizData.setVisibility(View.GONE);
                binding.resetQuiz.setVisibility(View.GONE);
                binding.ViewQuiz.setVisibility(View.GONE);
                binding.layout2.setVisibility(View.GONE);
                binding.rankLayout.setVisibility(View.GONE);
                binding.correctAttamp.setText("");
                binding.incorrectAttamp.setText("");
                binding.skippedQue.setText("");
                binding.accuracyResult.setText("");
                Log.e("finalWeek", "" + week);
                Intent intent = new Intent(topicResultActivity, QuestionQuiz.class);
                intent.putExtra("TopicWiseTest", "TopicWiseTest");
                intent.putExtra("language", "english");
                intent.putExtra("testitem", testitem);
                intent.putExtra("catname", cat_name);
                intent.putExtra("catname", cat_name);
                intent.putExtra("testname", test_name);
                intent.putExtra("correctmark", CorrectM);
                intent.putExtra("wrongmark", WromgM);
                intent.putExtra("totalque", total_question);
                intent.putExtra("time", time);
                intent.putExtra("topictest", true);
                intent.putExtra("RANKUID", RankID);
                intent.putExtra("TestTitleName", TestTitleName);
                intent.putExtra("week", week);
                intent.putExtra("TestName_quiz", TestName);
                topicResultActivity.startActivityForResult(intent, 1000);
                topicResultActivity.finish();
            }
        });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
        );
        AlertDialog alert = builder.create();
        alert.setTitle("Reset Quiz");
        alert.show();
    }
}
