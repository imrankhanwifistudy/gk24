package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.daily.currentaffairs.AppTour;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.ProfileActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.DialogMultipleBinding;
import com.daily.currentaffairs.databinding.FontBinding;
import com.daily.currentaffairs.databinding.FragmentSettingFragmentBinding;
import com.daily.currentaffairs.databinding.ThemePopUpBinding;
import com.daily.currentaffairs.databinding.ToastBinding;

import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import ui.utils.Constants;

public class SettingClickListner {
    private Activity activity;
    private SharedPreferences sharedPreferences;
    private FragmentSettingFragmentBinding binding;

    public SettingClickListner(Activity activity, FragmentSettingFragmentBinding binding) {
        this.activity = activity;
        this.binding = binding;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }

    public void onToolbarLayout(View view) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        activity.startActivity(intent);
    }

    public void onUpdateApp(View view) {
        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.APPURL)));
    }

    public void onShare(View view) {
        Utils.ShareDialog(activity);
    }

    public void onJob24(View view) {
        parseUrl("https://play.google.com/store/apps/details?id=com.govtjobs.alertapp");
    }

    public void onGk24(View view) {
        parseUrl("https://play.google.com/store/apps/details?id=com.englishvocabulary&hl=en");
    }

    public void onBookmark(View view) {
        Intent intent = new Intent(activity, BookmarkActivity.class);
        activity.startActivity(intent);
    }


    public void onImageSwitch(View view) {
        select_font(2);
    }

    public void onRlFont(View view) {
        select_font(1);
    }

    private void select_font(int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final FontBinding fontBinding = DataBindingUtil.inflate(inflater, R.layout.font, null, false);
        builder.setView(fontBinding.getRoot());
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        if (type == 1) {
            fontBinding.llFont.setVisibility(View.VISIBLE);
            fontBinding.tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (alertDialog != null && alertDialog.isShowing() && activity != null)
                        alertDialog.dismiss();
                }
            });
            boolean swImageUS = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_DEFAULT);
            boolean swImage = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_SMALLER);
            fontBinding.rbSmaller.setChecked(false);
            fontBinding.rbNormal.setChecked(false);
            fontBinding.rbLarge.setChecked(false);
            if (swImageUS) {
                fontBinding.rbNormal.setChecked(true);
            } else if (swImage) {
                fontBinding.rbSmaller.setChecked(true);
            } else {
                fontBinding.rbLarge.setChecked(true);
            }
            fontBinding.rbSmaller.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_SMALLER, true);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_DEFAULT, false);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_LARGE, false);
                    fontBinding.rbSmaller.setChecked(true);
                    fontBinding.rbNormal.setChecked(false);
                    fontBinding.rbLarge.setChecked(false);
                    binding.fontSetting.setText("Small");
                    if (alertDialog != null && alertDialog.isShowing() && activity != null)
                        alertDialog.dismiss();
                }
            });
            fontBinding.rbNormal.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_SMALLER, false);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_DEFAULT, true);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_LARGE, false);
                    fontBinding.rbSmaller.setChecked(false);
                    fontBinding.rbNormal.setChecked(true);
                    fontBinding.rbLarge.setChecked(false);
                    binding.fontSetting.setText("Default");
                    if (alertDialog != null && alertDialog.isShowing() && activity != null)
                        alertDialog.dismiss();
                }
            });
            fontBinding.rbLarge.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_SMALLER, false);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_DEFAULT, false);
                    SharePrefrence.getInstance(activity).putBoolean(Utills.AUTO_IMAGE_LARGE, true);
                    fontBinding.rbSmaller.setChecked(false);
                    fontBinding.rbNormal.setChecked(false);
                    fontBinding.rbLarge.setChecked(true);
                    binding.fontSetting.setText("Large");
                    if (alertDialog != null && alertDialog.isShowing() && activity != null)
                        alertDialog.dismiss();
                }
            });
        } else {
            fontBinding.llFont.setVisibility(View.GONE);
            fontBinding.llLang.setVisibility(View.VISIBLE);
            String text;
            text = "To save your data, you can turn off image in capsule section";
            fontBinding.textPopup.setText(text);
        }
    }

    public void onWifistudy(View view) {
        parseUrl("https://play.google.com/store/apps/details?id=com.wifistudy.onlinetest&hl=en");
    }

    public void onTheme(View view) {
        select_theme();
    }

    public void onFeedback(View view) {
        Utils.ReportDialog(activity, "", "3", "");
    }

    public void onRateus(View view) {
        dialog_RateNow();
    }

    private void dialog_RateNow() {
        final Dialog fb_dialog = new Dialog(activity);
        DialogMultipleBinding dialogMultipleBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_multiple, null, false);
        fb_dialog.setContentView(dialogMultipleBinding.getRoot());
        dialogMultipleBinding.shareDialog.setVisibility(View.GONE);
        dialogMultipleBinding.rateDialog.setVisibility(View.VISIBLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(fb_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        fb_dialog.getWindow().setAttributes(lp);
        fb_dialog.show();
        String s = "Rate us 5 Stars on Play Store if you enjoy learning with us!";
        SpannableStringBuilder str = new SpannableStringBuilder(s);
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 8, 15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        dialogMultipleBinding.txt.setText(str);
        dialogMultipleBinding.rateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = Constants.APPURL;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                activity.startActivity(i);
                if (fb_dialog != null && fb_dialog.isShowing() && activity != null)
                    fb_dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("ResourceType")
                    @Override
                    public void run() {
                        LayoutInflater inflater = activity.getLayoutInflater();
                        ToastBinding toastBinding = DataBindingUtil.inflate(inflater, R.layout.toast, null, false);
                        toastBinding.getRoot().setBackgroundResource(Color.TRANSPARENT);
                        Toast toast = new Toast(activity);
                        toast.setView(toastBinding.getRoot());
                        toastBinding.toastTxt.setText(Html.fromHtml("<html>\n" +
                                "please scroll down to rate us (5 \n" +
                                "<span style=\"font-size:150%;color:#FAB120;\">&starf;</span>\n" +
                                ").\n" +
                                "</body>\n" +
                                "</html>"));
                        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toastBinding.toastTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        toast.show();
                    }
                }, 3000);
            }
        });
        dialogMultipleBinding.later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fb_dialog != null && fb_dialog.isShowing() && activity != null)
                    fb_dialog.dismiss();
            }
        });
    }

    private void select_theme() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        ThemePopUpBinding themebinding = DataBindingUtil.inflate(inflater, R.layout.theme_pop_up, null, false);
        builder.setView(themebinding.getRoot());
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
        themebinding.rbDefault.setChecked(false);
        themebinding.rbNight.setChecked(false);
        themebinding.rbSepia.setChecked(false);
        switch (theme2) {
            case "night":
                themebinding.rbNight.setChecked(true);
                break;
            case "sepia":
                themebinding.rbSepia.setChecked(true);
                break;
            default:
                themebinding.rbDefault.setChecked(true);
                break;
        }
        themebinding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing() && activity != null)
                    alertDialog.dismiss();
            }
        });
        themebinding.rbDefault.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                SharePrefrence.getInstance(activity).putString("Themes", "default");
                binding.Themes.setText("Default");
                RestartActivity(alertDialog);
            }
        });
        themebinding.rbNight.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                SharePrefrence.getInstance(activity).putString("Themes", "night");
                binding.Themes.setText("Night");
                RestartActivity(alertDialog);
            }
        });
        themebinding.rbSepia.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                SharePrefrence.getInstance(activity).putString("Themes", "sepia");
                binding.Themes.setText("Sepia");
                RestartActivity(alertDialog);
            }
        });
    }

    private void parseUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        activity.startActivity(i);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, "please scroll down to rate app", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    private void RestartActivity(AlertDialog alertDialog) {
        activity.finish();
        Intent mainIntent = new Intent(activity, AppTour.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(mainIntent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        System.exit(0);
        if (alertDialog != null && alertDialog.isShowing() && activity != null)
            alertDialog.dismiss();
    }
}