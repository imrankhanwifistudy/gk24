package ClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.widget.Toast;

import com.daily.currentaffairs.AppTour;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.EditorialActivity;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.DialogBinding;
import com.daily.currentaffairs.databinding.ParagraphAdapterBinding;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import Modal.BookParaItem;
import Modal.DateItem;

public class ParagraphClickListner{
    private ParagraphAdapterBinding binding;
    private Activity activity;
    private ViewPager pagerMain;
    private ArrayList<BookParaItem> desc;
    private DatabaseHandler db;
    private SharedPreferences sharedPreferences;
    private String activity_Name="";
    private ArrayList<DateItem> testitem;
    private int date_position;
    private long lastClickTime = 0;
    private long DOUBLE_CLICK_TIME_DELTA = 300;
    public ParagraphClickListner(ParagraphAdapterBinding binding, Activity activity, ViewPager pagerMain) {
        this.binding=binding;
        this.activity=activity;
        this.pagerMain=pagerMain;
    }
    public ParagraphClickListner(Activity activity, ParagraphAdapterBinding binding, ArrayList<BookParaItem> desc, ViewPager horizental_pager) {
        this.binding=binding;
        this.activity=activity;
        pagerMain=horizental_pager;
        this.desc=desc;
        db=new DatabaseHandler(activity);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }
    public ParagraphClickListner(ParagraphAdapterBinding binding, Activity activity, ViewPager pagerMain, ArrayList<DateItem> testitem, int date_position) {
        this.activity=activity;
        this.binding=binding;
        activity_Name="paragraph";
        this.pagerMain=pagerMain;
        this.testitem=testitem;
        this.date_position=date_position;
        db=new DatabaseHandler(activity);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }
    public void onBackPost(View view) {
        pagerMain.setCurrentItem(pagerMain.getCurrentItem()-1);
    }
    public void onForwordPost(View view) {
        pagerMain.setCurrentItem(pagerMain.getCurrentItem()+1);
    }
    public void onTheme(View view) {
        if(SharePrefrence.getInstance(activity).getString("Themes").equalsIgnoreCase("night")){
            SharePrefrence.getInstance(activity).putString("Themes", "default");
        }else {
            SharePrefrence.getInstance(activity).putString("Themes", "night");
        }
        activity.finish();
        Intent mainIntent = new Intent(activity, AppTour.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(mainIntent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        System.exit(0);
    }
    public void onBack(View view) {
        activity.finish();
        if(activity_Name.equalsIgnoreCase("paragraph"))
            MainActivity.adapter2.notifyDataSetChanged();
    }
    public void onShare(View view) {
        Utils.ShareData(activity);
    }
    public void onLangPref(View view) {
        if(activity_Name.equalsIgnoreCase("paragraph"))
        {
            HomeparaChange();
        }else{
            try {
                JSONObject object = new JSONObject(desc.get(pagerMain.getCurrentItem()).getDesc());
                paraChange(object.getString("title"),object.getString("paragraph"),object.getString("title_h"),object.getString("paragraph_h"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("SetJavaScriptEnabled")
    private void paraChange(String title, String paragraph, String title_h, String paragraph_h){
        String theme= SharePrefrence.getInstance(activity).getString("Themes");
        if( BookmarkActivity.lang_flag){
            BookmarkActivity.lang_flag=false;
            try {
                binding.meaniniTv.setText(Html.fromHtml(title));
                Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
                Matcher m = REMOVE_TAGS.matcher(paragraph.replace("<p>", "").replace("</p>", ""));
                String ss = m.replaceAll("").replace(".", ". ");
                String str = "<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
                switch (theme) {
                    case "night":
                        binding.paragraph .setBackgroundColor(Color.parseColor("#2B3E50"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                        break;
                    case "sepia":
                        binding.paragraph .setBackgroundColor(Color.parseColor("#F8F2E2"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" + ss + "</span> ";
                        break;
                    default:
                        binding.paragraph .setBackgroundColor(Color.parseColor("#ffffff"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" + ss + "</span> ";
                        break;
                }
                binding.paragraph .loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
                binding.paragraph .getSettings().setJavaScriptEnabled(true);
                binding.paragraph .getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                binding.paragraph .getSettings().setTextSize(WebSettings.TextSize.NORMAL);
                binding.paragraph .getSettings().setAppCacheEnabled(false);
                binding.paragraph .getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                binding.paragraph .getSettings().setBuiltInZoomControls(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            BookmarkActivity.lang_flag=true;
            try {
                binding.meaniniTv.setText(Html.fromHtml(title_h));
                Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
                Matcher m = REMOVE_TAGS.matcher(paragraph_h.replace("<p>", "").replace("</p>", ""));
                String ss = m.replaceAll("").replace(".", ". ");
                String str = "<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> <script  type='text/javascript'></script><script>{.css('color','#D4D9DC');$(_this).css('color','#FE5C57');Android.wordClicked($(_this).text());}</script></head> <body>";
                switch (theme) {
                    case "night":
                        binding.paragraph .setBackgroundColor(Color.parseColor("#2B3E50"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                        break;
                    case "sepia":
                        binding.paragraph .setBackgroundColor(Color.parseColor("#F8F2E2"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss + "</span> ";
                        break;
                    default:
                        binding.paragraph .setBackgroundColor(Color.WHITE);
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" + ss +"</span> ";
                        break;
                }
                binding.paragraph .loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
                binding.paragraph .getSettings().setJavaScriptEnabled(true);
                binding.paragraph .getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                binding.paragraph .getSettings().setTextSize(WebSettings.TextSize.NORMAL);
                binding.paragraph .getSettings().setAppCacheEnabled(false);
                binding.paragraph .getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                binding.paragraph .getSettings().setBuiltInZoomControls(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("SetJavaScriptEnabled")
    private void HomeparaChange(){
        String theme= SharePrefrence.getInstance(activity).getString("Themes");
        if( EditorialActivity.lang_flag){
            EditorialActivity.lang_flag=false;
            binding.meaniniTv.setText(Html.fromHtml(testitem.get(pagerMain.getCurrentItem()).getTitle()));
            Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
            Matcher m = REMOVE_TAGS.matcher(testitem.get(pagerMain.getCurrentItem()).getParagraphCapsule().replace("<p>", "").replace("</p>", ""));
            String ss = m.replaceAll("").replace(".", ". ");
            String str="<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
            switch (theme) {
                case "night":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' onclick='wordClicked(this)'>" +  "</span> ";
                    break;
                case "sepia":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + "</span> ";
                    break;
                default:
                    binding.paragraph.setBackgroundColor(Color.parseColor("#ffffff"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" +  "</span> ";
                    break;
            }
            binding.paragraph.loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
            binding.paragraph.getSettings().setJavaScriptEnabled(true);
            binding.paragraph.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            binding.paragraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
            binding.paragraph.getSettings().setAppCacheEnabled(false);
            binding.paragraph.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            binding.paragraph.getSettings().setBuiltInZoomControls(false);
        }else{
            EditorialActivity.lang_flag=true;
            binding.meaniniTv.setText(Html.fromHtml(testitem.get(pagerMain.getCurrentItem()).getTitle_h()));
            Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
            Matcher m = REMOVE_TAGS.matcher(testitem.get(pagerMain.getCurrentItem()).getParagraph_h().replace("<p>", "").replace("</p>", ""));
            String ss = m.replaceAll("").replace(".", ". ");
            String str = "<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
            switch (theme) {
                case "night":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >"  + ss + "</span> ";
                    break;
                case "sepia":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss +  "</span> ";

                    break;
                default:
                    binding.paragraph.setBackgroundColor(Color.WHITE);
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" + ss + "</span> ";
                    break;
            }
            binding.paragraph.loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
            binding.paragraph.getSettings().setJavaScriptEnabled(true);
            binding.paragraph.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            binding.paragraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
            binding.paragraph.getSettings().setAppCacheEnabled(false);
            binding.paragraph.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            binding.paragraph.getSettings().setBuiltInZoomControls(false);
        }
        EditorialActivity.adapter.notifyDataSetChanged();
    }
    public void onBookMark(View view) {
        try {
            JSONObject object = new JSONObject(desc.get(pagerMain.getCurrentItem()).getDesc());
            Dialog(object.optString("active_date"),object.optString("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @SuppressLint("SetTextI18n")
    public void Dialog(final String date, final String id) {
        final Dialog dialog = new Dialog(activity);
        DialogBinding Dialogbinding= DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.dialog_,null,false);
        dialog.setContentView(Dialogbinding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        Dialogbinding.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog != null && activity != null && dialog.isShowing())
                    dialog.dismiss();
                Utils.ShareDialog(activity);
            }
        });
        final String uniq=date+id;
        boolean isAlready=false;
        try{
            isAlready = db.checkBookPara(uniq);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (isAlready) {
            Dialogbinding.bookmark.setText("Remove Bookmark");
        } else {
            Dialogbinding.bookmark.setText("Add Bookmark");
        }
        Dialogbinding.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog != null && activity != null && dialog.isShowing())
                    dialog.dismiss();
                try{
                    boolean isBookMark = db.checkBookPara(uniq);
                    if (isBookMark) {
                        db.deleteBookPara(uniq);
                        Utils.unbookmark(id,"0",activity,sharedPreferences.getString("uid",""));
                        Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                        desc.remove( pagerMain.getCurrentItem());
                        BookmarkActivity.pos = 0;
                        activity.startActivity(new Intent(activity, BookmarkActivity.class));
                        activity.finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }
    public void onLearnVocab(View view) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("position", date_position);
        intent.putExtra("position_pager", (testitem.get(pagerMain.getCurrentItem()).getVocabCount() - 1) * 10);
        intent.putExtra("Fragment", "WordFragment");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }
    public void onBookmarkbtn(View view) {
        bookmarkData();
    }
    public void onMainLayout(View view) {
        if(activity_Name.equalsIgnoreCase("paragraph"))
        {
            long clickTime = System.currentTimeMillis();
            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                bookmarkData();
            }
            lastClickTime = clickTime;
        }
    }
    private void bookmarkData(){
        String titleData;
        if(! EditorialActivity.lang_flag)
            titleData = testitem.get(pagerMain.getCurrentItem()).getTitle();
        else
            titleData = testitem.get(pagerMain.getCurrentItem()).getTitle_h();
        String uniqid=testitem.get(pagerMain.getCurrentItem()).getDate()+testitem.get(pagerMain.getCurrentItem()).getId();
        try{
            boolean isBookMark = db.checkBookPara(uniqid);
            if (isBookMark) {
                db.deleteBookPara(uniqid);
                Utils.unbookmark(testitem.get(pagerMain.getCurrentItem()).getId(),"0",activity,sharedPreferences.getString("uid",""));
                Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_small, 0, 0, 0);
                    }
                });
            } else {
                db.addParaBook(testitem.get(pagerMain.getCurrentItem()).getCourtsy(), testitem.get(pagerMain.getCurrentItem()).getDataobject(),
                        testitem.get(pagerMain.getCurrentItem()).getImages(), uniqid, titleData,testitem.get(pagerMain.getCurrentItem()).getId());
                Utils.bookmark_para(testitem.get(pagerMain.getCurrentItem()).getId(),"0",activity,sharedPreferences.getString("uid",""));
                Log.d("uniqid",uniqid);
                Toast.makeText(activity, "Bookmarked", Toast.LENGTH_SHORT).show();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_done_small, 0, 0, 0);
                    }
                });
            }
        }catch ( Exception e){
            e.printStackTrace();
        }
    }
    public void onSharebtn(View view) {
       Utils.ShareData(activity);
    }
}