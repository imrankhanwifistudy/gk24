package ui.views;

import ui.model.LoginModel;

public interface ILoginView extends  IGk24View {
    void onSuccess(LoginModel model, String displayName);
}
