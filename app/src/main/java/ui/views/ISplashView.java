package ui.views;

import ui.model.ConfigModel;

public interface ISplashView extends  IGk24View {
    void onSplashTimeOut();
    void onSuccess(ConfigModel model);
}
