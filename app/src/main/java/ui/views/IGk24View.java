package ui.views;

import android.content.Context;

public interface IGk24View {

    Context getContext();
    void enableLoadingBar(Context context, boolean enable, String s);

    void onError(String reason);
}