package ui.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import ui.utils.SystemUtility;

import static ui.utils.SystemUtility.encipherPV;

public class AppPreferenceManager {


    private static final String PREF_USER_NAME = "User_Name";
    private static final String PREF_IS_LOGGED_IN = "Login_Status";
    private static final String PREF_IS_SHOW_VIDEO = "Video_Status";
    private static final String PREF_SHOW_SOFT_UPDATE = "SHOW_SOFT_UPDATE";
    private static final String PREF_USER_ID = "User_ID";
    private static final String PREF_USER_EMAIL = "User_Email";
    private static final String PREF_USER_PHONE = "User_Phone";
    private static final String PREF_PHONE_VERIFIED = "mobile_varified";
    private static final String PREF_PACKAGE_EXPIRE = "package_expire";
    private static final String PREF_PRIME_STATUS = "prime_status";
    private static final String PREF_USER_THUMB = "User_Thumb";
    private static final String PREF_CURRENT_DATE = "current_date";
    private static final String Login_type = "login_type";
    private static final String Theme = "Theme";
    private static final String AUTO_IMAGE = "auto_image";
    private static final String AUTO_SOUND = "auto_sound";
    private static final String AUTO_ENABLE_COPY = "auto_enable_copy";
    private static final String FONT = "font";
    private static final String VOICE = "voice";
    private static final String STORAGE = "storage";
    private static final String Language = "language";
    private static final String LanguageCode = "language_code";
    private static final String LanguageApi = "language_api";
    private static final String SERVER_PRIME_CONFIRMATION = "language";
    private static final String PAYMENT_ID = "payment_id";
    private static final String ORDER_ID = "order_id";
    private static final String STATE = "user_state";
    private static String LAST_PLAN_PRICE = "last_plan_price";
    private static final String PREF_FCM_ID = "wifi_fcm_id";
    static String LAST_REMINDER_ID = "lastReminderId";
    static String TAP_LIMIT = "TAP_LIMIT";
    static String TAP_WORD_COUNT = "TAP_WORD_COUNT";

    private static final String PREF_NAME = "gk24Preference";
    public static final String PREF_NEED_REFRESH_HOME = "NEED_TO_REFRESH_HOME";
    public static final String PREF_LAST_USER_ID = "PREF_LAST_USER_ID";


    private static SharedPreferences mSharedPreferences;
    public static SharedPreferences getSharedPreferences(Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }


    public static String getString(Context context,String key) {
        return getSharedPreferences(context).getString(key, "");

    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString((key), (value));
        editor.commit();
    }



    public static void setUserLoggedIn(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_IS_LOGGED_IN, login);
        editor.apply();
    }

    public static boolean getIsUserLoggedIn(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_IS_LOGGED_IN, false);
    }

    public static void setVideoShow(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_IS_SHOW_VIDEO, login);
        editor.apply();
    }

    public static boolean geVideoShow (Context context) {
        return getSharedPreferences(context).getBoolean(PREF_IS_SHOW_VIDEO, false);
    }


    public static void setUserName(Context context, String name) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_NAME, name);
        editor.apply();
    }


    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_NAME, "");
    }


    public static void setShowSoftUpdate(Context context, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_SHOW_SOFT_UPDATE, value);
        editor.apply();
    }

    public static boolean getSoftUpdate(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_SHOW_SOFT_UPDATE, false);
    }


    public static void setUserId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_ID, id);
        editor.apply();
    }


    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_ID, "");
    }


    public static void setUserEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_EMAIL, email);
        editor.apply();
    }

    public static String getUserEmail(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_EMAIL, "");
    }


    public static void setUserPhone(Context context, String phone) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_PHONE, phone);
        editor.apply();
    }


    public static String getUserPhone(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_PHONE, "");
    }

    public static void setMobileVerified(Context context, String mobileVerifiedFlag) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PHONE_VERIFIED, mobileVerifiedFlag);
        editor.apply();
    }


    public static String getMobileVerified(Context context) {
        return getSharedPreferences(context).getString(PREF_PHONE_VERIFIED, "");
    }


    public static void setLastPlanPrice(Context context, String price) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(LAST_PLAN_PRICE, price);
        editor.apply();
    }


    public static String getLastPlanPrice(Context context) {
        return getSharedPreferences(context).getString(LAST_PLAN_PRICE, "");
    }


    /*Prime package expire Date save */
    public static void setPrimeExpiredDate(Context context, String PrimeExpireDate) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(encipherPV(PREF_PACKAGE_EXPIRE), encipherPV(PrimeExpireDate));
        editor.apply();
    }

    /*Prime package expire Date get */
    public static String getPrimeExpiredDate(Context context) {
        return SystemUtility.decipherPV123(getSharedPreferences(context).getString(encipherPV(PREF_PACKAGE_EXPIRE), ""));
    }


    public static void setPrimeActive(Context context, String primeStatus) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        String str = AppPreferenceManager.getUserId(context) + "prime" + primeStatus + AppPreferenceManager.getUserId(context);
        editor.putString(encipherPV(PREF_PRIME_STATUS), encipherPV(str));
        editor.apply();
    }


    public static String getPrimeActive(Context context) {
        return SystemUtility.decipherPV123(getSharedPreferences(context).getString(encipherPV(PREF_PRIME_STATUS), "")).replace("prime", "").replace(AppPreferenceManager.getUserId(context), "");
    }


    public static void setUserPrimeConfirmFromApi(Context context, String status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(SERVER_PRIME_CONFIRMATION, status);
        editor.apply();
    }

    public static String getUserPrimeConfirmFromApi(Context context) {
        return getSharedPreferences(context).getString(SERVER_PRIME_CONFIRMATION , "");
    }


    public static void setUserPrimeConfirmPaymentID(Context context, String paymentId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PAYMENT_ID, paymentId);
        editor.apply();
    }

    public static String getUserPrimeConfirmPaymentID(Context context) {
        return getSharedPreferences(context).getString(PAYMENT_ID , "");
    }


  public static void setUserPrimeConfirmOrdertID(Context context, String paymentId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(ORDER_ID, paymentId);
        editor.apply();
    }

    public static String getUserPrimeConfirmOrderID(Context context) {
        return getSharedPreferences(context).getString(ORDER_ID , "");
    }


 public static void setUserState(Context context, String paymentId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(STATE, paymentId);
        editor.apply();
    }

    public static String getUserState(Context context) {
        return getSharedPreferences(context).getString(STATE , "");
    }


    public static void setUserThumb(Context context, String thumb) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_THUMB, thumb);
        editor.apply();
    }

    public static String getUserThumb(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_THUMB, "");
    }


    public static void setCurrentDate(Context context, String thumb) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_CURRENT_DATE, thumb);
        editor.apply();
    }

    public static String getCurrentDate(Context context) {
        return getSharedPreferences(context).getString(PREF_CURRENT_DATE, "");
    }

    public static void setLoginType(Context context, String time) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Login_type, time);
        editor.apply();
    }


    public static String getLoginType(Context context) {
        return getSharedPreferences(context).getString(Login_type, "");
    }


    public static void setTheme(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Theme, value);
        editor.apply();
    }

    public static String getTheme(Context context) {
        return getSharedPreferences(context).getString(Theme, "default");
    }

    /*
     * Language screen Preferences
     * */

    public static void setLang(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Language, value);
        editor.apply();
    }

    public static String getLang(Context context) {
        return getSharedPreferences(context).getString(Language, "");
    }

    public static void setLangCode(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(LanguageCode, value);
        editor.apply();
    }

    public static String getLangCode(Context context) {
        return getSharedPreferences(context).getString(LanguageCode, "");
    }

    public static void setLangApi(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(LanguageApi, value);
        editor.apply();
    }

    public static String getLangApi(Context context) {
        return getSharedPreferences(context).getString(LanguageApi, "");
    }

    public static void setAutoSound(Context context, Boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(AUTO_IMAGE, value);
        editor.apply();
    }

    public static Boolean getAutoSound(Context context) {
        return getSharedPreferences(context).getBoolean(AUTO_IMAGE, true);
    }

    public static void setAutoImage(Context context, Boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(AUTO_SOUND, value);
        editor.apply();
    }

    public static Boolean getAutoImage(Context context) {
        return getSharedPreferences(context).getBoolean(AUTO_SOUND, true);
    }

    public static void setAutoEnableCopy(Context context, Boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(AUTO_ENABLE_COPY, value);
        editor.apply();
    }

    public static Boolean getAutoEnableCopy(Context context) {
        return getSharedPreferences(context).getBoolean(AUTO_ENABLE_COPY, false);
    }

    public static void setFont(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(FONT, value);
        editor.apply();
    }

    public static String getFont(Context context) {
        return getSharedPreferences(context).getString(FONT, "Default");
    }

    public static void setVoice(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(VOICE, value);
        editor.apply();
    }

    public static String getVoice(Context context) {
        return getSharedPreferences(context).getString(VOICE, "US");
    }

    public static void setStorage(Context context, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(STORAGE, value);
        editor.apply();
    }

    public static String getStorage(Context context) {
        return getSharedPreferences(context).getString(STORAGE, "Internal");
    }


    public static void prefPrimeStatusDelete() {
        mSharedPreferences.edit().remove(encipherPV(PREF_PRIME_STATUS)).apply();
    }

    public static void prefPrimeExpDateDelete() {
        mSharedPreferences.edit().remove(encipherPV(PREF_PACKAGE_EXPIRE)).apply();
    }


    public static void setRemindersId(Context context, int id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(LAST_REMINDER_ID, id);
        editor.commit();
    }

    public static int getRemindersId(Context context) {
        return getSharedPreferences(context).getInt(LAST_REMINDER_ID,0);
    }


    public static void setPushRegId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_FCM_ID, id);
        editor.apply();
    }


    public static String getPushRegId(Context context) {
        return getSharedPreferences(context).getString(PREF_FCM_ID, "");
    }

    public static void setTabLimit(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(TAP_LIMIT, id);
        editor.apply();
    }


    public static String getTabLimit(Context context) {
        return getSharedPreferences(context).getString(TAP_LIMIT, "");
    }

    public static void setTabCount(Context context, int id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(TAP_WORD_COUNT, id);
        editor.apply();
    }


    public static Integer getTabCount(Context context) {
        return getSharedPreferences(context).getInt(TAP_WORD_COUNT, 0);
    }

    public static void setNeedToRefresh(Context context, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_NEED_REFRESH_HOME, value);
        editor.apply();
    }

    public static boolean getNeedToRefresh(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_NEED_REFRESH_HOME, false);
    }
    public static void setLastUserId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_LAST_USER_ID, id);
        editor.apply();
    }

    public static String getLastUserId(Context context) {
        return getSharedPreferences(context).getString(PREF_LAST_USER_ID, "");
    }

    public static boolean HasInt(String key) {
        return mSharedPreferences.contains(key);
    }

    public static boolean getBoolean(String key) {

        return mSharedPreferences.getBoolean(key, false);
    }


    public static void putInteger(String key, Integer value, Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(key, value);
        editor.commit();
    }


    public static Integer getInteger(String key) {
        return mSharedPreferences.getInt(key, 0);
    }


    public static void prefDelete(String Key) {
        mSharedPreferences.edit().remove(encipherPV(Key)).apply();
    }

    public static void clearAll(Context context) {

        String id = getUserId(context);

        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.apply();

        setLastUserId(context, id);

    }
}
