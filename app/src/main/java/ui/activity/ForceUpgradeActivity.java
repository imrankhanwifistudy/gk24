package ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivityForceUpgradeBinding;

import ui.AppController;
import ui.preferences.AppPreferenceManager;
import ui.utils.Constants;


/**
 * Created by imran khan
 */

public class ForceUpgradeActivity extends BaseActivity {

    public ActivityForceUpgradeBinding binding;
    boolean isLogout = false;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_force_upgrade);
        binding.setActivity(this);

        if(getIntent()!=null && getIntent().hasExtra("KeyLogout")) {
            isLogout = getIntent().getBooleanExtra("KeyLogout", false);
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

    }


    public void onClick (View view) {
        switch(view.getId())  {
            case R.id.forceupdateButton:
                if(isLogout) {
                    AppPreferenceManager.clearAll(AppController.getInstance());

                    sharedPreferences.edit().clear().apply();
                    sharedPreferences.edit().remove("uid").apply();
                    sharedPreferences.edit().remove("LoginStatus").apply();
                    sharedPreferences.edit().remove("razorpayPaymentID").apply();
                }
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.APPURL)));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
