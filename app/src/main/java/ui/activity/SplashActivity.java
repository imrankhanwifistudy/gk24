package ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.databinding.DataBindingUtil;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivitySplashBinding;
import ui.model.ConfigModel;
import ui.preferences.AppPreferenceManager;
import ui.presenter.SplashPresenter;
import ui.utils.NetworkAlertUtility;
import ui.views.ISplashView;

public class SplashActivity extends BaseActivity implements ISplashView {

    ActivitySplashBinding binding;
    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        presenter = new SplashPresenter();
        presenter.setView(this);

        if (NetworkAlertUtility.isConnectingToInternet(this)) {
            presenter.getConfigData(this);
        } else {
            NetworkAlertUtility.showNetworkFailureAlert(this);

             /*move user to next screen*/
            presenter.moveToNextScreen();
        }

    }

    @Override
    public void onSplashTimeOut() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSuccess(ConfigModel configData) {

        if (configData != null && configData.getConfigData().getForceUpdate()) {
            /* check for the force update */
            onForceUpdate(false);
            finish();
        } else if (configData != null && configData.getConfigData().getForceUpdateWithLogout()) {
            /* check for the force update with logout  */
            onForceUpdate(true);
            finish();
        } else {
            AppPreferenceManager.setVideoShow(this , configData.getConfigData().getVideo_show());
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public Context getContext() {
        return this;
    }
}