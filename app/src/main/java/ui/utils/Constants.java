package ui.utils;

public class Constants {

    public static final long SPLASH_TIME = 1000;
    public static String BASE_URL_NEW = "http://vocab24.com/gk24/app/";
    public static String BASE_URL_IMAGE = "https://himmatnursingacademy.com/public";
    public static String TC = "https://himmatnursingacademy.com/terms.php";
    public static String ABOUT = "https://himmatnursingacademy.com/about.php";
    public static String CONTACT_US = "https://himmatnursingacademy.com/contact_us.php";
    public static String PRIVACY_POLICY = "https://himmatnursingacademy.com/privacy.php";
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 1;
    public static final String FACEBOOK_URL = "https://www.facebook.com/107445504542612/posts/124801179473711/?sfnsn=wiwspmo";
    public static final String INSTAGRAM_URL = "https://instagram.com/himmat_nursing_academy?igshid=y782ab0qewnu";

    public static String CRUX = "abcABCdefghij]kIJKlmnopqrst[uvwx{yzUVW:XYZDEFGHLM}NOPQRST123456789";
    public static final String APPURL = "https://play.google.com/store/apps/details?id=com.daily.currentaffairs&hl=en";


}
