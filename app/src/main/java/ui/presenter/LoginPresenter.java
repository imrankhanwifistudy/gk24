package ui.presenter;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;

import com.daily.currentaffairs.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ui.AppController;
import ui.model.ConfigModel;
import ui.model.LoginModel;
import ui.utils.Constants;
import ui.views.ILoginView;
import ui.views.ISplashView;


public class LoginPresenter extends BasePresenter<ILoginView> {

    public void getLoginData(Activity context , String email, String mobile, String password, String status, String displayName) {
        getView().enableLoadingBar(context, true, context.getString(R.string.loading));

        AppController.getInstance().getApiService().login( email , mobile ,  password , status , displayName)
                .enqueue(new Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        getView().enableLoadingBar(context, false, "");

                        if (!handleError(response, context)) {
                            if (response.isSuccessful()) {
                                assert response.body() != null;
                                getView().onSuccess(response.body() , displayName);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        getView().enableLoadingBar(context, false, "");
                        t.printStackTrace();
                        getView().onError(null);

                    }
                });


    }


   
}
