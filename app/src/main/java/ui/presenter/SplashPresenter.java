package ui.presenter;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;

import com.daily.currentaffairs.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ui.AppController;
import ui.model.ConfigModel;
import ui.utils.Constants;
import ui.views.ISplashView;


public class SplashPresenter extends BasePresenter<ISplashView> {

    public void moveToNextScreen() {
        try {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    getView().onSplashTimeOut();
                }
            }, Constants.SPLASH_TIME);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getConfigData(Activity context) {
        getView().enableLoadingBar(context, true, context.getString(R.string.loading));

        AppController.getInstance().getApiService().getConfigData( )
                .enqueue(new Callback<ConfigModel>() {
                    @Override
                    public void onResponse(Call<ConfigModel> call, Response<ConfigModel> response) {
                        getView().enableLoadingBar(context, false, "");

                        if (!handleError(response, context)) {
                            if (response.isSuccessful()) {
                                assert response.body() != null;
                                getView().onSuccess(response.body());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ConfigModel> call, Throwable t) {
                        getView().enableLoadingBar(context, false, "");
                        t.printStackTrace();
                        getView().onError(null);

                    }
                });

    }


}
