package ui.api;


import com.google.android.material.slider.Slider;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ui.model.ConfigModel;
import ui.model.LoginData;
import ui.model.LoginModel;
import ui.model.SliderModel;
import ui.model.VideoTypeModel;

public interface ApiService {

    @POST("setconfig.php")
    Call<ConfigModel> getConfigData();

    @POST("user-login.php")
    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    Call<LoginModel> login(@Field("email") String email,
                           @Field("mobile") String mobile,
                           @Field("password") String password,
                           @Field("type") String status,
                           @Field("name") String displayName);


    @POST("user-login.php")
    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    Call<LoginData> user_login(@Field("password") String password, @Field("email") String email, @Field("type") int status);



    @POST("slider.php")
    Call<SliderModel> getSlider();


    @POST("video_type.php")
    Call<VideoTypeModel> getVideoType();

}