package ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoTypeModel {
    @SerializedName("response")
    @Expose
    private List<Response> response = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Response {

        @SerializedName("catid")
        @Expose
        private String catid;
        @SerializedName("title")
        @Expose
        private String title;

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }




    }
}
