package ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("fbstatus")
    @Expose
    private Integer fbstatus = 0;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prime_status")
    @Expose
    private String prime_status;
    @SerializedName("current_Date")
    @Expose
    private String current_Date;
    @SerializedName("expire_date")
    @Expose
    private String expire_date;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrime_status() {
        return prime_status;
    }

    public void setPrime_status(String prime_status) {
        this.prime_status = prime_status;
    }

    public String getCurrent_Date() {
        return current_Date;
    }

    public void setCurrent_Date(String current_Date) {
        this.current_Date = current_Date;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getMobile_status() {
        return mobile_status;
    }

    public void setMobile_status(String mobile_status) {
        this.mobile_status = mobile_status;
    }

    @SerializedName("mobile_status")
    @Expose
    private String mobile_status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFbstatus() {
        return fbstatus;
    }

    public void setFbstatus(Integer fbstatus) {
        this.fbstatus = fbstatus;
    }
}
