package ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SliderModel {

    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("info")
    @Expose
    private List<Info> info = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Info> getInfo() {
        return info;
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }
    public class Info {

        @SerializedName("headning")
        @Expose
        private String headning;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("callToLink")
        @Expose
        private String callToLink;
        @SerializedName("callToAction")
        @Expose
        private String callToAction;
        @SerializedName("prime_status")
        @Expose
        private String primeStatus;

        public String getHeadning() {
            return headning;
        }

        public void setHeadning(String headning) {
            this.headning = headning;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getCallToLink() {
            return callToLink;
        }

        public void setCallToLink(String callToLink) {
            this.callToLink = callToLink;
        }

        public String getCallToAction() {
            return callToAction;
        }

        public void setCallToAction(String callToAction) {
            this.callToAction = callToAction;
        }

        public String getPrimeStatus() {
            return primeStatus;
        }

        public void setPrimeStatus(String primeStatus) {
            this.primeStatus = primeStatus;
        }
    }

}
