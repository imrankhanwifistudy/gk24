package ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfigModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("configData")
    @Expose
    private ConfigData configData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ConfigData getConfigData() {
        return configData;
    }

    public void setConfigData(ConfigData configData) {
        this.configData = configData;
    }

    public class ConfigData {

        @SerializedName("BASE_URL")
        @Expose
        private String bASEURL;
        @SerializedName("offer_status")
        @Expose
        private String offerStatus;
        @SerializedName("offer_heading")
        @Expose
        private String offerHeading;
        @SerializedName("offer_img")
        @Expose
        private String offerImg;
        @SerializedName("offer_link")
        @Expose
        private String offerLink;
        @SerializedName("version")
        @Expose
        private String version;
        @SerializedName("soft_update")
        @Expose
        private Boolean softUpdate;
        @SerializedName("force_update")
        @Expose
        private Boolean forceUpdate;
        @SerializedName("video_show")
        @Expose
        private Boolean video_show;
        @SerializedName("force_update_with_logout")
        @Expose
        private Boolean forceUpdateWithLogout;
        @SerializedName("tap_count")
        @Expose
        private String tapCount;
        @SerializedName("prime_status")
        @Expose
        private String primeStatus;
        @SerializedName("package_expire")
        @Expose
        private String packageExpire;
        @SerializedName("mobile_varified")
        @Expose
        private String mobileVarified;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("current_date")
        @Expose
        private String currentDate;

        public String getBASEURL() {
            return bASEURL;
        }

        public void setBASEURL(String bASEURL) {
            this.bASEURL = bASEURL;
        }

        public String getOfferStatus() {
            return offerStatus;
        }

        public void setOfferStatus(String offerStatus) {
            this.offerStatus = offerStatus;
        }

        public String getOfferHeading() {
            return offerHeading;
        }

        public void setOfferHeading(String offerHeading) {
            this.offerHeading = offerHeading;
        }

        public String getOfferImg() {
            return offerImg;
        }

        public void setOfferImg(String offerImg) {
            this.offerImg = offerImg;
        }

        public String getOfferLink() {
            return offerLink;
        }

        public void setOfferLink(String offerLink) {
            this.offerLink = offerLink;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public Boolean getSoftUpdate() {
            return softUpdate;
        }

        public void setSoftUpdate(Boolean softUpdate) {
            this.softUpdate = softUpdate;
        }

        public Boolean getForceUpdate() {
            return forceUpdate;
        }

        public void setForceUpdate(Boolean forceUpdate) {
            this.forceUpdate = forceUpdate;
        }

        public Boolean getForceUpdateWithLogout() {
            return forceUpdateWithLogout;
        }

        public void setForceUpdateWithLogout(Boolean forceUpdateWithLogout) {
            this.forceUpdateWithLogout = forceUpdateWithLogout;
        }

        public String getTapCount() {
            return tapCount;
        }

        public void setTapCount(String tapCount) {
            this.tapCount = tapCount;
        }

        public String getPrimeStatus() {
            return primeStatus;
        }

        public void setPrimeStatus(String primeStatus) {
            this.primeStatus = primeStatus;
        }

        public String getPackageExpire() {
            return packageExpire;
        }

        public void setPackageExpire(String packageExpire) {
            this.packageExpire = packageExpire;
        }

        public String getMobileVarified() {
            return mobileVarified;
        }

        public void setMobileVarified(String mobileVarified) {
            this.mobileVarified = mobileVarified;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getCurrentDate() {
            return currentDate;
        }

        public void setCurrentDate(String currentDate) {
            this.currentDate = currentDate;
        }

        public Boolean getVideo_show() {
            return video_show;
        }

        public void setVideo_show(Boolean video_show) {
            this.video_show = video_show;
        }
    }

}
