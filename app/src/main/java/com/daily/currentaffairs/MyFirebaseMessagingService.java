package com.daily.currentaffairs;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import Custom.PushMessageHandler;


@SuppressWarnings("ALL")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Bitmap image;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        /*Log.e( "From: ", "? "+ remoteMessage.getFrom());
        Log.e( "Message Body: " , "? "+remoteMessage.getNotification().getBody());
        Log.e( "FCM Data: " ,"? "+ remoteMessage.getData().toString());*/

        String tag ="";// remoteMessage.getNotification().getTag();
        String msg =remoteMessage.getData().get("text");//remoteMessage.getNotification().getBody();
        String notification_id="0"+remoteMessage.getData().get("notification_id");

        try {
            Map<String, String> data = remoteMessage.getData();
            Bundle bundle = new Bundle();
            for (Map.Entry<String, String> entry : data.entrySet()) {
                bundle.putString((String) entry.getKey(), (String) entry.getValue());
            }
            PushMessageHandler.readMessage(getApplicationContext(), remoteMessage.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }
    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    public MyFirebaseMessagingService() {
        super();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

}
