package com.daily.currentaffairs;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.daily.currentaffairs.databinding.ActivityQuestionQuizBinding;

import java.util.ArrayList;

import Adapter.SolutionAdapter;
import ClickListener.QuestionClickListner;
import Custom.Utils;
import DB.SharePrefrence;
import Modal.FreeTestItem;

public class QuizViewActivity extends AppCompatActivity {
    SolutionAdapter AdapterEnglish;
    private ArrayList<FreeTestItem> testitem;
    int PERMISSION_REQUEST_CODE =2;
    String language,cat_name,test_name,correct_mark,wrong_mark,totalques,RankID,TestTitleName;
    ActivityQuestionQuizBinding binding;
    Drawable drawBack,drawShare,drawBookmark,drawReport;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        String theme2=SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        drawBack= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_navigation_arrow_back,"#000000");
        drawShare= Utils.DrawableChange(QuizViewActivity.this,R.drawable.share_quiz,"#000000");
        drawBookmark= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_bookmark_white,"#000000");
        drawReport= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_warining,"#000000");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                drawBack= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_navigation_arrow_back,"#ffffff");
                drawShare= Utils.DrawableChange(QuizViewActivity.this,R.drawable.share_quiz,"#ffffff");
                drawBookmark= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_bookmark_white,"#ffffff");
                drawReport= Utils.DrawableChange(QuizViewActivity.this,R.drawable.ic_warining,"#ffffff");
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding= DataBindingUtil.setContentView(this,R.layout.activity_question_quiz);
        binding.swipeContainer.setEnabled(false);
        binding.backimage.setBackgroundDrawable(drawBack);
        binding.icShareQuiz.setImageDrawable(drawShare);
        binding.icBookmark.setImageDrawable(drawBookmark);
        binding.icReportQuiz.setImageDrawable(drawReport);
        binding.timer.setVisibility(View.GONE);
        binding.icBookmark.setVisibility(View.VISIBLE);
        binding.icShareQuiz.setVisibility(View.VISIBLE);
        binding.icReportQuiz.setVisibility(View.VISIBLE);
        testitem = new ArrayList<>();
        if (testitem.size() > 0)
            testitem.clear();
        TestTitleName=getIntent().getStringExtra("TestTitleName");
        Intent i = getIntent();
        RankID = i.getStringExtra("RankID");
        language = i.getStringExtra("language");
        cat_name = i.getStringExtra("catname");
        test_name = i.getStringExtra("testname");
        correct_mark = i.getStringExtra("correctmark");
        wrong_mark = i.getStringExtra("wrongmark");
        totalques = i.getStringExtra("totalque");
        testitem = (ArrayList<FreeTestItem>) i.getSerializableExtra("testitem");
        AdapterEnglish = new SolutionAdapter(QuizViewActivity.this, testitem, binding.pager, cat_name, test_name, correct_mark,
                wrong_mark, binding.tvQuestionNo, binding.backimage ,binding.pb ,binding.icBookmark,binding.langPref,RankID, TestTitleName);
        binding.pager.setAdapter(AdapterEnglish);
        AdapterEnglish.notifyDataSetChanged();
        binding.setClick(new QuestionClickListner(QuizViewActivity.this,binding.pager,binding,TestTitleName,test_name,RankID,testitem));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}