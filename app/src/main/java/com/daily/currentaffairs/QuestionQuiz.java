package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.daily.currentaffairs.databinding.ActivityQuestionQuizBinding;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import Adapter.Start_Test_AdapterEnglish;
import ClickListener.QuestionClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Interface.TimerValuee;
import Modal.FreeTestItem;

public class QuestionQuiz extends AppCompatActivity implements TimerValuee {
    Start_Test_AdapterEnglish AdapterEnglish;
    String language, cat_name, test_name, correct_mark = "",TestName_quiz="", TopicWiseTest="",wrong_mark = "", totalques = "",
            totalTime = "", UID = "", RANKUID = "",week="", PractiseTestName = "", TestTitleName = "",date = "",theme2;
    public static CountDownTimer count;
    long millisecondss;
    boolean isTimer = false;
    DatabaseHandler db;
    Drawable drawBack,drawTime,drawPlay,drawlang;
    ActivityQuestionQuizBinding binding;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        theme2=SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        drawBack= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_pause_24dp,"#000000");
        drawTime= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_watch_24dp,"#000000");
        drawPlay= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_play_black_24dp,"#000000");
        drawlang = Utils.DrawableChange(QuestionQuiz.this,R.drawable.language_pref_black,"#000000");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                drawBack= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_pause_24dp,"#ffffff");
                drawTime= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_watch_24dp,"#ffffff");
                drawPlay= Utils.DrawableChange(QuestionQuiz.this,R.drawable.ic_play_black_24dp,"#ffffff");
                drawlang = Utils.DrawableChange(QuestionQuiz.this,R.drawable.language_pref_black,"#ffffff");
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding= DataBindingUtil.setContentView(this,R.layout.activity_question_quiz);
        db=new DatabaseHandler(getApplicationContext());
        binding.swipeContainer.setEnabled(false);
        binding.backimage.setCompoundDrawablesWithIntrinsicBounds(drawBack, null, null, null);
        binding.timer.setCompoundDrawablesWithIntrinsicBounds(drawTime, null, null, null);
        binding.langPref.setImageDrawable(drawlang);
        binding.timer.setPadding(0,0,50,0);
        ArrayList<FreeTestItem> testitem = new ArrayList<>();
        if (testitem.size() > 0)
            testitem.clear();
        Intent i = getIntent();
        language =""+ i.getStringExtra("language");
        cat_name = ""+i.getStringExtra("catname");
        test_name =""+ i.getStringExtra("testname");
        correct_mark = ""+i.getStringExtra("correctmark");
        wrong_mark = ""+i.getStringExtra("wrongmark");
        totalques = ""+i.getStringExtra("totalque");
        totalTime =""+ i.getStringExtra("time");
        if (i.hasExtra("UID"))
            UID = i.getStringExtra("UID");
        if(i.hasExtra("TestName_quiz"))
            TestName_quiz=i.getStringExtra("TestName_quiz");
        if (i.hasExtra("RANKUID"))
            RANKUID = i.getStringExtra("RANKUID");
        if( i.hasExtra("week"))
            week=i.getStringExtra("week");
        if (i.hasExtra("date"))
            date = i.getStringExtra("date");
        if (i.hasExtra("TopicWiseTest"))
            TopicWiseTest = i.getStringExtra("TopicWiseTest");
        if (i.hasExtra("PractiseTestName"))
            PractiseTestName = i.getStringExtra("PractiseTestName");
        if (i.hasExtra("TestTitleName"))
            TestTitleName = i.getStringExtra("TestTitleName");
        if (correct_mark.length() > 0 && wrong_mark.length() > 0 && totalTime.length() > 0) { // for paid section
            binding.timer.setVisibility(View.VISIBLE);
            long timeGet = Long.parseLong(totalTime);
            long timeMillis = TimeUnit.MINUTES.toMillis(timeGet);
            timerTask(timeMillis);
            binding.positiveMarks.setText("+" + correct_mark);
            binding.negativeMarks.setText("-" + wrong_mark);
            isTimer = true;
        } else {
            binding.timer.setVisibility(View.VISIBLE);
            correct_mark = "1";
            wrong_mark = "0.25";
            long total_q = Long.parseLong(totalques);
            long timeGet = 5;
            long timeMillis;
            int timee= Integer.parseInt(totalques)/2;
            timeMillis = TimeUnit.MINUTES.toMillis(timee);
            timerTask(timeMillis);
            binding.positiveMarks.setText("+" + 1);
            binding.negativeMarks.setText("-" + 0.25);
            isTimer = true;
        }
        binding.langPref.setVisibility(View.VISIBLE);
        try {
            db.removeTestTable(test_name , SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        testitem = (ArrayList<FreeTestItem>) i.getSerializableExtra("testitem");
        AdapterEnglish = new Start_Test_AdapterEnglish(QuestionQuiz.this, testitem, binding.pager, test_name, correct_mark, wrong_mark,
                binding.tvQuestionNo, binding.backimage, binding.pb, UID, RANKUID, isTimer, totalTime, PractiseTestName, TestTitleName, this,week);
        binding.pager.setAdapter(AdapterEnglish);
        AdapterEnglish.notifyDataSetChanged();
        binding.setClick(new QuestionClickListner(QuestionQuiz.this,binding.pager,binding,AdapterEnglish,millisecondss,
                TestTitleName,test_name,correct_mark,RANKUID,totalTime,PractiseTestName,testitem,UID,wrong_mark));
    }
    @Override
    public void onBackPressed() {
        QuestionQuiz.count.cancel();
        AdapterEnglish.ConfirmDialogeQuizExit("Are you sure you want to Exit ", false);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    public void timerTask(long millis) {
        count = new CountDownTimer(millis, 1000) { // adjust the milli seconds here
            @SuppressLint("DefaultLocale")
            public void onTick(long millisUntilFinished) {
                SharePrefrence.getInstance(getApplicationContext()).putLong(SharePrefrence.PAUSEBUTTON, millisUntilFinished);

                millisecondss = millisUntilFinished;
                binding.timer.setText(String.format("%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                binding.timertest.setText(String.format("%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }
            public void onFinish() {
                count.cancel();
                if (!isFinishing()) {
                    AdapterEnglish.ConfirmDialogeQuizExit("Are you sure you want to Exit ", true);
                }
            }
        }.start();
    }
    @Override
    public void TimerValue(long millis) {
        timerTask(millis);
    }
}