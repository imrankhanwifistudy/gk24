package com.daily.currentaffairs;

import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;
import com.daily.currentaffairs.databinding.LanguageSelectBinding;
import com.romainpiel.shimmer.Shimmer;
import java.util.ArrayList;
import Adapter.DefaultLanguagePrefAdapter;
import DB.SharePrefrence;
import DB.Utills;

public class DefaultLanguageActivity extends AppCompatActivity {
    ArrayList<String> languageName;
    ArrayList<String> languageinLN;
    DefaultLanguagePrefAdapter adapter;
    Shimmer shimmer;
    SharedPreferences spfs ;
    LanguageSelectBinding binding;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        String theme2=SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding= DataBindingUtil.setContentView(this,R.layout.language_select);
        spfs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        binding.GetStart.setVisibility(View.VISIBLE);
        shimmer = new Shimmer();
        shimmer.start(binding.GetStart);
        binding.GetStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharePrefrence.getInstance(getApplicationContext()).HasInt(Utills.DEFAULT_LANGUAGE)) {
                    String Default_Language = SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE);
                    if (spfs.contains("LoginStatus") &&
                            spfs.getString("LoginStatus","").equals("Success")){
                        Intent intent = new Intent(DefaultLanguageActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(DefaultLanguageActivity.this, LoginNew.class);
                        startActivity(intent);
                        finish();
                    }
                    Toast.makeText(DefaultLanguageActivity.this, "Language Selected:-"+Default_Language, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(DefaultLanguageActivity.this, "Please Select a Language First", Toast.LENGTH_SHORT).show();
                }
            }
        });
        languageName = new ArrayList<>();
        languageinLN = new ArrayList<>();
        addArrayListData();
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setTitle("Choose Language ");
        binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        int selected = SharePrefrence.getInstance(DefaultLanguageActivity.this).getInteger(Utills.DEFAULT_LANGUAGE_POSITION);
        binding.listviewLanguage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharePrefrence.getInstance(DefaultLanguageActivity.this).putString(Utills.DEFAULT_LANGUAGE, languageName.get(i));
                SharePrefrence.getInstance(DefaultLanguageActivity.this).putInteger(Utills.DEFAULT_LANGUAGE_POSITION, i);
                adapter.notifyDataSetChanged();
            }
        });
        adapter = new DefaultLanguagePrefAdapter(DefaultLanguageActivity.this, languageName , languageinLN);
        binding.listviewLanguage.setAdapter(adapter);
    }
    private void addArrayListData() {
        languageinLN.add("अंग्रेज़ी");
        languageinLN.add("हिन्दी");
        languageName.add("ENGLISH");
        languageName.add("HINDI");
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}