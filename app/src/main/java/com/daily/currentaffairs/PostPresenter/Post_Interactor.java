package com.daily.currentaffairs.PostPresenter;

import android.content.Context;
import com.daily.currentaffairs.databinding.ParagraphAdapterInsideBinding;

public interface Post_Interactor {
    interface OnLoginFinishedListener {
           void onSuccess(String DailyDate, ParagraphAdapterInsideBinding holder, String year, String DailyDate1, String Responce);
    }
    void login(Context ctx, String DailyDate,  ParagraphAdapterInsideBinding holder, String year, String DailyDate1, OnLoginFinishedListener listener);
}
