package com.daily.currentaffairs.PostPresenter;

import android.content.Context;

import com.daily.currentaffairs.databinding.ParagraphAdapterInsideBinding;

public interface Post_Presenter {
    void onDestroy();

    void validateCredentials(Context activity, String dailyDate, ParagraphAdapterInsideBinding holder, String year, String dailyDate1);
}
