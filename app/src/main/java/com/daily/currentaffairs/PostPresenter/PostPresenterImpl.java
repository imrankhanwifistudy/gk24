package com.daily.currentaffairs.PostPresenter;

import android.content.Context;

import com.daily.currentaffairs.databinding.ParagraphAdapterInsideBinding;

public class PostPresenterImpl implements Post_Presenter, Post_Interactor.OnLoginFinishedListener {
    private Post_ConfirmView loginView;
    private Post_Interactor loginInteractor;

    public PostPresenterImpl(Context ctx, Post_ConfirmView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new Post_InteractorImpl();
    }

    @Override
    public void validateCredentials(Context ctx, String dailyDate, ParagraphAdapterInsideBinding holder, String year, String dailyDate1) {
        loginInteractor.login(ctx, dailyDate, holder, year, dailyDate1, this);
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onSuccess(String DailyDate, ParagraphAdapterInsideBinding holder, String year, String DailyDate1, String reponce) {
        if (loginView != null) {
            loginView.navigateToHome(DailyDate, holder, year, DailyDate1, reponce);
        }
    }
}
