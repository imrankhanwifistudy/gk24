package com.daily.currentaffairs.PostPresenter;

import android.content.Context;
import android.util.Log;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.ParagraphAdapterInsideBinding;
import org.json.JSONObject;
import DB.SharePrefrence;
import DB.Utills;
import static com.daily.currentaffairs.MainActivity.activity;

public class Post_InteractorImpl implements Post_Interactor {
    private Context ctx ;
    @Override
    public void login(Context ctx, String DailyDate,  ParagraphAdapterInsideBinding holder, String year, String DailyDate1,
                      OnLoginFinishedListener listener) {
        this.ctx = ctx ;
        Get_Catagery( DailyDate,  holder,  year, DailyDate1, listener);
    }

    private void Get_Catagery(final String dailyDate, final ParagraphAdapterInsideBinding holder, final String year, final String dailyDate1,
                              final OnLoginFinishedListener listener) {
        Log.e("quiz_api_url",Utills.BASE_URLGK +"get_capsule.php?lang="+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE)
                +"&type="+"0"+"&dttime="+dailyDate);
        AndroidNetworking.post(Utills.BASE_URLGK +"get_capsule.php")
                .addBodyParameter("lang", "")
                .addBodyParameter("type", "0")
                .addBodyParameter("dttime", dailyDate)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ParagraphAdapter Call" , String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if (success == 1) {
                                listener.onSuccess(   dailyDate,  holder,  year,  dailyDate1,String.valueOf(response));

                            } else if (success == 0) {
                                listener.onSuccess(   dailyDate,  holder,  year,  dailyDate1,String.valueOf(response));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
}
