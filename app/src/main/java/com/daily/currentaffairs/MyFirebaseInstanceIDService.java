package com.daily.currentaffairs;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Custom.AsyncJob;
import DB.SharePrefrence;
import DB.Utills;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    ExecutorService executorService ;
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        executorService = Executors.newSingleThreadExecutor();
        // TODO: Implement this method to send any registration to your app's servers.
        AsyncJob.OnBackgroundJob job = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                // Pretend to do some background processing
                sendRegistrationToServer(refreshedToken);
                // This toast should show a difference of 1000ms between calls
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        //Toast.makeText(getApplicationContext(), "Finished on: "+System.currentTimeMillis(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        AsyncJob.doInBackground(job, executorService);

    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.

            AndroidNetworking.post(Utills.BASE_URLGK +"updateinstall.php")
                    .addBodyParameter("gcmid", token)
                    .addBodyParameter("uid", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.USERID))
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("TAG" , String.valueOf(response));

                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.printStackTrace();

                        }
                    });


    }
}
