package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.ActivityLoginNewBinding;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONObject;

import DB.SharePrefrence;
import DB.Utills;
import ui.AppController;
import ui.activity.BaseActivity;
import ui.model.LoginModel;
import ui.presenter.LoginPresenter;
import ui.views.ILoginView;

import static com.google.android.gms.auth.api.Auth.GoogleSignInApi;
import static ui.AppController.mGoogleApiClient;

public class LoginNew extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener
        , GoogleApiClient.ConnectionCallbacks, ILoginView {
    private int RC_SIGN_IN = 100;
    GoogleApiAvailability google_api_availability;
    String strEmail = "", strmobile = "";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ActivityLoginNewBinding binding;

    LoginPresenter presenter;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_new);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginNew.this);
        editor = sharedPreferences.edit();
        google_api_availability = GoogleApiAvailability.getInstance();
        buidNewGoogleApiClient();
        binding.email.setOnClickListener(this);

        presenter = new LoginPresenter();
        presenter.setView(this);
    }

    protected void setGooglePlusButtonText(SignInButton signInButton) {
        // Search all the views inside SignInButton for TextView
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);
            // if the view is instance of TextView then change the text SignInButton
            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText("");
                tv.setBackgroundColor(Color.TRANSPARENT);
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv.setTextColor(Color.WHITE);
                tv.setPadding(50, 0, 0, 0);
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sign_in_button) {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                revokeAccess();
                            }
                        });
            }
            signIn();
        } else if (v.getId() == R.id.email) {
            IntentEmail();
        }
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(AppController.mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnected(Bundle arg0) {
        AppController.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        AppController.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
        if (!result.hasResolution()) {
            int request_code = 100;
            google_api_availability.getErrorDialog(LoginNew.this, result.getErrorCode(), request_code).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void buidNewGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        binding.signInButton.setVisibility(View.VISIBLE);
        binding.signInButton.setSize(SignInButton.SIZE_WIDE);
        binding.signInButton.setScopes(gso.getScopeArray());
        AppController.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        binding.signInButton.setOnClickListener(this);
        setGooglePlusButtonText(binding.signInButton);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result != null && result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            assert acct != null;
            strEmail = acct.getEmail();
            strmobile = "";
            try {
                if (acct.getPhotoUrl() != null && acct.getPhotoUrl().toString().length() > 0) {
                    String profile = acct.getPhotoUrl().toString();
                    SharePrefrence.getInstance(LoginNew.this).putString("social_image", profile);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            presenter.getLoginData(this, acct.getEmail(), strmobile, "google", "", acct.getDisplayName());
          //  LoginTask(acct.getEmail(), "google", "", acct.getDisplayName());
        } else {
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        try {
            if (intent != null && requestCode == RC_SIGN_IN && responseCode == Activity.RESULT_OK) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
                handleSignInResult(result);
            } else {
                IntentEmail();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();
    }

    void IntentEmail() {
        startActivity(new Intent(getApplicationContext(), EmailActivity.class));
    }

    private void LoginTask(final String email, final String pass, final String Status, final String displayName) {
        final ProgressDialog dialog = new ProgressDialog(LoginNew.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();


        AndroidNetworking.post(Utills.BASE_URLGK + "user-login.php")
                .addBodyParameter("password", pass)
                .addBodyParameter("email", email)
                .addBodyParameter("mobile", strmobile)
                .addBodyParameter("type", Status) // type = 1 for login and type= 0 for register and blank for Google and facebook
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("LOGIN", String.valueOf(response));
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            Toast.makeText(LoginNew.this, "" + jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            if (jsonObject.getString("status").equals("3")) { // first time logi and payment success
                                SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_home", true);
                                SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_ques", true);
                                SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_word", true);
                                AppController.Email = strEmail;
                                editor.putString("email", strEmail);
                                editor.putString("mobile", strmobile.trim());
                                editor.putString("LoginStatus", "Success");
                                editor.putString("uid", "" + jsonObject.getString("uid"));
                                editor.putString("mobile", jsonObject.optString("mobile"));
                                editor.putString("name", displayName);
                                editor.commit();
                                SharePrefrence.getInstance(getApplication()).putString(Utills.USERID, jsonObject.getString("uid"));
                                Intent intent = new Intent(LoginNew.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(LoginNew.this, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        anError.printStackTrace();
                    }
                });
    }

    @Override
    public void onSuccess(LoginModel model, String displayName) {

        Toast.makeText(LoginNew.this, "" + model.getMsg(), Toast.LENGTH_SHORT).show();
        if (model.getStatus() == 3) { // first time logi and payment success
            SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_home", true);
            SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_ques", true);
            SharePrefrence.getInstance(LoginNew.this).putBoolean("firsts_word", true);
            AppController.Email = strEmail;
            editor.putString("email", strEmail);
            editor.putString("mobile", strmobile.trim());
            editor.putString("LoginStatus", "Success");
            editor.putString("uid", "" + model.getUid());
            editor.putString("mobile", model.getMobile());
            editor.putString("name", displayName);
            editor.commit();
            SharePrefrence.getInstance(getApplication()).putString(Utills.USERID, model.getUid());
            Intent intent = new Intent(LoginNew.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(LoginNew.this, "Please try again", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public Context getContext() {
        return this;
    }
}