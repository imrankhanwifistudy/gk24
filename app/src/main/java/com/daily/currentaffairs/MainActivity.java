package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.ActivityMainBinding;
import com.github.javiersantos.appupdater.AppUpdater;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Adapter.DateAdapterMain2;
import ClickListener.MainClickListner;
import Custom.AsyncJob;
import Custom.MainUtils;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Interface.DatePosition;
import Interface.OnNewsItemSelectedListener;
import Modal.DateItem;
import Modal.FreeTestItem;
import am.appwise.components.ni.NoInternetDialog;
import fragment.LearnFragment;
import fragment.Paragraph;
import fragment.QuizFragment;
import fragment.SettingFRagment;
import fragment.VideoFragment1;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ui.AppController;
import ui.model.SliderModel;
import ui.model.VideoTypeModel;

public class MainActivity extends AppCompatActivity implements OnNewsItemSelectedListener, DatePosition {
    public LinearLayout footorLayout;
    private static final String TAG = MainActivity.class.getSimpleName();
    ArrayList<DateItem> arrDateItem;
    ArrayList<DateItem> arrDateItemwithBlank;
    Intent mIntent;
    boolean doubleBackToExitPressedOnce = false;
    IntentFilter eintentFilter;
    @SuppressLint("StaticFieldLeak")
    public static MainActivity activity;
    public static ArrayList<String> weeks = new ArrayList<>();
    public static ArrayList<String> weeks1 = new ArrayList<>();
    public static ArrayList<String> months = new ArrayList<>();
    public static ArrayList<String> actualdate;
    public static int pager_pos = 0, size_date;
    public static String quizdate = "";
    String total_time, total_question, correct_mark, wrong_mark, attempt, title, fcm_id;
    ArrayList<FreeTestItem> testitem2;
    DatabaseHandler db;
    public static ArrayList<String> monthsyear = new ArrayList<>();
    public static String quiz_type = "Daily";
    @SuppressLint("StaticFieldLeak")
    public static DateAdapterMain2 adapter2;
    public static Boolean flag = true, like_flag = false, launchFlag = false;
    public static Paragraph paragraph;
    public static QuizFragment quizFragment;
    public static VideoFragment1 videoFragment1;
    public static LearnFragment learnFragment;
    public static SettingFRagment settingFRagment;
    @SuppressLint("StaticFieldLeak")
    public static ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        String theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }


        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        footorLayout = activityMainBinding.footorLayout;
        db = new DatabaseHandler(this);
        activity = MainActivity.this;
        mIntent = getIntent();
        SharedPreferences spfs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!spfs.contains("LoginStatus") && !spfs.getString("LoginStatus", "").equals("Success")) {
            Intent intent = new Intent(MainActivity.this, LoginNew.class);
            startActivity(intent);
            finish();
        }
        if (SharePrefrence.getInstance(getApplicationContext()).HasInt(Utills.DEFAULT_LANGUAGE)) {
            AppController.DefaultLanguage = SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE);
        }
        MainUtils.setZero(MainActivity.this);
        footorLayout = new LinearLayout(MainActivity.this) {
            @Override
            public void forceLayout() {
                super.forceLayout();
            }
        };
        arrDateItem = new ArrayList<>();
        arrDateItemwithBlank = new ArrayList<>();
        eintentFilter = new IntentFilter("com.daily.currentaffairs.Service.USER_ACTION");
        Intent intent = new Intent("com.daily.currentaffairs.Service.USER_ACTION");
        sendBroadcast(intent);
        FCMID();
        MainUtils.weekcount();
        MainUtils.date(MainActivity.this, arrDateItem, arrDateItemwithBlank);

        Video_Count();

        /*
        * @params slider apis for show bottom slider
        * */
       // Slider_api();

        SharePrefrence.getInstance(MainActivity.this).putString(Utills.WHICH_SELECTED, "1");
        if (paragraph == null) {
            paragraph = new Paragraph();
            getSupportFragmentManager().beginTransaction().add(R.id.continar, paragraph).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
        }
        MainUtils.replaceFragmentFun(paragraph, MainActivity.this);
        activityMainBinding.llQuizType.setVisibility(View.GONE);
        activityMainBinding.txteditorial.setAlpha(1);
        activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
        activityMainBinding.pagerdate.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                try {
                    ((OnNewsItemSelectedListener) getApplicationContext()).onNewsItemPicked(position);
                    try {
                        if (adapter2 != null) {
                            adapter2.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception cce) {
                    cce.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            launchFlag = extras.containsKey("IntentLocation");
            Log.e(TAG, "onCreate()");
            if (extras.containsKey("calltoaction")) {
                String calltoaction = extras.getString("calltoaction");
                assert calltoaction != null;
                if (calltoaction.length() == 0) {
                    if (extras.containsKey("calltolink")) {
                        String link = extras.getString("calltolink");
                        assert link != null;
                        if (link.trim().equalsIgnoreCase("start test")) {
                            activityMainBinding.llQuizType.setVisibility(View.GONE);
                            flag = true;
                            activityMainBinding.progressView.setVisibility(View.GONE);
                            SharePrefrence.getInstance(MainActivity.this).putString(Utills.WHICH_SELECTED, "3");
                            MainUtils.setZero(MainActivity.this);
                            if (quizFragment == null) {
                                MainUtils.date(MainActivity.this, arrDateItem, arrDateItemwithBlank);
                                quizFragment = new QuizFragment();
                                getSupportFragmentManager().beginTransaction().add(R.id.continar, quizFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                        .commitAllowingStateLoss();
                            }
                            MainUtils.replaceFragmentFun(quizFragment, MainActivity.this);
                            activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtquiz.setAlpha(1);
                            activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.topLayout.setVisibility(View.VISIBLE);
                        } else if (link.trim().equalsIgnoreCase("View Video")) {
                            activityMainBinding.llQuizType.setVisibility(View.GONE);
                            flag = true;
                            activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
                            activityMainBinding.calendarOpen.setVisibility(View.GONE);
                            MainUtils.setZero(MainActivity.this);
                            if (videoFragment1 == null) {
                                videoFragment1 = new VideoFragment1();
                                getSupportFragmentManager().beginTransaction().add(R.id.continar, videoFragment1).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                        .commitAllowingStateLoss();
                            }
                            MainUtils.replaceFragmentFun(videoFragment1, MainActivity.this);
                            activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                            activityMainBinding.txtvideo.setAlpha(1);
                            activityMainBinding.topLayout.setVisibility(View.GONE);
                            try {
                                if (adapter2 != null) {
                                    adapter2.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (link.length() > 0) {
                            Log.e(TAG, "Bypass" + " , " + link);
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(extras.getString("calltolink")));
                            startActivity(browserIntent);
                        }
                    }
                }
                NotificationManagerCompat.from(getApplicationContext()).cancelAll();
            }
        }
        activityMainBinding.setClick(new MainClickListner(activityMainBinding, arrDateItem, arrDateItemwithBlank, MainActivity.this));


    }

    private void FCMID() {
        fcm_id = FirebaseInstanceId.getInstance().getToken();
        Log.e("FCM_ID", "" + fcm_id);
        FirebaseMessaging.getInstance().subscribeToTopic("gkdlb");
        if (fcm_id != null && fcm_id.length() > 0) {
            SendId(fcm_id);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(TAG, "onNewIntent(), intent = " + intent);
        Bundle bundle = intent.getExtras();
        String quiz_post_id = bundle.getString("post_id");
        String quiz_post_type = bundle.getString("post_type");
        String quiz_week;
        String quiz_lang = bundle.getString("lang");
        if (!quiz_post_type.equalsIgnoreCase("")) {
            switch (quiz_post_type) {
                case "0":
                    quiz_week = "";
                    Get_Catagery(quiz_post_id, quiz_post_type, quiz_week, quiz_lang);
                    break;
                case "2":
                    quiz_week = "";
                    Get_Catagery(quiz_post_id, quiz_post_type, quiz_week, quiz_lang);
                    break;
                default:
                    quiz_week = bundle.getString("week");
                    Get_Catagery(quiz_post_id, quiz_post_type, quiz_week, quiz_lang);
                    break;
            }
        }
        setIntent(intent);
        if (mIntent.hasExtra("IntentLocation")) {
            recreate();
            launchFlag = true;
        } else {
            launchFlag = false;
        }
        if (intent.getExtras().containsKey("calltoaction")) {
            String calltoaction = bundle.getString("calltoaction");
            assert calltoaction != null;
            if (calltoaction.length() == 0) {
                if (bundle.containsKey("calltolink")) {
                    String link = bundle.getString("calltolink");
                    assert link != null;
                    if (link.trim().equalsIgnoreCase("start test")) {
                        activityMainBinding.llQuizType.setVisibility(View.GONE);
                        flag = true;
                        activityMainBinding.progressView.setVisibility(View.GONE);
                        MainUtils.setZero(MainActivity.this);
                        SharePrefrence.getInstance(MainActivity.this).putString(Utills.WHICH_SELECTED, "3");
                        if (quizFragment == null) {
                            quizFragment = new QuizFragment();
                            getSupportFragmentManager().beginTransaction().add(R.id.continar, quizFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .commitAllowingStateLoss();
                        }
                        MainUtils.replaceFragmentFun(quizFragment, MainActivity.this);
                        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtquiz.setAlpha(1);
                        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtvideo.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.topLayout.setVisibility(View.VISIBLE);
                    } else if (link.trim().equalsIgnoreCase("View Video")) {
                        activityMainBinding.llQuizType.setVisibility(View.GONE);
                        flag = true;
                        activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
                        activityMainBinding.calendarOpen.setVisibility(View.GONE);
                        MainUtils.setZero(MainActivity.this);
                        if (videoFragment1 == null) {
                            videoFragment1 = new VideoFragment1();
                            getSupportFragmentManager().beginTransaction().add(R.id.continar, videoFragment1)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commitAllowingStateLoss();
                        }
                        MainUtils.replaceFragmentFun(videoFragment1, MainActivity.this);
                        activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                        activityMainBinding.txtvideo.setAlpha(1);
                        activityMainBinding.topLayout.setVisibility(View.GONE);
                        try {
                            if (adapter2 != null) {
                                adapter2.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (link.length() > 0) {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bundle.getString("calltolink")));
                            startActivity(browserIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                NotificationManagerCompat.from(getApplicationContext()).cancelAll();
            }
        } else {
            SharePrefrence.getInstance(MainActivity.this).putString(Utills.WHICH_SELECTED, "1");
            if (paragraph == null) {
                paragraph = new Paragraph();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.continar, paragraph)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commitAllowingStateLoss();
            }
            MainUtils.replaceFragmentFun(paragraph, MainActivity.this);
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    private void SendId(final String fcmid) {
        AndroidNetworking.post(Utills.BASE_URLGK + "updateinstall.php")
                .addBodyParameter("gcmid", fcmid)
                .addBodyParameter("uid", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.USERID))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter", String.valueOf(response));
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            finishAffinity();
            System.exit(0);
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String which = SharePrefrence.getInstance(getApplicationContext()).getString(Utills.WHICH_SELECTED);
        switch (which) {
            case "3":
                activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
                break;
            case "1":
                activityMainBinding.ivQuiz.setVisibility(View.GONE);
                break;
            default:
                activityMainBinding.ivQuiz.setVisibility(View.GONE);
                break;
        }
        if (mIntent.hasExtra("Fragment") && mIntent.getStringExtra("Fragment").equals("VideoFragment")) {
            try {
                activityMainBinding.pagerdate.setCurrentItem(mIntent.getIntExtra("position", 0));
                if (videoFragment1 == null) {
                    videoFragment1 = new VideoFragment1();
                    MainUtils.setZero(MainActivity.this);
                    getSupportFragmentManager().beginTransaction().add(R.id.continar, videoFragment1)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commitAllowingStateLoss();
                    activityMainBinding.txtvideo.setAlpha(1);
                    activityMainBinding.txtquiz.setAlpha(Float.parseFloat("0.5"));
                    activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                    activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                    activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                    activityMainBinding.topLayout.setVisibility(View.GONE);
                    mIntent.setAction("");
                    getIntent().removeExtra("Fragment");
                }
                MainUtils.replaceFragmentFun(videoFragment1, MainActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (mIntent.hasExtra("Fragment") && mIntent.getStringExtra("Fragment").equals("quiz")) {
            try {
                MainUtils.setZero(MainActivity.this);
                SharePrefrence.getInstance(MainActivity.this).putString(Utills.WHICH_SELECTED, "3");
                quizFragment = new QuizFragment();
                getSupportFragmentManager().beginTransaction().add(R.id.continar, quizFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commitAllowingStateLoss();
                activityMainBinding.txtquiz.setAlpha(1);
                activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                activityMainBinding.topLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mIntent.setAction("");
            getIntent().removeExtra("Fragment");
            MainUtils.replaceFragmentFun(quizFragment, MainActivity.this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onNewsItemPicked(int position) {
        activityMainBinding.pagerdate.setCurrentItem(position, true);
        if (activityMainBinding.pagerdate.getCurrentItem() > 14)
            MainActivity.activityMainBinding.backimage.setVisibility(View.VISIBLE);
        else
            MainActivity.activityMainBinding.backimage.setVisibility(View.GONE);
        if (activityMainBinding.pagerdate.getCurrentItem() > 1) {
            String which = SharePrefrence.getInstance(getApplicationContext()).getString(Utills.WHICH_SELECTED);
            if (which.equalsIgnoreCase("3"))
                MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
            else
                MainActivity.activityMainBinding.calendarOpen.setVisibility(View.VISIBLE);
            int TIME = 9000;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
                }
            }, TIME);
        } else
            MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
        try {
            if (adapter2 != null)
                adapter2.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 100) {
                String UID = data.getStringExtra("UID");
                String RANKUID = data.getStringExtra("RANKUID");
                Intent intent = new Intent(MainActivity.this, TopicResultActivity.class);
                intent.putExtra("testId", "" + UID);
                intent.putExtra("Id", "" + RANKUID);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Get_Catagery(final String quiz_post_id, final String quiz_post_type, final String quiz_week, final String quiz_lang) {
        testitem2 = new ArrayList<>();
        AndroidNetworking.post(Utills.BASE_URLGK + "quiz.php")
                .addBodyParameter("lang", quiz_lang)
                .addBodyParameter("type", quiz_post_type)
                .addBodyParameter("dttime", quiz_post_id)
                .addBodyParameter("week", quiz_week)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter", String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if (success == 1) {
                                correct_mark = obj.getString("right_mark");
                                wrong_mark = obj.getString("rowsqsmar");
                                total_question = obj.getString("total_question");
                                total_time = obj.getString("total_time");
                                attempt = obj.getString("attemp");
                                JSONArray array = obj.getJSONArray("response");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsobj = array.getJSONObject(i);
                                    FreeTestItem item = new FreeTestItem();
                                    item.setDirection(jsobj.getString("direction"));
                                    item.setQuestionenglish(jsobj.getString("question"));
                                    item.setOpt_en_1(jsobj.getString("optiona"));
                                    item.setOp_en_2(jsobj.getString("optionb"));
                                    item.setOp_en_3(jsobj.getString("optionc"));
                                    item.setOp_en_4(jsobj.getString("optiond"));
                                    if (jsobj.has("optione")) {
                                        item.setOp_en_5(jsobj.getString("optione"));
                                    } else {
                                        item.setOp_en_5("");
                                    }
                                    item.setAnswer(jsobj.getString("correct_answer"));
                                    item.setSolutionenglish(jsobj.getString("explaination"));
                                    item.setNoofoption(jsobj.getString("noofoption"));
                                    item.setTime(total_time);
                                    item.setTotalquestio(total_question);
                                    item.setCorrectmarks(correct_mark);
                                    item.setWrongmarks(wrong_mark);
                                    item.setNo_of_attempt(attempt);
                                    testitem2.add(item);
                                }
                                Calendar calendar = Calendar.getInstance();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
                                String year = YearForm.format(calendar.getTime());
                                String uniqKey = quiz_post_id + quiz_post_id + year;
                                String test_name = uniqKey;
                                if (quiz_post_type.equals("1"))
                                    test_name = uniqKey + quiz_week;
                                Intent i = new Intent(activity, QuestionQuiz.class);
                                i.putExtra("language", "english");
                                i.putExtra("testitem", testitem2);
                                i.putExtra("catname", "");
                                i.putExtra("testname", test_name);
                                i.putExtra("correctmark", "");
                                i.putExtra("wrongmark", "");
                                i.putExtra("totalque", total_question);
                                i.putExtra("UID", test_name);
                                i.putExtra("RANKUID", quiz_post_id + "");
                                i.putExtra("time", "");
                                activity.startActivityForResult(i, 100);
                                activity.overridePendingTransition(0, 0);
                            } else if (success == 0) {
                                Toast.makeText(activity, "Internal error ocured", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    @Override
    public void DatePosition(int position) {
        if (MainActivity.quiz_type.equals("Daily")) {
            weeks.remove(position);
            months.remove(position);
            actualdate.remove(position);
            Paragraph.adapterr.notifyDataSetChanged();
            adapter2.notifyDataSetChanged();
        }
    }

    private void Get_Catagery() {
        // TODO Auto-generated method stub
        Log.e("Url", Utills.BASE_URLGK + "video_new.php?lang=" + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + "&type=" + SharePrefrence.getInstance(getApplicationContext()).getString("All_id") + "&page=0");
        AndroidNetworking.post(Utills.BASE_URLGK + "video_new.php")
                .addBodyParameter("lang", "" + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE))
                .addBodyParameter("type", "" + SharePrefrence.getInstance(getApplicationContext()).getString("All_id"))
                .addBodyParameter("page", "" + 0)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning", String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            JSONArray array = obj.getJSONArray("response");
                            int count = 0;
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsobj = array.getJSONObject(i);
                                String created_date = jsobj.getString("created_date");
                                String day = Utils.getDays(created_date);
                                if (day.equalsIgnoreCase("0")) {
                                    count++;
                                }
                                Log.e("Meaning", count + " , " + SharePrefrence.getInstance(getApplicationContext()).getInteger("count"));
                                if (count > 0 && count != SharePrefrence.getInstance(getApplicationContext()).getInteger("count")) {
                                    activityMainBinding.ivNewVideo.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    void Video_Count() {
        AsyncJob.OnBackgroundJob job = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                if (SharePrefrence.getInstance(getApplicationContext()).getString("All_id").equalsIgnoreCase("")) {
                    Pager_Data("video_type.php");
                } else {
                  //  Get_Catagery();
                }
            }
        };
        AsyncJob.doInBackground(job);
    }

    void Slider_api() {
        AsyncJob.OnBackgroundJob job = new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                Pager_Data("slider.php");
            }
        };
        AsyncJob.doInBackground(job);
    }

    private void Pager_Data(final String api) {

        if (api.equalsIgnoreCase("slider.php")) {
            AppController.getInstance().getApiService().getSlider()
                    .enqueue(new Callback<SliderModel>() {
                        @Override
                        public void onResponse(Call<SliderModel> call, Response<SliderModel> response) {

                            if (response.isSuccessful()) {
                                Gson gson = new Gson();
                                SharePrefrence.getInstance(getApplicationContext()).putString("slider_data", gson.toJson(response));

                            }

                        }

                        @Override
                        public void onFailure(Call<SliderModel> call, Throwable t) {
                            t.printStackTrace();

                        }
                    });
        } else {

            AppController.getInstance().getApiService().getVideoType()
                    .enqueue(new Callback<VideoTypeModel>() {
                        @Override
                        public void onResponse(Call<VideoTypeModel> call, Response<VideoTypeModel> response) {

                            if (response.isSuccessful()) {
                                SharePrefrence.getInstance(getApplicationContext()).putString("All_id", response.body().getResponse().get(0).getCatid());
                             //   Get_Catagery();

                            }

                        }

                        @Override
                        public void onFailure(Call<VideoTypeModel> call, Throwable t) {
                            t.printStackTrace();

                        }
                    });
        }


       /* AndroidNetworking.post(Utills.BASE_URLGK + api)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getInt("status") == 1) {
                                if (api.equalsIgnoreCase("slider.php")) {
                                    SharePrefrence.getInstance(getApplicationContext()).putString("slider_data", String.valueOf(response));
                                } else {
                                    JSONArray res = response.getJSONArray("response");
                                    JSONObject OBJ = res.getJSONObject(0);
                                    SharePrefrence.getInstance(getApplicationContext()).putString("All_id", OBJ.getString("catid"));
                                    Get_Catagery();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });*/
    }

    @Override
    protected void onStart() {
        super.onStart();

        NoInternetDialog builder = new NoInternetDialog.Builder(this).setCancelable(true).build();

        AppUpdater appUpdater = new AppUpdater(this);

        new AppUpdater(this)
                .setTitleOnUpdateAvailable("Update available")
                .setContentOnUpdateAvailable("Check out the latest version available of " + getString(R.string.app_name) + "App!")
                .setTitleOnUpdateNotAvailable("Update not available")
                .setContentOnUpdateNotAvailable("No update available. Check for updates again later!")
                .setButtonUpdate("Update now?")
                .setButtonUpdateClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(intent);
                    }
                })
                .setButtonDismiss("Maybe later")
                .setButtonDismissClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setButtonDoNotShowAgain("Huh, not interested")
                .setButtonDoNotShowAgainClickListener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.cancel();
                    }
                })
                .setIcon(R.mipmap.ic_launcher) // Notification icon
                .setCancelable(false); // Dialog could not be dismissable


        appUpdater.start();
    }
}