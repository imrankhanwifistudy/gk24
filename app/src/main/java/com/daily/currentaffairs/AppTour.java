package com.daily.currentaffairs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.daily.currentaffairs.databinding.AppTourBinding;

import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import ui.activity.SplashActivity;

public class AppTour extends AppCompatActivity implements View.OnClickListener {
    SharedPreferences spfs;
    AppTourBinding binding;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.app_tour);
        spfs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        binding.layout1.editorialnext.setOnClickListener(this);
        binding.layout2.voacbnext.setOnClickListener(this);
        binding.layout3.quiznext.setOnClickListener(this);
        binding.layout4.learnnext.setOnClickListener(this);
        binding.skip.setOnClickListener(this);
        boolean isFirst = SharePrefrence.getInstance(AppTour.this).getBoolean(Utills.IS_FIRST);
        if (!isFirst) {
            SharePrefrence.getInstance(getApplicationContext()).putBoolean(Utills.AUTO_IMAGE_DEFAULT, true);
            SharePrefrence.getInstance(AppTour.this).putInteger(Utills.DEFAULT_LANGUAGE_POSITION, 0);
            SharePrefrence.getInstance(AppTour.this).putString(Utills.DEFAULT_LANGUAGE, "ENGLISH");
            SharePrefrence.getInstance(AppTour.this).putBoolean(Utills.AUTO_SWITCH, true);
            SharePrefrence.getInstance(AppTour.this).putBoolean(Utills.IS_FIRST, true);
            SharePrefrence.getInstance(getApplicationContext()).putBoolean(Utills.AUTO_IMAGE_SWITCH, true);
        } else {
            if (spfs.contains("LoginStatus") && spfs.getString("LoginStatus", "").equals("Success")) {
                Intent intent = new Intent(AppTour.this, SplashActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(AppTour.this, LoginNew.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editorialnext:
                layoutVisibilty(binding.layout1.internetUna, binding.layout2.internetUna);
                break;
            case R.id.voacbnext:
               //  layoutVisibilty(binding.layout2.internetUna, binding.layout4.internetUna);
                layoutVisibilty(binding.layout3.internetUna, binding.layout4.internetUna);

                break;
            case R.id.quiznext:
               //  layoutVisibilty(binding.layout3.internetUna, binding.layout4.internetUna);
                nextActivity();
                break;
            case R.id.learnnext:
                nextActivity();
                break;
            case R.id.skip:
                nextActivity();
                break;
        }
    }

    void nextActivity() {
        if (Utils.hasConnection(getApplication())) {
            if (SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE).equalsIgnoreCase("ENGLISH")) {
                Intent intent = new Intent(AppTour.this, DefaultLanguageActivity.class);
                startActivity(intent);
                finish();
            } else {
                if (spfs.contains("LoginStatus") && spfs.getString("LoginStatus", "").equals("Success")) {
                    Intent intent = new Intent(AppTour.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(AppTour.this, LoginNew.class);
                    startActivity(intent);
                    finish();
                }
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void layoutVisibilty(RelativeLayout layout1, RelativeLayout layout2) {
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
    }
}