package com.daily.currentaffairs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daily.currentaffairs.databinding.CategListBinding;
import com.daily.currentaffairs.databinding.ListHeaderBinding;
import com.daily.currentaffairs.databinding.VideoviewItemBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ClickListener.CategoriesClickListner;
import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;

public class CategoriesActivity extends AppCompatActivity {
    private static final String TAG = CategoriesActivity.class.getSimpleName();
    String Title, CatID, activity_color = "", back_color = "";
    int position;
    List<ExpandableListAdapter.Item> data;
    CategListBinding binding;
    String theme2;
    ExpandableListAdapter adapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.setContentView(this, R.layout.categ_list);
        Intent intent = getIntent();
        CatID = intent.getStringExtra("CatID");
        Title = intent.getStringExtra("Title");
        position = intent.getIntExtra("position", 0);
        try {
            ParseJson(SharePrefrence.getInstance(getApplicationContext()).getString(Utills.GK));
        } catch (Exception e) {
            e.printStackTrace();
        }
        TOOLBAR();
        initRecyclerView();
    }

    void TOOLBAR() {
        setSupportActionBar(binding.toolbar);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(Title);
        binding.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (position == 0) {
            activity_color = "#f8a942";
            back_color = "#FFFFC478";
        } else if (position == 1) {
            activity_color = "#50adf3";
            back_color = "#FF84C4F4";
        } else if (position == 2) {
            activity_color = "#ee6c78";
            back_color = "#FFF5939C";
        } else if (position == 3) {
            activity_color = "#1bbe9f";
            back_color = "#FF7AE6D1";
        } else if (position == 4) {
            activity_color = "#5599c8";
            back_color = "#FF7BB6DF";
        } else if (position == 5) {
            activity_color = "#9e62b8";
            back_color = "#FFC188DA";
        } else if (position == 6) {
            activity_color = "#5558d9";
            back_color = "#FF8A8CE7";
        } else if (position == 7) {
            activity_color = "#1eb0bc";
            back_color = "#FF49D1DC";
        }
    }

    private void initRecyclerView() {
        binding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CategoriesActivity.this, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setNestedScrollingEnabled(false);
        adapter = new ExpandableListAdapter(CategoriesActivity.this, data, position, Title, CatID);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        if (!theme2.equalsIgnoreCase("night") && !theme2.equalsIgnoreCase("sepia")) {
            binding.recyclerView.setBackgroundColor(Color.parseColor(back_color));
            binding.toolbar.setBackgroundColor(Color.parseColor(activity_color));
            adapter.notifyDataSetChanged();
        }
    }

    public static class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        static final int HEADER = 0;
        static final int CHILD = 1;
        private List<Item> data;
        private Activity mContext;
        private int mParent;
        private JSONArray mColors;
        int clickPos;
        String title, catID;

        ExpandableListAdapter(Activity applicationContext, List<Item> data, int clickPos, String title, String catID) {
            this.data = data;
            this.mContext = applicationContext;
            this.clickPos = clickPos;
            this.title = title;
            this.catID = catID;
            loadJSONFromAsset(mContext);
        }

        @Override
        public void onViewDetachedFromWindow(final RecyclerView.ViewHolder holder) {
            holder.itemView.clearAnimation();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
            switch (type) {
                case HEADER:
                    final LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final ListHeaderBinding ListBinding = DataBindingUtil.inflate(inflater, R.layout.list_header, parent, false);
                    ListHeaderViewHolder header = new ListHeaderViewHolder(ListBinding.getRoot(), ListBinding);
                    return header;
                case CHILD:
                    LayoutInflater child = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    VideoviewItemBinding view = DataBindingUtil.inflate(child, R.layout.videoview_item, parent, false);
                    return new MyViewHolder(view.getRoot(), view);
            }
            return null;
        }

        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (data.get(position).type) {
                case HEADER:
                    final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                    itemController.listBinding.setItem(data.get(position));
                    break;
                case CHILD:
                    final MyViewHolder childitemController = (MyViewHolder) holder;
                    childitemController.videobinding.setItem(data.get(position));
                    childitemController.videobinding.setButtonClick(new CategoriesClickListner(data, title, position, clickPos, catID, mContext));
                    setColors(position, childitemController.videobinding.layoutcolor);
                    String theme = SharePrefrence.getInstance(mContext).getString("Themes");
                    switch (theme) {
                        case "night":
                            childitemController.videobinding.txttitle.setTextColor(Color.WHITE);
                            childitemController.videobinding.duration.setTextColor(Color.WHITE);
                            childitemController.videobinding.words.setTextColor(Color.WHITE);
                            childitemController.videobinding.colottxt.setTextColor(Color.WHITE);
                            childitemController.videobinding.lockStstus.setImageResource(R.drawable.ic_arrow_white_24dp);
                            break;
                    }
                    break;
            }
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.up_from_bottom);
            holder.itemView.startAnimation(animation);
        }

        @Override
        public int getItemViewType(int position) {
            return data.get(position).type;
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        private void setColors(int position, View colottxt) {
            try {
                String colorString = "#" + mColors.getJSONArray(mParent % 10).getString(position % 10);
                Drawable drwa = Utils.DrawableChange(mContext, R.drawable.wrong, colorString);
                colottxt.setBackgroundDrawable(drwa);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        private void loadJSONFromAsset(Context ctx) {
            try {
                InputStream is = ctx.getAssets().open("colors.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                String stringJson = new String(buffer, "UTF-8");
                mColors = new JSONArray(stringJson);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        class ListHeaderViewHolder extends RecyclerView.ViewHolder {
            ListHeaderBinding listBinding;
            Item refferalItem;

            ListHeaderViewHolder(final View itemView, final ListHeaderBinding binding) {
                super(itemView);
                this.listBinding = binding;
            }
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            VideoviewItemBinding videobinding;

            MyViewHolder(final View view, final VideoviewItemBinding binding) {
                super(view);
                videobinding = binding;
            }
        }

        public static class Item {
            public int type;
            public int pos;
            public String text;
            public String Subcatid;

            Item(int type, String text, int pos, String Subcatid) {
                this.type = type;
                this.text = text;
                this.pos = pos;
                this.Subcatid = Subcatid;
            }
        }
    }

    private void ParseJson(String response) throws Exception {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("response");
        data = new ArrayList<>();
        if (position != 6) {
            JSONObject jsonObject1 = jsonArray.getJSONObject(position);
            JSONArray jsonArray1 = jsonObject1.getJSONArray("subcat");
            for (int k = 0; k < jsonArray1.length(); k++) {
                JSONObject obj = jsonArray1.getJSONObject(k);
                switch (Title) {
                    case "Indian History":
                        if (k == 0) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Ancient", k, obj.getString("catid")));
                        } else if (k == 10) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Medieval", k, obj.getString("catid")));
                        } else if (k == 16) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Modern", k, obj.getString("catid")));
                        }
                        data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.CHILD, obj.getString("title"), k, obj.getString("catid")));
                        break;
                    case "Geography":
                        if (k == 0) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Physical", k, obj.getString("catid")));
                        } else if (k == 12) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "World", k, obj.getString("catid")));
                        } else if (k == 26) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Indian", k, obj.getString("catid")));
                        }
                        data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.CHILD, obj.getString("title"), k, obj.getString("catid")));
                        break;
                    case "General Science":
                        if (k == 0) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Physics", k, obj.getString("catid")));
                        } else if (k == 14) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Chemistry", k, obj.getString("catid")));
                        } else if (k == 26) {
                            data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.HEADER, "Life Science", k, obj.getString("catid")));
                        }
                        data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.CHILD, obj.getString("title"), k, obj.getString("catid")));
                        break;
                    default:
                        data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.CHILD, obj.getString("title"), k, obj.getString("catid")));
                        break;
                }
            }
        } else {
            for (int k = 0; k < 6; k++) {
                JSONObject obj = jsonArray.getJSONObject(k);
                data.add(new CategoriesActivity.ExpandableListAdapter.Item(CategoriesActivity.ExpandableListAdapter.CHILD, obj.getString("title"), k, obj.getString("catid")));
            }
        }
    }
}