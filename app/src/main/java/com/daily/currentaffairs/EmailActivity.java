package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.ActivityEmailBinding;
import com.daily.currentaffairs.databinding.ForgotpasswordpopupBinding;

import org.json.JSONObject;

import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ui.AppController;
import ui.activity.BaseActivity;
import ui.model.LoginData;

public class EmailActivity extends BaseActivity {
    int resstatus = 1;
    ActivityEmailBinding binding;

    private Animation animMove, animMove1, animMoveback, animMoveback1;
    private String email = "", pass = "", status = "1";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_email);

        animMove = AnimationUtils.loadAnimation(this, R.anim.move);
        animMove1 = AnimationUtils.loadAnimation(this, R.anim.move1);
        animMoveback = AnimationUtils.loadAnimation(this, R.anim.moveback);
        animMoveback1 = AnimationUtils.loadAnimation(this, R.anim.moveback1);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        intit();
    }

    public void intit() {
        binding.etEmail.requestFocus();
        binding.password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() < 6) {
                    binding.passContinue.setBackgroundColor(Color.parseColor("#656769"));
                } else {
                    binding.passContinue.setBackgroundColor(Color.parseColor("#63B867"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!Utils.isEmailValid(s.toString())) {
                    binding.contuine.setBackgroundColor(Color.parseColor("#656769"));
                } else {
                    binding.contuine.setBackgroundColor(Color.parseColor("#63B867"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //  binding.setClick(new EmailActivityClickListner(binding, EmailActivity.this));

    }

    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_back1:
                resstatus = 1;
                Log.d("layout", "back1");
                binding.rlLayout1.startAnimation(animMoveback1);
                binding.rlLayout2.startAnimation(animMoveback);
                binding.rlLayout1.setVisibility(View.VISIBLE);
                binding.rlLayout2.setVisibility(View.GONE);
                binding.ivBack1.setVisibility(View.GONE);
                binding.ivBack.setVisibility(View.VISIBLE);
                binding.contuine.setVisibility(View.VISIBLE);
                binding.passContinue.setVisibility(View.GONE);
                binding.etEmail.requestFocus();
                binding.etEmail.setVisibility(View.VISIBLE);
                binding.password.setVisibility(View.GONE);
                break;
            case R.id.forgot:
                if (binding.forgot.getText().toString().trim().equalsIgnoreCase("Forgot Password?")) {
                    initiatepopupwindow();
                }
                break;
            case R.id.iv_back:
                finish();
                break;

            case R.id.pass_continue:
                pass = binding.password.getText().toString().trim();
                if (pass.length() > 5) {
                    LoginTask(email, pass);
                }
                break;
            case R.id.contuine:
                email = binding.etEmail.getText().toString().trim();
                status = "1";
                pass = "";
                if (Utils.isEmailValid(email)) {
                    LoginTask(email, pass);
                }
                break;
        }

    }

    private void initiatepopupwindow() {
        // TODO Auto-generated method stub
        try {
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final ForgotpasswordpopupBinding Forgotbinding = DataBindingUtil.inflate(inflater, R.layout.forgotpasswordpopup, null, false);
            final PopupWindow popup = new PopupWindow(Forgotbinding.getRoot(), android.widget.Toolbar.LayoutParams.FILL_PARENT,
                    android.widget.Toolbar.LayoutParams.FILL_PARENT);
            Forgotbinding.mailid.setText(binding.etEmail.getText().toString());
            Forgotbinding.mailid.setEnabled(false);
            Forgotbinding.mailid.setClickable(false);
            Forgotbinding.backto.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (popup != null && popup.isShowing())
                        popup.dismiss();
                }
            });
            Forgotbinding.cancellay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    String femail = Forgotbinding.mailid.getText().toString();
                    if (femail.length() > 0 && Utils.isEmailValid(femail)) {
                        ForgetPassTask(femail);
                        if (popup != null && popup.isShowing())
                            popup.dismiss();
                    } else {
                        Toast.makeText(EmailActivity.this, "please enter your Email", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            popup.setFocusable(true);
            popup.update();
            popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            popup.setOutsideTouchable(true);
            popup.setTouchInterceptor(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View arg0, MotionEvent event) {
                    // TODO Auto-generated method stub
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        if (popup != null && popup.isShowing())
                            popup.dismiss();
                        return true;
                    }
                    return false;
                }
            });
            popup.showAtLocation(Forgotbinding.getRoot(), Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void ForgetPassTask(final String email) {
        final ProgressDialog dialog = new ProgressDialog(EmailActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK + "forgot-password.php")
                .addBodyParameter("email", email)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        Log.e("FORGOT", String.valueOf(response));
                        try {
                            JSONObject object = new JSONObject(String.valueOf(response));
                            Toast.makeText(EmailActivity.this, "" + object.getString("msg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        anError.printStackTrace();
                    }
                });
    }

    private void LoginTask(final String email, final String pass) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();

        AppController.getInstance().getApiService().user_login(pass, email, resstatus)
                .enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                        if (response.isSuccessful() && response.code() == 200) {
                            Log.e("Email Activity Retrofit", String.valueOf(response.body().toString()));

                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                            try {

                                resstatus = response.body().getStatus();
                                int resstatusfb = response.body().getFbstatus();
                                String msg = response.body().getMsg();
                                Toast.makeText(EmailActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                                if (resstatus == 1) {
                                    binding.forgot.setText(String.format("Forgot Password?"));
                                    binding.forgot.setTextSize(14);
                                    binding.text1.setText(String.format("Enter Password to login into your account"));
                                    binding.passContinue.setText(String.format("Login"));
                                    binding.etEmail.setHint("Enter password minimum 6 characters");
                                    if (resstatusfb == 1) {
                                        binding.text1.setText(String.format("Choose password "));
                                        binding.forgot.setVisibility(View.GONE);
                                    }
                                    binding.rlLayout1.startAnimation(animMove);
                                    binding.rlLayout2.startAnimation(animMove1);
                                    binding.rlLayout1.setVisibility(View.GONE);
                                    binding.rlLayout2.setVisibility(View.VISIBLE);
                                    binding.contuine.setVisibility(View.GONE);
                                    binding.passContinue.setVisibility(View.VISIBLE);
                                    binding.password.requestFocus();
                                    binding.ivBack1.setVisibility(View.VISIBLE);
                                    binding.ivBack.setVisibility(View.GONE);
                                    binding.etEmail.setVisibility(View.GONE);
                                    binding.password.setVisibility(View.VISIBLE);
                                } else if (resstatus == 2) {
                                    binding.forgot.setText("2/2");
                                    binding.forgot.setTextSize(16);
                                    binding.text1.setText(String.format("Choose password to continue registration process"));
                                    binding.passContinue.setText(String.format("Continue"));
                                    binding.etEmail.setHint("Choose password");
                                    binding.rlLayout1.startAnimation(animMove);
                                    binding.rlLayout2.startAnimation(animMove1);
                                    binding.rlLayout1.setVisibility(View.GONE);
                                    binding.rlLayout2.setVisibility(View.VISIBLE);
                                    binding.contuine.setVisibility(View.GONE);
                                    binding.passContinue.setVisibility(View.VISIBLE);
                                    binding.password.requestFocus();
                                    binding.ivBack1.setVisibility(View.VISIBLE);
                                    binding.ivBack.setVisibility(View.GONE);
                                    binding.etEmail.setVisibility(View.GONE);
                                    binding.password.setVisibility(View.VISIBLE);
                                } else if (resstatus == 3) {
                                    SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_home", true);
                                    SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_vocab", true);
                                    SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_quiz", true);
                                    SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_word", true);
                                    editor.putString("email", email);
                                    AppController.Email = email;
                                    editor.putString("mobile", "" + response.body().getMobile().trim().replace("null", ""));
                                    editor.putString("LoginStatus", "Success");
                                    editor.putString("uid", "" + response.body().getUid());
                                    editor.putString("name", "" + response.body().getName());
                                    editor.commit();
                                    SharePrefrence.getInstance(EmailActivity.this).putString(Utills.USERID, response.body().getUid());
                                    Intent intent = new Intent(EmailActivity.this, AppTour.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<LoginData> call, Throwable t) {
                        t.printStackTrace();

                    }
                });


        /*AndroidNetworking.post(Utills.BASE_URLGK + "user-login.php")
                .addBodyParameter("password", pass)
                .addBodyParameter("email", email)
                .addBodyParameter("type", "" + resstatus)// type = 1 for login and type= 0 for register
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Email Activity", String.valueOf(response));

                        if (dialog != null && dialog.isShowing() )
                            dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            resstatus = jsonObject.getInt("status");
                            int resstatusfb = jsonObject.optInt("fbstatus");
                            String msg = jsonObject.getString("msg");
                            Toast.makeText(EmailActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                            if (resstatus == 1) {
                                binding.forgot.setText(String.format("Forgot Password?"));
                                binding.forgot.setTextSize(14);
                                binding.text1.setText(String.format("Enter Password to login into your account"));
                                binding.passContinue.setText(String.format("Login"));
                                binding.etEmail.setHint("Enter password minimum 6 characters");
                                if (resstatusfb == 1) {
                                    binding.text1.setText(String.format("Choose password "));
                                    binding.forgot.setVisibility(View.GONE);
                                }
                                binding.rlLayout1.startAnimation(animMove);
                                binding.rlLayout2.startAnimation(animMove1);
                                binding.rlLayout1.setVisibility(View.GONE);
                                binding.rlLayout2.setVisibility(View.VISIBLE);
                                binding.contuine.setVisibility(View.GONE);
                                binding.passContinue.setVisibility(View.VISIBLE);
                                binding.password.requestFocus();
                                binding.ivBack1.setVisibility(View.VISIBLE);
                                binding.ivBack.setVisibility(View.GONE);
                                binding.etEmail.setVisibility(View.GONE);
                                binding.password.setVisibility(View.VISIBLE);
                            } else if (resstatus == 2) {
                                binding.forgot.setText("2/2");
                                binding.forgot.setTextSize(16);
                                binding.text1.setText(String.format("Choose password to continue registration process"));
                                binding.passContinue.setText(String.format("Continue"));
                                binding.etEmail.setHint("Choose password");
                                binding.rlLayout1.startAnimation(animMove);
                                binding.rlLayout2.startAnimation(animMove1);
                                binding.rlLayout1.setVisibility(View.GONE);
                                binding.rlLayout2.setVisibility(View.VISIBLE);
                                binding.contuine.setVisibility(View.GONE);
                                binding.passContinue.setVisibility(View.VISIBLE);
                                binding.password.requestFocus();
                                binding.ivBack1.setVisibility(View.VISIBLE);
                                binding.ivBack.setVisibility(View.GONE);
                                binding.etEmail.setVisibility(View.GONE);
                                binding.password.setVisibility(View.VISIBLE);
                            } else if (resstatus == 3) {
                                SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_home", true);
                                SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_vocab", true);
                                SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_quiz", true);
                                SharePrefrence.getInstance(EmailActivity.this).putBoolean("firsts_word", true);
                                editor.putString("email", email);
                                AppController.Email = email;
                                editor.putString("mobile", "" + jsonObject.getString("mobile").trim().replace("null", ""));
                                editor.putString("LoginStatus", "Success");
                                editor.putString("uid", "" + jsonObject.getString("uid"));
                                editor.putString("name", "" + jsonObject.getString("name"));
                                editor.commit();
                                SharePrefrence.getInstance(EmailActivity.this).putString(Utills.USERID, jsonObject.getString("uid"));
                                Intent intent = new Intent(EmailActivity.this, AppTour.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (dialog != null && dialog.isShowing() )
                            dialog.dismiss();
                        anError.printStackTrace();
                    }
                });*/
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        if (binding.rlLayout2.getVisibility() == View.VISIBLE) {
            resstatus = 1;
            binding.rlLayout1.startAnimation(animMoveback1);
            binding.rlLayout2.startAnimation(animMoveback);
            binding.rlLayout1.setVisibility(View.VISIBLE);
            binding.rlLayout2.setVisibility(View.GONE);
            binding.ivBack1.setVisibility(View.GONE);
            binding.ivBack.setVisibility(View.VISIBLE);
            binding.contuine.setVisibility(View.VISIBLE);
            binding.passContinue.setVisibility(View.GONE);
            binding.etEmail.setVisibility(View.VISIBLE);
            binding.password.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}