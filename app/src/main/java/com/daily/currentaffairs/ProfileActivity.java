package com.daily.currentaffairs;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.daily.currentaffairs.databinding.ActivityProfileBinding;

import DB.SharePrefrence;
import fragment.ProfileFragment;
import fragment.SecurityFragment;

public class ProfileActivity extends AppCompatActivity {
    int int_items = 2;
    ActivityProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        String theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setTitle("My Profile");
        binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.viewpager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        binding.tabs.post(new Runnable() {
            @Override
            public void run() {
                binding.tabs.setupWithViewPager(binding.viewpager);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    class MyAdapter extends FragmentStatePagerAdapter {
        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ProfileFragment();
                case 1:
                    return new SecurityFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Profile";
                case 1:
                    return "Security";
            }
            return null;
        }
    }
}