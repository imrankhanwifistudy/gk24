package com.daily.currentaffairs;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.daily.currentaffairs.databinding.ActivityViewVideoBinding;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Custom.Config;

public class YoutubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public int videoPlaybackMillis;
    public long videoPlaybackStartMillis;
    public boolean isFullScreen;
    String Video_URL, Key;
    YouTubePlayer youtubePlayer;
    ActivityViewVideoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_video);
        Video_URL = getIntent().getStringExtra("url");
        Key = extractYoutubeVideoId(Video_URL);
        binding.youtubeView.setVisibility(View.VISIBLE);
        // Initializing video player with developer key
        binding.youtubeView.initialize(Config.DEVELOPER_KEY, YoutubeActivity.this);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    public YoutubeActivity() {
        super();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewGroup fragViews = (ViewGroup) binding.youtubeView.getRootView();
        if (fragViews != null) {
            removeYouTubeButton(fragViews.toString(), fragViews);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseYouTubePlayer();
    }

    private void releaseYouTubePlayer() {
        if (this.youtubePlayer != null) {
            this.youtubePlayer.release();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.youtubePlayer = player;
        if (!wasRestored) {
            Log.e("Key", "" + Key);
            player.loadVideo(Key);
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            ViewGroup fragViews = (ViewGroup) binding.youtubeView.getRootView();
            if (fragViews != null) {
                removeYouTubeButton(fragViews.toString(), fragViews);
            }
            youtubePlayer.setShowFullscreenButton(true);
            youtubePlayer.setManageAudioFocus(false);
            youtubePlayer.setOnFullscreenListener(new C16311());
            youtubePlayer.setPlayerStateChangeListener(new C16322());
            youtubePlayer.setPlaybackEventListener(new C16333());
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ViewGroup fragViews = (ViewGroup) binding.youtubeView.getRootView();
        if (fragViews != null) {
            removeYouTubeButton(fragViews.toString(), fragViews);
        }
        if (newConfig.orientation == 2) {
            youtubePlayer.setFullscreen(true);

        } else if (newConfig.orientation == 1) {
            youtubePlayer.setFullscreen(false);
        }
    }

    private void removeYouTubeButton(String parentIfAnyOrSelf, ViewGroup curView) {
        for (int counter = 0; counter < curView.getChildCount(); counter++) {
            View curChild = curView.getChildAt(counter);
            if (curChild instanceof ViewGroup) {
                ViewGroup curChildViewGroup = (ViewGroup) curView.getChildAt(counter);
                if (curChildViewGroup.getChildCount() > 0) {
                    removeYouTubeButton(curChildViewGroup.toString(), curChildViewGroup);
                }
            }
            if ((parentIfAnyOrSelf.contains("app:id/bottom_end_container") && curChild.toString().contains("V.ED")) ||
                    (curChild instanceof ImageButton)) {
                curChild.setVisibility(8);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    class C16311 implements YouTubePlayer.OnFullscreenListener {
        C16311() {
        }

        public void onFullscreen(boolean b) {
            if (b) {
                isFullScreen = true;
                setRequestedOrientation(6);
                return;
            }
            isFullScreen = false;
            setRequestedOrientation(7);
        }
    }

    class C16322 implements YouTubePlayer.PlayerStateChangeListener {
        C16322() {
        }

        public void onLoading() {
        }

        public void onLoaded(String s) {
        }

        public void onAdStarted() {
        }

        public void onVideoStarted() {
            videoPlaybackMillis = 0;
        }

        public void onVideoEnded() {
        }

        public void onError(YouTubePlayer.ErrorReason errorReason) {
        }
    }

    class C16333 implements YouTubePlayer.PlaybackEventListener {
        boolean tempFlag = true;

        C16333() {
        }

        public void onPlaying() {
            videoPlaybackStartMillis = System.currentTimeMillis();
        }

        public void onPaused() {
            this.tempFlag = false;
            if (videoPlaybackStartMillis > 0) {
                videoPlaybackMillis = (int) (((long) videoPlaybackMillis) + (System.currentTimeMillis() - videoPlaybackStartMillis));
                videoPlaybackStartMillis = 0;
            }
        }

        public void onStopped() {
            if (videoPlaybackStartMillis > 0) {
                videoPlaybackMillis = (int) (((long) videoPlaybackMillis) + (System.currentTimeMillis() - videoPlaybackStartMillis));
                videoPlaybackStartMillis = 0;
            }
        }

        public void onBuffering(boolean b) {
            if (videoPlaybackStartMillis > 0) {
                videoPlaybackMillis = (int) (((long) videoPlaybackMillis) + (System.currentTimeMillis() - videoPlaybackStartMillis));
                videoPlaybackStartMillis = 0;
            }
        }

        public void onSeekTo(int i) {
            if (videoPlaybackStartMillis > 0) {
                videoPlaybackMillis = (int) (((long) videoPlaybackMillis) + (System.currentTimeMillis() - videoPlaybackStartMillis));
                videoPlaybackStartMillis = 0;
            }
        }
    }

    public static String extractYoutubeVideoId(String ytUrl) {
        String vId = null;
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(ytUrl);
        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}