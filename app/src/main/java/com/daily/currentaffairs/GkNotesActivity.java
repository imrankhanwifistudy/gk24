package com.daily.currentaffairs;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.daily.currentaffairs.databinding.EditorialActivityBinding;

import java.util.ArrayList;

import Adapter.GkPagerAdapter;
import DB.DatabaseHandler;
import Modal.QuizModel;

public class GkNotesActivity extends AppCompatActivity {
    GkPagerAdapter adapter;
    int position = 0;
    DatabaseHandler db;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        EditorialActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.editorial_activity);

        db = new DatabaseHandler(getApplicationContext());
        Intent intent = getIntent();

        final ArrayList<QuizModel> arrayList = (ArrayList<QuizModel>) intent.getSerializableExtra("array");
        if (intent.hasExtra("position"))
            position = intent.getIntExtra("position", 0);

        adapter = new GkPagerAdapter(GkNotesActivity.this, arrayList, binding.editorialviewpager);

        binding.editorialviewpager.setAdapter(adapter);
        binding.editorialviewpager.setCurrentItem(position);

        binding.editorialviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}