package com.daily.currentaffairs;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.daily.currentaffairs.databinding.GkActivityBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import DB.SharePrefrence;
import DB.Utills;
import Modal.GkTitleModel;
import fragment.GkBulletian;
import fragment.GkCapsule;
import fragment.VideoFragment1;
import ui.preferences.AppPreferenceManager;

public class GkActivity extends AppCompatActivity {
    private static final String TAG = GkActivity.class.getSimpleName();
    public static String Title, CatID;
    public static String SubCatID;
    public static int position;
    public static ArrayList<GkTitleModel> arrayList;
    String activity_color = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        String theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        GkActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.gk_activity);
        try {
            ParseJson(SharePrefrence.getInstance(getApplicationContext()).getString(Utills.GK));
        } catch (Exception e) {
            e.printStackTrace();
        }
        setSupportActionBar(binding.toolbar);
        Intent intent = getIntent();
        CatID = intent.getStringExtra("CatID");
        SubCatID = intent.getStringExtra("SubCatID");
        Log.e(TAG, CatID + " , " + SubCatID);
        Title = intent.getStringExtra("Title");
        position = intent.getIntExtra("position", 0);
        SharePrefrence.getInstance(getApplicationContext()).putString("gk_activity_name", Title);
        if (position == 0) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#f8a942"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FFFFC478"));
            activity_color = "#f8a942";
        } else if (position == 1) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#50adf3"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FF84C4F4"));
            activity_color = "#50adf3";
        } else if (position == 2) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#ee6c78"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FFF5939C"));
            activity_color = "#ee6c78";
        } else if (position == 3) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#1bbe9f"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FF7AE6D1"));
            activity_color = "#1bbe9f";
        } else if (position == 4) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#5599c8"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FF7BB6DF"));
            activity_color = "#5599c8";
        } else if (position == 5) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#9e62b8"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FFC188DA"));
            activity_color = "#9e62b8";
        } else if (position == 6) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#5558d9"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FF8A8CE7"));
            activity_color = "#5558d9";
        } else if (position == 7) {
            binding.toolbar.setBackgroundColor(Color.parseColor("#1eb0bc"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FF49D1DC"));
            activity_color = "#1eb0bc";
        } else {
            binding.toolbar.setBackgroundColor(Color.parseColor("#f8a942"));
            binding.tabs.setBackgroundColor(Color.parseColor("#FFFFC478"));
            activity_color = "#f8a942";
        }

        if (getIntent() != null && getIntent().hasExtra("fromScreen") ) {
            setupVideoViewPager(binding.viewpager);
            binding.tabs.setVisibility(View.GONE);
        } else {
            setupViewPager(binding.viewpager);
        }



        binding.viewpager.setCurrentItem(0);
        binding.tabs.setupWithViewPager(binding.viewpager);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(Title);

        binding.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.viewpager.setOffscreenPageLimit(5);
    }

    private void ParseJson(String response) throws Exception {
        arrayList = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("response");
        for (int i = 0; i < jsonArray.length(); i++) {
            GkTitleModel item = new GkTitleModel();
            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
            String txtTitle = jsonObject1.getString("title");
            String catId = jsonObject1.getString("catid");
            item.setTitle(txtTitle);
            item.setCatId(catId);
            JSONArray jsonArray1 = jsonObject1.getJSONArray("subcat");
            item.setJsonArray(jsonArray1);
            arrayList.add(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new GkCapsule(), "Notes", CatID, SubCatID, activity_color);
        adapter.addFragment(new GkBulletian(), "Bulletian", CatID, SubCatID, activity_color);
        viewPager.setAdapter(adapter);
    }


    private void setupVideoViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new VideoFragment1(), "Gk Video", CatID, SubCatID, activity_color);
        viewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title, String CatID, String SubCatID, String activity_color) {
            Log.e("activity_color", "" + activity_color);
            Bundle bundle = new Bundle();
            bundle.putString("CatID", CatID);
            bundle.putString("SubCatID", SubCatID);
            bundle.putString("TitleName", title);
            bundle.putString("activity_color", activity_color);
            fragment.setArguments(bundle);
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            super.onBackPressed();
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            try {
                String UID = data.getStringExtra("UID");
                String RANKUID = data.getStringExtra("RANKUID");
                String PractiseTestName = data.getStringExtra("PractiseTestName");
                String TestTitleName = data.getStringExtra("TestTitleName");
                Intent intent = new Intent(GkActivity.this, TopicResultActivity.class);
                intent.putExtra("testId", "" + UID);
                intent.putExtra("Id", "" + RANKUID);
                intent.putExtra("TestName", "" + PractiseTestName);
                intent.putExtra("TestTitleName", "" + TestTitleName);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}