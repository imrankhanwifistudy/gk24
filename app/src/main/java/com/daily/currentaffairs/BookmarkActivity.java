package com.daily.currentaffairs;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.daily.currentaffairs.databinding.ActivityBookmarkBinding;

import DB.SharePrefrence;
import DB.Utills;
import fragment.BookMarkVideoFragment;
import fragment.ParagraphBookmark;
import fragment.QuestionBookmark;

public class BookmarkActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityBookmarkBinding binding;
    public static boolean lang_flag = true;
    public static int pos = 0;

    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    private FragmentRefreshListener fragmentRefreshListener;

    public FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener2(FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private FragmentRefreshListener2 fragmentRefreshListener2;

    public FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener1(FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private FragmentRefreshListener1 fragmentRefreshListener1;
    String theme2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bookmark);
        BookmarkActivity.lang_flag = !SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE).equalsIgnoreCase("English");
        binding.viewpager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        binding.viewpager.setCurrentItem(pos);
        binding.tabs.post(new Runnable() {
            @Override
            public void run() {
                binding.tabs.setupWithViewPager(binding.viewpager);
            }
        });
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setTitle("My Bookmarks");
        binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.SyncAll.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Sync_all:
                PopupMenu popup = new PopupMenu(BookmarkActivity.this, binding.SyncAll);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.sync_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Capsule:
                                if (getFragmentRefreshListener2() != null) {
                                    getFragmentRefreshListener2().onRefresh();
                                }
                                break;
                            case R.id.Question:
                                if (getFragmentRefreshListener() != null) {
                                    getFragmentRefreshListener().onRefresh();
                                }
                                break;
                            case R.id.Video:
                                if (getFragmentRefreshListener1() != null) {
                                    getFragmentRefreshListener1().onRefresh();
                                }
                                break;
                        }
                        return true;
                    }
                });
                popup.show(); //showing popup menu
                break;
        }
    }

    private class MyAdapter extends FragmentStatePagerAdapter {
        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ParagraphBookmark();
                case 1:
                    return new QuestionBookmark();
               /* case 2:
                    return new BookMarkVideoFragment();*/
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Capsule";
                case 1:
                    return "Question";
               /* case 2:
                    return "Video";*/
            }
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (theme2) {
            case "night":
                binding.tabs.setBackgroundColor(Color.parseColor("#363636"));
                break;
            case "sepia":
                binding.tabs.setBackgroundColor(Color.parseColor("#EEE3C7"));
                break;
            default:
                binding.tabs.setBackgroundColor(Color.parseColor("#16235a"));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public interface FragmentRefreshListener {
        void onRefresh();
    }

    public interface FragmentRefreshListener2 {
        void onRefresh();
    }

    public interface FragmentRefreshListener1 {
        void onRefresh();
    }
}