package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.DialogStartTestBinding;
import com.daily.currentaffairs.databinding.GkNotesVideosBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import Adapter.TestListRecyAdapter;
import ClickListener.ItemClickSupport;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;
import Modal.TopicTestList;
import ui.AppController;

import static ui.AppController.sharedPreferences;

public class PracticeTestActivity extends AppCompatActivity {
    int InitialValue = 1;
    boolean isend = false;
    String TitleName;
    ArrayList<TopicTestList> arrTestList;
    DatabaseHandler db;
    String cat_name, total_time, total_question, correct_mark, wrong_mark, attempt;
    GkNotesVideosBinding binding;
    private ArrayList<FreeTestItem> testitem;
    TestListRecyAdapter adapterRecy;
    String CatID, SubCatID;
    String theme2;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding= DataBindingUtil.setContentView(this,R.layout.gk_notes_videos);
        AppController.Qtype = 2 ;
        SharePrefrence.getInstance(getApplicationContext()).putString("quiz_lang", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
        CatID = getIntent().getStringExtra("CatID");
        SubCatID = getIntent().getStringExtra("SubCatID");
        TitleName = getIntent().getStringExtra("Title");
        TOOLBAR();
        db = new DatabaseHandler(getApplicationContext());
        if (Utils.hasConnection(getApplicationContext())) {
            get_Detail(CatID, SubCatID);
        } else {
            String gk_data = SharePrefrence.getInstance(getApplicationContext()).getString(SharePrefrence.getInstance(getApplicationContext()).getString("gk_activity_name") + "Test");
            if (gk_data.length() > 0) {
                binding.progressView.setVisibility(View.GONE);
                parasData(gk_data);
            } else {
                Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
                binding.progressView.setVisibility(View.GONE);
                isend = true;
                InitialValue = 1;
            }
        }
        ItemClickSupport.addTo(binding.quizList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int i, View v) {
                TopicTestList model = arrTestList.get(i);
                testitem = new ArrayList<>();
                Calendar calendar = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
                String year = YearForm.format(calendar.getTime());
                String uniq = year + model.getId() + "TopicTest" + SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE);
                try {
                    boolean isData = db.CheckIsDataAlreadyInReadUnread(uniq);
                    if (isData) {
                        boolean flag = db.CheckIsResultAlreadyInDBorNotReadyStock(uniq, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                        if (flag) {
                            Intent intent = new Intent(getApplicationContext(), TopicResultActivity.class);
                            intent.putExtra("testId", "" + uniq);
                            intent.putExtra("Id", "" + arrTestList.get(i).getId());
                            intent.putExtra("TestName", "" + model.getName());
                            intent.putExtra("TestTitleName", model.getName());
                            startActivityForResult(intent, 1000);
                        } else {
                            if (Utils.hasConnection(getApplicationContext())) {
                                testitem.clear();
                                String positive = model.getPositive();
                                String negative = model.getNegative();
                                String totalTime = model.getTime();
                                Utils.PROGRESS(PracticeTestActivity.this);
                                String TestName = model.getName() + " " + arrTestList.get(i).getName() + "Result";
                                getTestDetails(arrTestList.get(i).getId(), uniq, positive, negative, totalTime, TestName, model.getName());
                            }
                        }
                    } else {
                        if (Utils.hasConnection(getApplicationContext())) {
                            testitem.clear();
                            String positive = model.getPositive();
                            String negative = model.getNegative();
                            String totalTime = model.getTime();
                            Utils.PROGRESS(PracticeTestActivity.this);
                            String TestName = model.getName() + " " + arrTestList.get(i).getName() + "Result";
                            getTestDetails(arrTestList.get(i).getId(), uniq, positive, negative, totalTime, TestName, model.getName());
                        } else {
                            Toast.makeText(PracticeTestActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void get_Detail(final String catID, final String subCatID) {
        AndroidNetworking.post(Utills.BASE_URLGK+"gk-test.php")
                .addBodyParameter("uid", sharedPreferences.getString("uid",""))
                .addBodyParameter("catid", catID)
                .addBodyParameter("subcatid", subCatID)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning",String.valueOf(response));
                        binding.progressView.setVisibility(View.GONE);
                        Log.e("response", String.valueOf(response));
                        try {
                            arrTestList = new ArrayList<>();
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if (success == 1) {
                                binding.progressView.setVisibility(View.GONE);
                                String gk_title= SharePrefrence.getInstance(getApplicationContext()).getString("gk_activity_name")+"Test";
                                SharePrefrence.getInstance(getApplicationContext()).putString(gk_title,String.valueOf(response));
                                parasData(String.valueOf(response));
                            } else if (success == 0) {
                                binding.progressView.setVisibility(View.GONE);
                                isend = true;
                                InitialValue = 1;
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
    public void parasData(String response){
        arrTestList = new ArrayList<>();
        try{
            JSONObject obj = new JSONObject(response);
            JSONArray array = obj.getJSONArray("response");
            for (int j = 0; j < array.length(); j++) {
                try {
                    TopicTestList testModel = new TopicTestList();
                    JSONObject objTest = array.getJSONObject(j);
                    testModel.setStauts(objTest.getString("tstatus"));
                    testModel.setTime(objTest.getString("timeoftest"));
                    testModel.setNegative(objTest.getString("negative"));
                    testModel.setPositive(objTest.getString("positive"));
                    testModel.setName(objTest.getString("test_title"));
                    testModel.setId(objTest.getString("testid"));
                    testModel.setNoofQuestion(objTest.getString("total_question"));
                    arrTestList.add(testModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (arrTestList.size() > 0)
                binding.noTest.setVisibility(View.GONE);
            else
                binding.noTest.setVisibility(View.VISIBLE);
            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            binding.quizList.setHasFixedSize(true);
            binding.quizList.setLayoutManager(manager);
            adapterRecy = new TestListRecyAdapter(PracticeTestActivity.this, arrTestList);
            binding.quizList.setAdapter(adapterRecy);
            if (adapterRecy.getItemCount() > 0)
                binding.noTest.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(adapterRecy != null)
        {
            adapterRecy = new TestListRecyAdapter(PracticeTestActivity.this, arrTestList);
            binding.quizList.setAdapter(adapterRecy);
            adapterRecy.notifyDataSetChanged();
        }
        theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        if(!theme2.equalsIgnoreCase("night") && !theme2.equalsIgnoreCase("sepia"))
        {
            binding.quizList.setBackgroundColor(Color.parseColor("#FF8A8CE7"));
            binding.toolbar.setBackgroundColor(Color.parseColor("#5558d9"));
        }
    }
    private void getTestDetails(final String id, final String UID, final String positive, final String nagative, final String totalTime,
                                final String PractiseTestName, final String TestTitleName) {
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        final String test_name = year +id + "TopicTest"+ SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE);
        Log.e("Tesrt Url", Utills.BASE_URLGK + "gk-questions.php?testid="+id+"&lang="+ SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
        AndroidNetworking.post(Utills.BASE_URLGK+"gk-questions.php")
                .addBodyParameter("testid", id)
                .addBodyParameter("lang", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning",String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            correct_mark = obj.getString("right_mark");
                            wrong_mark = obj.getString("rowsqsmar");
                            total_question = obj.getString("total_question");
                            total_time = obj.getString("total_time");
                            attempt = obj.getString("attemp");
                            if (success == 1) {
                                JSONArray array = obj.getJSONArray("response");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsobj = array.getJSONObject(i);
                                    FreeTestItem item = new FreeTestItem();
                                    item.setId(jsobj.getString("id"));
                                    item.setDirection(jsobj.getString("direction"));
                                    item.setQuestionenglish(jsobj.getString("question"));
                                    item.setOpt_en_1(jsobj.getString("optiona"));
                                    item.setOp_en_2(jsobj.getString("optionb"));
                                    item.setOp_en_3(jsobj.getString("optionc"));
                                    item.setOp_en_4(jsobj.getString("optiond"));
                                    if (jsobj.has("optione")) {
                                        item.setOp_en_5(jsobj.getString("optione"));
                                    } else {
                                        item.setOp_en_5("");
                                    }
                                    item.setAnswer(jsobj.getString("correct_answer"));
                                    item.setSolutionenglish(jsobj.getString("explaination"));
                                    item.setNoofoption(jsobj.getString("noofoption"));
                                    item.setTime(totalTime);
                                    item.setTotalquestio(total_question);
                                    item.setCorrectmarks(positive);
                                    item.setWrongmarks(nagative);
                                    item.setNo_of_attempt(attempt);
                                    testitem.add(item);
                                }
                                try {
                                    boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                                    if (!isData) {
                                        db.addQuizRes(test_name, String.valueOf(response), SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (success == 0) {
                                Toast.makeText(getApplicationContext(), "Internal error ocured", Toast.LENGTH_LONG).show();
                            }
                            if (positive.trim().length() > 0 && nagative.trim().length() > 0) {
                                final Dialog fb_dialog = new Dialog(PracticeTestActivity.this,R.style.MyActivityDialogTheme);
                                DialogStartTestBinding dialogStartTestBinding=DataBindingUtil.inflate(LayoutInflater.from(PracticeTestActivity.this),
                                        R.layout.dialog_start_test,null,false);
                                fb_dialog.setContentView(dialogStartTestBinding.getRoot());
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                lp.copyFrom(fb_dialog.getWindow().getAttributes());
                                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                fb_dialog.getWindow().setAttributes(lp);
                                fb_dialog.show();
                                dialogStartTestBinding.testQuestion.setText(String.valueOf(total_question));
                                dialogStartTestBinding.testTime.setText(String.valueOf( total_time ));
                                dialogStartTestBinding.testMarks.setText(String.valueOf( (Integer.parseInt(total_question) * Integer.parseInt(positive))));
                                dialogStartTestBinding.testCorrectMarks.setText(String.valueOf( positive));
                                dialogStartTestBinding.testIncorrectMarks.setText(String.valueOf( nagative));
                                dialogStartTestBinding.testCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        fb_dialog.dismiss();
                                    }
                                });
                                dialogStartTestBinding.testStart.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            db.addreadUnread(test_name, "TopicTest");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        Intent intent = new Intent(getApplicationContext(), QuestionQuiz.class);
                                        intent.putExtra("TopicWiseTest", "TopicWiseTest");
                                        intent.putExtra("language", "english");
                                        intent.putExtra("testitem", testitem);
                                        intent.putExtra("catname", cat_name);
                                        intent.putExtra("testname", test_name);
                                        intent.putExtra("correctmark", positive);
                                        intent.putExtra("wrongmark", nagative);
                                        intent.putExtra("totalque", total_question);
                                        intent.putExtra("time", totalTime);
                                        intent.putExtra("topictest", true);
                                        intent.putExtra("UID", UID);
                                        intent.putExtra("RANKUID", id);
                                        intent.putExtra("PractiseTestName", PractiseTestName);
                                        intent.putExtra("TestTitleName", TestTitleName);
                                        startActivityForResult(intent, 1000);
                                        fb_dialog.dismiss();
                                    }
                                });
                            } else {
                                try {
                                    db.addreadUnread(test_name, "TopicTest");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(getApplicationContext(), QuestionQuiz.class);
                                intent.putExtra("TopicWiseTest", "TopicWiseTest");
                                intent.putExtra("language", "english");
                                intent.putExtra("testitem", testitem);
                                intent.putExtra("catname", cat_name);
                                intent.putExtra("testname", test_name);
                                intent.putExtra("correctmark", positive);
                                intent.putExtra("wrongmark", nagative);
                                intent.putExtra("totalque", total_question);
                                intent.putExtra("time", totalTime);
                                intent.putExtra("topictest", true);
                                intent.putExtra("UID", UID);
                                intent.putExtra("RANKUID", id);
                                intent.putExtra("PractiseTestName", PractiseTestName);
                                intent.putExtra("TestTitleName", TestTitleName);
                                startActivityForResult(intent, 1000);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
    void TOOLBAR(){
        binding.toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(TitleName);
        binding.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}