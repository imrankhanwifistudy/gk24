package com.daily.currentaffairs.QuizPresenter;

import android.widget.TextView;
import Adapter.QuizInsideAdapter;

public interface Quiz_ConfirmView {
    void navigateToHome(String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String test_name, String type, String week_no, TextView testButton,
                        String quiz_type, int position, String reponce);
}
