package com.daily.currentaffairs.QuizPresenter;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import org.json.JSONException;
import org.json.JSONObject;
import Adapter.QuizInsideAdapter;
import DB.SharePrefrence;
import DB.Utills;
import static com.daily.currentaffairs.MainActivity.activity;

public class Quiz_InteractorImpl implements Quiz_Interactor {
    private Context ctx ;
    @Override
    public void login(Context ctx, String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz,
                      String test_name ,String type, String week_no,TextView testButton,
                      OnLoginFinishedListener listener, String ui, int position) {
        this.ctx = ctx ;
        Get_Catagery(idQuiz, holder ,dateQuiz  ,test_name, type, week_no, testButton, ui ,position , listener);

    }
    private void Get_Catagery(final String idQuiz, final QuizInsideAdapter.ViewHolder holder, final String dateQuiz, final String test_name, final String type,
                              final String week_no, final TextView testButton, final String quiz_type, final int position, final OnLoginFinishedListener listener) {
        Log.e("quiz_api_url",Utills.BASE_URLGK +"current_quiz.php?lang="+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE)
                +"&type="+type+"&dttime="+dateQuiz+"&week="+week_no+"&quiz_type="+quiz_type);
        AndroidNetworking.post(Utills.BASE_URLGK +"current_quiz.php")
                .addBodyParameter("lang", SharePrefrence.getInstance(ctx).getString(Utills.DEFAULT_LANGUAGE))
                .addBodyParameter("type", type)
                .addBodyParameter("dttime", dateQuiz)
                .addBodyParameter("week",week_no)
                .addBodyParameter("quiz_type",quiz_type)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter" , String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if (success == 1) {
                                listener.onSuccess(  idQuiz,   holder,   dateQuiz,
                                  test_name,   type,
                                  week_no,   testButton,  quiz_type,  position,String.valueOf(response));

                            } else if (success == 0) {
                                listener.onSuccess(  idQuiz,   holder,   dateQuiz,
                                  test_name,   type,
                                  week_no,   testButton,  quiz_type,   position,String.valueOf(response));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
}
