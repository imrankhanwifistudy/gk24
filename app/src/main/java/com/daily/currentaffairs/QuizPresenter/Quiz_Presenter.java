package com.daily.currentaffairs.QuizPresenter;

import android.content.Context;
import android.widget.TextView;
import Adapter.QuizInsideAdapter;

public interface Quiz_Presenter {
    void validateCredentials(Context ctx, String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String testname, String type, String week_no,
                             TextView testButton, String ui, int position);
    void onDestroy();
}
