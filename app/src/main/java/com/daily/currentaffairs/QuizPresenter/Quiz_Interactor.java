package com.daily.currentaffairs.QuizPresenter;

import android.content.Context;
import android.widget.TextView;
import Adapter.QuizInsideAdapter;

public interface Quiz_Interactor {
    interface OnLoginFinishedListener {
        void onSuccess(String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String test_name, String type, String week_no, TextView testButton, String quiz_type, int position, String Responce);
    }
    void login(Context ctx, String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String username,String password, String phone,TextView testButton,
               OnLoginFinishedListener listener, String ui, int position);
}
