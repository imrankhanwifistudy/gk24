package com.daily.currentaffairs.QuizPresenter;

import android.content.Context;
import android.widget.TextView;
import Adapter.QuizInsideAdapter;

public class QuizPresenterImpl implements Quiz_Presenter, Quiz_Interactor.OnLoginFinishedListener {
    private Quiz_ConfirmView loginView;
    private Quiz_Interactor loginInteractor;
    public QuizPresenterImpl(Context ctx , Quiz_ConfirmView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new Quiz_InteractorImpl();
    }
    @Override
    public void validateCredentials(Context ctx, String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String test_name, String type,
                                    String week_no, TextView testButton, String ui, int position) {
        loginInteractor.login(ctx , idQuiz,  holder, dateQuiz,  test_name,  type, week_no,  testButton, this,ui ,position);
    }
    @Override
    public void onDestroy() {
        loginView = null;
    }
    @Override
    public void onSuccess(String idQuiz, QuizInsideAdapter.ViewHolder holder, String dateQuiz, String test_name,
                          String type, String week_no, TextView testButton, String quiz_type, int position, String reponce) {
        if (loginView != null)
            loginView.navigateToHome( idQuiz,  holder,  dateQuiz,  test_name, type,  week_no,  testButton,  quiz_type,  position,reponce);
    }
}
