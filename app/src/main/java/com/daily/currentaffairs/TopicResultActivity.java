package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.databinding.StartTestActivityBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ClickListener.ResultClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

public class TopicResultActivity extends AppCompatActivity {
    @SuppressLint("StaticFieldLeak")
    public String user_rank_id = "", test_name, total_question, correct_mark, wrong_mark, correctanswerstr = "", incorrectanswerstr = "",
            skipstr = "", accuracystr = "", TestName, TestTitleName = "", week = "", idQuiz, RankID, rank = "0", totalStudent, Device_Idd,
            CorrectM, WromgM, time, totalMarks = "0";
    int wbTotal_question;
    FreeTestItem item;
    DatabaseHandler db;
    boolean isData = false;
    private ArrayList<FreeTestItem> testitem;
    StartTestActivityBinding binding;
    Drawable drawReload, drawShare;

    void TOOLBAR(String TestName) {
        binding.ResultLayout.setVisibility(View.VISIBLE);
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setTitle("" + TestName);
        binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        binding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void SETUI() {
        binding.Reload.setVisibility(View.GONE);
        binding.Reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                if (flag) {
                    Cursor c = db.getresult(test_name);
                    if (c.moveToFirst()) {
                        while (!c.isAfterLast()) {
                            correctanswerstr = c.getString(c.getColumnIndex("correctanswer"));
                            c.moveToNext();
                        }
                    }
                    c.close();
                }
                if (correctanswerstr.length() > 0 && Utils.hasConnection(getApplicationContext())) {
                    binding.Reload.setVisibility(View.GONE);
                    binding.pbar.setVisibility(View.VISIBLE);
                    getRank(correctanswerstr);
                } else {
                    Toast.makeText(TopicResultActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @SuppressLint({"HardwareIds", "SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        String theme2 = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        drawReload = Utils.DrawableChange(TopicResultActivity.this, R.drawable.ic_refresh_white_24dp, "#16235a");
        drawShare = Utils.DrawableChange(TopicResultActivity.this, R.drawable.ic_share_white_24dp, "#16235a");
        switch (theme2) {
            case "night":
                setTheme(R.style.night);
                drawReload = Utils.DrawableChange(TopicResultActivity.this, R.drawable.ic_refresh_white_24dp, "#ffffff");
                drawShare = Utils.DrawableChange(TopicResultActivity.this, R.drawable.ic_share_white_24dp, "#ffffff");
                break;
            case "sepia":
                setTheme(R.style.sepia);
                break;
            default:
                setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.setContentView(this, R.layout.start_test_activity);
        binding.Reload.setImageDrawable(drawReload);
        binding.shareImageView.setImageDrawable(drawShare);
        SETUI();
        SharePrefrence.getInstance(getApplicationContext()).putString("quiz_lang", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
        testitem = new ArrayList<>();
        db = new DatabaseHandler(getApplicationContext());
        Device_Idd = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Intent intent = getIntent();
        idQuiz = intent.getStringExtra("testId");
        RankID = intent.getStringExtra("Id");
        TestName = intent.getStringExtra("TestName");
        if (intent.hasExtra("week"))
            week = intent.getStringExtra("week");
        if (intent.hasExtra("TestName"))
            TestName = intent.getStringExtra("TestName");
        else TestName = "Quiz Result";
        if (intent.hasExtra("TestTitleName"))
            TestTitleName = intent.getStringExtra("TestTitleName");
        TOOLBAR(TestName);
        binding.ViewQuiz.setText(" VIEW SOLUTIONS ");
        if (TestName.equals("Quiz Report"))
            user_rank_id = "";
        else {
            MainActivity.quizdate = "";
            user_rank_id = RankID;
        }
        test_name = idQuiz;
        boolean flag = false;
        try {
            flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (flag) {
            Cursor c = null;
            try {
                c = db.getresult(test_name);
            } catch (Exception e) {
                e.printStackTrace();
            }
            assert c != null;
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    correctanswerstr = c.getString(c.getColumnIndex("correctanswer"));
                    incorrectanswerstr = c.getString(c.getColumnIndex("incorrectanswer"));
                    CorrectM = c.getString(c.getColumnIndex("correct"));
                    WromgM = c.getString(c.getColumnIndex("incorrect"));
                    time = c.getString(c.getColumnIndex("totaltime"));
                    skipstr = c.getString(c.getColumnIndex("skipanswer"));
                    total_question = "" + (Integer.parseInt(correctanswerstr) + Integer.parseInt(incorrectanswerstr) + Integer.parseInt(skipstr));
                    accuracystr = c.getString(c.getColumnIndex("accuracy"));
                    Log.e("Accuracy:---", accuracystr);
                    rank = c.getString(c.getColumnIndex("rank"));
                    if (rank == null)
                        rank = "updating";
                    binding.RankTextViwe.setText("" + rank);
                    binding.pbar.setVisibility(View.INVISIBLE);
                    binding.Reload.setVisibility(View.VISIBLE);
                    binding.correctAttamp.setText(" (" + correctanswerstr + ")");
                    binding.incorrectAttamp.setText(" (" + incorrectanswerstr + ")");
                    binding.skippedQue.setText(" (" + skipstr + ")");
                    if (accuracystr.contains("-")) {
                        accuracystr = "00";
                        binding.accuracyResult.setText("" + "00 %");
                        binding.progress4.setText("00 %");
                        binding.progress4.setProgressValue(0);
                        binding.progress4.setProgressValue2(100);
                    } else {
                        try {
                            if (accuracystr.length() >= 0) {
                                String acc1[] = accuracystr.split("\\.");
                                binding.progress4.setText(String.format("%.2f", Float.parseFloat(accuracystr)) + "%");
                                Log.e("accuracystr:---", accuracystr + " , " + acc1[0]);
                                binding.progress4.setProgressValue(Integer.parseInt(acc1[0]));
                                binding.progress4.setProgressValue2(100);
                            } else {
                                binding.progress4.setText("0 %");
                                binding.progress4.setProgressValue(0);
                                binding.progress4.setProgressValue2(100);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                       /* float performancePercent = Float.parseFloat(accuracystr);

                        if (performancePercent >= 90) {
                            binding.tvPerformane.setText(getResources().getString(R.string.good_performance));
                            binding.tvPerformane.setTextColor(getResources().getColor(R.color.good));

                            binding.ivPerforamce.setImageResource(R.drawable.performance_good);

                        } else if (performancePercent >= 70) {
                            binding.tvPerformane.setText(getResources().getString(R.string.excellent_performance));
                            binding.tvPerformane.setTextColor(getResources().getColor(R.color.execllent));
                            binding.ivPerforamce.setImageResource(R.drawable.performance_excellent);

                        } else if (performancePercent >= 40) {
                            binding.tvPerformane.setText(getResources().getString(R.string.average_performance));
                            binding.tvPerformane.setTextColor(getResources().getColor(R.color.average));
                            binding.ivPerforamce.setImageResource(R.drawable.performance_average);

                        } else {
                            binding.tvPerformane.setText(getResources().getString(R.string.poor_performance));
                            binding.tvPerformane.setTextColor(getResources().getColor(R.color.poor));
                            binding.ivPerforamce.setImageResource(R.drawable.performance_poor);

                        }*/



                    }
                    float CorrectMark = Float.parseFloat(correctanswerstr) * Float.parseFloat(CorrectM);
                    float INCorrectMark = Float.parseFloat(incorrectanswerstr) * Float.parseFloat(WromgM);
                    float wbTotal_question = Float.parseFloat(correctanswerstr) + Float.parseFloat(incorrectanswerstr) + Integer.parseInt(skipstr);
                    float totalNumber = wbTotal_question * Float.parseFloat(CorrectM);
                    float GetNumber = CorrectMark - INCorrectMark;
                    String srTotalNumber = String.valueOf(totalNumber);
                    String srTotalNur[] = srTotalNumber.split("\\.");
                    totalMarks = srTotalNur[0];
                    binding.progress2.setText(GetNumber + "/" + srTotalNur[0]);
                    String PbValue = String.valueOf((GetNumber * 100) / totalNumber);
                    String sarray[] = PbValue.split("//.");
                    float f1 = Float.parseFloat(sarray[0]);
                    if (f1 < 0)
                        f1 = 0;
                    binding.progress2.setProgressValue(Math.round((int) f1));
                    binding.progress2.setProgressValue2(100);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("data", "" + Float.parseFloat(correctanswerstr) + "," + Float.parseFloat(incorrectanswerstr) + "," + Float.parseFloat(skipstr));
                            float[] data = {Float.parseFloat(correctanswerstr), Float.parseFloat(incorrectanswerstr), Float.parseFloat(skipstr)};
                            binding.pieChart.setData(data);
                        }
                    });
                    c.moveToNext();
                }
            }
            c.close();
            binding.pbar.setVisibility(View.INVISIBLE);
            binding.Reload.setVisibility(View.VISIBLE);
            if (Utils.hasConnection(getApplicationContext()))
                getRank(correctanswerstr);
            binding.startQuiz.setVisibility(View.GONE);
            binding.QuizTitle.setVisibility(View.GONE);
            binding.quizDate.setVisibility(View.GONE);
            binding.totalquestion.setVisibility(View.GONE);
            binding.resetQuiz.setVisibility(View.VISIBLE);
            binding.ViewQuiz.setVisibility(View.VISIBLE);
            binding.layout2.setVisibility(View.VISIBLE);
            binding.rankLayout.setVisibility(View.VISIBLE);
        }
        try {
            isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
            if (isData) {
                String response = db.getQuizRes(test_name, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                ParseData(response);
                binding.startQuiz.setText("START QUIZ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.setClick(new ResultClickListner(TopicResultActivity.this, binding, testitem, test_name, correct_mark, wrong_mark,
                total_question, TestTitleName, RankID, correctanswerstr, week, CorrectM, WromgM, time, TestName));
    }

    private void getRank(final String score) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.pbar.setVisibility(View.VISIBLE);
                binding.Reload.setVisibility(View.GONE);
                Log.e("result_api", Utills.BASE_URLGK + "userranks.php?deviceid=" + Device_Idd + "&id=" + user_rank_id +
                        "&score=" + score + "&total_marks=" + totalMarks + "&quizdate=" + MainActivity.quizdate +
                        "&uid=" + SharePrefrence.getInstance(getApplicationContext()).getString(Utills.USERID));
                AndroidNetworking.post(Utills.BASE_URLGK + "userranks.php")
                        .addBodyParameter("deviceid", Device_Idd)
                        .addBodyParameter("id", user_rank_id)
                        .addBodyParameter("score", score)
                        .addBodyParameter("total_marks", totalMarks)
                        .addBodyParameter("quizdate", MainActivity.quizdate)
                        .addBodyParameter("uid", SharePrefrence.getInstance(getApplicationContext()).getString(Utills.USERID))
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onResponse(JSONObject response) {
                                binding.pbar.setVisibility(View.INVISIBLE);
                                binding.Reload.setVisibility(View.VISIBLE);
                                try {
                                    JSONObject obj = new JSONObject(String.valueOf(response));
                                    int status = obj.getInt("status");
                                    if (status == 1) {
                                        rank = obj.getString("rank");
                                        totalStudent = obj.getString("totalst");
                                        if (totalStudent.equals("0")) {
                                            totalStudent = "1";
                                        }
                                        ContentValues contentValues = new ContentValues();
                                        contentValues.put("rank", rank + "/" + totalStudent);
                                        try {
                                            db.RankUpdate("" + rank, test_name, contentValues, SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        binding.RankTextViwe.setText(rank + "/" + totalStudent);
                                    }
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                anError.printStackTrace();
                                binding.pbar.setVisibility(View.INVISIBLE);
                                binding.Reload.setVisibility(View.VISIBLE);
                            }
                        });
            }
        }, 3000);
    }

    @Override
    public void onResume() {
        super.onResume();
        String theme = SharePrefrence.getInstance(getApplicationContext()).getString("Themes");
        switch (theme) {
            case "night":
                binding.progress4.setColorText(1);
                binding.progress2.setColorText(1);
                break;
            case "sepia":
                binding.progress4.setColorText(0);
                binding.progress2.setColorText(0);
                break;
            default:
                binding.progress4.setColorText(0);
                binding.progress2.setColorText(0);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void ParseData(String response) {
        if (testitem != null && testitem.size() > 0)
            testitem.clear();
        try {
            JSONObject obj = new JSONObject(response);
            int success = obj.getInt("status");
            correct_mark = obj.getString("right_mark");
            wrong_mark = obj.getString("rowsqsmar");
            total_question = obj.getString("total_question");
            if (success == 1) {
                JSONArray array = obj.getJSONArray("response");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsobj = array.getJSONObject(i);
                    item = new FreeTestItem();
                    if (jsobj.has("id"))
                        item.setId(jsobj.getString("id"));
                    else
                        item.setId("");
                    item.setDirection(jsobj.getString("direction"));
                    item.setQuestionenglish(jsobj.getString("question"));
                    item.setOpt_en_1(jsobj.getString("optiona"));
                    item.setOp_en_2(jsobj.getString("optionb"));
                    item.setOp_en_3(jsobj.getString("optionc"));
                    item.setOp_en_4(jsobj.getString("optiond"));
                    if (jsobj.has("optione"))
                        item.setOp_en_5(jsobj.getString("optione"));
                    else
                        item.setOp_en_5("");
                    item.setAnswer(jsobj.getString("correct_answer"));
                    item.setSolutionenglish(jsobj.getString("explaination"));
                    item.setNoofoption(jsobj.getString("noofoption"));
                    item.setTime(obj.getString("total_time"));
                    item.setTotalquestio(total_question);
                    item.setCorrectmarks(correct_mark);
                    item.setWrongmarks(wrong_mark);
                    item.setNo_of_attempt(obj.getString("attemp"));
                    binding.totalquestion.setText(total_question + " Questions");
                    testitem.add(item);
                }
            } else if (success == 0) {
                Toast.makeText(getApplicationContext(), "Internal error ocured", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            try {
                runOnUiThread(new Runnable() {
                    @SuppressLint({"SetTextI18n", "DefaultLocale"})
                    @Override
                    public void run() {
                        binding.RankTextViwe.setText("updating...");
                        String wrongM = data.getStringExtra("wrongM");
                        String correctM = data.getStringExtra("correctM");
                        String correctanswerstr = data.getStringExtra("correct");
                        String incorrect_marks = data.getStringExtra("incorrect");
                        String skip_que = data.getStringExtra("skipanswer");
                        binding.correctAttamp.setText(" " + data.getStringExtra("correct"));
                        binding.incorrectAttamp.setText(" " + data.getStringExtra("incorrect"));
                        binding.skippedQue.setText(" " + data.getStringExtra("skipanswer"));
                        if (data.getStringExtra("acc").contains("-")) {
                            binding.progress4.setText("00 %");
                            binding.progress4.setProgressValue(0);
                            binding.progress4.setProgressValue2(100);
                            wbTotal_question = Integer.parseInt(correctanswerstr) + Integer.parseInt(incorrect_marks) + Integer.parseInt(skip_que);
                            totalMarks = String.valueOf(wbTotal_question);
                            binding.progress2.setText("00 /" + wbTotal_question);
                            binding.progress2.setProgressValue(0);
                            binding.progress2.setProgressValue2(100);
                        } else {
                            if (wrongM.length() > 0 && correctM.length() > 0 /*|| time.length() > 0*/) {
                                float CorrectMark = Float.parseFloat(correctanswerstr) * Float.parseFloat(correctM);
                                float INCorrectMark = Float.parseFloat(incorrect_marks) * Float.parseFloat(wrongM);
                                float wbTotal_question = Float.parseFloat(correctanswerstr) + Float.parseFloat(incorrect_marks) + Integer.parseInt(skip_que);
                                float totalNumber = wbTotal_question * Float.parseFloat(correctM);
                                float GetNumber = CorrectMark - INCorrectMark;
                                String srGetNumber = String.valueOf(GetNumber);
                                String srTotalNumber = String.valueOf(totalNumber);
                                if (srGetNumber.contains("-"))
                                    srGetNumber = "00";
                                Log.e("Get Marks", "" + srGetNumber);
                                Log.e("Get Marks", "" + srTotalNumber);
                                String srTotalNur[] = srTotalNumber.split("\\.");
                                binding.progress2.setText(srGetNumber + "/" + srTotalNur[0]);
                                String PbValue = String.valueOf((GetNumber * 100) / totalNumber);
                                totalMarks = srTotalNur[0];
                                String sarray[] = PbValue.split("//.");
                                float f1 = Float.parseFloat(sarray[0]);
                                if (f1 < 0 || f1 > 100)
                                    f1 = 0;
                                binding.progress2.setProgressValue(Math.round((int) f1));
                                binding.progress2.setProgressValue2(100);
                                String[] arrAcr = data.getStringExtra("acc").split("\\.");
                                binding.progress4.setText(String.format("%.2f", Float.parseFloat(data.getStringExtra("acc"))) + "%");
                                binding.progress4.setProgressValue(Integer.parseInt(arrAcr[0]));
                                binding.progress4.setProgressValue2(100);
                            } else {
                                wbTotal_question = Integer.parseInt(correctanswerstr) + Integer.parseInt(incorrect_marks) + Integer.parseInt(skip_que);
                                binding.accuracyResult.setText("" + data.getStringExtra("acc") + "%");
                                String[] arrAcr = data.getStringExtra("acc").split("\\.");
                                binding.progress4.setText(arrAcr[0] + "%");
                                binding.progress4.setProgressValue(Integer.parseInt(arrAcr[0]));
                                binding.progress4.setProgressValue2(100);
                                String PbValue = String.valueOf((Integer.parseInt(correctanswerstr) * 100) / wbTotal_question);
                                String sarray[] = PbValue.split("//.");
                                binding.progress2.setText(data.getStringExtra("correct") + "/" + wbTotal_question);
                                binding.progress2.setProgressValue(Integer.parseInt(sarray[0]));
                                binding.progress2.setProgressValue2(100);
                            }
                        }
                        binding.startQuiz.setVisibility(View.GONE);
                        binding.QuizTitle.setVisibility(View.GONE);
                        binding.quizDate.setVisibility(View.GONE);
                        binding.totalquestion.setVisibility(View.GONE);
                        binding.resetQuiz.setVisibility(View.VISIBLE);
                        binding.ViewQuiz.setVisibility(View.VISIBLE);
                        binding.layout2.setVisibility(View.VISIBLE);
                        binding.rankLayout.setVisibility(View.VISIBLE);
                        binding.quizData.setVisibility(View.VISIBLE);
                        float[] data = {Float.parseFloat(correctanswerstr),
                                Float.parseFloat(incorrect_marks), Float.parseFloat(skip_que)};
                        binding.pieChart.setData(data);
                        if (Utils.hasConnection(getApplicationContext()))
                            getRank(correctanswerstr);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}