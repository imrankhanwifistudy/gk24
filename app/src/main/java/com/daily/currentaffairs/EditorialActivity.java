package com.daily.currentaffairs;

import android.annotation.SuppressLint;
import android.content.Intent;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import com.daily.currentaffairs.databinding.EditorialActivityBinding;
import java.util.ArrayList;
import Adapter.ParagraphPagerAdapter;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;

public class EditorialActivity extends AppCompatActivity {
    private static final String TAG =EditorialActivity.class.getSimpleName() ;
    @SuppressLint("StaticFieldLeak")
    public static ParagraphPagerAdapter adapter;
    DatabaseHandler db;
    public static int quiz_attemp=0;
    int position = 0 , date_position=0;
    public static boolean lang_flag=true;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        EditorialActivityBinding binding= DataBindingUtil.setContentView(this,R.layout.editorial_activity);
        db = new DatabaseHandler(getApplicationContext());
        EditorialActivity.lang_flag = !SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE).equalsIgnoreCase("English");
        Intent intent = getIntent();
        final ArrayList<DateItem> arrayList = (ArrayList<DateItem>) intent.getSerializableExtra("array");
        if (intent.hasExtra("position")) {
            position = intent.getIntExtra("position", 0);
            date_position = intent.getIntExtra("date_position", 0);
        }
        adapter = new ParagraphPagerAdapter(EditorialActivity.this, arrayList, binding.editorialviewpager, position, date_position);
        binding.editorialviewpager.setAdapter(adapter);
        binding.editorialviewpager.setCurrentItem(position);
        binding.editorialviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                String webDate = arrayList.get(position).getDate();
                final String uniqid = webDate + arrayList.get(position).getId() + "ParaRead"+ SharePrefrence.getInstance(getApplicationContext()).getString(Utills.DEFAULT_LANGUAGE);
                Log.e("Editorial ID ", "" + uniqid);
                String value = db.getReadUnread(uniqid);
                if (value.equals("")) {
                    try {
                        db.addreadUnread(uniqid, "paragraphRead");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            MainActivity.adapter2.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}