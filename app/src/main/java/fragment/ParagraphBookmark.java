package fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.FragParagraphBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import Adapter.BookParaPagerAdapter;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.BookParaItem;

public class ParagraphBookmark extends Fragment {
    public static ArrayList<BookParaItem> arrDateItem;
    DatabaseHandler db;
    String user_id;
    SharedPreferences sharedPreferences;
    FragParagraphBinding binding;
    BookParaPagerAdapter adapterr;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String theme= SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding= DataBindingUtil.inflate(inflater,R.layout.frag_paragraph,container,false);
        binding.bookmarkparagraphpager.setClipToPadding(false);
        ((BookmarkActivity)getActivity()).setFragmentRefreshListener2(new BookmarkActivity.FragmentRefreshListener2() {
            @Override
            public void onRefresh() {
                bookmark_sync();
            }
        });
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                bookmark_sync();
                binding.swipeContainer.setRefreshing(false);
            }
        });
        binding.swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        user_id=sharedPreferences.getString("uid","");
        arrDateItem = new ArrayList<>();
        db = new DatabaseHandler(getActivity());
        try {
            arrDateItem= db.getBookPara();
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.bookmarkparagraphpager.setVisibility(View.VISIBLE);
        if(arrDateItem.size()>0){
            adapterr = new BookParaPagerAdapter(getActivity(), arrDateItem , binding.bookmarkparagraphpager);
            binding.bookmarkparagraphpager.setAdapter(adapterr);
        } else if(Utils.hasConnection(getActivity())){
            binding.swipeContainer.setRefreshing(true);
            binding.swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    binding.swipeContainer.setRefreshing(true);
                    bookmark_sync();
                }
            });
        }else{
            binding.bookmarkparagraphpager.setVisibility(View.GONE);
            binding.noDataAvailable.setVisibility(View.VISIBLE);
        }
        binding.bookmarkparagraphpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE );
            }
        });
        return binding.getRoot();
    }
    private void enableDisableSwipeRefresh(boolean enable) {
        if ( binding.swipeContainer != null) {
            binding.swipeContainer.setEnabled(enable);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    private void bookmark_sync() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK+"Getbookmarked.php")
                .addBodyParameter("uid",user_id )
                .addBodyParameter("type_id","0")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning",String.valueOf(response));
                        if(dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                        binding.swipeContainer.setRefreshing(false);
                        try {
                            JSONObject res=new JSONObject(String.valueOf(response));
                            String err=res.getString("err");
                            if(err.equals("1")){
                                JSONArray data=res.getJSONArray("response");
                                for(int i=0 ; i< data.length() ; i++){
                                    JSONObject jsonObject=data.getJSONObject(i);
                                    final String uniqid = jsonObject.optString("active_date") + jsonObject.optString("id");
                                    try {
                                        boolean isBookMark = db.checkBookPara(uniqid);
                                        if(!isBookMark){
                                            Log.d("uniqid",uniqid);
                                            db.addParaBook(jsonObject.getString("title"), String.valueOf(jsonObject),
                                                    jsonObject.optString("image"),
                                                    uniqid, jsonObject.optString("title"),jsonObject.optString("id"));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                binding.bookmarkparagraphpager.setVisibility(View.VISIBLE);
                                binding.noDataAvailable.setVisibility(View.GONE);
                                try {
                                    arrDateItem= db.getBookPara();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                adapterr = new BookParaPagerAdapter(getActivity(), arrDateItem , binding.bookmarkparagraphpager);
                                binding.bookmarkparagraphpager.setAdapter(adapterr);

                            }else{
                                binding.bookmarkparagraphpager.setVisibility(View.GONE);
                                binding.noDataAvailable.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                    }
                });
    }
}