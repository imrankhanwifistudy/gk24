package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.CategoriesActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.LearnViewBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapter.LearnAdapter;
import ClickListener.ItemClickSupport;
import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import Modal.GkTitleModel;

public class LearnFragment extends Fragment {
    LearnAdapter adapter;
    ArrayList<GkTitleModel> arrTitles;
    ArrayList<Integer> arrImages;
    ArrayList<String> arrColor;
    int width;
    LearnViewBinding binding;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.learn_view, container, false);
        binding.progressBar2.setVisibility(View.GONE);
        arrImages = new ArrayList<>();
        arrColor = new ArrayList<>();
        addData();
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        width = (display.getWidth());
        int height = (display.getHeight());
        ViewGroup.LayoutParams params = binding.serviceList.getLayoutParams();
        params.height = height - 60;
        binding.serviceList.setLayoutParams(params);
        if (SharePrefrence.getInstance(getActivity()).HasString(Utills.GK)) {
            try {
                ParseJson(SharePrefrence.getInstance(getActivity()).getString(Utills.GK));
            } catch (Exception e) {
                e.printStackTrace();
            }
            binding.progressBar2.setVisibility(View.GONE);
        } else {
            if (Utils.hasConnection(getActivity())) {
                GetCategory();
            } else {
                binding.internetUna.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "Check your Intenet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        ItemClickSupport.addTo(binding.serviceList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (position != 7) {
                    Intent intent = new Intent(getActivity(), CategoriesActivity.class);
                    String CatID = arrTitles.get(position).getCatId();
                    String Title = arrTitles.get(position).getTitle();
                    intent.putExtra("CatID", CatID);
                    intent.putExtra("Title", Title);
                    intent.putExtra("position", position);
                    startActivity(intent);
                }
            }
        });
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utils.hasConnection(getActivity())) {
                    GetCategory();
                } else {
                    binding.internetUna.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "Check your Intenet Connection", Toast.LENGTH_SHORT).show();
                }
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
        binding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        binding.swipeRefreshLayout.setDistanceToTriggerSync(200);
        return binding.getRoot();
    }

    private void addData() {
        arrImages.add(R.drawable.economics);
        arrImages.add(R.drawable.polity);
        arrImages.add(R.drawable.history);
        arrImages.add(R.drawable.gk);
        arrImages.add(R.drawable.science);
        arrImages.add(R.drawable.geography);
        arrImages.add(R.drawable.practisetest);
        arrImages.add(R.drawable.state_wise);
        arrColor.add("#f8a942");
        arrColor.add("#50adf3");
        arrColor.add("#ee6c78");
        arrColor.add("#1bbe9f");
        arrColor.add("#5599c8");
        arrColor.add("#9e62b8");
        arrColor.add("#5558d9");
        arrColor.add("#1eb0bc");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void GetCategory() {
        AndroidNetworking.post(Utills.BASE_URLGK + "categories.php")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SharePrefrence.getInstance(getActivity()).putString(Utills.GK, String.valueOf(response));
                        try {
                            ParseJson(String.valueOf(response));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        binding.progressBar2.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        binding.progressBar2.setVisibility(View.GONE);
                    }
                });
    }

    private void ParseJson(String response) throws Exception {
        binding.progressBar2.setVisibility(View.GONE);
        arrTitles = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("response");
        for (int i = 0; i < jsonArray.length(); i++) {
            GkTitleModel item = new GkTitleModel();
            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
            item.setTitle(jsonObject1.getString("title"));
            item.setCatId(jsonObject1.getString("catid"));
            item.setJsonArray(jsonObject1.getJSONArray("subcat"));
            arrTitles.add(item);
        }


        adapter = new LearnAdapter(getActivity(), arrTitles, arrImages, arrColor);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        binding.serviceList.setHasFixedSize(true);
        binding.serviceList.setLayoutManager(manager);
        binding.serviceList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}