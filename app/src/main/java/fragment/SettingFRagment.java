package fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.GkActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.FragmentSettingFragmentBinding;

import ClickListener.SettingClickListner;
import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;

public class SettingFRagment extends Fragment {
    SharedPreferences sharedPreferences;
    FragmentSettingFragmentBinding binding;
    String theme2;
    Drawable drwa, drawimage, drawQues, drawFeed, drawShare, drawRate;

    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        drwa = Utils.DrawableChange(getActivity(), R.drawable.profile_tab_update_app, "#ffffff");
        drawimage = Utils.DrawableChange(getActivity(), R.drawable.ic_image, "#ffffff");
        drawQues = Utils.DrawableChange(getActivity(), R.drawable.profile_help_white, "#ffffff");
        drawFeed = Utils.DrawableChange(getActivity(), R.drawable.ic_feedback_settings, "#ffffff");
        drawShare = Utils.DrawableChange(getActivity(), R.drawable.ic_share_settings, "#ffffff");
        drawRate = Utils.DrawableChange(getActivity(), R.drawable.ic_rateus_settings, "#ffffff");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                drwa = Utils.DrawableChange(getActivity(), R.drawable.profile_tab_update_app, "#000000");
                drawimage = Utils.DrawableChange(getActivity(), R.drawable.ic_image, "#000000");
                drawQues = Utils.DrawableChange(getActivity(), R.drawable.profile_help_white, "#000000");
                drawFeed = Utils.DrawableChange(getActivity(), R.drawable.ic_feedback_settings, "#000000");
                drawShare = Utils.DrawableChange(getActivity(), R.drawable.ic_share_settings, "#000000");
                drawRate = Utils.DrawableChange(getActivity(), R.drawable.ic_rateus_settings, "#000000");
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting_fragment, container, false);
        binding.updateApp.setCompoundDrawablesWithIntrinsicBounds(drwa, null, null, null);
        binding.ivImage.setCompoundDrawablesWithIntrinsicBounds(drawimage, null, null, null);
        binding.feedback.setCompoundDrawablesWithIntrinsicBounds(drawFeed, null, null, null);
        binding.share.setCompoundDrawablesWithIntrinsicBounds(drawShare, null, null, null);
        binding.rateus.setCompoundDrawablesWithIntrinsicBounds(drawRate, null, null, null);
        binding.ivImageSwitch.setImageDrawable(drawQues);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SetUI();
        binding.setClick(new SettingClickListner(getActivity(), binding));

        binding.setFragment(this);
        return binding.getRoot();
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.watch_video:
                Intent intent = new Intent(getActivity(), GkActivity.class);
                intent.putExtra("CatID", "catID");
                intent.putExtra("SubCatID", "");
                intent.putExtra("Title", "Gk Videos");
                intent.putExtra("position", -1);
                intent.putExtra("fromScreen", SettingFRagment.class.getSimpleName());
                startActivity(intent);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void SetUI() {
        SpannableString span = new SpannableString("Powered by Vocab24.com ");
        span.setSpan(new RelativeSizeSpan(0.80f), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.powered.setText(span);
        if (sharedPreferences.contains("LoginStatus") && sharedPreferences.getString("LoginStatus", "").equals("Success")) {
            binding.LoginSignUp.setText(sharedPreferences.getString("email", ""));
        } else {
            binding.LoginSignUp.setText("Signin/SignUp");
        }
        boolean swImageUS = SharePrefrence.getInstance(getActivity()).getBoolean(Utills.AUTO_IMAGE_DEFAULT);
        boolean swImage1 = SharePrefrence.getInstance(getActivity()).getBoolean(Utills.AUTO_IMAGE_SMALLER);
        if (swImageUS) {
            binding.fontSetting.setText("Default");
        } else if (swImage1) {
            binding.fontSetting.setText("Small");
        } else {
            binding.fontSetting.setText("Large");
        }
        boolean swImage = SharePrefrence.getInstance(getActivity()).getBoolean(Utills.AUTO_IMAGE_SWITCH);
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        if (swImage) {
            binding.imageswitch.setChecked(true);
            if (Build.VERSION.SDK_INT > 19) {
                binding.imageswitch.setTrackResource(R.drawable.switch_track);
                if (theme2.equals("night")) {
                    binding.imageswitch.setThumbResource(R.drawable.ic_dark_hd_toggle_on);
                    binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_black);
                } else {
                    binding.imageswitch.setThumbResource(R.drawable.ic_hd_on);
                    binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_blue);
                }
            } else {
                binding.backSwitch.setVisibility(View.GONE);
            }
        } else {
            binding.imageswitch.setChecked(false);
            if (Build.VERSION.SDK_INT > 19) {
                binding.imageswitch.setTrackResource(R.drawable.switch_track);
                if (theme2.equals("night")) {
                    binding.imageswitch.setThumbResource(R.drawable.ic_dark_hd_toggle_off);
                    binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_light_black);
                } else {
                    binding.imageswitch.setThumbResource(R.drawable.ic_hd_off);
                    binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_white);
                }
            } else {
                binding.backSwitch.setVisibility(View.GONE);
            }
        }
        binding.imageswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
                if (b) {
                    SharePrefrence.getInstance(getActivity()).putBoolean(Utills.AUTO_IMAGE_SWITCH, true);
                    if (Build.VERSION.SDK_INT > 19) {
                        binding.imageswitch.setTrackResource(R.drawable.switch_track);
                        if (theme2.equals("night")) {
                            binding.imageswitch.setThumbResource(R.drawable.ic_dark_hd_toggle_on);
                            binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_black);
                        } else {
                            binding.imageswitch.setThumbResource(R.drawable.ic_hd_on);
                            binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_blue);
                        }
                    } else {
                        binding.backSwitch.setVisibility(View.GONE);
                    }
                } else {
                    SharePrefrence.getInstance(getActivity()).putBoolean(Utills.AUTO_IMAGE_SWITCH, false);
                    if (Build.VERSION.SDK_INT > 19) {
                        binding.imageswitch.setTrackResource(R.drawable.switch_track);
                        if (theme2.equals("night")) {
                            binding.imageswitch.setThumbResource(R.drawable.ic_dark_hd_toggle_off);
                            binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_light_black);
                        } else {
                            binding.imageswitch.setThumbResource(R.drawable.ic_hd_off);
                            binding.backSwitch.setBackgroundResource(R.drawable.switch_bg_white);
                        }
                    } else {
                        binding.backSwitch.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                binding.Themes.setText("Night");
                break;
            case "sepia":
                binding.Themes.setText("Sepia");
                break;
            default:
                binding.Themes.setText("Default");
                break;
        }
        String image = SharePrefrence.getInstance(getActivity()).getString("social_image");
        if (image.equals("")) {
            binding.logouttxt.setImageResource(R.drawable.ic_dp_settings);
        } else {
            Glide.with(getActivity()).load(image).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL);
        }
        if (sharedPreferences.contains("LoginStatus") &&
                sharedPreferences.getString("LoginStatus", "").equals("Success")) {
            binding.LoginSignUp.setText("" + sharedPreferences.getString("email", ""));
        } else {
            binding.LoginSignUp.setText("Signin/SignUp");
        }
        if (sharedPreferences != null && sharedPreferences.contains("email")) {
            String user_name = sharedPreferences.getString("name", "");
            if (user_name.length() == 0 || user_name.equals("1")) {
                user_name = sharedPreferences.getString("email", "");
            }
            if (user_name.contains("@")) {
                String[] u_name = user_name.split("@");
                binding.LoginSignName.setText(u_name[0]);
            } else {
                binding.LoginSignName.setText(user_name);
            }
        }
    }
}