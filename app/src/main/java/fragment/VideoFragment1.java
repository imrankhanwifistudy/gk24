package fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.FragmentVideoBinding;
import com.daily.currentaffairs.databinding.VideoLayoutBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapter.MyRecyclerVideoAdapter;
import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DataObject;
import Modal.QuizModel;

@SuppressLint("Registered")
public class VideoFragment1 extends Fragment {
    ArrayList<QuizModel> exam_list = new ArrayList<>();
    ExamAdapter examAdapter;
    FragmentVideoBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false);
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        if (Utils.hasConnection(getActivity())) {
            api();
        } else {
            String res = SharePrefrence.getInstance(getActivity()).getString("Video_cat");
            if (res.length() > 0)
                parseVideo_cat(res);
        }
        return binding.getRoot();
    }

    private void api() {
        exam_list = new ArrayList<>();
        AndroidNetworking.post(Utills.BASE_URLGK + "video_type.php")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("CAPSULE_RESPONSE", String.valueOf(response));
                        try {
                            JSONObject object = new JSONObject(String.valueOf(response));
                            int status = object.getInt("status");
                            if (status == 1) {
                                SharePrefrence.getInstance(getActivity()).putString("Video_cat", String.valueOf(response));
                                parseVideo_cat(String.valueOf(response));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    void parseVideo_cat(String response) {
        try {
            JSONObject object = new JSONObject(response);
            int status = object.getInt("status");
            if (status == 1) {
                JSONArray res = object.getJSONArray("response");
                for (int i = 0; i < res.length(); i++) {
                    QuizModel item = new QuizModel();
                    JSONObject OBJ = res.getJSONObject(i);
                    item.setId(OBJ.getString("catid"));
                    item.setTitle(OBJ.getString("title"));
                    exam_list.add(item);
                }
                examAdapter = new ExamAdapter(getActivity(), exam_list, binding.topicwiseviewpager);
                binding.topicwiseviewpager.setAdapter(examAdapter);
                binding.videotabLayout.setupWithViewPager(binding.topicwiseviewpager);
                binding.topicwiseviewpager.setOffscreenPageLimit(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ExamAdapter extends PagerAdapter {
        private Activity activity;
        ArrayList<QuizModel> exam_list;
        ViewPager viewpager;
        ArrayList<DataObject> list;
        int page = 0;
        MyRecyclerVideoAdapter adapter;
        LinearLayoutManager layoutManager1;

        ExamAdapter(Activity activity, ArrayList<QuizModel> exam_list, ViewPager viewpager) {
            this.activity = activity;
            this.exam_list = exam_list;
            this.viewpager = viewpager;
        }

        @Override
        public int getCount() {
            return exam_list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return exam_list.get(position).getTitle();
        }

        @Override
        public Object instantiateItem(View collection, final int position) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
            final VideoLayoutBinding Videobinding = DataBindingUtil.inflate(inflater, R.layout.video_layout, null, false);
            list = new ArrayList<>();
            switch (theme2) {
                case "night":
                    activity.setTheme(R.style.night);
                    break;
                case "sepia":
                    activity.setTheme(R.style.sepia);
                    break;
                default:
                    activity.setTheme(R.style.defaultt);
                    break;
            }
            layoutManager1 = new LinearLayoutManager(activity);
            Videobinding.livetestRecyclerView.setLayoutManager(layoutManager1);
            Videobinding.livetestRecyclerView.setHasFixedSize(true);
            adapter = new MyRecyclerVideoAdapter(activity, list, "", Videobinding.noTest);
            Videobinding.livetestRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (Utils.hasConnection(activity)) {
                dataparse(list, position, Videobinding, page, adapter);
            } else {
                String res = SharePrefrence.getInstance(activity).getString(exam_list.get(position).getId() + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (res.length() > 0) {
                    parseJson(res, list, adapter, Videobinding);
                } else {
                    Toast.makeText(activity, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
                    Videobinding.progressView.setVisibility(View.GONE);
                }
            }
            Videobinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    list = new ArrayList<>();
                    page = 0;
                    if (Utils.hasConnection(activity)) {
                        dataparse(list, viewpager.getCurrentItem(), Videobinding, page, adapter);
                    } else {
                        String res = SharePrefrence.getInstance(activity).getString(exam_list.get(viewpager.getCurrentItem()).getId() + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                        if (res.length() > 0) {
                            parseJson(res, list, adapter, Videobinding);
                        } else {
                            Toast.makeText(activity, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            Videobinding.progressView.setVisibility(View.GONE);
                        }
                    }
                    Videobinding.swipeRefreshLayout.setRefreshing(false);
                }
            });
            Videobinding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
            viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    list = new ArrayList<>();
                    page = 0;
                    if (Utils.hasConnection(activity)) {
                        dataparse(list, position, Videobinding, page, adapter);
                    } else {
                        String res = SharePrefrence.getInstance(activity).getString(exam_list.get(position).getId() + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                        if (res.length() > 0) {
                            parseJson(res, list, adapter, Videobinding);
                        } else {
                            Toast.makeText(activity, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            Videobinding.progressView.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            ((ViewPager) collection).addView(Videobinding.getRoot());
            return Videobinding.getRoot();
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        private void Get_Catagery(final ArrayList<DataObject> list, final String id, final int page, final MyRecyclerVideoAdapter adapter, final VideoLayoutBinding Videobinding) {
            // TODO Auto-generated method stub
            Videobinding.progressView.setVisibility(View.VISIBLE);
            Videobinding.swipeRefreshLayout.setRefreshing(false);
            Log.e("Url", Utills.BASE_URLGK + "video_new.php?lang=" + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + "&type=" + id + "&page=" + page);
            AndroidNetworking.post(Utills.BASE_URLGK + "video_new.php")
                    .addBodyParameter("lang", "" + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE))
                    .addBodyParameter("type", "" + id)
                    .addBodyParameter("page", "" + page)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Meaning", String.valueOf(response));
                            Videobinding.progressView.setVisibility(View.GONE);
                            try {
                                JSONObject obj = new JSONObject(String.valueOf(response));
                                int status = Integer.parseInt(obj.getString("status"));
                                if (status == 1) {
                                    Videobinding.noTest.setVisibility(View.GONE);
                                    parseJson(String.valueOf(response), list, adapter, Videobinding);
                                    SharePrefrence.getInstance(activity).putString(id + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE), String.valueOf(response));
                                } else {
                                    if (page == 0) {
                                        Videobinding.noTest.setVisibility(View.VISIBLE);
                                    }
                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.printStackTrace();
                            Videobinding.progressView.setVisibility(View.GONE);
                        }
                    });
        }

        private void parseJson(String reponce, final ArrayList<DataObject> list, final MyRecyclerVideoAdapter adapter, final VideoLayoutBinding Videobinding) {
            try {
                Videobinding.swipeRefreshLayout.setRefreshing(false);
                Videobinding.progressView.setVisibility(View.GONE);
                JSONObject obj = new JSONObject(reponce);
                JSONArray array = obj.getJSONArray("response");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsobj = array.getJSONObject(i);
                    DataObject item = new DataObject();
                    item.setName(jsobj.getString("title"));
                    item.setDuration(jsobj.getString("time"));
                    item.setUrl(jsobj.getString("video_url"));
                    item.setImageUrl(jsobj.getString("image"));
                    item.setDate(jsobj.getString("created_date"));
                    item.setId(jsobj.getString("id"));
                    item.setType(jsobj.getString("type"));
                    item.setPpt_url(jsobj.getString("ppt_url"));
                    item.setPackageName(String.valueOf(jsobj));
                    item.setStatuss(-1);
                    item.setEncode_url(jsobj.optString("encode_url"));
                    list.add(item);
                }
                adapter.notifyDataSetChanged();
                Videobinding.livetestRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        //      if (layoutManager1.findLastVisibleItemPosition() == adapter.getItemCount() - 1) {
                        String res1 = SharePrefrence.getInstance(activity).getString(exam_list.get(viewpager.getCurrentItem()).getId() + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                        Log.e("page", "" + page + " , " + res1);
                        if (res1.length() > 0) {
                            page = page + 1;
                            Log.e("page", "" + page);
                            Videobinding.progressView.setVisibility(View.VISIBLE);
                            String res = SharePrefrence.getInstance(activity).getString(exam_list.get(viewpager.getCurrentItem()).getId() + page + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                            if (res.length() > 0) {
                                parseJson(res, list, adapter, Videobinding);
                            } else {
                                dataparse(list, viewpager.getCurrentItem(), Videobinding, page, adapter);
                            }
                        }
                        //      }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        recyclerView.setVerticalScrollbarPosition(page * 15);

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        void dataparse(ArrayList<DataObject> list, int position, VideoLayoutBinding Videobinding, int page, MyRecyclerVideoAdapter adapter) {
            Get_Catagery(list, exam_list.get(position).getId(), page, adapter, Videobinding);
            Videobinding.internetUna.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}