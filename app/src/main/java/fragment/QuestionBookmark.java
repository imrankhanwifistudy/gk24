package fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ActivityQuestionQuizBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import Adapter.BookmarkQuizAdapter;
import ClickListener.QuestionClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

public class QuestionBookmark extends Fragment {
    private static final String TAG = QuestionBookmark.class.getSimpleName();
    BookmarkQuizAdapter AdapterEnglish;
    ArrayList<FreeTestItem> testitem;
    String  user_id,theme2;
    SharedPreferences sharedPreferences;
    DatabaseHandler db;
    ActivityQuestionQuizBinding binding;
    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        user_id = sharedPreferences.getString("uid", "");
        theme2=SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding= DataBindingUtil.inflate(inflater,R.layout.activity_question_quiz,container,false);
        binding.timer.setVisibility(View.GONE);
        binding.backimage.setVisibility(View.GONE);
        binding.icBookmark.setVisibility(View.GONE);
        binding.icShareQuiz.setVisibility(View.GONE);
        binding.pb.setVisibility(View.GONE);
        testitem = new ArrayList<>();
        db = new DatabaseHandler(getActivity());
        binding.icReportQuiz.setVisibility(View.VISIBLE);
        binding.icReportQuiz.setImageResource(R.drawable.ic_more_vert_radish_24dp);
        ((BookmarkActivity)getActivity()).setFragmentRefreshListener(new BookmarkActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                getbookmark();
            }
        });
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getbookmark();
                binding.swipeContainer.setRefreshing(false);
            }
        });
        binding.swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        binding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE );
            }
        });
        return binding.getRoot();
    }
    private void enableDisableSwipeRefresh(boolean enable) {
        if (binding.swipeContainer != null) {
            binding.swipeContainer.setEnabled(enable);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        binding.tvBookmark.setVisibility(View.GONE);
        user_id = sharedPreferences.getString("uid", "");
        testitem = new ArrayList<>();
        if (testitem.size() > 0) {
            testitem.clear();
        }
        try {
            testitem = db.getQuizWord();
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.rlQuestion.setVisibility(View.VISIBLE);
        if (testitem.size() > 0) {
            binding.rlQuestion.setVisibility(View.VISIBLE);
            AdapterEnglish = new BookmarkQuizAdapter(getActivity(), testitem, binding.pager,  "uniqBookmarks2016",binding.tvQuestionNo);
            binding.pager.setAdapter(AdapterEnglish);
            AdapterEnglish.notifyDataSetChanged();
            binding.setClick(new QuestionClickListner(getActivity(),binding.pager,binding,testitem,AdapterEnglish));
        }else if(Utils.hasConnection(getActivity())){
            binding.swipeContainer.setRefreshing(true);
            binding.swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    binding.swipeContainer.setRefreshing(true);
                    getbookmark();
                }
            });
        }else{
            binding.tvBookmark.setVisibility(View.VISIBLE);
            binding.rlQuestion.setVisibility(View.GONE);
        }
    }
    private void getbookmark() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        testitem=new ArrayList<>();
        AndroidNetworking.post(Utills.BASE_URLGK +"Getbookmarked.php")
                .addBodyParameter("uid",user_id )
                .addBodyParameter("type_id","1")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning",String.valueOf(response));
                        if(dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                        binding.swipeContainer.setRefreshing(false);
                        Log.e("word Vocab2", String.valueOf(response));
                        try {
                            JSONObject res=new JSONObject(String.valueOf(response));
                            String err=res.getString("err");
                            if(err.equals("1")){
                                binding.rlQuestion.setVisibility(View.VISIBLE);
                                JSONArray data=res.getJSONArray("response");
                                for(int i=0 ; i< data.length() ; i++){
                                    JSONObject jsonObject=data.getJSONObject(i);
                                    String date=jsonObject.getString("date");
                                    @SuppressLint("SimpleDateFormat")
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd");
                                    Date date1 = null;
                                    try {
                                        date1 = inputFormat.parse(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("Date",""+date1);
                                    @SuppressLint("SimpleDateFormat")
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    String datee = dateFormat.format(date1);
                                    final String uniqid = datee+ datee+jsonObject.getString("id");
                                    try {
                                        boolean isBookMark = db.CheckIsDataAlreadyInBookMarkQuiz(uniqid, SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                                        if(!isBookMark){
                                            db.addQuizBook(jsonObject.getString("question"), jsonObject.getString("correct_answer"),
                                                    jsonObject.getString("optiona"), jsonObject.getString("optionb"),
                                                    jsonObject.getString("optionc"), jsonObject.getString("optiond"),
                                                    jsonObject.getString("optione"), jsonObject.getString("explaination"),
                                                    jsonObject.getString("direction"), uniqid ,
                                                    SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE),
                                                    jsonObject.getString("id"));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                binding.rlQuestion.setVisibility(View.VISIBLE);
                                try {
                                    testitem = db.getQuizWord();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                String totalques=String.valueOf(testitem.size());
                                binding.pager.setOffscreenPageLimit(Integer.parseInt(totalques));
                                AdapterEnglish = new BookmarkQuizAdapter(getActivity(), testitem, binding.pager, "uniqBookmarks2016", binding.tvQuestionNo); binding.pager.setAdapter(AdapterEnglish);
                                AdapterEnglish.notifyDataSetChanged();
                                binding.setClick(new QuestionClickListner(getActivity(),binding.pager,binding,testitem,AdapterEnglish));
                            }else{
                                binding.tvBookmark.setVisibility(View.VISIBLE);
                                binding.rlQuestion.setVisibility(View.GONE);
                            }
                            AdapterEnglish.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                    }
                });
    }
}