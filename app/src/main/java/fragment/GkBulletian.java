package fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.GkNotesActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.GkTestBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapter.TestAdapter;
import Custom.Utils;
import DB.SharePrefrence;
import DB.Utills;
import Modal.QuizModel;

import static ui.AppController.sharedPreferences;

public class GkBulletian extends Fragment {
    ArrayList<QuizModel> feeditem;
    Adapter.TestAdapter TestAdapter;
    int InitialValue = 1;
    boolean isend = false;
    String CatID, SubCatID;
    String lastid = "", last_id_api = "";
    ArrayList<String> array_Id;
    GkTestBinding binding;
    String theme2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.gk_test, container, false);
        feeditem = new ArrayList<>();
        array_Id = new ArrayList<>();
        Bundle bundle = getArguments();
        CatID = bundle.getString("CatID");
        SubCatID = bundle.getString("SubCatID");
        Log.e("Notes Ids:----", "" + CatID + " , " + SubCatID);
        if (feeditem != null) {
            feeditem.clear();
        }
        String gk_data = SharePrefrence.getInstance(getActivity()).getString(SharePrefrence.getInstance(getActivity()).getString("gk_activity_name") + "Bulletian" + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
        if (gk_data.length() > 0) {
            parasData(gk_data, 0);
            binding.progressView.setVisibility(View.GONE);
        }
        if (Utils.hasConnection(getActivity())) {
            get_Detail(CatID, SubCatID, "");
        } else {
            Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
            binding.progressView.setVisibility(View.GONE);
            binding.noTest.setVisibility(View.VISIBLE);
            isend = true;
            InitialValue = 1;
        }
        TestAdapter = new TestAdapter(getActivity(), feeditem);
        binding.quizList.setAdapter(TestAdapter);
        binding.quizList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getActivity(), GkNotesActivity.class);
                intent.putExtra("array", feeditem);
                intent.putExtra("position", position);
                getActivity().startActivity(intent);
            }
        });
        binding.quizList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.e("api", "" + firstVisibleItem + " , " + visibleItemCount + " , " + totalItemCount);
                if (firstVisibleItem == totalItemCount - visibleItemCount && totalItemCount > 0) {
                    if (!last_id_api.equalsIgnoreCase(lastid)) {
                        binding.progressView.setVisibility(View.VISIBLE);
                        last_id_api = lastid;
                        if (Utils.hasConnection(getActivity())) {
                            get_Detail(CatID, SubCatID, lastid);
                        } else {
                            String gk_data = SharePrefrence.getInstance(getActivity()).getString(lastid + SharePrefrence.getInstance(getActivity()).getString("gk_activity_name") + "Bulletian" + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                            if (gk_data.length() > 0) {
                                parasData(gk_data, 0);
                                binding.progressView.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                                binding.progressView.setVisibility(View.GONE);
                                binding.noTest.setVisibility(View.VISIBLE);
                                isend = true;
                                InitialValue = 1;
                            }
                        }
                    }
                }
            }
        });
        if (TestAdapter.getCount() > 0) {
            binding.noTest.setVisibility(View.GONE);
        }
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                feeditem = new ArrayList<>();
                array_Id = new ArrayList<>();
                if (Utils.hasConnection(getActivity())) {
                    get_Detail(CatID, SubCatID, "");
                } else {
                    String gk_data = SharePrefrence.getInstance(getActivity()).getString(SharePrefrence.getInstance(getActivity()).getString("gk_activity_name") + "Bulletian" + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                    if (gk_data.length() > 0) {
                        parasData(gk_data, 0);
                        binding.progressView.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                        binding.progressView.setVisibility(View.GONE);
                        binding.noTest.setVisibility(View.VISIBLE);
                        isend = true;
                        InitialValue = 1;
                    }
                }
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
        binding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        return binding.getRoot();
    }

    private void get_Detail(final String catID, final String subCatID, final String lastid) {
        Log.e("api", Utills.BASE_URLGK + "bulletin.php?uid=" + sharedPreferences.getString("uid", "") + "&catid=" + catID + "&subcatid=" + subCatID + "&lang=" + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE) + "&id=" + lastid);
        AndroidNetworking.post(Utills.BASE_URLGK + "bulletin.php")
                .addBodyParameter("uid", sharedPreferences.getString("uid", ""))
                .addBodyParameter("catid", catID)
                .addBodyParameter("subcatid", subCatID)
                .addBodyParameter("lang", SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE).toLowerCase())
                .addBodyParameter("id", lastid)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning", String.valueOf(response));
                        binding.progressView.setVisibility(View.GONE);
                        Log.e("Gk Notesresponse", String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            if (success == 1) {
                                binding.noTest.setVisibility(View.GONE);
                                binding.progressView.setVisibility(View.GONE);
                                String gk_title = SharePrefrence.getInstance(getActivity()).getString("gk_activity_name") + "Bulletian" + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE);
                                SharePrefrence.getInstance(getActivity()).putString(lastid + gk_title, String.valueOf(response));
                                if (lastid.equalsIgnoreCase("")) {
                                    parasData(String.valueOf(response), 1);
                                } else {
                                    parasData(String.valueOf(response), 0);
                                }
                            } else if (success == 0 && lastid.equalsIgnoreCase("")) {
                                binding.progressView.setVisibility(View.GONE);
                                binding.noTest.setVisibility(View.VISIBLE);
                                isend = true;
                                InitialValue = 1;
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (TestAdapter != null) {
                TestAdapter.notifyDataSetChanged();
            }
            theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
            switch (theme2) {
                case "night":
                    binding.noTest.setTextColor(Color.WHITE);
                    binding.rlMain.setBackgroundColor(Color.BLACK);
                    break;
                default:
                    binding.noTest.setTextColor(Color.BLACK);
                    binding.rlMain.setBackgroundColor(Color.WHITE);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parasData(String response, int data) {
        try {
            JSONObject obj = new JSONObject(response);
            JSONArray array = obj.getJSONArray("response");
            for (int i = 0; i < array.length(); i++) {
                QuizModel item = new QuizModel();
                JSONObject jsobj = array.getJSONObject(i);
                item.setId(jsobj.getString("id"));
                item.setHeadings(jsobj.getString("headings"));
                item.setTitle(jsobj.getString("title"));
                item.setNote(jsobj.getString("note"));
                item.setImages(jsobj.getString("images"));
                String stauts = jsobj.getString("tstatus");
                item.setStauts(stauts);
                if (i == array.length() - 1) {
                    lastid = jsobj.getString("id");
                }
                if (data == 1) {
                    Log.e("last_id", "" + jsobj.getString("id"));
                    if (!array_Id.contains(jsobj.getString("id"))) {
                        array_Id.add(jsobj.getString("id"));
                        feeditem.add(0, item);
                    }
                } else {
                    if (!array_Id.contains(jsobj.getString("id"))) {
                        array_Id.add(jsobj.getString("id"));
                        feeditem.add(item);
                    }
                }
            }
            Log.e("feeditem", "" + feeditem.size());
            TestAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}