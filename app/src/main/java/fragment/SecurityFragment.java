package fragment;

import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.SecurityFragmentBinding;

import org.json.JSONObject;

import DB.Utills;
@SuppressWarnings("ALL")
public class SecurityFragment extends Fragment {
    private static final String TAG = SecurityFragment.class.getSimpleName();
    String new_pass,comfrim;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SecurityFragmentBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.security_fragment,container,false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = sharedPreferences.edit();
        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_pass=binding.tvNew.getText().toString();
                comfrim=binding.tvComfrim.getText().toString();
                if(new_pass.length()==0)
                {
                    binding.tvNew.setError("Please enter new password");
                }else if(new_pass.length() < 6)
                {
                    binding.tvNew.setError("Password should be greater than 6");
                }else if (comfrim.length() == 0) {
                    binding.tvComfrim.setError("Please retype password");
                } else if (!new_pass.equals(comfrim)) {
                    binding.tvComfrim.setError("New and Comfirm password should be same");
                } else {
                    change_password();
                }
            }
        });
        return binding.getRoot();
    }
    private void change_password()
    {
        AndroidNetworking.post(Utills.BASE_URLGK+"change_password.php")
                .addBodyParameter("token","123456789")
                .addBodyParameter("email",sharedPreferences.getString("email", ""))
                .addBodyParameter("password",new_pass)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG,"RESPONSE" + String.valueOf(response));
                            JSONObject login=response;
                            int status=login.getInt("status");
                            String msg=login.getString("msg");
                            Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
                            binding.tvComfrim.setText("");
                            binding.tvNew.setText("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
}