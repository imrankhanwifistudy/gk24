package fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.DateDialogBinding;
import com.daily.currentaffairs.databinding.ParagraphBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import Adapter.ParagraphAdapter;
import Custom.HorizontalViewPager;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Interface.DatePosition;
import Interface.OnNewsItemSelectedListener;
import Modal.DateItemNew;

public class Paragraph extends Fragment implements OnNewsItemSelectedListener, DatePosition {
    @SuppressLint("StaticFieldLeak")
    public static ParagraphAdapter adapterr;
    public static HorizontalViewPager pagerMain;
    ArrayList<DateItemNew> arrDateItem;
    ArrayList<DateItemNew> arrDateItemwithBlank;
    DatabaseHandler db;
    String selMonth, selDay, selYear;
    private int year, month, day, hour, min;
    private static final int PERMISSION_REQUEST_CODE = 112;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ParagraphBinding binding = DataBindingUtil.inflate(inflater, R.layout.paragraph, container, false);
        db = new DatabaseHandler(getActivity());
        pagerMain = binding.pagerMain;
        arrDateItem = new ArrayList<>();
        arrDateItemwithBlank = new ArrayList<>();
        MainActivity.activityMainBinding.backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.activityMainBinding.backimage.setVisibility(View.GONE);
                MainActivity.activityMainBinding.pagerdate.setCurrentItem(0, true);
            }
        });
        pagerMain.setClipToPadding(false);
        pagerMain.setPadding(15, 0, 15, 0);
        pagerMain.setPageMargin(10);
        pagerMain.setOffscreenPageLimit(1);
        @SuppressLint("PrivateResource")
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_grow_fade_in_from_bottom);
        pagerMain.startAnimation(animation);
        parseData();
        MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
        MainActivity.activityMainBinding.calendarOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
                openDialog();
            }
        });
        MainActivity.activityMainBinding.pagerdate.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                pagerMain.setCurrentItem(position);
                SharePrefrence.getInstance(getContext()).putInteger(Utills.POSI_VIEWPAGER_WORD, position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        pagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                final String uniqid = MainActivity.actualdate.get(position);
                try {
                    int value = db.TodayReadParaGraph(uniqid);
                    int valueccount = db.TodayTotalParaGraphCount(uniqid);
                    if (value == valueccount)
                        db.addreadUnread(uniqid, "Para");
                    else
                        db.deleteTodayReadParaGraph(uniqid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    ((OnNewsItemSelectedListener) getActivity()).onNewsItemPicked(position);
                } catch (Exception cce) {
                    cce.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        return binding.getRoot();
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setCancelable(true);
        DateDialogBinding dateDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.date_dialog, null, false);
        dialog.setContentView(dateDialogBinding.getRoot());
        WindowManager.LayoutParams windowManager = new WindowManager.LayoutParams();
        windowManager.copyFrom(dialog.getWindow().getAttributes());
        windowManager.height = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.width = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.gravity = Gravity.CENTER_HORIZONTAL;
        dialog.getWindow().setAttributes(windowManager);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        Calendar cl = Calendar.getInstance();
        Calendar cm = Calendar.getInstance();

        cl.set(2017, 0, 20);
        cl.set(2019, 0, 20);

       /* cm.set(Calendar.YEAR, 2019);
        cm.set(Calendar.MONTH, 11);
        cm.set(Calendar.DAY_OF_MONTH, 20);*/


        cm.add(Calendar.MONTH, 0);
        updateDateTime();
        dateDialogBinding.titleText.setText(MainActivity.actualdate.get(MainActivity.activityMainBinding.pagerdate.getCurrentItem()));
        dateDialogBinding.okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < MainActivity.actualdate.size(); i++) {
                            String currentItem = MainActivity.actualdate.get(i);
                            if (currentItem.equals("" + selYear + "-" + selMonth + "-" + selDay)) {
                                MainActivity.activityMainBinding.pagerdate.setCurrentItem(i, true);
                                pagerMain.setCurrentItem(i, true);
                                MainActivity.activityMainBinding.calendarOpen.setVisibility(View.GONE);
                                adapterr.notifyDataSetChanged();
                            }
                        }
                        if (dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                    }
                });
            }
        });
        dateDialogBinding.cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing() && getActivity() != null)
                    dialog.dismiss();
            }
        });
        dateDialogBinding.datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker view, int yearr, int monthr, int dayr) {
                year = yearr;
                month = monthr;
                day = dayr;
                updateDateTime();
            }
        });
        dateDialogBinding.datePicker.setMinDate(cl.getTimeInMillis());
        dateDialogBinding.datePicker.setMaxDate(cm.getTimeInMillis());
        dateDialogBinding.timePicker.setCurrentHour(hour);
        dateDialogBinding.timePicker.setCurrentMinute(min);
        dateDialogBinding.timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hourr, int minr) {
                hour = hourr;
                min = minr;
                updateDateTime();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                if (dialog != null && getActivity() != null)
                    dialog.dismiss();
            }
        });
    }

    public void updateDateTime() {
        GregorianCalendar mDay = new GregorianCalendar(year, month, day, hour, min);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat ftMonth = new SimpleDateFormat("MM");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat ftDay = new SimpleDateFormat("dd");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat ftYear = new SimpleDateFormat("yyyy");
        ftMonth.setCalendar(mDay);
        selMonth = ftMonth.format(mDay.getTime());
        ftMonth.setCalendar(mDay);
        selDay = ftDay.format(mDay.getTime());
        ftMonth.setCalendar(mDay);
        selYear = ftYear.format(mDay.getTime());
    }

    private void parseData() {
        adapterr = new ParagraphAdapter(getActivity(), pagerMain, MainActivity.actualdate, MainActivity.activityMainBinding.pagerdate.getCurrentItem());
        pagerMain.setAdapter(adapterr);
        pagerMain.setCurrentItem(MainActivity.activityMainBinding.pagerdate.getCurrentItem(), true);
        if (MainActivity.activityMainBinding.pagerdate.getCurrentItem() > 14)
            MainActivity.activityMainBinding.backimage.setVisibility(View.VISIBLE);
        else
            MainActivity.activityMainBinding.backimage.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
        MainActivity.activityMainBinding.weeklyCount.setVisibility(View.GONE);
        MainActivity.activityMainBinding.monthlyCount.setVisibility(View.GONE);
        try {
            if (adapterr != null)
                adapterr.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewsItemPicked(int position) {
        pagerMain.setCurrentItem(position);
    }

    @Override
    public void DatePosition(int position) {
        if (MainActivity.quiz_type.equals("Daily")) {
            MainActivity.actualdate.remove(position);
            arrDateItemwithBlank.remove(position);
            adapterr.notifyDataSetChanged();
        }
    }
}