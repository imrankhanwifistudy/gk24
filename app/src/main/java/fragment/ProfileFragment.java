package fragment;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ProfileFragmentBinding;

import ClickListener.ProfileClickListner;
import Custom.Utils;
import DB.SharePrefrence;

@SuppressWarnings("ALL")
public class ProfileFragment extends Fragment {
    SharedPreferences sharedPreferences;
    ProfileFragmentBinding binding;
    Drawable drawName, drawEmail, drawPhone, drawSign;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        drawName = Utils.DrawableChange(getActivity(), R.drawable.ic_attemp, "#686868");
        drawEmail = Utils.DrawableChange(getActivity(), R.drawable.ic_feedback_settings, "#686868");
        drawPhone = Utils.DrawableChange(getActivity(), R.drawable.ic_phone_black, "#686868");
        drawSign = Utils.DrawableChange(getActivity(), R.drawable.signout, "#686868");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                drawName = Utils.DrawableChange(getActivity(), R.drawable.ic_attemp, "#ffffff");
                drawEmail = Utils.DrawableChange(getActivity(), R.drawable.ic_feedback_settings, "#ffffff");
                drawPhone = Utils.DrawableChange(getActivity(), R.drawable.ic_phone_black, "#ffffff");
                drawSign = Utils.DrawableChange(getActivity(), R.drawable.signout, "#ffffff");
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false);
        binding.name.setCompoundDrawablesRelativeWithIntrinsicBounds(drawName, null, null, null);
        binding.emailid.setCompoundDrawablesRelativeWithIntrinsicBounds(drawEmail, null, null, null);
        binding.mobile.setCompoundDrawablesRelativeWithIntrinsicBounds(drawPhone, null, null, null);
        binding.SignINOUT.setCompoundDrawablesRelativeWithIntrinsicBounds(drawSign, null, null, null);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (sharedPreferences != null && sharedPreferences.contains("email"))
            binding.emailid.setText("" + sharedPreferences.getString("email", ""));
        if (sharedPreferences != null && sharedPreferences.contains("mobile")) {
            if (sharedPreferences.getString("mobile", "Mobile No").trim().length() > 0) {
                binding.mobile.setText("" + sharedPreferences.getString("mobile", "Mobile No"));
            } else {
                binding.mobile.setHint("Mobile No");
            }
        }
        if (sharedPreferences != null && sharedPreferences.contains("email")) {
            String user_name = sharedPreferences.getString("name", "");
            if (user_name.length() == 0 || user_name.equals("1")) {
                user_name = sharedPreferences.getString("email", "");
            }
            if (user_name.contains("@")) {
                String[] u_name = user_name.split("@");
                binding.name.setText(u_name[0]);
            } else {
                binding.name.setText(user_name);
            }
        }
        String image = SharePrefrence.getInstance(getActivity()).getString("social_image");
        if (image.equals("")) {
            binding.logouttxt.setImageResource(R.drawable.ic_dp_settings);
        } else {
            Glide.with(getActivity()).
                    load(image)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
        }
        binding.setClick(new ProfileClickListner(binding, getActivity()));
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}