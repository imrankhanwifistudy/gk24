package fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.VideoLayoutBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapter.MyRecyclerVideoAdapter;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DataObject;

@SuppressLint("Registered")
public class BookMarkVideoFragment extends Fragment {
    ArrayList<DataObject> list;
    ArrayList<DataObject> list_array;
    @SuppressLint("StaticFieldLeak")
    public static MyRecyclerVideoAdapter adapter;
    DatabaseHandler db;
    String user_id;
    SharedPreferences sharedPreferences;
    VideoLayoutBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String theme2 = SharePrefrence.getInstance(getActivity()).getString("Themes");
        switch (theme2) {
            case "night":
                getActivity().setTheme(R.style.night);
                break;
            case "sepia":
                getActivity().setTheme(R.style.sepia);
                break;
            default:
                getActivity().setTheme(R.style.defaultt);
                break;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.video_layout, null, false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        db = new DatabaseHandler(getActivity());
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onRefresh() {
                list_array = new ArrayList<>();
                list = new ArrayList<>();
                try {
                    list_array = db.getVideoPara();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (list_array.size() > 0) {
                    parasadata();
                } else {
                    if (Utils.hasConnection(getActivity())) {
                        bookmark_sync();
                    } else {
                        binding.progressView.setVisibility(View.GONE);
                        binding.noTest.setVisibility(View.VISIBLE);
                        binding.noTest.setText("No Bookmark Found");
                    }
                }
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
        binding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        ((BookmarkActivity) getActivity()).setFragmentRefreshListener1(new BookmarkActivity.FragmentRefreshListener1() {
            @Override
            public void onRefresh() {
                bookmark_sync();
            }
        });

        return binding.getRoot();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        user_id = sharedPreferences.getString("uid", "");

        list_array = new ArrayList<>();
        list = new ArrayList<>();
        try {
            list_array = db.getVideoPara();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (list_array.size() > 0) {
            parasadata();
        } else {
            if (Utils.hasConnection(getActivity())) {
                binding.swipeRefreshLayout.setRefreshing(true);
                binding.swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipeRefreshLayout.setRefreshing(true);
                        bookmark_sync();
                    }
                });
            } else {
                binding.progressView.setVisibility(View.GONE);
                binding.noTest.setVisibility(View.VISIBLE);
                binding.noTest.setText("No Bookmark Found");
            }
        }
    }

    void parasadata() {
        try {
            binding.progressView.setVisibility(View.GONE);
            for (int i = 0; i < list_array.size(); i++) {
                JSONObject jsobj = new JSONObject(list_array.get(i).getPackageName());
                DataObject item = new DataObject();
                String title = jsobj.getString("title");
                String created_date = jsobj.getString("created_date");
                String urlVideo = jsobj.getString("video_url");
                // Log.e("url",urlVideo);
                String ImageUrl = jsobj.getString("image");
                String time = jsobj.getString("time");
                item.setPackageName(String.valueOf(jsobj));
                item.setName(title);
                item.setDuration(time);
                item.setUrl(urlVideo);
                item.setImageUrl(ImageUrl);
                item.setDate(created_date);
                item.setId(jsobj.getString("id"));
                item.setType(jsobj.optString("type"));
                item.setPpt_url(jsobj.optString("ppt_url"));
                list.add(item);
            }
            RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
            binding.livetestRecyclerView.setLayoutManager(layoutManager1);
            binding.livetestRecyclerView.setHasFixedSize(true);
            adapter = new MyRecyclerVideoAdapter(getActivity(), list, "bookmark", binding.noTest);
            binding.livetestRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bookmark_sync() {
        list_array = new ArrayList<>();
        list = new ArrayList<>();
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        Log.e("url", Utills.BASE_URLGK + "Getbookmarked.php?uid=" + user_id + "&type_id=2");
        AndroidNetworking.post(Utills.BASE_URLGK + "Getbookmarked.php")
                .addBodyParameter("uid", user_id)
                .addBodyParameter("type_id", "2")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Meaning", String.valueOf(response));
                        if (dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                        binding.swipeRefreshLayout.setRefreshing(false);
                        Log.e("word Vocab", String.valueOf(response));
                        try {
                            JSONObject res = new JSONObject(String.valueOf(response));
                            String err = res.getString("err");
                            if (err.equals("1")) {
                                JSONArray data = res.getJSONArray("response");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.getJSONObject(i);
                                    try {
                                        Boolean check = db.checkVideoPara(jsonObject.getString("id") + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                                        Log.e("Error", "" + jsonObject.getString("id") + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                                        if (!check) {
                                            db.addVideoBook(String.valueOf(jsonObject), jsonObject.getString("video_url"), jsonObject.getString("id") + SharePrefrence.getInstance(getActivity()).getString(Utills.DEFAULT_LANGUAGE));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                try {
                                    list_array = db.getVideoPara();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                parasadata();
                            } else {
                                binding.progressView.setVisibility(View.GONE);
                                binding.noTest.setVisibility(View.VISIBLE);
                                binding.noTest.setText("No Bookmark Found");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if (dialog != null && dialog.isShowing() && getActivity() != null)
                            dialog.dismiss();
                    }
                });
    }
}