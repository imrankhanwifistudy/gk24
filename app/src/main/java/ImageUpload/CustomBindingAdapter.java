package ImageUpload;

import androidx.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class CustomBindingAdapter {
    @BindingAdapter({"images"})
    public static void loadImage(ImageView imageView, String images) {
        Glide.with(imageView.getContext()).load(images).thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }
}
