package DB;

import java.text.DateFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utills {
  //  public static String BASE_URLGK = "http://gk24.co.in/app/";
    public static String BASE_URLGK = "http://vocab24.com/gk24/app/";
    public static String POSI_VIEWPAGER_PARA = "anim_position_para";
    public static String POSI_VIEWPAGER_WORD = "anim_position_word";
    public static String POSI_VIEWPAGER_QUIZ = "anim_position_quiz";
    public static String AUTO_SWITCH = "auto_switch";
    public static String AUTO_IMAGE_SWITCH = "auto_image_switch";
    public static String AUTO_IMAGE_SMALLER = "auto_image_smaller";
    public static String AUTO_IMAGE_DEFAULT = "auto_image_default";
    public static String AUTO_IMAGE_LARGE = "auto_image_large";
    public static String IS_FIRST = "is_first";
    public static String PARAGRAPH = "typeParagraph";
    public static String GK = "Gk_Caterory";
    public static String DEFAULT_LANGUAGE = "defaultlaguage";
    public static String DEFAULT_LANGUAGE_POSITION = "defaultlaguagePOSITION";
    public static String CAPSULE = "typeDailyCapsule";
    public static String CAPSULE_Daily_DATE = "webserice_capsule_date";
    public static String WHICH_SELECTED = "which_tab_selected";
    public static String USERID = "USERID";

    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        return matcher.replaceAll("");
    }

    public static String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        return matcher.replaceAll("");
    }

    public static String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }
}