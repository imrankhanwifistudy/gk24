package DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

import Modal.BookParaItem;
import Modal.DataObject;
import Modal.FreeTestItem;
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    // Database Name
    private static final String DATABASE_NAME = "GK_DB";
    // Login table name
    private static final String TABLE_USER_DETAIL = "user_detail";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_Mobile = "mobile";
    private static final String KEY_Pic = "pic";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_Refcode = "refcode";
    private static final String KEY_reward_point = "rewardpoint";
    private static final String KEY_State = "state";
    private static final String KEY_UserId = "userid";
    private static final String KEY_DOB = "dob";
    private static final String KEY_Qualification = "qualification";
    private static final String KEY_Gender = "gender";
    private static final String KEY_City = "city";
    private static String KEY_Country = "country";
    private static String TABLE_DICTONARY = "dictonary";
    private static String TABLE_DAY_PARAGRAPH_COUNT = "paragraphcount";

    private String CREATE_USER_DETAIL_TABLE = "CREATE TABLE " + TABLE_USER_DETAIL + "(" + KEY_UserId + " TEXT,"
            + KEY_USERNAME + " TEXT," + KEY_EMAIL + " TEXT," + KEY_PASSWORD + " TEXT," + KEY_Pic + " TEXT,"
            + KEY_DOB + " TEXT," + KEY_Gender + " TEXT," + KEY_City + " TEXT," + KEY_Country + " TEXT,"
            + KEY_State + " TEXT," + KEY_Qualification + " TEXT," + KEY_Refcode + " TEXT," + KEY_reward_point + " TEXT," + KEY_Mobile + " TEXT" + ")";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String examlist, bookmarklist, bookmarkWord, QuizList, bookmarkQuiz, readUnread, bookmarkPara,
                urllist, homemenu, pdflist, capusletable, testtable, videotable, quiztable,
                concepttable, childConcept, childTest, testnametable, resultlist, paragraph,
                offlinedic,offlinebookmark,WordHistory,VideoBookmark;
        examlist = "CREATE TABLE IF NOT EXISTS examlist ( Id INTEGER PRIMARY KEY, dbid TEXT, categ_id TEXT, examname TEXT)";
        urllist = "CREATE TABLE IF NOT EXISTS urllist ( Id INTEGER PRIMARY KEY, basepath TEXT, background TEXT, packpath TEXT,other TEXT,discuss TEXT)";
        homemenu = "CREATE TABLE IF NOT EXISTS homemenu ( Id INTEGER PRIMARY KEY, menuname TEXT, examname TEXT,position TEXT)";
        testtable = "CREATE TABLE IF NOT EXISTS testtable ( Id INTEGER PRIMARY KEY, questions TEXT, answers TEXT, attempt_answer TEXT, correct_marks TEXT, negative_marks TEXT, total_question TEXT, total_marks TEXT, attempt_question TEXT, position TEXT, testname TEXT ,explation TEXT,language TEXT)";
        videotable = "CREATE TABLE IF NOT EXISTS videotable ( Id INTEGER PRIMARY KEY, detail TEXT, views TEXT, days TEXT, examname TEXT, category TEXT, position TEXT)";
        quiztable = "CREATE TABLE IF NOT EXISTS quiztable ( Id INTEGER PRIMARY KEY, detail TEXT, views TEXT, days TEXT, examname TEXT, category TEXT, position TEXT)";
        capusletable = "CREATE TABLE IF NOT EXISTS capusletable ( Id INTEGER PRIMARY KEY, detail TEXT, views TEXT, days TEXT, examname TEXT, category TEXT, position TEXT)";
        concepttable = "CREATE TABLE IF NOT EXISTS concepttable ( Id INTEGER PRIMARY KEY, english TEXT, hindi TEXT, examname TEXT, category TEXT, position TEXT)";
        childConcept = "CREATE TABLE IF NOT EXISTS childConcept ( Id INTEGER PRIMARY KEY, test TEXT, attemp TEXT, examname TEXT, category TEXT,parent TEXT, position TEXT)";
        testnametable = "CREATE TABLE IF NOT EXISTS testnametable ( Id INTEGER PRIMARY KEY, english TEXT, hindi TEXT, examname TEXT, category TEXT, position TEXT)";
        childTest = "CREATE TABLE IF NOT EXISTS childTest ( Id INTEGER PRIMARY KEY, test TEXT, attemp TEXT, examname TEXT, category TEXT,parent TEXT, position TEXT)";
        pdflist = "CREATE TABLE IF NOT EXISTS pdflist ( Id INTEGER PRIMARY KEY, pdfenglish TEXT, pdfhindi TEXT, examname TEXT, category TEXT,title TEXT, position TEXT)";
        bookmarklist = "CREATE TABLE IF NOT EXISTS bookmarklist ( Id INTEGER PRIMARY KEY, newstitle TEXT, pdfname TEXT, newstype TEXT, language TEXT, textid TEXT)";
        resultlist = "CREATE TABLE IF NOT EXISTS resultlist ( Id INTEGER PRIMARY KEY, correctanswer TEXT, incorrectanswer TEXT, skipanswer TEXT,accuracy TEXT,testname TEXT,rank TEXT,correct TEXT,incorrect TEXT,totaltime TEXT,language TEXT)";
        paragraph = "CREATE TABLE IF NOT EXISTS paragraph (_id INTEGER PRIMARY KEY,dataid TEXT,date TEXT,year TEXT,response TEXT,type TEXT,language TEXT)";
        bookmarkWord = "CREATE TABLE IF NOT EXISTS bookmarkWord (_id INTEGER PRIMARY KEY,mainword TEXT,meaning TEXT,synonyms TEXT,antonyms TEXT,example TEXT,image TEXT,uniq TEXT,forms TEXT,related_forms TEXT ,part_of_speech TEXT)";
        bookmarkPara = "CREATE TABLE IF NOT EXISTS bookmarkPara (_id INTEGER PRIMARY KEY,title TEXT,desc TEXT,image TEXT,maintitle TEXT,uniq TEXT,ID TEXT)";
        bookmarkQuiz = "CREATE TABLE IF NOT EXISTS bookmarkQuiz (_id INTEGER PRIMARY KEY,question TEXT,answer TEXT,opa TEXT,opb TEXT,opc TEXT,opd TEXT,ope TEXT,solution TEXT,dire TEXT,uniq TEXT ,language TEXT,ID TEXT)";
        readUnread = "CREATE TABLE IF NOT EXISTS readUnread (_id INTEGER PRIMARY KEY,uniq TEXT,value TEXT)";
        QuizList = "CREATE TABLE IF NOT EXISTS QuizList (_id INTEGER PRIMARY KEY,uniq TEXT, language TEXT , response TEXT)";
        TABLE_DAY_PARAGRAPH_COUNT = "CREATE TABLE IF NOT EXISTS "+ TABLE_DAY_PARAGRAPH_COUNT+" (_id INTEGER PRIMARY KEY,uniq TEXT,count INTEGER)";
        offlinedic = "CREATE TABLE IF NOT EXISTS offlinedic (_id INTEGER PRIMARY KEY,hindi TEXT,english TEXT,jsonres TEXT)";
        TABLE_DICTONARY = "CREATE TABLE IF NOT EXISTS dictonary (_id INTEGER PRIMARY KEY,mainword TEXT,meaning TEXT,synonyms TEXT,antonyms TEXT,example TEXT,image TEXT,uniq TEXT,forms TEXT,related_forms TEXT ,part_of_speech TEXT)";
        offlinebookmark = "CREATE TABLE IF NOT EXISTS offlinebookmark (_id INTEGER PRIMARY KEY,hindi TEXT,english TEXT,jsonres TEXT)";
        WordHistory = "CREATE TABLE IF NOT EXISTS WordHistory (_id INTEGER PRIMARY KEY,uniq TEXT, eword TEXT,hword TEXT , responce TEXT)";
        VideoBookmark = "CREATE TABLE IF NOT EXISTS VideoBookmark (_id INTEGER PRIMARY KEY,title TEXT,url TEXT,uniq_id TEXT)";

        db.execSQL(TABLE_DICTONARY);
        db.execSQL(examlist);
        db.execSQL(CREATE_USER_DETAIL_TABLE);
        db.execSQL(TABLE_DAY_PARAGRAPH_COUNT);
        db.execSQL(urllist);
        db.execSQL(homemenu);
        db.execSQL(testtable);
        db.execSQL(videotable);
        db.execSQL(quiztable);
        db.execSQL(capusletable);
        db.execSQL(concepttable);
        db.execSQL(testnametable);
        db.execSQL(childConcept);
        db.execSQL(childTest);
        db.execSQL(pdflist);
        db.execSQL(bookmarklist);
        db.execSQL(resultlist);
        db.execSQL(paragraph);
        db.execSQL(bookmarkWord);
        db.execSQL(QuizList);
        db.execSQL(bookmarkPara);
        db.execSQL(readUnread);
        db.execSQL(bookmarkQuiz);
        db.execSQL(offlinedic);
        db.execSQL(offlinebookmark);
        db.execSQL(WordHistory);
        db.execSQL(VideoBookmark);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + "examlist");
        db.execSQL("DROP TABLE IF EXISTS " + "urllist");
        db.execSQL("DROP TABLE IF EXISTS " + "homemenu");
        db.execSQL("DROP TABLE IF EXISTS " + "testtable");
        db.execSQL("DROP TABLE IF EXISTS " + "videotable");
        db.execSQL("DROP TABLE IF EXISTS " + "quiztable");
        db.execSQL("DROP TABLE IF EXISTS " + "capusletable");
        db.execSQL("DROP TABLE IF EXISTS " + "concepttable");
        db.execSQL("DROP TABLE IF EXISTS " + "testnametable");
        db.execSQL("DROP TABLE IF EXISTS " + "childConcept");
        db.execSQL("DROP TABLE IF EXISTS " + "childTest");
        db.execSQL("DROP TABLE IF EXISTS " + "pdflist");
        db.execSQL("DROP TABLE IF EXISTS " + "bookmarklist");
        db.execSQL("DROP TABLE IF EXISTS " + "resultlist");
        db.execSQL("DROP TABLE IF EXISTS " + "paragraph");
        db.execSQL("DROP TABLE IF EXISTS " + "bookmarkWord");
        db.execSQL("DROP TABLE IF EXISTS " + "dictonary");
        db.execSQL("DROP TABLE IF EXISTS " + "bookmarkPara");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DICTONARY);
        db.execSQL("DROP TABLE IF EXISTS " + "offlinedic");
        db.execSQL("DROP TABLE IF EXISTS " + "offlinebookmark");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DAY_PARAGRAPH_COUNT);
        db.execSQL("DROP TABLE IF EXISTS " + "videodownloads");
        db.execSQL("DROP TABLE IF EXISTS " + "WordHistory");
        db.execSQL("DROP TABLE IF EXISTS " + "VideoBookmark");

        onCreate(db);
    }

    public Cursor getresult(String testname) {
        SQLiteDatabase database = this.getWritableDatabase();
        String Query = "Select * from  resultlist  where  testname "
                + "=" + "'" + testname + "'";
        return database.rawQuery(Query, null);
    }


    public void addQuizBook(String question, String answer, String opa, String opb, String opc,
                            String opd, String ope
            , String solution, String dire, String uniq, String language,String ID) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("question", question);
        values.put("answer", answer);
        values.put("opa", opa);
        values.put("opb", opb);
        values.put("opc", opc);
        values.put("opd", opd);
        values.put("ope", ope);
        values.put("solution", solution);
        values.put("dire", dire);
        values.put("uniq", uniq);
        values.put("language", language);
        values.put("ID",ID);
        db.insert("bookmarkQuiz", null, values);
        db.close();
    }

    public void addParaBook(String title, String desc, String image, String uniq, String maintitle,String ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("desc", desc);
        values.put("image", image);
        values.put("uniq", uniq);
        values.put("maintitle", maintitle);
        values.put("ID",ID);
        db.insert("bookmarkPara", null, values);
        db.close();
    }

    public void addWebservice(String dataid, String date, String year, String response, String type, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("dataid", dataid);
        values.put("date", date);
        values.put("year", year);
        values.put("response", response);
        values.put("type", type);
        values.put("language", language);
        db.insert("paragraph", null, values);
        db.close();
    }

    public void addreadUnread(String uniq, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("uniq", uniq);
        values.put("value", value);
        db.insert("readUnread", null, values);
        db.close();
    }

    public ArrayList<FreeTestItem> getQuizWord() {

        ArrayList<FreeTestItem> data = new ArrayList<>();
        String selectQuery = "SELECT  " + "question" + " ," + "answer" + " ," + "opa"
                + " ," + "opb" + " ," + "opc" + " ," + "opd" + " ," + "ope" + " ,"
                + "solution" + " ," + "dire" + " ," + "uniq"+ " ," + "ID" + " FROM " + "bookmarkQuiz";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        // Log.i("cursor size", "" + cursor.getCount());
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                FreeTestItem item = new FreeTestItem();

                item.setQuestionenglish(cursor.getString(0));
                item.setAnswer(cursor.getString(1));
                item.setOpt_en_1(cursor.getString(2));
                item.setOp_en_2(cursor.getString(3));
                item.setOp_en_3(cursor.getString(4));
                item.setOp_en_4(cursor.getString(5));
                item.setOp_en_5(cursor.getString(6));
                item.setSolutionenglish(cursor.getString(7));
                item.setDirection(cursor.getString(8));
                item.setUniq(cursor.getString(9));
                item.setNoofoption("5");
                item.setId(cursor.getString(10));

                data.add(item);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return data;
    }

    public String getReadUnread(String uniq) {
        String value = "";
        String selectQuery = "SELECT  " + "value" + " FROM " + "readUnread" + " WHERE uniq" + " = '" + uniq + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                value = cursor.getString(0);
                cursor.moveToNext();
            }
            cursor.close();
        }
        db.close();
        return value;
    }

    public String TodayQuizRead(String uniq) {
        String value = "";
        String TodayQuizRead = "SELECT  " + "value" + " FROM " + "readUnread" + " WHERE uniq LIKE" + " '%" + uniq + "%'" + "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(TodayQuizRead, null);

        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                value = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return value;
    }

    public void TodayTotalParaGraph( String ID , int Count) // insert the paragraph count in DB for particular day
    {
        ContentValues values = new ContentValues();
        values.put("uniq", ID);
        values.put("count", Count);
        SQLiteDatabase db = this.getWritableDatabase();
        int value=0;
        String TodayQuizRead = "SELECT  " + "uniq" + " FROM paragraphcount" +
                " WHERE uniq = " + " '" + ID + "'" + "";
        Cursor cursor = db.rawQuery(TodayQuizRead, null);
        if (cursor != null && !cursor.isClosed()) {
            value = cursor.getCount();
        }
        if (value > 0) {
            db.update("paragraphcount", values, "uniq ='" + ID + "'", null);
        }else {
            db.insert("paragraphcount", null, values);
        }
        db.close();
    }

    public int TodayTotalParaGraphCount(String uniq) { // count the today total paragraph
        int value=0;
        String TodayQuizRead = "SELECT  " + "count" + " FROM paragraphcount"  + " WHERE uniq =" + " '" + uniq + "'" + "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(TodayQuizRead, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                value =cursor.getInt(0) ;
                cursor.moveToNext();
            }
        }
        else{
            value = 0;
        }
        assert cursor != null;
        cursor.close();
        db.close();

        return value;
    }
    public int TodayReadParaGraph(String uniq) { // count the today readed paragraph
        int value=0;
        String TodayQuizRead = "SELECT  " + "value" + " FROM " + "readUnread" + " WHERE uniq LIKE" + " '%" + uniq + "%'" + "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(TodayQuizRead, null);
        if (cursor != null && !cursor.isClosed()) {
            value = cursor.getCount();
        }
        assert cursor != null;
        cursor.close();
        db.close();

        return value;
    }
    public void deleteTodayReadParaGraph(String uniq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + "readUnread" + " WHERE " + "uniq" + " = '" + uniq + "'");
    }

    public ArrayList<BookParaItem> getBookPara() {
        ArrayList<BookParaItem> data = new ArrayList<>();
        String selectQuery = "SELECT  " + "desc" + " ," + "title" + " ," + "image" + " ," + " maintitle" + " ," + " uniq" + " ," + "  ID" + " FROM " + "bookmarkPara";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                BookParaItem item = new BookParaItem();
                item.setDesc(cursor.getString(0));
                item.setTitle(cursor.getString(1));
                item.setImage(cursor.getString(2));
                item.setMainTitle(cursor.getString(3));
                item.setuId(cursor.getString(4));
                item.setID(cursor.getString(5));
                data.add(item);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return data;
    }

    public boolean CheckIsDataAlreadyInBookMarkQuiz(String uniq, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "bookmarkQuiz" + " where "
                + "uniq" + "=" + "'" + uniq + "'"
                + " AND " + "language" + " = " + "'" + language + "'";
        Cursor cursor = db.rawQuery(Query, null);
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }

    public boolean checkBookPara(String uniq) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "bookmarkPara" + " where " + "uniq"
                + "=" + "'" + uniq + "'";
        Cursor cursor = db.rawQuery(Query, null);
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }

    //paragraph dataid date year response  type ;
    public boolean checkWebserviceData(String dataid, String date, String year, String type, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "paragraph" + " where " + "dataid"
                + "=" + "'" + dataid + "'" + " AND " + "date" + " = " + "'" + date + "'" + " AND " +
                "year" + " = " + "'" + year + "'" + " AND " + "type" + " = " + "'" + type + "'"+ " AND " + "language" + " = " + "'" + language + "'";
        Cursor cursor = db.rawQuery(Query, null); // add the String your
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }

        cursor.close();
        db.close();
        return hasObject;
    }

    public String getWEBResponse(String dataid, String date, String year, String type, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select response from " + "paragraph" + " where " + "dataid"
                + "=" + "'" + dataid + "'" + " AND " + "date" + " = " + "'" + date + "'" + " AND " + "year" + " = " + "'" + year + "'" + " AND " + "type" + " = " + "'" + type + "'"+ " AND " + "language" + " = " + "'" + language + "'";
        Cursor cursor = db.rawQuery(Query, null);
        String response = "";
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                response = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return response;
    }


    public void deleteBookPara(String uniq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + "bookmarkPara" + " WHERE " + "uniq" + " = '" + uniq + "'");
    }

    public void deleteBookQuiz(String uniq, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + "bookmarkQuiz" + " WHERE " + "uniq" + " = '" + uniq + "'"+ "AND " + "language ='" + language + "'");
    }

    public void addtesttable(String questions, String answers, String attempt_answer, String correct_marks,
                             String negative_marks, String total_question, String total_marks,
                             String attempt_question, String position, String testname, String Explation, String language) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("questions", questions);
        values.put("answers", answers);
        values.put("attempt_answer", attempt_answer);
        values.put("correct_marks", correct_marks);
        values.put("negative_marks", negative_marks);
        values.put("total_question", total_question);
        values.put("total_marks", total_marks);
        values.put("attempt_question", attempt_question);
        values.put("position", position);
        values.put("testname", testname);
        values.put("explation", Explation);
        values.put("language", language);
        db.insert("testtable", null, values);
        db.close();

    }

    public void updatetesttable(String questions, String answers, String attempt_answer, String correct_marks, String negative_marks,
                                String total_question, String total_marks, String attempt_question, String position,
                                String testname, String Explation, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("questions", questions);
        values.put("answers", answers); // Name
        values.put("attempt_answer", attempt_answer);
        values.put("correct_marks", correct_marks);
        values.put("negative_marks", negative_marks);
        values.put("total_question", total_question);
        values.put("total_marks", total_marks);
        values.put("attempt_question", attempt_question);
        values.put("position", position);
        values.put("testname", testname);
        values.put("explation", Explation);
        values.put("language", language);
        db.update("testtable", values, "position ='" + position + "'" + "AND " + "testname ='" + testname + "'", null);
        db.close();
    }

    public void addresult(String correct, String incorrect, String skip, String accuracy, String testname,
                          String correctMark, String incorrectMark, String totaltime, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("correctanswer", correct);
        values.put("incorrectanswer", incorrect);
        values.put("skipanswer", skip);
        values.put("accuracy", accuracy);
        values.put("testname", testname);
        values.put("correct", correctMark);
        values.put("incorrect", incorrectMark);
        values.put("totaltime", totaltime);
        values.put("language", language);
        db.insert("resultlist", null, values);
        db.close();
    }

    public void updateresult(String correct, String incorrect, String skip, String accuracy,
                             String testname, String correctmark, String incorrectMark, String totaltime, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("correctanswer", correct);
        values.put("incorrectanswer", incorrect);
        values.put("skipanswer", skip);
        values.put("accuracy", accuracy);
        values.put("testname", testname);
        values.put("correct", correctmark);
        values.put("incorrect", incorrectMark);
        values.put("totaltime", totaltime);
        values.put("language", language);
        db.update("resultlist", values, "testname ='" + testname + "'", null);
        db.close();
    }


    public void removeTestTable(String testname, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("testtable", "testname=" + DatabaseUtils.sqlEscapeString(testname) + "AND " + "language ='" + language + "'", null);
        db.close();
    }

    public void removeResultTable(String testname, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("resultlist", "testname=" + DatabaseUtils.sqlEscapeString(testname)+ "AND " + "language ='" + language + "'", null);
        db.close();
    }

    public boolean CheckIsDataAlreadyInDBorNotReadyStock(String id, String testname, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from testtable where " + "position"
                + "=" + "'" + id + "'" + "AND " + "testname ='" + testname + "'"
                + "AND " + "language ='" + language + "'";
        Cursor cursor = db.rawQuery(Query, null); // add the String your
         boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }

        cursor.close();
        db.close();
        return hasObject;
    }

    public boolean CheckIsResultAlreadyInDBorNotReadyStock(String testname, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from resultlist where " + "testname"
                + "=" + "'" + testname + "'"+ "AND " + "language ='" + language + "'";
        Cursor cursor = db.rawQuery(Query, null);
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }

    public void RankUpdate(String Rank, String testname, ContentValues values, String language) {
        SQLiteDatabase db = this.getReadableDatabase();
        int i = db.update("resultlist", values, "testname ='" + testname + "'"+ "AND " + "language ='" + language + "'", null);

    }
    public String getTestAns(String position, String testname, String language) {

        String Answer = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select  attempt_answer , explation from  testtable  where  position "
                + "=" + "'" + position + "'" + "AND " + "testname ='" + testname + "'"
                + "AND " + "language ='" + language + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Answer = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return Answer;
    }

    public String getTestexplanation(String position, String testname) {
        String explanation = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select  explation from  testtable  where  position "
                + "=" + "'" + position + "'" + "AND " + "testname ='" + testname + "'";
        Cursor cursor1 = db.rawQuery(Query, null);
        if (cursor1 != null) {
            cursor1.moveToFirst();
            for (int i = 0; i < cursor1.getCount(); i++) {
                explanation = cursor1.getString(0);
                cursor1.moveToNext();
            }
        }
        assert cursor1 != null;
        cursor1.close();
        db.close();
        return explanation;
    }



    public HashMap<String, String> getDetails() {
        HashMap<String, String> user = new HashMap<>();
        String selectQuery = "SELECT  " + KEY_UserId + " ," + KEY_USERNAME + " ," + KEY_EMAIL + " ," + KEY_PASSWORD + " ," + KEY_Pic + " ,"
                + KEY_DOB + " ," + KEY_Gender + " ," + KEY_City + " ," + KEY_Country + " ," + KEY_State + " ,"
                + KEY_Qualification + " ," + KEY_Refcode + " ," + KEY_reward_point + " ," + KEY_Mobile + " FROM " + TABLE_USER_DETAIL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("userid", cursor.getString(0));
            user.put("username", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("password", cursor.getString(3));
            user.put("pic", cursor.getString(4));
            user.put("dob", cursor.getString(5));
            user.put("gender", cursor.getString(6));
            user.put("city", cursor.getString(7));
            user.put("country", cursor.getString(8));
            user.put("state", cursor.getString(9));
            user.put("qualification", cursor.getString(10));
            user.put("refcode", cursor.getString(11));
            user.put("rewardpoint", cursor.getString(12));
            user.put("mobile", cursor.getString(13));


        }
        cursor.close();
        db.close();
        return user;
    }


    public void addQuizRes(String uniq, String value, String language) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("uniq", uniq);
        values.put("response", value);
        values.put("language", language);
        db.insert("QuizList", null, values);
        db.close();
    }


    public String getQuizRes(String uniq, String language) {
        String value = "";
        String selectQuery = "SELECT  " + "response" + " FROM " + "QuizList"
                + " WHERE uniq" + " = '" + uniq + "'"
                + "AND "
                + "language" + "=" + "'" + language + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                value = cursor.getString(0);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return value;
    }

    public boolean CheckInQuiz(String uniq, String Language) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "QuizList" + " where "
                + "uniq" + "=" + "'" + uniq + "'"
                + "AND "
                + "language" + "=" + "'" + Language + "'";
        Cursor cursor = db.rawQuery(Query, null);
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }


    public boolean CheckIsDataAlreadyInReadUnread(String uniq) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "readUnread" + " where " + "uniq"
                + "=" + "'" + uniq + "'";
        Cursor cursor = db.rawQuery(Query, null); // add the String your
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }






    public void ClearAttamptedOption(String id, String testname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + "testtable" + " WHERE " + "position"
                + "=" + "'" + id + "'" + "AND " + "testname ='" + testname + "'");
    }

    public void deleteVideoBook(String uniq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + "VideoBookmark" + " WHERE " + "uniq_id" + " = '" + uniq + "'");
    }
    public void addVideoBook(String title, String url, String uniq_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("url", url);
        values.put("uniq_id", uniq_id);
        db.insert("VideoBookmark", null, values);
        db.close();
    }



    public boolean checkVideoPara(String uniq) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "Select * from " + "VideoBookmark" + " where " + "uniq_id"
                + "=" + "'" + uniq + "'";
        Cursor cursor = db.rawQuery(Query, null);
        boolean hasObject = false;
        if (cursor.getCount() > 0) {
            hasObject = true;
        }
        cursor.close();
        db.close();
        return hasObject;
    }
    public ArrayList<DataObject> getVideoPara() {
        ArrayList<DataObject> data = new ArrayList<>();
        String selectQuery = "SELECT  " + "title" + " ," + "uniq_id" + " FROM " + "VideoBookmark";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                DataObject item = new DataObject();
                item.setPackageName(cursor.getString(0));
                item.setId(cursor.getString(1));
                data.add(item);
                cursor.moveToNext();
            }
        }
        assert cursor != null;
        cursor.close();
        db.close();
        return data;
    }
}
