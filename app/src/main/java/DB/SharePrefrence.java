package DB;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SharePrefrence {

    private SharedPreferences sharePreference;
    SharedPreferences.Editor editor;

    public static String PAUSEBUTTON = "pausebutton";
    private static SharePrefrence mySession;
    public SharePrefrence(Context context) {
        String sharePrefName = "mySharePrefwifistudy";
        sharePreference = context.getSharedPreferences(sharePrefName, Context.MODE_PRIVATE);
        editor = sharePreference.edit();
    }


    public static SharePrefrence getInstance(Context context) {

        if (mySession == null) {
            mySession = new SharePrefrence(context);

        } else {

        }
        return mySession;
    }

    public void putLong(String key, long value) {

        editor.putLong(key, value);
        editor.commit();
    }
    public long getLong(String key) {

        return sharePreference.getLong(key, 0);
    }

    public String getString(String key) {

        return sharePreference.getString(key, "");
    }

    public void putString(String key, String value) {

        editor.putString(key, value);
        editor.commit();
    }

    public boolean HasInt(String key) {
        return sharePreference.contains(key);
    }
    public boolean HasString(String key) {
        return sharePreference.contains(key);
    }

    public void putBoolean(String key, Boolean value) {

        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {

        return sharePreference.getBoolean(key, false);
    }
    public void putInteger(String key, Integer value) {

        editor.putInt(key, value);
        editor.commit();
    }

    public Integer getInteger(String key) {

        return sharePreference.getInt(key, 0);
    }



     public void prefDeleteKey(String Key){
         sharePreference.edit().remove(Key).commit();

}
    public void prefDelete(){
        sharePreference.edit().clear().commit();

    }
    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

}
