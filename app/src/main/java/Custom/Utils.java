package Custom;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import ui.AppController;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.FeedbackBinding;
import com.daily.currentaffairs.databinding.QuizProgessBinding;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import DB.SharePrefrence;
import DB.Utills;
import static com.daily.currentaffairs.MainActivity.activity;

public final class Utils {
    public static boolean hasConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
    public static  void ShareDialog(final Context ctx){
       ShareCompat.IntentBuilder.from((Activity) ctx) // getActivity() or activity field if within Fragment
               .setText("Hey, Today I found the best GK & Current Affairs app." +
                       " Stay updated with latest news and check your knowledge by daily Quiz" +
                       "Download it here: https://play.google.com/store/apps/details?id=com.daily.currentaffairs")
               .setType("text/plain") // most general text sharing MIME type
               .setChooserTitle("Choose Option")
               .startChooser();
    }
    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null)
            tv.setTag(tv.getText());
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }
    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv, final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 2, "View More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;
    }
    public static String getTime(String commentDate) {
        final int SECOND = 1000;
        final int MINUTE = 60 * SECOND;
        final int HOUR = 60 * MINUTE;
        final int DAY = 24 * HOUR;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date systemDate = Calendar.getInstance().getTime();
        String myDate = sdf.format(systemDate);
        StringBuffer text = new StringBuffer("");
        String diff="";
        try {
            Date Date1 = sdf.parse(myDate);
            Date Date2 = sdf.parse(commentDate);
            long millse = Date1.getTime() - Date2.getTime();
            if(millse >= 0){
                diff="game over";
            }else {
                long mills = Math.abs(millse);
                int Hours = (int) (mills / (1000 * 60  *60));
                int Mins = (int) (mills / (1000 * 60)) % 60;
                long Secs = (int) (mills / 1000) % 60;
                diff = mills / DAY+","+(Hours - (mills / DAY*24)) +","+ Mins + ","+Secs;
                Log.e("time_cal",""+diff+" , "+mills+","+millse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diff;
    }
    public static Drawable DrawableChange(Activity ctx , int d , String color){
        Drawable drawable1=ctx.getResources().getDrawable(d).mutate();
        drawable1.setColorFilter(new PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_IN));
        return drawable1;
    }
    public static String getDays(String commentDate) {
        final int SECOND = 1000;
        final int MINUTE = 60 * SECOND;
        final int HOUR = 60 * MINUTE;
        final int DAY = 24 * HOUR;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM,yyyy");
        Date systemDate = Calendar.getInstance().getTime();
        String myDate = sdf.format(systemDate);
        StringBuffer text = new StringBuffer("");
        String diff="";
        try {
            Date Date1 = sdf.parse(myDate);
            Date Date2 = sdf.parse(commentDate);
            long millse = Date1.getTime() - Date2.getTime();
            long mills = Math.abs(millse);
            int Hours = (int) (mills / (1000 * 60  *60));
            int Mins = (int) (mills / (1000 * 60)) % 60;
            long Secs = (int) (mills / 1000) % 60;
            diff = ""+mills / DAY;
            Log.e("time_cal",""+diff+" , "+mills+","+millse);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diff;
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    private static int PERMISSION_REQUEST_CODE = 2;
    public static void ShareData(Activity activity){
        if (checkPermission(activity)) {
            shareIntent(activity);
        } else requestPermission(activity);
    }
    private static boolean checkPermission(Activity activity) {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }
    private static void requestPermission(Activity activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    shareIntent(activity);
                break;
        }
    }
    private static void shareIntent(Activity activity) {
        Intent Intent1 = new Intent(Intent.ACTION_SEND);
        View v1 = activity.getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        Bitmap b = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);
        Intent1.setType("*/*");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        Uri imageUri = null;
        try {
            String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), b, "Title", null);
            imageUri = Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Intent1.putExtra(Intent.EXTRA_SUBJECT, "Wifi Study English Vocabulary");
        Intent1.putExtra(Intent.EXTRA_STREAM, imageUri);
        Intent1.setType("image/jpeg");
        Intent1.putExtra(Intent.EXTRA_TEXT, "Hey, Today I found the best GK & Current Affairs app. " +
                "Stay updated with latest news and check your knowledge by daily Quiz." +
                "Download it here: https://play.google.com/store/apps/details?id=com.daily.currentaffairs");
        activity.startActivity(Intent.createChooser(Intent1, "Share GK & Current Affairs via"));
    }
    public static void ReportDialog(final Activity activity, final String id, final String type, final String qtytype) {
        final Dialog fb_dialog = new Dialog(activity,R.style.MyActivityDialogTheme);
        fb_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        final FeedbackBinding binding= DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.feedback,null,false);
        fb_dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(fb_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        fb_dialog.getWindow().setAttributes(lp);
        fb_dialog.show();
        binding.email.setText(AppController.Email);
        binding.reportOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.feedback.getText().toString().trim().length() > 0 && binding.email.getText().toString().trim().length() > 0) {
                    if (Utils.hasConnection(activity)) {
                        if (binding.mobile.getText().toString().trim().length() > 9) {
                            fb_dialog.dismiss();
                            Report(binding.email.getText().toString(), binding.feedback.getText().toString() ,
                                    binding.mobile.getText().toString().trim(),activity,id,type,qtytype);
                        }else {
                            Toast.makeText(activity, "Please Enter your valid mobile No.", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(activity, "Check your Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, "Please Fill Feedback & Email Id", Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.reportCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fb_dialog.dismiss();
            }
        });
    }
    private static void Report(final String email, final String explain, String mobile, Activity activity, String id, String type, String qtytype) {
        final ProgressDialog dialog = new ProgressDialog(MainActivity.activity);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        Log.e("feedback_url", Utills.BASE_URLGK + "feedback.php?message="+explain+ "&email="+email+ "&mobile="+mobile+ "&type=2&vddate="+id +
                "&qtype="+qtytype );
        AndroidNetworking.post(Utills.BASE_URLGK + "feedback.php")
                .addBodyParameter("message", "" + explain)
                .addBodyParameter("email", "" + "" + email)
                .addBodyParameter("mobile", "" + "" + mobile)
                .addBodyParameter("type", "" + type)
                .addBodyParameter("qtype", qtytype)
                .addBodyParameter("vddate", "" + id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter" , String.valueOf(response));
                        dialog.dismiss();
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        dialog.dismiss();
                    }
                });
    }
    public static void unbookmark(final String capsuleid, String type, Activity activity, String user_id) {
        final ProgressDialog dialog = new ProgressDialog(MainActivity.activity);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        AndroidNetworking.post(Utills.BASE_URLGK +"unbookmark.php")
                .addBodyParameter("id",capsuleid)
                .addBodyParameter("type",type)
                .addBodyParameter("user_id",user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("QuizInsideAdapter" , String.valueOf(response));
                        if(dialog != null && dialog.isShowing() && MainActivity.activity != null)
                            dialog.dismiss();
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(dialog != null && dialog.isShowing() && MainActivity.activity != null)
                            dialog.dismiss();
                    }
                });
    }
    public static void bookmark_para(final String capsuleid, String type, Activity activity, String user_id) {
        AndroidNetworking.post(Utills.BASE_URLGK +"insert_bookmark.php")
                .addBodyParameter("mainid",capsuleid)
                .addBodyParameter("type_id",type)
                .addBodyParameter("uid",user_id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("BOOKMARK_RESPONSE" , String.valueOf(response));
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });

    }
    public static void PROGRESS(Activity Activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(Activity);
        LayoutInflater inflater = Activity.getLayoutInflater();
        final QuizProgessBinding processBinding=DataBindingUtil.inflate(inflater,R.layout.quiz_progess,null,false);
        builder.setView(processBinding.getRoot());
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        @SuppressLint("HandlerLeak")
        final Handler handle = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                processBinding.progressBar.incrementProgressBy(10);
            }
        };
        processBinding.progressBar.setMax(100);
        int t=10;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (processBinding.progressBar.getProgress() <= processBinding.progressBar.getMax()) {
                        handle.sendMessage(handle.obtainMessage());
                        Thread.sleep(200);
                        if (processBinding.progressBar.getProgress() == processBinding.progressBar.getMax()) {
                            if(alertDialog != null && alertDialog.isShowing() && alertDialog != null)
                                alertDialog.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public static void dialog_feedback(final LinearLayout rate_us_popup, final RelativeLayout rv_main_layout, final String finalFormattedDate,
                                       final LinearLayout logout_popup, final Activity Activity) {
        final Dialog fb_dialog = new Dialog(Activity);
        final FeedbackBinding feedbackBinding=DataBindingUtil.inflate(LayoutInflater.from(Activity),R.layout.feedback,null,false);
        fb_dialog.setContentView(feedbackBinding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(fb_dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        fb_dialog.getWindow().setAttributes(lp);
        fb_dialog.show();
        feedbackBinding.email.setText(AppController.Email);
        feedbackBinding.reportOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feedbackBinding.feedback.getText().toString().trim().length() > 0 && feedbackBinding.email.getText().toString().trim().length() > 0) {
                    SharePrefrence.getInstance(Activity).putString("rate_us", "2");
                    rate_us_popup.setVisibility(View.GONE);
                    logout_popup.setVisibility(View.GONE);
                    rv_main_layout.setVisibility(View.GONE);
                    rv_main_layout.setPadding(0, 0, 0, 0);
                    SharePrefrence.getInstance(MainActivity.activity).putString("date_not", finalFormattedDate);
                    if (feedbackBinding.mobile.getText().toString().trim().length() > 9) {
                        if(fb_dialog != null && MainActivity.activity != null && fb_dialog.isShowing())
                            fb_dialog.dismiss();
                        Report(feedbackBinding.email.getText().toString(), feedbackBinding.feedback.getText().toString(),feedbackBinding.mobile.getText().toString().trim(),
                                Activity,"","3","");
                    }else {
                        Toast.makeText(MainActivity.activity, "Please Enter your valid mobile No.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.activity, "Please Fill Feedback & Email Id", Toast.LENGTH_SHORT).show();
                }

            }
        });
        feedbackBinding.reportCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fb_dialog != null && MainActivity.activity != null && fb_dialog.isShowing())
                    fb_dialog.dismiss();
            }
        });
    }
}