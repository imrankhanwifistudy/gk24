package Custom;

import android.graphics.Typeface;

import ui.AppController;

import java.util.HashMap;

public class CustomFontFamily
{
    private static CustomFontFamily customFontFamily;
    private HashMap<String,String> fontMap=new HashMap<>();
    private HashMap<String,Typeface> fontCache=new HashMap<>();
    public static CustomFontFamily getInstance()
    {
        if(customFontFamily==null)
            customFontFamily=new CustomFontFamily();
        return customFontFamily;
    }
    public void addFont(String alias, String fontName){
        fontMap.put(alias,fontName);
    }
    Typeface getFont(String alias)
    {
        String fontFilename = fontMap.get(alias);
        if (fontFilename == null) {
            return null;
        }
        if(fontCache.containsKey(alias))
            return fontCache.get(alias);
        else
        {
            Typeface typeface = Typeface.createFromAsset(AppController.getInstance().getAssets(), "fonts/" + fontFilename);
            fontCache.put(fontFilename, typeface);
            return typeface;
        }
    }
}
