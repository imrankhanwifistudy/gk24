package Custom;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.daily.currentaffairs.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import Adapter.DateAdapterMain2;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;
import fragment.LearnFragment;
import fragment.Paragraph;
import fragment.QuizFragment;
import fragment.SettingFRagment;
import fragment.VideoFragment1;

public class MainUtils {
    public static void date(MainActivity activity, ArrayList<DateItem> arrDateItem, ArrayList<DateItem> arrDateItemwithBlank) {
        MainActivity.activityMainBinding.tvSearch.setVisibility(View.VISIBLE);
        MainActivity.activityMainBinding.tvDaily.setTextColor(Color.parseColor("#37C076"));
        MainActivity.activityMainBinding.monthly.setTextColor(Color.parseColor("#000000"));
        MainActivity.activityMainBinding.weekly.setTextColor(Color.parseColor("#000000"));
        MainActivity.activityMainBinding.tvSearch.setTextColor(Color.parseColor("#000000"));
        MainActivity.weeks = new ArrayList<>();
        MainActivity.months = new ArrayList<>();
        MainActivity.actualdate = new ArrayList<>();
        MainActivity.activityMainBinding.llQuizType.setVisibility(View.GONE);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date end = cal.getTime();
        Calendar calc = Calendar.getInstance();

       /* calc.set(Calendar.YEAR, 2017);
        calc.set(Calendar.MONTH, 00);
        calc.set(Calendar.DAY_OF_MONTH, 20);*/

        calc.set(Calendar.YEAR, 2019);
        calc.set(Calendar.MONTH, 00);
        calc.set(Calendar.DAY_OF_MONTH, 20);


        Date start = calc.getTime();
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(start);
        while (gcal.getTime().before(end)) {
            gcal.add(Calendar.DAY_OF_MONTH, 1);
            cal.add(Calendar.DAY_OF_MONTH, -1);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat ftMonth = new SimpleDateFormat("dd");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat ftdate = new SimpleDateFormat("MMM");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
            String actual_format = date_format.format((cal.getTime()));
            String Month = ftMonth.format(cal.getTime());
            String date = ftdate.format(cal.getTime());
            MainActivity.actualdate.add(actual_format);
            MainActivity.weeks.add(Month);
            MainActivity.months.add(date);
        }
        blankSpace(arrDateItem, arrDateItemwithBlank);
        MainActivity.adapter2 = new DateAdapterMain2(activity, MainActivity.weeks, MainActivity.months, "Daily", MainActivity.activityMainBinding.pagerdate, MainActivity.activityMainBinding.pagerdate.getCurrentItem());
        MainActivity.activityMainBinding.pagerdate.setAdapter(MainActivity.adapter2);
        MainActivity.activityMainBinding.pagerdate.setCurrentItem(MainActivity.pager_pos);
    }

    public static void blankSpace(ArrayList<DateItem> arrDateItem, ArrayList<DateItem> arrDateItemwithBlank) {
        for (int i = 0; i < 4; i++) {
            DateItem item = new DateItem();
            item.setDate("");
            item.setId("");
            item.setCountVocabWord(10);
            arrDateItemwithBlank.add(item);
            arrDateItem.add(item);
            MainActivity.months.add("");
            MainActivity.weeks.add("");
            MainActivity.monthsyear.add("");
            MainActivity.actualdate.add("");
        }
    }

    public static void weekcount() {
        MainActivity.weeks = new ArrayList<>();
        MainActivity.weeks1 = new ArrayList<>();
        MainActivity.months = new ArrayList<>();
        MainActivity.actualdate = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        Date end = cal.getTime();
        String Mons;
        int count = 0;
        Calendar calc = Calendar.getInstance();
        /*calc.set(Calendar.YEAR, 2017);
        calc.set(Calendar.MONTH, 00);
        calc.set(Calendar.DAY_OF_MONTH, 20);*/

        calc.set(Calendar.YEAR, 2019);
        calc.set(Calendar.MONTH, 00);
        calc.set(Calendar.DAY_OF_MONTH, 20);


        Date start = calc.getTime();
        System.out.println(calc.getActualMaximum(Calendar.WEEK_OF_MONTH));
        @SuppressLint("SimpleDateFormat") SimpleDateFormat Mon = new SimpleDateFormat("MMM");
        Mons = Mon.format(cal.getTime());
        String month_size = Mons;
        ArrayList<String> arrayList = new ArrayList<>();
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(start);
        while (gcal.getTime().before(end)) {
            gcal.add(Calendar.WEEK_OF_MONTH, 1);
            cal.add(Calendar.WEEK_OF_MONTH, -1);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat ftMonth = new SimpleDateFormat("MMM");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat actual_format = new SimpleDateFormat("yyyy-MM");
            String date = actual_format.format(cal.getTime());
            String Month = ftMonth.format(cal.getTime());
            if (month_size.equals(Month)) {
                count = count + 1;
                if (count != 5) {
                    String mon_count = String.valueOf(count);
                    arrayList.add(mon_count);
                }
            } else {
                Collections.reverse(arrayList);
                for (int k = 0; k < arrayList.size(); k++) {
                    MainActivity.months.add(arrayList.get(k));
                    MainActivity.weeks1.add(arrayList.get(k));
                }
                month_size = Month;
                count = 1;
                arrayList = new ArrayList<>();
                String mon_count = String.valueOf(count);
                arrayList.add(mon_count);
            }
            if (count != 5) {
                MainActivity.weeks.add(Month);
                MainActivity.actualdate.add(date);
            }
        }
        Collections.reverse(arrayList);
        for (int k = 0; k < arrayList.size(); k++) {
            String www = arrayList.get(k);
            MainActivity.months.add(www);
            MainActivity.weeks1.add(www);
        }
    }

    public static void replaceFragmentFun(final Fragment fragment, MainActivity activity) {
        String bucket = fragment.getClass().getSimpleName();
        if (bucket.equalsIgnoreCase(Paragraph.class.getSimpleName())) {
            MainActivity.activityMainBinding.ivQuiz.setVisibility(View.GONE);
            MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
            activity.getSupportFragmentManager().beginTransaction().show(MainActivity.paragraph).commitAllowingStateLoss();
            if (MainActivity.quizFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.quizFragment).commitAllowingStateLoss();
            }
            if (MainActivity.videoFragment1 != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.videoFragment1).commitAllowingStateLoss();
            }
            if (MainActivity.learnFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.learnFragment).commitAllowingStateLoss();
            }
            if (MainActivity.settingFRagment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.settingFRagment).commitAllowingStateLoss();
            }
        } else if (bucket.equalsIgnoreCase(QuizFragment.class.getSimpleName())) {
            MainActivity.activityMainBinding.ivQuiz.setVisibility(View.VISIBLE);
            activity.getSupportFragmentManager().beginTransaction().show(MainActivity.quizFragment).commitAllowingStateLoss();
            if (MainActivity.paragraph != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.paragraph).commitAllowingStateLoss();
            }
            if (MainActivity.videoFragment1 != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.videoFragment1).commitAllowingStateLoss();
            }
            if (MainActivity.learnFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.learnFragment).commitAllowingStateLoss();
            }
            if (MainActivity.settingFRagment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.settingFRagment).commitAllowingStateLoss();
            }
        } else if (bucket.equalsIgnoreCase(VideoFragment1.class.getSimpleName())) {
            activity.getSupportFragmentManager().beginTransaction().show(MainActivity.videoFragment1).commitAllowingStateLoss();
            if (MainActivity.paragraph != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.paragraph).commitAllowingStateLoss();
            }
            if (MainActivity.quizFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.quizFragment).commitAllowingStateLoss();
            }
            if (MainActivity.learnFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.learnFragment).commitAllowingStateLoss();
            }
            if (MainActivity.settingFRagment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.settingFRagment).commitAllowingStateLoss();
            }
        } else if (bucket.equalsIgnoreCase(LearnFragment.class.getSimpleName())) {
            activity.getSupportFragmentManager().beginTransaction().show(MainActivity.learnFragment).commitAllowingStateLoss();
            if (MainActivity.paragraph != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.paragraph).commitAllowingStateLoss();
            }
            if (MainActivity.quizFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.quizFragment).commitAllowingStateLoss();
            }
            if (MainActivity.videoFragment1 != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.videoFragment1).commitAllowingStateLoss();
            }
            if (MainActivity.settingFRagment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.settingFRagment).commitAllowingStateLoss();
            }
        } else if (bucket.equalsIgnoreCase(SettingFRagment.class.getSimpleName())) {
            activity.getSupportFragmentManager().beginTransaction().show(MainActivity.settingFRagment).commitAllowingStateLoss();
            if (MainActivity.paragraph != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.paragraph).commitAllowingStateLoss();
            }
            if (MainActivity.quizFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.quizFragment).commitAllowingStateLoss();
            }
            if (MainActivity.videoFragment1 != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.videoFragment1).commitAllowingStateLoss();
            }
            if (MainActivity.learnFragment != null) {
                activity.getSupportFragmentManager().beginTransaction().hide(MainActivity.learnFragment).commitAllowingStateLoss();
            }
        }
    }

    public static void setZero(MainActivity activity) {
        SharePrefrence.getInstance(activity).prefDeleteKey(Utills.POSI_VIEWPAGER_PARA);
        SharePrefrence.getInstance(activity).prefDeleteKey(Utills.POSI_VIEWPAGER_WORD);
        SharePrefrence.getInstance(activity).prefDeleteKey(Utills.POSI_VIEWPAGER_QUIZ);
    }
}
