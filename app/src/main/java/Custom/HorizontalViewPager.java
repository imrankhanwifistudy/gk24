package Custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Interpolator;

import androidx.viewpager.widget.ViewPager;

import java.lang.reflect.Field;

public class HorizontalViewPager extends ViewPager {
    protected int focusPosition;
    protected float focusedItemHeight, defaultItemHeight, flingScrollSpeedFactor;
    protected float minimumChange;
    private float   lastX;
    /** if the first swipe was from left to right (->), dont listen to swipes from the right */
    private boolean slidingLeft;
    /** if the first swipe was from right to left (<-), dont listen to swipes from the left */
    private boolean slidingRight;
    public HorizontalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        postInitViewPager();
    }
    private ScrollerCustomDuration mScroller = null;
    /**
     * Override the Scroller instance with our own class so we can change the
     * duration
     */
    private void postInitViewPager() {
        try {
            Field scroller = ViewPager.class.getDeclaredField("mScroller");
            scroller.setAccessible(true);
            Field interpolator = ViewPager.class.getDeclaredField("sInterpolator");
            interpolator.setAccessible(true);
            mScroller = new ScrollerCustomDuration(getContext(), (Interpolator) interpolator.get(null));
            scroller.set(this, mScroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Set the factor by which the duration will change
     */
   /* public void setScrollDurationFactor(double scrollFactor) {
        mScroller.setScrollDurationFactor(scrollFactor);
    }*/
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(final MotionEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow parent ViewPager to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(true);
                // save the current x position
                this.lastX = ev.getX();
                break;
            case MotionEvent.ACTION_UP:
                // Allow parent ViewPager to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(false);
                // save the current x position
                this.lastX = ev.getX();
                // reset swipe actions
                this.slidingLeft = false;
                this.slidingRight = false;
                break;
            case MotionEvent.ACTION_MOVE:
                /*
                 * if this is the first item, scrolling from left to
                 * right should navigate in the surrounding ViewPager
                 */
                if (this.getCurrentItem() == 0) {
                    // swiping from left to right (->)?
                    if (this.lastX <= ev.getX() && !this.slidingRight) {
                        // make the parent touch interception active -> parent pager can swipe
                        this.getParent().requestDisallowInterceptTouchEvent(false);
                    } else {
                        /*
                         * if the first swipe was from right to left, dont listen to swipes
                         * from left to right. this fixes glitches where the user first swipes
                         * right, then left and the scrolling state gets reset
                         */
                        this.slidingRight = true;
                        // save the current x position
                        this.lastX = ev.getX();
                        this.getParent().requestDisallowInterceptTouchEvent(true);
                    }
                } else
                /*
                 * if this is the last item, scrolling from right to
                 * left should navigate in the surrounding ViewPager
                 */
                    if (this.getCurrentItem() == this.getAdapter().getCount() - 1) {
                        // swiping from right to left (<-)?
                        if (this.lastX >= ev.getX() && !this.slidingLeft) {
                            // make the parent touch interception active -> parent pager can swipe
                            this.getParent().requestDisallowInterceptTouchEvent(false);
                        } else {
                        /*
                         * if the first swipe was from left to right, dont listen to swipes
                         * from right to left. this fixes glitches where the user first swipes
                         * left, then right and the scrolling state gets reset
                         */
                            this.slidingLeft = true;
                            // save the current x position
                            this.lastX = ev.getX();
                            this.getParent().requestDisallowInterceptTouchEvent(true);
                        }
                    }
                break;
        }
        super.onTouchEvent(ev);
        return true;
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
    }
}