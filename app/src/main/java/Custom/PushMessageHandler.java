package Custom;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class PushMessageHandler {
    @SuppressLint("StaticFieldLeak")
    private static Context ctx;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static synchronized void readMessage(Context ctx1, Map<String, String> remoteMessage) {
        synchronized (PushMessageHandler.class) {
            ctx = ctx1;
            Bitmap image = null;
            String post_type="",img="",title="",post_id="",week="",lang="",calltolink="",calltoaction="";
            String msg="";
            String notification_id="0"+remoteMessage.get("notification_id").replace("null","");
            if (remoteMessage.containsKey("text")) {
                msg=remoteMessage.get("text");
            }
            if (remoteMessage.containsKey("post_type")) {
                post_type=remoteMessage.get("post_type");
            }
            if (remoteMessage.containsKey("image")) {
                img = remoteMessage.get("image");
            }
            if (remoteMessage.containsKey("message")) {
                title =  remoteMessage.get("message");
            }
            if (remoteMessage.containsKey("post_id")) {
                post_id=remoteMessage.get("post_id");
            }
            if (remoteMessage.containsKey("week")) {
                week=remoteMessage.get("week");
            }
            if (remoteMessage.containsKey("lang")) {
                lang=remoteMessage.get("lang");
            }
            if (remoteMessage.containsKey("calltolink")) {
                calltolink=remoteMessage.get("calltolink");
            }
            if (remoteMessage.containsKey("calltoaction")) {
                calltoaction=remoteMessage.get("calltoaction");
            }
            try {
                if(img.equals("")){
                    sendNotification_withoutimg(img, msg, image,title,post_id,post_type,week,lang,
                            calltolink,calltoaction,notification_id);
                }else {
                    image = getBitmapFromURL(img);
                    sendNotification(img, msg, image,title,post_id,post_type,week,lang,calltolink,
                            calltoaction,notification_id);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static synchronized void sendNotification(String tag, String messageBody, Bitmap img, String title, String post_id, String post_type, String week,
                                                      String lang, String calltolink, String calltoaction, String notification_id) throws Exception {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("post_id", post_id);
        bundle.putString("post_type",post_type);
        bundle.putString("week",week);
        bundle.putString("lang",lang);
        intent.putExtra("calltolink",calltolink);
        intent.putExtra("calltoaction",calltoaction);
        intent.putExtras(bundle);
        intent.putExtra("IntentLocation", "true");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP|
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_NO_HISTORY );

        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        try {
            if (Build.VERSION.SDK_INT < 16) {
                        NotificationCompat.Builder n  = new NotificationCompat.Builder(ctx);
                        n.setContentTitle(title)
                        .setContentText(messageBody)
                        .setColor(0xE61A5F)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.ic_launcher))
                        .setStyle(new  NotificationCompat.BigPictureStyle().bigPicture(img).setSummaryText(messageBody))
                        .setVibrate(new long[] { 100, 100, 100, 100})
                        .setLights(0xffff7700, 300, 1000)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .getNotification();
                NotificationManager notificationManager =
                        (NotificationManager)ctx. getSystemService(Context.NOTIFICATION_SERVICE);
                assert notificationManager != null;
                notificationManager.notify(Integer.parseInt(notification_id), n.build());

            }
            else {
                NotificationCompat.Builder n  = new NotificationCompat.Builder(ctx);
                n .setContentTitle(title)
                        .setContentText(messageBody)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(),R.mipmap.ic_launcher))
                        .setStyle(new  NotificationCompat.BigPictureStyle().bigPicture(img).setSummaryText(messageBody))
                        .setVibrate(new long[] { 100, 100, 100, 100})
                        .setLights(0xffff7700, 300, 1000)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();

                NotificationManager notificationManager =
                        (NotificationManager)ctx. getSystemService(Context.NOTIFICATION_SERVICE);
                assert notificationManager != null;
                notificationManager.notify(Integer.parseInt(notification_id), n.build());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static void sendNotification_withoutimg(String tag, String messageBody, Bitmap img, String title, String post_id, String post_type, String week,
                                                    String lang, String calltolink, String calltoaction, String notification_id) throws Exception {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("post_id", post_id);
        bundle.putString("post_type",post_type);
        bundle.putString("week",week);
        bundle.putString("lang",lang);
        intent.putExtra("calltolink",calltolink);
        intent.putExtra("calltoaction",calltoaction);
        intent.putExtras(bundle);
        intent.putExtra("IntentLocation", "true");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP|
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_NO_HISTORY );
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        try {
            if (Build.VERSION.SDK_INT < 16) {
                NotificationCompat.Builder n  = new NotificationCompat.Builder(ctx);
                n.setContentTitle(title)
                        .setContentText(messageBody)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.ic_launcher))
                        .setVibrate(new long[] { 100, 100, 100, 100})
                        .setLights(0xffff7700, 300, 1000)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .getNotification();
                NotificationManager notificationManager =
                        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                assert notificationManager != null;
                notificationManager.notify(Integer.parseInt(notification_id), n.build());
            }
            else {
                NotificationCompat.Builder n  = new NotificationCompat.Builder(ctx);
                n.setContentTitle(title)
                        .setContentText(messageBody)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.ic_launcher))
                        .setVibrate(new long[] { 100, 100, 100, 100})
                        .setLights(0xffff7700, 300, 1000)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                NotificationManager notificationManager =
                        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                assert notificationManager != null;
                notificationManager.notify(Integer.parseInt(notification_id), n.build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Nullable
    private static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}