package Slider.SliderTypes;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.RenderTypeTextBinding;

public class TextSliderView extends BaseSliderView {
    public TextSliderView(Context context) {
        super(context);
    }
    @Override
    public View getView() {
        RenderTypeTextBinding binding= DataBindingUtil.inflate(LayoutInflater.from(getContext()),R.layout.render_type_text,null,false);
        binding.description.setText(getDescription());
        binding.heading.setText(geHeading());
        binding.title.setText(getTitle());
        bindEventAndShow(binding.getRoot(), binding.daimajiaSliderImage);
        return binding.getRoot();
    }
}