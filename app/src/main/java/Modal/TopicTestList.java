package Modal;

import org.json.JSONArray;

import java.io.Serializable;

public class TopicTestList implements Serializable {

    private String id;
    private String name;
    private String noofQuestion;
    private String Price ;
    private String Offer_Price;
    private String time;
    private String positive;
    private String stauts;
    private String negative;
    private String date,type,test_size;
    private JSONArray array;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getOffer_Price() {
        return Offer_Price;
    }

    public void setOffer_Price(String offer_Price) {
        Offer_Price = offer_Price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTest_size() {
        return test_size;
    }

    public void setTest_size(String test_size) {
        this.test_size = test_size;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStauts() {
        return stauts;
    }

    public void setStauts(String stauts) {
        this.stauts = stauts;
    }

    public String getNegative() {
        return negative;
    }

    public void setNegative(String negative) {
        this.negative = negative;
    }

    public String getPositive() {
        return positive;
    }

    public void setPositive(String positive) {
        this.positive = positive;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public JSONArray getArray() {
        return array;
    }

    public void setArray(JSONArray array) {
        this.array = array;
    }

    public String getNoofQuestion() {
        return noofQuestion;
    }

    public void setNoofQuestion(String noofQuestion) {
        this.noofQuestion = noofQuestion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



}
