package Modal;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dlb imarn on 10/4/2016.
 */
public class DateItem implements Serializable {


    private String id;
    private String StatusQuizVocab;
    private String VocabExist;
    private String title;
    private String courtsy;
    private String Images;
    private String time;
    private String call_action;
    private String call_link,dataobject,total_like;
    private String catId,week_no,pdf,title_h,paragraph_h,TypePrime,cap_type;

    public String getCap_type() {
        return cap_type;
    }

    public void setCap_type(String cap_type) {
        this.cap_type = cap_type;
    }

    public String getTotal_like() {
        return total_like;
    }

    public void setTotal_like(String total_like) {
        this.total_like = total_like;
    }

    public String getDataobject() {
        return dataobject;
    }

    public void setDataobject(String dataobject) {
        this.dataobject = dataobject;
    }

    public String getTypePrime() {
        return TypePrime;
    }

    public void setTypePrime(String typePrime) {
        TypePrime = typePrime;
    }

    public String getTitle_h() {
        return title_h;
    }

    public void setTitle_h(String title_h) {
        this.title_h = title_h;
    }

    public String getParagraph_h() {
        return paragraph_h;
    }

    public void setParagraph_h(String paragraph_h) {
        this.paragraph_h = paragraph_h;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getWeek_no() {
        return week_no;
    }

    public void setWeek_no(String week_no) {
        this.week_no = week_no;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCall_link() {
        return call_link;
    }

    public void setCall_link(String call_link) {
        this.call_link = call_link;
    }

    public String getCall_action() {
        return call_action;
    }

    public void setCall_action(String call_action) {
        this.call_action = call_action;
    }

    private String subCatId;

    private String ParagraphCapsule ;
    public String getParagraphCapsule() {
        return ParagraphCapsule;
    }
    public void setParagraphCapsule(String paragraph) {
        ParagraphCapsule = paragraph;
    }


    private String date;
    private int CountVocabWord;
    private JSONObject JsonCapsule;

    public JSONObject getJsonCapsule() {
        return JsonCapsule;
    }

    public void setJsonCapsule(JSONObject jsonCapsule) {
        JsonCapsule = jsonCapsule;
    }

    public int getCountVocabWord() {
        return CountVocabWord;
    }

    public void setCountVocabWord(int countVocabWord) {
        CountVocabWord = countVocabWord;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCourtsy() {
        return courtsy;
    }

    public void setCourtsy(String courtsy) {
        this.courtsy = courtsy;
    }

    public String getImages() {
        return Images;
    }

    public void setImages(String Images) {
        this.Images = Images;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVocabExist() {
        return VocabExist;
    }

    public void setVocabExist(String vocabExist) {
        VocabExist = vocabExist;
    }

    public String getStatusQuizVocab() {
        return StatusQuizVocab;
    }

    public void setStatusQuizVocab(String statusQuizVocab) {
        StatusQuizVocab = statusQuizVocab;
    }
    int VocabCount;

    public int getVocabCount() {
        return VocabCount;
    }

    public void setVocabCount(int vocabCount) {
        VocabCount = vocabCount;
    }
}
