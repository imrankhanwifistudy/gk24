package Modal;

import android.text.Html;

import java.io.Serializable;

public class QuizModel implements Serializable {
    private String title,date,CatID,Price,Offer_Price,stauts,id,headings,note,images,views;

    public String getHeadings() {
        return headings;
    }
    public void setHeadings(String headings) {
        this.headings = headings;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = String.valueOf(Html.fromHtml(note));
    }
    public String getImages() {
        return images;
    }
    public void setImages(String images) {
        this.images = images;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPrice() {
        return Price;
    }
    public void setPrice(String price) {
        Price = price;
    }
    public String getOffer_Price() {
        return Offer_Price;
    }
    public void setOffer_Price(String offer_Price) {
        Offer_Price = offer_Price;
    }
    public String getStauts() {
        return stauts;
    }
    public void setStauts(String stauts) {
        this.stauts = stauts;
    }
    public String getSubCatID() {
        return SubCatID;
    }
    public void setSubCatID(String subCatID) {
        SubCatID = subCatID;
    }
    public String getCatID() {
        return CatID;
    }
    public void setCatID(String catID) {
        CatID = catID;
    }
    private String SubCatID;
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getViews() {
        return views;
    }
    public void setViews(String views) {
        this.views = views;
    }
}