package Modal;

import java.io.Serializable;


public class FreeTestItem implements Serializable {
    private String direction;
    private String questionenglish;
    private String directionhindi;
    private String questionhindi;
    private String opt_en_1;
    private String op_hi_1;
    private String op_en_2;
    private String op_hi2;
    private String op_en_3;
    private String op_hi_3;
    private String op_en_4;
    private String op_hi_4;
    private String op_en_5;
    private String op_hi_5;
    private String answer;
    private String solutionenglish;
    private String solutionhindi;
    private String noofoption;
    private String totalquestio;
    private String time;
    private String correctmarks;
    private String wrongmarks;
    private String no_of_attempt,id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniq() {
        return uniq;
    }

    public void setUniq(String uniq) {
        this.uniq = uniq;
    }

    private String uniq;

    public FreeTestItem() {
    }

    public FreeTestItem(String direction, String questionenglish, String directionhindi, String questionhindi, String opt_en_1, String op_hi_1,
                        String op_en_2, String op_hi2, String op_en_3, String op_hi_3, String op_en_4, String op_hi_4,
                        String  op_en_5, String op_hi_5, String answer, String solutionenglish, String solutionhindi, String noofoption,
                        String totalquestio, String time, String correctmarks, String wrongmarks, String no_of_attempt) {
        this.direction = direction;
        this.questionenglish = questionenglish;
        this.directionhindi=directionhindi;
        this.questionhindi=questionhindi;
        this.opt_en_1=opt_en_1;
        this.op_hi_1=op_hi_1;
        this.op_en_2=op_en_2;
        this.op_hi2=op_hi2;
        this.op_en_3=op_en_3;
        this.op_hi_3=op_hi_3;
        this.op_en_4=op_en_4;
        this.op_hi_4=op_hi_4;
        this.op_en_5=op_hi_5;
        this.answer=answer;
        this.solutionenglish=solutionenglish;
        this.solutionhindi=solutionhindi;
        this.noofoption=noofoption;
        this.totalquestio=totalquestio;
        this.time=time;
        this.correctmarks=correctmarks;
        this.wrongmarks=wrongmarks;
        this.no_of_attempt=no_of_attempt;


    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirectionhindi() {
        return directionhindi;
    }

    public void setDirectionhindi(String directionhindi) {
        this.directionhindi = directionhindi;
    }


    public String getQuestionenglish() {
        return questionenglish;
    }

    public void setQuestionenglish(String questionenglish) {
        this.questionenglish = questionenglish;
    }

    public  String getQuestionhindi(){
        return questionhindi;
    }
    public void setQuestionhindi(String questionhindi){
        this.questionhindi=questionhindi;
    }
    public String getOpt_en_1(){
        return opt_en_1;
    }
    public  void setOpt_en_1(String opt_en_1){
        this.opt_en_1=opt_en_1;
    }
    public String getOp_hi_1(){
        return op_hi_1;
    }
    public void setOp_hi_1(String op_hi_1){
        this.op_hi_1=op_hi_1;
    }
    public String getOp_en_2(){
        return op_en_2;
    }
    public void setOp_en_2(String op_en_2){
        this.op_en_2=op_en_2;
    }
    public String getOp_hi2(){
        return op_hi2;
    }
    public void setOp_hi2(String op_hi2){
        this.op_hi2=op_hi2;
    }
    public String getOp_en_3(){
        return op_en_3;
    }
    public void setOp_en_3(String op_en_3){
        this.op_en_3=op_en_3;
    }
    public String getOp_hi_3(){
        return op_hi_3;
    }
    public void setOp_hi_3(String op_hi_3){
        this.op_hi_3=op_hi_3;
    }
    public String getOp_en_4(){
        return op_en_4;
    }
    public void setOp_en_4(String op_en_4){
        this.op_en_4=op_en_4;
    }
    public String getOp_hi_4(){
        return op_hi_4;
    }
    public void setOp_hi_4(String op_hi_4){
        this.op_hi_4=op_hi_4;
    }
    public String getOp_en_5(){
        return op_en_5;
    }
    public void setOp_en_5(String op_en_5){
        this.op_en_5=op_en_5;
    }
    public String getOp_hi_5(){
        return op_hi_5;
    }
    public void setOp_hi_5(String op_hi_5){
        this.op_hi_5=op_hi_5;
    }
    public String getAnswer(){
        return answer;
    }
    public void setAnswer(String answer){
        this.answer=answer;
    }
    public String getSolutionenglish(){
        return solutionenglish;
    }
    public void setSolutionenglish(String solutionenglish){
        this.solutionenglish=solutionenglish;
    }
    public String getSolutionhindi(){
        return solutionhindi;
    }
    public void setSolutionhindi(String solutionhindi){
        this.solutionhindi=solutionhindi;
    }
    public String getNoofoption(){
        return noofoption;
    }
    public void setNoofoption(String noofoption){
        this.noofoption=noofoption;
    }
    public String getTotalquestio(){
        return totalquestio;
    }
    public void setTotalquestio(String totalquestio){
        this.totalquestio=totalquestio;
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time=time;
    }
    public String getCorrectmarks(){
        return correctmarks;
    }
    public void setCorrectmarks(String correctmarks){
        this.correctmarks=correctmarks;
    }
    public String getWrongmarks(){
        return wrongmarks;
    }
    public void setWrongmarks(String wrongmarks){
        this.wrongmarks=wrongmarks;
    }

    public String getNo_of_attempt(){
        return no_of_attempt;
    }
    public void setNo_of_attempt(String no_of_attempt){
        this.no_of_attempt=no_of_attempt;
    }
}


