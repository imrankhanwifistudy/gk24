package SlimAdapterModal;

import Modal.DateItem;

public class User {
    private DateItem item;

    public User(DateItem item) {
        this.item = item;
    }

    public DateItem getItem() {
        return item;
    }
}