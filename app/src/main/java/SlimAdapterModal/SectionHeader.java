package SlimAdapterModal;

public class SectionHeader {
    private String date;

    public SectionHeader(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}