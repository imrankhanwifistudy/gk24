package Adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.BookmarkActivity;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.YoutubeActivity;
import com.daily.currentaffairs.databinding.ParagraphAdapterBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ClickListener.ParagraphClickListner;
import Custom.DoubleClickListener;
import Custom.HorizontalViewPager;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.BookParaItem;
import Modal.FreeTestItem;
import fragment.QuizFragment;

public class BookParaPagerAdapter extends PagerAdapter {
    private static final String TAG = BookParaPagerAdapter.class.getSimpleName();
    Activity activity;
    private ArrayList<BookParaItem> testitem;
    DatabaseHandler db;
    private String user_id;
    private ArrayList<FreeTestItem> testitem2;
    private String   total_question,   attempt ;
    private HorizontalViewPager Horizental_pager;
    public BookParaPagerAdapter(Activity activity, ArrayList<BookParaItem> testitem, HorizontalViewPager pager) {
        this.activity = activity;
        this.testitem = testitem;
        Horizental_pager = pager;
        db = new DatabaseHandler(activity);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        user_id= sharedPreferences.getString("uid","");
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @SuppressLint("InflateParams")
    @Override
    public Object instantiateItem(View collection, final int position) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ParagraphAdapterBinding binding= DataBindingUtil.inflate(inflater,R.layout.paragraph_adapter,null,false);
        binding.rlUpvote.setVisibility(View.GONE);
        binding.totalUpvotes.setVisibility(View.GONE);
        binding.bookMark.setVisibility(View.VISIBLE);
        binding.footerLayout.setVisibility(View.VISIBLE);
        binding.shareImage.setVisibility(View.GONE);
        binding.learnVocab.setVisibility(View.GONE);
        binding.bookmarkbootm.setVisibility(View.GONE);
        binding.share.setVisibility(View.GONE);
        binding.report.setVisibility(View.GONE);
        binding.theme.setVisibility(View.GONE);
        Drawable drawable1=activity.getResources().getDrawable(R.drawable.language_pref_black).mutate();
        drawable1.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#0287c3"), PorterDuff.Mode.SRC_IN));
        binding.langPref.setImageDrawable(drawable1);
        try{
            paradata(testitem.get(position).getDesc(),binding,position);
        }catch (Exception e){
            e.printStackTrace();
    }
    binding.setClick(new ParagraphClickListner(activity,binding,testitem,Horizental_pager));
       ((HorizontalViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((HorizontalViewPager) collection).removeView((View) view);
    }
    @SuppressLint("SetJavaScriptEnabled")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void paradata(String desc, final ParagraphAdapterBinding binding, int position){
        String Paragraph;
        try {
            final JSONObject object = new JSONObject(desc);
            Glide.with(activity).load(object.getString("images")).thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.image);
            if(! BookmarkActivity.lang_flag){
                binding.meaniniTv.setText(Html.fromHtml(object.optString("title")));
                Paragraph = object.getString("paragraph");
            }else{
                binding.meaniniTv .setText(Html.fromHtml(object.optString("title_h")));
                Paragraph = object.getString("paragraph_h");
            }
            Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
            Matcher m = REMOVE_TAGS.matcher(Paragraph.replace("<p>", "").replace("</p>", ""));
            final String ss = m.replaceAll("").replace(".", ". ");
            binding.paragraph.clearCache(true);
            binding.paragraph.clearView();
            String str = "<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
            if (! BookmarkActivity.lang_flag) {
                String theme1= SharePrefrence.getInstance(activity).getString("Themes");
                switch (theme1) {
                    case "night":
                        binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' onclick='wordClicked(this)'>" + ss + "</span> ";
                        break;
                    case "sepia":
                        binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                        break;
                    default:
                        binding.paragraph.setBackgroundColor(Color.parseColor("#ffffff"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                        break;
                }
            }else {
                String theme1= SharePrefrence.getInstance(activity).getString("Themes");
                switch (theme1) {
                    case "night":
                        binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                        break;
                    case "sepia":
                        binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss + "</span> ";
                         break;
                    default:
                        binding.paragraph.setBackgroundColor(Color.WHITE);
                        str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" + "</span> ";
                        break;
                }
            }
            binding.paragraph.loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
            binding.paragraph.getSettings().setJavaScriptEnabled(true);
            binding.paragraph.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            binding.paragraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
            binding.paragraph.getSettings().setAppCacheEnabled(false);
            binding.paragraph.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            binding.paragraph.getSettings().setBuiltInZoomControls(false);
            if (Build.VERSION.SDK_INT >= 19)
                WebView.setWebContentsDebuggingEnabled(true);
            binding.mainWord.setText(Html.fromHtml(object.getString("headings")));
            if (testitem.size() > 1) {
                SpannableString spannable = new SpannableString(position + 1 + "/" + testitem.size());
                StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                spannable.setSpan(bss, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                binding.pageNumber.setText( spannable);

            } else {
                binding.pageNumber.setText("");
            }
            binding.linearLayoutOne.setOnClickListener(new DoubleClickListener() {
                @Override
                public void onSingleClick(View v) {
                }
                @Override
                public void onDoubleClick(View v) {
                    try{
                        boolean isBookMark = db.checkBookPara(object.optString("active_date") + object.optString("id"));
                        if (isBookMark) {
                            db.deleteBookPara(object.optString("active_date") + object.optString("id"));
                            Utils.unbookmark(object.optString("id"),"0",activity,user_id);
                            Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                            testitem.remove( Horizental_pager.getCurrentItem());
                            notifyDataSetChanged();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            String calltoaction = object.getString("calltoaction");
            String linkofaction = object.getString("calltoaction_link");
            final String  catID = object.getString("catid");
            if (calltoaction.trim().length() > 0 && linkofaction.trim().length() > 0) {
                calltoaction=calltoaction+",";
                linkofaction=linkofaction+",";
                final String[] linkArray ,linkname;
                linkArray = calltoaction.split(",");
                linkname=linkofaction.split(",");
                binding.llLink.setOrientation(LinearLayout.VERTICAL);
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                binding.llLink.setLayoutParams(params1);
                for( int j = 0; j < linkArray.length; j++ ) {
                    TextView textView = new TextView(activity);
                    textView.setText(linkArray[j]);
                    textView.setGravity(Gravity.CENTER);
                    textView.setTextSize(15);
                    textView.setTextColor(Color.parseColor("#ffffff"));
                    textView.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.back_quiz_start));
                    textView.setPadding(30, 30,30, 30);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20,20,20,20);
                    params.setLayoutDirection(Gravity.CENTER_HORIZONTAL);
                    textView.setLayoutParams(params);
                    textView.setId(j);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int t = v.getId();
                            try {
                                    if (linkArray[t].trim().equalsIgnoreCase("View Video")) {
                                        Intent intent = new Intent(activity, YoutubeActivity.class);
                                        intent.putExtra("url", linkname[t]);
                                        activity.startActivity(intent);
                                    }else if (linkArray[t].trim().equalsIgnoreCase("share now")) {
                                        Utils.ShareDialog(activity);
                                    }else if (linkArray[t].trim().equalsIgnoreCase("start test")&& catID.trim().length() == 0) {
                                        activity.finish();
                                        MainActivity myActivity = MainActivity.activity;
                                        QuizFragment fragment = new QuizFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("position", 0);
                                        FragmentManager fragmentManager = myActivity.getSupportFragmentManager();
                                        FragmentTransaction ft = fragmentManager.beginTransaction();
                                        fragment.setArguments(bundle);
                                        ft.replace(R.id.continar, fragment);
                                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                        ft.commitAllowingStateLoss();
                                        MainActivity.activityMainBinding.txtquiz.setAlpha(1);
                                        MainActivity.activityMainBinding.txteditorial.setAlpha(Float.parseFloat("0.5"));
                                        MainActivity.activityMainBinding.txtsetting.setAlpha(Float.parseFloat("0.5"));
                                        MainActivity.activityMainBinding.txtvocab.setAlpha(Float.parseFloat("0.5"));
                                        MainActivity.activityMainBinding.txtlearn.setAlpha(Float.parseFloat("0.5"));
                                    }else if (linkArray[t].trim().equalsIgnoreCase("Start Quiz")) {
                                        if(Utils.hasConnection(activity)){
                                            Get_Catagery(linkname[t]);
                                        }else{
                                            Toast.makeText(activity,"Please check internet connection",Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        if(!linkname[t].trim().equalsIgnoreCase("")){
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkname[t]));
                                            activity.startActivity(browserIntent);
                                        }
                                    }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    binding.llLink.addView(textView);
                }
            } else{
                binding.llLink.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void Get_Catagery(final String quiz_post_id) {
        testitem2=new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        final String test_name = year +quiz_post_id +
                "TopicTest"+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE);
        AndroidNetworking.post(Utills.BASE_URLGK +"gk-questions.php")
                .addBodyParameter("testid", quiz_post_id)
                .addBodyParameter("lang", SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG , String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            total_question = obj.getString("total_question");
                            attempt = obj.getString("attemp");
                            if (success == 1) {
                                JSONArray array = obj.getJSONArray("response");
                                Log.e("question", "rec" + array.length());
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsobj = array.getJSONObject(i);
                                    FreeTestItem item = new FreeTestItem();
                                    item.setId(jsobj.getString("id"));
                                    item.setDirection(jsobj.getString("direction"));
                                    item.setQuestionenglish(jsobj.getString("question"));
                                    item.setOpt_en_1(jsobj.getString("optiona"));
                                    item.setOp_en_2(jsobj.getString("optionb"));
                                    item.setOp_en_3(jsobj.getString("optionc"));
                                    item.setOp_en_4(jsobj.getString("optiond"));
                                    if (jsobj.has("optione")) {
                                        item.setOp_en_5(jsobj.getString("optione"));
                                    } else {
                                        item.setOp_en_5("");
                                    }
                                    item.setAnswer(jsobj.getString("correct_answer"));
                                    item.setSolutionenglish(jsobj.getString("explaination"));
                                    item.setNoofoption(jsobj.getString("noofoption"));
                                    item.setTime(String.valueOf((int) Math.ceil(Double.parseDouble(total_question)*2)));
                                    item.setTotalquestio(total_question);
                                    item.setCorrectmarks("1");
                                    item.setWrongmarks("0.25");
                                    item.setNo_of_attempt(attempt);
                                    testitem2.add(item);
                                }
                                try {
                                    boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                    if (!isData) {
                                        db.addQuizRes(test_name, String.valueOf(response), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (success == 0) {
                                if(progressDialog != null && activity != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                Toast.makeText(activity, "Internal error ocured", Toast.LENGTH_LONG).show();
                            }
                            try {
                                db.addreadUnread(test_name, "TopicTest");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(activity, QuestionQuiz.class);
                            intent.putExtra("TopicWiseTest", "TopicWiseTest");
                            intent.putExtra("language", "english");
                            intent.putExtra("testitem", testitem2);
                            intent.putExtra("catname", "");
                            intent.putExtra("testname", test_name);
                            intent.putExtra("correctmark", "1");
                            intent.putExtra("wrongmark", "0.25");
                            intent.putExtra("totalque", total_question);
                            intent.putExtra("time", String.valueOf((int) Math.ceil(Double.parseDouble(total_question)*2)));
                            intent.putExtra("topictest", true);
                            intent.putExtra("UID", test_name);
                            intent.putExtra("RANKUID", quiz_post_id);
                            intent.putExtra("PractiseTestName", "");
                            intent.putExtra("TestTitleName", "");
                            activity.startActivityForResult(intent, 1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if(progressDialog != null && activity != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(progressDialog != null && activity != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                    }
                });
    }
}