package Adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.EditorialActivity;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.YoutubeActivity;
import com.daily.currentaffairs.databinding.ParagraphAdapterBinding;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ClickListener.ParagraphClickListner;
import Custom.DoubleClickListener;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;
import Modal.FreeTestItem;

public class ParagraphPagerAdapter extends PagerAdapter {
    final Activity activity;
    private ArrayList<DateItem> testitem;
    private ViewPager pagerMain;
    DatabaseHandler db;
    private long lastClickTime = 0;
    private long DOUBLE_CLICK_TIME_DELTA = 300;
    private SharedPreferences.Editor editor;
    private int positionn ,date_position;
    private ArrayList<FreeTestItem> testitem2;
    private String   total_question,   attempt;
    private String user_id="";
    String text;
    ParagraphAdapterBinding binding;
    @SuppressLint("CommitPrefEdits")
    public ParagraphPagerAdapter(Activity activity, ArrayList<DateItem> testitem, ViewPager pagerMain, int positionn, int date_position) {
        SharedPreferences spfd = PreferenceManager.getDefaultSharedPreferences(activity);
        user_id= spfd.getString("uid","");
        editor = spfd.edit();
        this.activity = activity;
        this.testitem = testitem;
        this.pagerMain = pagerMain;
        this.positionn = positionn;
        this.date_position = date_position;
        db = new DatabaseHandler(activity);
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public Object instantiateItem(View collection, final int position) {
        String theme2=SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding= DataBindingUtil.inflate(inflater,R.layout.paragraph_adapter,null,false);
        binding.rlUpvote.setVisibility(View.VISIBLE);
        binding.langPref.setVisibility(View.VISIBLE);
        binding.bookmarkbootm.setVisibility(View.GONE);
        binding.share.setVisibility(View.GONE);
        final String id = testitem.get(position).getId();
        final String webDate = testitem.get(position).getDate();
        final String VocabExist = testitem.get(position).getVocabExist();
        if (!VocabExist.equals("1"))
            binding.learnVocab.setVisibility(View.GONE);
        if (positionn == position) {
            String webDatee = testitem.get(position).getDate();
            final String uniqid = webDatee+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + testitem.get(position).getId()+ "ParaRead";
            try{
                String value = db.getReadUnread(uniqid);
                if (value.equals("")) {
                    db.addreadUnread(uniqid, "paragraphRead");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (testitem.size() > 1) {
            SpannableString spannable = new SpannableString(position + 1 + "/" + testitem.size());
            StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            spannable.setSpan(bss, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            binding.pageNumber.setText(spannable);
        } else {
            binding.pageNumber.setText("");
        }
        boolean isBookMark=false;
        final String uniqid = webDate + id;
        try{
            isBookMark = db.checkBookPara(uniqid);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (isBookMark) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_done_small, 0, 0, 0);
                }
            });
        } else {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_small, 0, 0, 0);
                }
            });
        }
        binding.back.setVisibility(View.VISIBLE);
        pagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try{
                    String webDate = testitem.get(position).getDate();
                    final String uniqid = webDate+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + testitem.get(position).getId()+ "ParaRead";
                    String value = db.getReadUnread(uniqid);
                    if (value.equals("")) {
                        db.addreadUnread(uniqid, "paragraphRead");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onPageSelected(int position) {
              try{
                  String webDate = testitem.get(position).getDate();
                  final String uniqid = webDate+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + testitem.get(position).getId() + "ParaRead";
                  String value = db.getReadUnread(uniqid);
                    if (value.equals("")) {
                        db.addreadUnread(uniqid, "paragraphRead");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        DateItem  item = testitem.get(position);
        try {
            parseData(item,  webDate,uniqid,testitem.get(position).getId(),position,binding);
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.setClick(new ParagraphClickListner(binding,activity,pagerMain,testitem,date_position));
        ((ViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void parseData(final DateItem response, final String webdate, final String uniqid, final String id, final int position,
                           final ParagraphAdapterBinding binding) throws JSONException {
        lastClickTime = 0;
        String paragraph_cap;
        final String image = response.getImages();
        final String courtesy = response.getCourtsy();
        String titleee;
        if(! EditorialActivity.lang_flag){
            titleee = response.getTitle();
            paragraph_cap= response.getParagraphCapsule();
        }else{
            titleee = response.getTitle_h();
            paragraph_cap= response.getParagraph_h();
        }
        final String paragraph = paragraph_cap;
        final String titleData=titleee;
        binding.meaniniTv.setText(Html.fromHtml(titleData));
        if (titleData.equalsIgnoreCase(".."))
            binding.learnVocab.setVisibility(View.GONE);
        binding.mainWord.setText(Html.fromHtml(courtesy));
        String calltoaction = response.getCall_action();
        String linkofaction = response.getCall_link();
        Drawable drawable1=Utils.DrawableChange(activity,R.drawable.language_pref_black,"#0287c3");
        binding.langPref.setImageDrawable(drawable1);
        Drawable drawable2=Utils.DrawableChange(activity,R.drawable.ic_brightness_medium_white_24dp,"#0287c3");
        binding.theme.setCompoundDrawablesWithIntrinsicBounds(drawable2,null,null,null);
        if (calltoaction.trim().length() > 0 && linkofaction.trim().length() > 0) {
            calltoaction=calltoaction+",";
            linkofaction=linkofaction+",";
            final String[] linkArray ,linkname;
            linkArray = calltoaction.split(",");
            linkname=linkofaction.split(",");
            binding.llLink.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            binding.llLink.setLayoutParams(params1);
            for( int j = 0; j < linkArray.length; j++ ) {
                TextView textView = new TextView(activity);
                textView.setText(linkArray[j]);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(15);
                textView.setTextColor(Color.parseColor("#ffffff"));
                textView.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.back_quiz_start));
                textView.setPadding(30, 30,30, 30);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(20,20,20,20);
                textView.setLayoutParams(params);
                textView.setId(j);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int t = v.getId();
                        try {
                                 if (linkArray[t].trim().equalsIgnoreCase("Watch Video")) {
                                    Intent intent = new Intent(activity, YoutubeActivity.class);
                                    intent.putExtra("url", linkname[t]);
                                    activity.startActivity(intent);
                                }else if (linkArray[t].trim().equalsIgnoreCase("View Video")) {
                                     Intent intent1 = new Intent(activity, MainActivity.class);
                                     intent1.putExtra("position", "");
                                     intent1.putExtra("position_pager", "");
                                     intent1.putExtra("Fragment", "VideoFragment");
                                     intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                     intent1.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                     intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                     activity.startActivity(intent1);
                                     activity.finish();
                                } else if (linkArray[t].trim().equalsIgnoreCase("share now")) {
                                    Utils.ShareDialog(activity);
                                } else if (linkArray[t].trim().equalsIgnoreCase("start test")) {
                                     Intent intent1 = new Intent(activity, MainActivity.class);
                                     intent1.putExtra("position", date_position);
                                     intent1.putExtra("position_pager", (testitem.get(position).getVocabCount() - 1) * 10);
                                     intent1.putExtra("Fragment", "quiz");
                                     intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                     intent1.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                     intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                     activity.startActivity(intent1);
                                     activity.finish();
                                }else if (linkArray[t].trim().equalsIgnoreCase("Start Quiz")) {
                                    if(Utils.hasConnection(activity)){
                                        Utils.PROGRESS(activity);
                                        Get_Catagery(linkname[t]);
                                    }else{
                                        Toast.makeText(activity,"Please check internet connection",Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    if(!linkname[t].trim().equalsIgnoreCase("")){
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkname[t]));
                                        activity.startActivity(browserIntent);
                                    }
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                binding.llLink.addView(textView);
            }
        }else{
            binding.llLink.setVisibility(View.GONE);
        }
        boolean swImage = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_SWITCH);
        if (swImage) {
            Glide.with(activity).load(image).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.image);
        }
        Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
        Matcher m = REMOVE_TAGS.matcher(paragraph.replace("<p>", "").replace("</p>", ""));
        final String ss = m.replaceAll("").replace(".", ". ");
        binding.paragraph.clearCache(true);
        binding.paragraph.clearView();
        String str = "<html> <head> <title>Career Pathshala</title> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
        if (! EditorialActivity.lang_flag) {
            String theme= SharePrefrence.getInstance(activity).getString("Themes");
            switch (theme) {
                case "night":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                    break;
                case "sepia":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                    break;
                default:
                    binding.paragraph.setBackgroundColor(Color.parseColor("#ffffff"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                    break;
            }
        }else {
            String theme= SharePrefrence.getInstance(activity).getString("Themes");
            switch (theme) {
                case "night":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                    break;
                case "sepia":
                    binding.paragraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss + "</span> ";
                    break;
                default:
                    binding.paragraph.setBackgroundColor(Color.WHITE);
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;'>" +ss + "</span> ";
                    break;
            }
        }
        binding.paragraph.loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
        binding.paragraph.getSettings().setJavaScriptEnabled(true);
        binding.paragraph.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        binding.paragraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        binding.paragraph.getSettings().setAppCacheEnabled(false);
        binding.paragraph.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        binding.paragraph.getSettings().setBuiltInZoomControls(false);
        if (Build.VERSION.SDK_INT >= 19)
            WebView.setWebContentsDebuggingEnabled(true);
        boolean AUTO_IMAGE_SMALLER = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_SMALLER);
        if (AUTO_IMAGE_SMALLER)
            binding.paragraph.getSettings().setTextZoom(85);
        boolean AUTO_IMAGE_DEFAULT = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_DEFAULT);
        if (AUTO_IMAGE_DEFAULT)
            binding.paragraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        boolean AUTO_IMAGE_LARGE = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_LARGE);
        if (AUTO_IMAGE_LARGE)
            binding.paragraph.getSettings().setTextZoom(120);
        binding.linearLayout.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {
            }
            @Override
            public void onDoubleClick(View v) {
                try{
                    boolean isBookMark = db.checkBookPara(uniqid);
                    if (isBookMark) {
                        db.deleteBookPara(uniqid);
                        Utils.unbookmark(id,"0",activity,user_id);
                        Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_small, 0, 0, 0);
                            }
                        });
                    } else {
                        db.addParaBook(courtesy, response.getDataobject(), image, uniqid, titleData,id);
                        Utils.bookmark_para(id,"0",activity,user_id);
                        Toast.makeText(activity, "Bookmarked", Toast.LENGTH_SHORT).show();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                binding.bookmarkbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_done_small, 0, 0, 0);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        if(Integer.parseInt(response.getTotal_like()) > 1){
            binding.totalUpvotes.setText(Integer.parseInt(response.getTotal_like())+" Upvotes");
        }else if(Integer.parseInt(response.getTotal_like()) != 0) {
            binding.totalUpvotes.setText(Integer.parseInt(response.getTotal_like())+" Upvote");
        }else{
            binding.totalUpvotes.setText("Upvote");
        }
        if(Utils.hasConnection(activity)){
            getLikeStatus(response.getId(),"",binding.upvote,binding.totalUpvotes,Integer.parseInt(response.getTotal_like()),position);
        }
        binding.upvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.hasConnection(activity)){
                    if( SharePrefrence.getInstance(activity).getBoolean("like_flG"+position)){
                        MainActivity.like_flag=true;
                        getLikeStatus(response.getId(),"like", binding.upvote, binding.totalUpvotes, Integer.parseInt(response.getTotal_like()),position);
                    }
                }
            }
        });
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
    private void Get_Catagery(final String quiz_post_id) {
        testitem2=new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        final String test_name = year +quiz_post_id + "TopicTest"+SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE);
        AndroidNetworking.post(Utills.BASE_URLGK +"gk-questions.php")
                .addBodyParameter("testid", quiz_post_id)
                .addBodyParameter("lang", SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("GkPagerAdapter" , String.valueOf(response));
                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            int success = obj.getInt("status");
                            total_question = obj.getString("total_question");
                            attempt = obj.getString("attemp");
                            if (success == 1) {
                                JSONArray array = obj.getJSONArray("response");
                                Log.e("question", "rec" + array.length());
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsobj = array.getJSONObject(i);
                                    FreeTestItem item = new FreeTestItem();
                                    item.setId(jsobj.getString("id"));
                                    item.setDirection(jsobj.getString("direction"));
                                    item.setQuestionenglish(jsobj.getString("question"));
                                    item.setOpt_en_1(jsobj.getString("optiona"));
                                    item.setOp_en_2(jsobj.getString("optionb"));
                                    item.setOp_en_3(jsobj.getString("optionc"));
                                    item.setOp_en_4(jsobj.getString("optiond"));
                                    if (jsobj.has("optione")) {
                                        item.setOp_en_5(jsobj.getString("optione"));
                                    } else {
                                        item.setOp_en_5("");
                                    }
                                    item.setAnswer(jsobj.getString("correct_answer"));
                                    item.setSolutionenglish(jsobj.getString("explaination"));
                                    item.setNoofoption(jsobj.getString("noofoption"));
                                    item.setTime(String.valueOf((int) Math.ceil(Double.parseDouble(total_question)*2)));
                                    item.setTotalquestio(total_question);
                                    item.setCorrectmarks("1");
                                    item.setWrongmarks("0.25");
                                    item.setNo_of_attempt(attempt);
                                    testitem2.add(item);
                                }
                                boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                if (!isData) {
                                    db.addQuizRes(test_name, String.valueOf(response), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                                }
                            } else if (success == 0) {
                                if(progressDialog != null && activity != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                Toast.makeText(activity, "Internal error ocured", Toast.LENGTH_LONG).show();
                            }
                            db.addreadUnread(test_name, "TopicTest");
                            Intent intent = new Intent(activity, QuestionQuiz.class);
                            intent.putExtra("TopicWiseTest", "TopicWiseTest");
                            intent.putExtra("language", "english");
                            intent.putExtra("testitem", testitem2);
                            intent.putExtra("catname", "");
                            intent.putExtra("testname", test_name);
                            intent.putExtra("correctmark", "1");
                            intent.putExtra("wrongmark", "0.25");
                            intent.putExtra("totalque", total_question);
                            intent.putExtra("time", String.valueOf((int) Math.ceil(Double.parseDouble(total_question)*2)));
                            intent.putExtra("topictest", true);
                            intent.putExtra("UID", test_name);
                            intent.putExtra("RANKUID", quiz_post_id);
                            intent.putExtra("PractiseTestName", "");
                            intent.putExtra("TestTitleName", "");
                            activity.startActivityForResult(intent, 1000);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if(progressDialog != null && activity != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                        if(progressDialog != null && activity != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }
    /*Like or unlike api*/
    private void getLikeStatus(final String capsule_id, final String type, final TextView upvote, final TextView total_upvotes, final int totallike, final int position) {
        AndroidNetworking.post(Utills.BASE_URLGK +"post_like.php")
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("post_id", capsule_id)
                .addBodyParameter("type", type)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject respons) {
                        try {
                            if(respons.getInt("status") == 1){
                                SharePrefrence.getInstance(activity).putBoolean("like_flG"+position,false);
                                if(!type.equalsIgnoreCase("")){
                                    DateItem item=testitem.get(position);
                                    int likes=totallike+1;
                                    item.setTotal_like(String.valueOf(totallike+1));
                                    upvote.setTextColor(Color.parseColor("#45B97B"));
                                    upvote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.upvote_enabled,0,0,0);
                                    if(likes > 1)
                                        total_upvotes.setText(""+likes+" Upvotes");
                                    else if(likes != 0)
                                        total_upvotes.setText(""+likes+" Upvote");
                                    else
                                        total_upvotes.setText("Upvote");
                                }else{
                                    upvote.setTextColor(Color.parseColor("#45B97B"));
                                    upvote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.upvote_enabled,0,0,0);
                                    if(totallike > 1)
                                        total_upvotes.setText(""+totallike+" Upvotes");
                                    else if(totallike != 0)
                                        total_upvotes.setText(""+totallike+" Upvote");
                                    else
                                        total_upvotes.setText("Upvote");
                                }
                            }else{
                                SharePrefrence.getInstance(activity).putBoolean("like_flG"+position,true);
                                upvote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.post_like_icon,0,0,0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }
}