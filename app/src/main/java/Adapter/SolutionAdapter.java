package Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ui.AppController;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.SolutionAdapterBinding;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import java.util.List;
import BitmapCache.URLImageParser;
import Custom.DoubleClickListener;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;

@SuppressWarnings("ALL")
public class SolutionAdapter extends PagerAdapter {
    List<FreeTestItem> testitem;
    Activity activity;
    boolean isclicked = true;
    ViewPager pager;
    private Context context;
    private int total_question_count;
    String posi;
    FreeTestItem item;
    String type,week;
    int attemptCount=0;
    String cat_name, test_name, total_time, total_question, correct_mark, wrong_mark, attempt;
    String Questions, Answers, CorrectMarks, NegativeMarks, Total_Question, Total_Marks, Attempt_Question, Cat_name, Test_name, correctM, wrongM ,explation;
    DatabaseHandler db;
    String CorrectQuestionn = "0";
    String InCorrectQuestionn = "0";
    String Attempt_Questionn = "0";
    String Non_AttemptQuestion = "0";
    String Total_Markss = "0";
    String Percentagee = "0";
    TextView tvQuestionNumber;
    TextView backImage;
    private String Mode;
    CircularProgressBar pb;
    ImageView ic_bookmark,lang_pref;
    RelativeLayout Top;
    String theme2,user_id="",rankID="",testTitleName="";
    Drawable drawBookMark;
    SharedPreferences sharedPreferences;
    public SolutionAdapter(Activity activity, List<FreeTestItem> testitem, ViewPager pager, String Cat_name, String Test_name,
                           String correctM, String wrongM, TextView tvQuestionNumber, TextView backImage, CircularProgressBar pb,
                           ImageView ic_bookmark, ImageView lang_pref, String rankID, String testTitleName) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        user_id=sharedPreferences.getString("uid","");
        this.activity = activity;
        context = activity;
        this.testitem = testitem;
        this.pager = pager;
        this.backImage = backImage;
        this.Cat_name = Cat_name;
        this.Test_name = Test_name;
        this.correctM = correctM;
        this.wrongM = wrongM;
        this.pb = pb;
        this.tvQuestionNumber = tvQuestionNumber;
        this.ic_bookmark = ic_bookmark;
        this.lang_pref=lang_pref;
        this.rankID = rankID;
        this.testTitleName=testTitleName;
        db = new DatabaseHandler(activity);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @Override
    public Object instantiateItem(View collection, final int position) {
        theme2=SharePrefrence.getInstance(activity).getString("Themes");
        drawBookMark=Utils.DrawableChange(activity,R.drawable.ic_bookmark_white,"#000000");
        if(theme2.equals("night")){
            activity.setTheme(R.style.night);
            drawBookMark=Utils.DrawableChange(activity,R.drawable.ic_bookmark_white,"#ffffff");
        }else if(theme2.equals("sepia")){
            activity.setTheme(R.style.sepia);
        }else {
            activity.setTheme(R.style.defaultt);
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final SolutionAdapterBinding binding= DataBindingUtil.inflate(inflater,R.layout.solution_adapter,null,false);
        item = testitem.get(position);
        lang_pref.setVisibility(View.VISIBLE);
        binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "").replace("null",
                ""), new URLImageParser(binding.tvDirection, activity), null));
        binding.tvDirection.post(new Runnable() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int lineCount = binding.tvDirection.getLineCount();
                        if (lineCount > 5) {
                            Utils.makeTextViewResizable(binding.tvDirection, 5, "View More", true);
                        }
                    }
                });
            }
        });
        if (item.getDirection().trim().equalsIgnoreCase("") || item.getDirection().trim().equalsIgnoreCase("null")
                || item.getDirection().trim().length() == 0 ) {
            binding.tvDirection.setVisibility(View.GONE);
        } else {
            binding.tvDirection.setVisibility(View.VISIBLE);
        }
        binding.scrollViewLayout.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {
            }
            @Override
            public void onDoubleClick(View v) {
                boolean isAlready=false;
                try{
                    isAlready = db.CheckIsDataAlreadyInBookMarkQuiz(Test_name +testitem.get(pager.getCurrentItem()).getId(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (isAlready) {
                    Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                    try {
                        db.deleteBookQuiz(Test_name +testitem.get(pager.getCurrentItem()).getId(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Utils.unbookmark(testitem.get(pager.getCurrentItem()).getId(),"1",activity,user_id);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ic_bookmark.setImageDrawable(drawBookMark);
                    } else {
                        ic_bookmark.setImageDrawable(drawBookMark);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ic_bookmark.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_done, activity.getApplicationContext().getTheme()));
                    } else {
                        ic_bookmark.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_done));
                    }
                    Toast.makeText(activity, "Bookmark Added", Toast.LENGTH_SHORT).show();
                    Log.d("uniq_question2",""+Test_name +testitem.get(pager.getCurrentItem()).getId());
                    try{
                        db.addQuizBook(testitem.get(pager.getCurrentItem()).getQuestionenglish(),
                                testitem.get(pager.getCurrentItem()).getAnswer(), testitem.get(pager.getCurrentItem()).getOpt_en_1(),
                                testitem.get(pager.getCurrentItem()).getOp_en_2(), testitem.get(pager.getCurrentItem()).getOp_en_3(),
                                testitem.get(pager.getCurrentItem()).getOp_en_4(), testitem.get(pager.getCurrentItem()).getOp_en_5(),
                                testitem.get(pager.getCurrentItem()).getSolutionenglish(), testitem.get(pager.getCurrentItem()).getDirection(),
                                Test_name + testitem.get(pager.getCurrentItem()).getId(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE), testitem.get(pager.getCurrentItem()).getId());
                        Utils.bookmark_para(testitem.get(pager.getCurrentItem()).getId(),"1",activity,user_id);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        if (item.getOp_en_5().trim().length() == 0)
            binding.QueELayout.setVisibility(View.GONE);
        int p = position + 1;
        posi = String.valueOf(p);
        binding.BtnPrevious.setVisibility(View.GONE);
        if (item.getQuestionenglish().equals(""))
            binding.txtQuestion.setVisibility(View.GONE);
        bookMarkWork(item);
        String Attempt_answer="",Explanation="";
        try{
            Attempt_answer = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            Explanation = db.getTestexplanation(posi, Test_name);

        }catch (Exception e){
            e.printStackTrace();
        }
        if (Attempt_answer == null)
            Attempt_answer ="Answers";
        if (Explanation != null && Explanation.trim().length() > 0)
            binding.Explation.setText(""+Explanation);
        else
            binding.Explation.setText("");
        Answers = Html.fromHtml(item.getAnswer()).toString();
        Log.e("AnswerAttempt", Attempt_answer + "");
        Log.e("Answers", Answers + "");
        if (Attempt_answer != null) {
            setprogress(1);
            if (Attempt_answer.equals("Answers")) {
                if (Answers.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.correct);
                }
                if (Answers.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.correct);
                }
                if (Answers.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.correct);
                }
                if (Answers.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.correct);
                }
                if (Answers.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.correct);
                }
            } else {
                if (Attempt_answer.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.wrong);
                    binding.QueALayout.setBackgroundColor(Color.parseColor("#F9A1A8"));
                }
                if (Attempt_answer.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.wrong);
                    binding.QueBLayout.setBackgroundColor(Color.parseColor("#F9A1A8"));
                }
                if (Attempt_answer.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.wrong);
                    binding.QueCLayout.setBackgroundColor(Color.parseColor("#F9A1A8"));
                }
                if (Attempt_answer.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.wrong);
                    binding.QueDLayout.setBackgroundColor(Color.parseColor("#F9A1A8"));
                }
                if (Attempt_answer.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.wrong);
                    binding.QueELayout.setBackgroundColor(Color.parseColor("#F9A1A8"));
                }
                if (Answers.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.correct);
                    binding.QueALayout.setBackgroundColor(Color.parseColor("#B1E1C4"));
                }
                if (Answers.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.correct);
                    binding.QueBLayout.setBackgroundColor(Color.parseColor("#B1E1C4"));
                }
                if (Answers.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.correct);
                    binding.QueCLayout.setBackgroundColor(Color.parseColor("#B1E1C4"));
                }
                if (Answers.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.correct);
                    binding.QueDLayout.setBackgroundColor(Color.parseColor("#B1E1C4"));
                }
                if (Answers.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.correct);
                    binding.QueELayout.setBackgroundColor(Color.parseColor("#B1E1C4"));
                }
            }
        }
        Questions = Html.fromHtml(item.getQuestionenglish()).toString();
        String Options = item.getNoofoption();
        int option = Integer.parseInt(Options);
        if (option < 5) {
            binding.tvOptionE.setVisibility(View.GONE);
            binding.ViewE.setVisibility(View.GONE);
            binding.IDE.setVisibility(View.GONE);
        }
        binding.BtnSubmit.setVisibility(View.GONE);
        binding.BtnNext.setVisibility(View.GONE);
        CorrectMarks = item.getCorrectmarks();
        NegativeMarks = item.getWrongmarks();
        Total_Question = item.getTotalquestio();
        total_question_count = Integer.parseInt(item.getTotalquestio());
        if (AppController.sharedPreferences.contains("Language") && AppController.sharedPreferences.getString("Hindi", "").equals("Hindi")) {
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionhindi().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            notifyDataSetChanged();
        } else {
            isclicked = true;
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionenglish().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            binding.tvOptionA.setText(Html.fromHtml(item.getOpt_en_1().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionA, activity), null));
            binding.tvOptionB.setText(Html.fromHtml(item.getOp_en_2().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionB, activity), null));
            binding.tvOptionC.setText(Html.fromHtml(item.getOp_en_3().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionC, activity), null));
            binding.tvOptionD.setText(Html.fromHtml(item.getOp_en_4().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionD, activity), null));
            binding.tvOptionE.setText(Html.fromHtml(item.getOp_en_5().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionE, activity), null));
            binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "").replace("null",""), new URLImageParser(binding.tvDirection, activity), null));
            binding.Explation.setText(Html.fromHtml(item.getSolutionenglish().replaceFirst("<p>", ""), new URLImageParser(binding.Explation, activity), null));
            binding.ViewE.setVisibility(View.GONE);
        }
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                int p = 1 + arg0;
                String ps = String.valueOf(p);
                tvQuestionNumber.setText("" + ps + "/" + item.getTotalquestio());
                lang_pref.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        ((ViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    private void bookMarkWork(final FreeTestItem item) {
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                boolean isAlready =false;
                try{
                    isAlready = db.CheckIsDataAlreadyInBookMarkQuiz(Test_name + testitem.get(position).getId(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (isAlready) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ic_bookmark.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_done, activity.getApplicationContext().getTheme()));
                    } else {
                        ic_bookmark.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_done));
                    }

                } else {
                   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                       ic_bookmark.setImageDrawable(drawBookMark);
                   } else {
                       ic_bookmark.setImageDrawable(drawBookMark);
                   }
                }
            }
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public void setprogress(int count){
        attemptCount  = attemptCount+count;
        final int total = testitem.size();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int attempHun = attemptCount*100;
                int progress = attempHun/total;
                pb.setProgress(attemptCount);
            }
        });
    }
}