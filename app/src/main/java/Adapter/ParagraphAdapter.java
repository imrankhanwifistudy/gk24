package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.daily.currentaffairs.EditorialActivity;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.PostPresenter.PostPresenterImpl;
import com.daily.currentaffairs.PostPresenter.Post_ConfirmView;
import com.daily.currentaffairs.PostPresenter.Post_Presenter;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ParagraphAdapterInsideBinding;
import com.daily.currentaffairs.databinding.ToastBinding;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ClickListener.ItemClickSupport;
import Custom.HorizontalViewPager;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Interface.DatePosition;
import Modal.DateItem;
import Slider.Animations.DescriptionAnimation;
import Slider.SliderLayout;
import Slider.SliderTypes.BaseSliderView;
import Slider.SliderTypes.TextSliderView;
import SlimAdapterModal.CurrentHeader;
import SlimAdapterModal.SectionHeader;
import SlimAdapterModal.User;
import ui.utils.Constants;

public class ParagraphAdapter extends PagerAdapter implements Post_ConfirmView {
    Activity activity;
    private ViewPager pagerMain;
    @SuppressLint("StaticFieldLeak")
    DatabaseHandler db;
    private int currentItem;
    private ArrayList<String> actualdate;
    private List<String> datee = new ArrayList<>();
    private Post_Presenter presenter;
    private Animation animMove, animMove1;
    private ArrayList<DateItem> pager_list;
    private LinearLayoutManager layoutManager1;

    public ParagraphAdapter(Activity activity, ViewPager pagerMain, ArrayList<String> actualdate, int currentItem) {
        this.activity = activity;
        this.pagerMain = pagerMain;
        this.currentItem = currentItem;
        this.actualdate = actualdate;
        db = new DatabaseHandler(activity);
        animMove = AnimationUtils.loadAnimation(activity, R.anim.move);
        animMove1 = AnimationUtils.loadAnimation(activity, R.anim.move1);
    }

    @Override
    public int getCount() {
        return actualdate.size() - 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Object instantiateItem(View collection, final int position) {
        String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final ParagraphAdapterInsideBinding binding = DataBindingUtil.inflate(inflater, R.layout.paragraph_adapter_inside, null, false);
        if (MainActivity.activityMainBinding.continar.getHeight() > 0 && SharePrefrence.getInstance(activity).getInteger("frame_height") == 0) {
            SharePrefrence.getInstance(activity).putInteger("frame_height", MainActivity.activityMainBinding.continar.getHeight());
        }
        presenter = new PostPresenterImpl(activity, this);
        try {
            final String DailyDate = MainActivity.actualdate.get(position);
            String current_date = MainActivity.actualdate.get(0);
            final Calendar calendar = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
            final String year = YearForm.format(calendar.getTime());
            binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    presenter.validateCredentials(activity, DailyDate, binding, year, DailyDate);
                }
            });
            binding.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
            binding.swipeRefreshLayout.setDistanceToTriggerSync(200);
            binding.swipeRefreshLayout.setOnChildScrollUpCallback(new SwipeRefreshLayout.OnChildScrollUpCallback() {
                @Override
                public boolean canChildScrollUp(SwipeRefreshLayout parent, @Nullable View child) {
                    return false;
                }
            });
            boolean isDataAlready = db.checkWebserviceData("", DailyDate, year, Utills.CAPSULE, "");
            if (isDataAlready) {
                if (Utils.hasConnection(activity)) {
                    if (current_date.equalsIgnoreCase(DailyDate)) {
                        presenter.validateCredentials(activity, DailyDate, binding, year, DailyDate);
                    }
                    if (MainActivity.like_flag) {
                        MainActivity.like_flag = false;
                        presenter.validateCredentials(activity, DailyDate, binding, year, DailyDate);
                    } else {
                        String response = db.getWEBResponse("", DailyDate, year, Utills.CAPSULE, "");
                        parseDataWeb(response, DailyDate, binding);
                    }
                } else {
                    String response = db.getWEBResponse("", DailyDate, year, Utills.CAPSULE, "");
                    parseDataWeb(response, DailyDate, binding);
                }
            } else {
                if (Utils.hasConnection(activity)) {
                    presenter.validateCredentials(activity, DailyDate, binding, year, DailyDate);
                } else {
                    binding.internetUna.setVisibility(View.VISIBLE);
                    Toast.makeText(activity, "Please Connect to Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((HorizontalViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((HorizontalViewPager) collection).removeView((View) view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void parseDataWeb(String response, String date, final ParagraphAdapterInsideBinding binding) {
        List<Object> data = new ArrayList<>();
        List<Object> Dailygk = new ArrayList<>();
        List<Object> DailyCurrent = new ArrayList<>();
        try {
            final ArrayList<DateItem> arrWord = new ArrayList<>();
            final ArrayList<DateItem> arrWord1 = new ArrayList<>();
            final ArrayList<DateItem> arrWord2 = new ArrayList<>();
            ArrayList<JSONObject> jsonCapsule;
            jsonCapsule = new ArrayList<>();
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("response");
            db.TodayTotalParaGraph(date, array.length());
            for (int i = 0; i < array.length(); i++) {
                try {
                    DateItem item = new DateItem();
                    JSONObject jsonObject = array.getJSONObject(i);
                    jsonCapsule.add(jsonObject);
                    item.setId(jsonObject.getString("id"));
                    item.setDataobject(jsonObject.toString());
                    item.setStatusQuizVocab("0");
                    item.setTotal_like(jsonObject.optString("total_like"));
                    item.setCap_type(jsonObject.optString("cap_type"));
                    item.setCall_action(jsonObject.optString("calltoaction"));
                    item.setCall_link(jsonObject.optString("calltoaction_link"));
                    item.setTitle_h(jsonObject.optString("title_h"));
                    item.setParagraph_h(jsonObject.optString("paragraph_h"));
                    item.setCatId(jsonObject.optString("catid"));
                    item.setSubCatId(jsonObject.optString("subcatid"));
                    item.setParagraphCapsule(jsonObject.optString("paragraph"));
                    item.setTitle(jsonObject.optString("title"));
                    item.setCourtsy(jsonObject.optString("headings"));
                    item.setImages(jsonObject.optString("images"));
                    item.setTime(jsonObject.optString("notify_date"));
                    item.setVocabExist("");
                    item.setVocabCount(0);
                    item.setDate(date);
                    item.setTypePrime(jsonObject.optString("type"));
                    if (jsonObject.optString("cap_type").equalsIgnoreCase("0")) {
                        arrWord.add(item);
                        DailyCurrent.add(new User(item));
                    } else {
                        arrWord2.add(item);
                        Dailygk.add(new User(item));
                    }
                    if (i == 0) {
                        data.add(new SectionHeader(date));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (DailyCurrent.size() > 0) {
                data.add(new CurrentHeader("Current Affairs"));
                arrWord1.addAll(arrWord);
                data.addAll(DailyCurrent);
            }
            if (Dailygk.size() > 0) {
                data.add(new CurrentHeader("GK Updates"));
                arrWord1.addAll(arrWord2);
                data.addAll(Dailygk);
            }
            String res = SharePrefrence.getInstance(activity).getString("slider_data");
            pager_list = new ArrayList<>();
            try {
                JSONObject response1 = new JSONObject(res);
                if (response1.getInt("status") == 1) {
                    JSONArray info = response1.getJSONArray("info");
                    if (info.length() > 0) {
                        data.add("");
                    }
                    for (int k = 0; k < info.length(); k++) {
                        DateItem item = new DateItem();
                        JSONObject obj = info.getJSONObject(k);
                        item.setCourtsy(obj.getString("headning"));
                        item.setTitle(obj.getString("title"));
                        item.setImages(obj.getString("img"));
                        item.setCall_action(obj.getString("callToAction"));
                        item.setCall_link(obj.getString("callToLink"));
                        pager_list.add(item);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            binding.swipeRefreshLayout.setRefreshing(false);
            layoutManager1 = new LinearLayoutManager(activity);
            binding.itemsRecyclerView.setLayoutManager(layoutManager1);
            SlimAdapter slimAdapter = SlimAdapter.create()
                    .register(R.layout.recycler_header, new SlimInjector<SectionHeader>() {
                        @Override
                        public void onInject(@NonNull SectionHeader data, @NonNull final IViewInjector injector) {
                            final LinearLayout logout_popup = (LinearLayout) injector.findViewById(R.id.logout_popup);
                            final LinearLayout rate_us_popup = (LinearLayout) injector.findViewById(R.id.rate_us_popup);
                            final RelativeLayout rv_main_layout = (RelativeLayout) injector.findViewById(R.id.rv_main_layout);
                            String formattedDate = "", formattedDate1 = "";
                            try {
                                Calendar c = Calendar.getInstance();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c.getTime());
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                if (SharePrefrence.getInstance(activity).HasString("date_not")) {
                                    Date myDate = dateFormat.parse(SharePrefrence.getInstance(activity).getString("date_not"));
                                    c.setTime(myDate);
                                    c.add(Calendar.DAY_OF_YEAR, 10);
                                    formattedDate1 = df.format(c.getTime());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            final String finalFormattedDate = formattedDate;
                            if (!data.getDate().equalsIgnoreCase(formattedDate) || SharePrefrence.getInstance(activity).getString("rate_us").equalsIgnoreCase("1")) {
                                setLayou(logout_popup, rv_main_layout);
                            } else {
                                if (SharePrefrence.getInstance(activity).getString("rate_us").equalsIgnoreCase("2") && !data.getDate().equalsIgnoreCase(formattedDate1)) {
                                    setLayou(logout_popup, rv_main_layout);
                                } else {
                                    logout_popup.setVisibility(View.VISIBLE);
                                    rv_main_layout.setVisibility(View.VISIBLE);
                                }
                                injector.clicked(R.id.not_really, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Utils.dialog_feedback(rate_us_popup, rv_main_layout, finalFormattedDate, logout_popup, activity);
                                    }
                                })
                                        .clicked(R.id.tv_yes, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                logout_popup.startAnimation(animMove);
                                                rate_us_popup.startAnimation(animMove1);
                                                logout_popup.setVisibility(View.GONE);
                                                rate_us_popup.setVisibility(View.VISIBLE);
                                                injector.gone(R.id.tv_yes)
                                                        .gone(R.id.not_really);
                                            }
                                        })
                                        .clicked(R.id.tv_ok_sure, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                SharePrefrence.getInstance(activity).putString("rate_us", "1");
                                                setLayou(rate_us_popup, rv_main_layout);
                                                String url = Constants.APPURL;
                                                Intent i = new Intent(Intent.ACTION_VIEW);
                                                i.setData(Uri.parse(url));
                                                activity.startActivity(i);
                                                new Handler().postDelayed(new Runnable() {
                                                    @SuppressLint("ResourceType")
                                                    @Override
                                                    public void run() {
                                                        LayoutInflater inflater = activity.getLayoutInflater();
                                                        ToastBinding toastBinding = DataBindingUtil.inflate(inflater, R.layout.toast, null, false);
                                                        toastBinding.getRoot().setBackgroundResource(Color.TRANSPARENT);
                                                        Toast toast = new Toast(activity);
                                                        toast.setView(toastBinding.getRoot());
                                                        toastBinding.toastTxt.setText(Html.fromHtml("<html>\n" +
                                                                "please scroll down to rate us (5 \n" +
                                                                "<span style=\"font-size:150%;color:#FAB120;\">&starf;</span>\n" +
                                                                ").\n" +
                                                                "</body>\n" +
                                                                "</html>"));
                                                        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                                                        toast.setDuration(Toast.LENGTH_SHORT);
                                                        toastBinding.toastTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                                        toast.show();
                                                    }
                                                }, 3000);
                                            }
                                        })
                                        .clicked(R.id.not_now, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                SharePrefrence.getInstance(activity).putString("rate_us", "2");
                                                setLayou(rate_us_popup, rv_main_layout);
                                                SharePrefrence.getInstance(activity).putString("date_not", finalFormattedDate);
                                            }
                                        });
                            }
                        }
                    })
                    .register(R.layout.item_setion_header, new SlimInjector<CurrentHeader>() {
                        @Override
                        public void onInject(CurrentHeader data, IViewInjector injector) {
                            injector.text(R.id.section_title, data.getTittle());
                        }
                    })
                    .register(R.layout.editorial_adapter, new SlimInjector<User>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onInject(@NonNull final User data, @NonNull IViewInjector injector) {
                            Typeface face = null;
                            try {
                                face = Typeface.createFromAsset(activity.getAssets(), "fonts/OpenSans_Semibold.ttf");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            injector.typeface(R.id.title, face)
                                    .typeface(R.id.heading, face)
                                    .typeface(R.id.time, face);
                            injector.with(R.id.imageView, new IViewInjector.Action<ImageView>() {
                                @Override
                                public void action(ImageView view) {
                                    Glide.with(activity).load(data.getItem().getImages()).into(view);
                                }
                            });
                            if (SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE).equalsIgnoreCase("English")) {
                                injector.text(R.id.title, Html.fromHtml(data.getItem().getTitle() + data.getItem().getParagraphCapsule()));
                            } else {
                                injector.text(R.id.title, Html.fromHtml(data.getItem().getTitle_h() + data.getItem().getParagraph_h()));
                            }
                            int total = 0;
                            if (data.getItem().getTotal_like().length() > 0) {
                                total = Integer.parseInt(data.getItem().getTotal_like());
                            }
                            if (total > 1) {
                                injector.text(R.id.time, data.getItem().getTime().toLowerCase() + " \u2022 " + data.getItem().getTotal_like() + " Upvotes" + " \u2022 "
                                        + data.getItem().getCourtsy().replace("of the day", ""));
                            } else {
                                injector.text(R.id.time, data.getItem().getTime().toLowerCase() + " \u2022 " + data.getItem().getTotal_like() + " Upvote" + " \u2022 "
                                        + data.getItem().getCourtsy().replace("of the day", ""));
                            }
                            injector.textColor(R.id.time, Color.GRAY);
                            if (data.getItem().getTitle().equals(".."))
                                injector.text(R.id.title, "Quote of the Day");
                            else if (data.getItem().getTitle().trim().length() == 0)
                                injector.text(R.id.title, "Quote of the Day");
                            final String uniqid = data.getItem().getDate() + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + data.getItem().getId() + "ParaRead";
                            String value = "";
                            try {
                                value = db.getReadUnread(uniqid);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (!value.equals("")) {
                                injector.textColor(R.id.title, Color.GRAY)
                                        .textColor(R.id.heading, Color.GRAY)
                                        .textColor(R.id.heading, Color.GRAY);
                            } else {
                                String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
                                switch (theme2) {
                                    case "night":
                                        injector.textColor(R.id.title, Color.WHITE)
                                                .textColor(R.id.title, Color.WHITE);
                                        break;
                                    case "sepia":
                                        injector.textColor(R.id.title, Color.BLACK);
                                        break;
                                    default:
                                        injector.textColor(R.id.title, Color.BLACK);
                                        break;
                                }
                                injector.textColor(R.id.heading, Color.parseColor("#007CAA"));
                            }
                        }
                    })
                    .registerDefault(R.layout.activity_slider, new SlimInjector<String>() {
                        @SuppressLint("ClickableViewAccessibility")
                        @Override
                        public void onInject(String data, IViewInjector injector) {
                            SliderLayout mDemoSlider = (SliderLayout) injector.findViewById(R.id.slider);
                            mDemoSlider.removeAllSliders();
                            for (int name = 0; name < pager_list.size(); name++) {
                                TextSliderView textSliderView = new TextSliderView(activity);
                                textSliderView.description(pager_list.get(name).getCourtsy())
                                        .heading(pager_list.get(name).getCourtsy())
                                        .title(pager_list.get(name).getTitle())
                                        .image(pager_list.get(name).getImages())
                                        .setScaleType(BaseSliderView.ScaleType.Fit);
                                final int finalName = name;
                                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        if (!pager_list.get(finalName).getCall_action().equalsIgnoreCase("") && pager_list.get(finalName).getCall_action().equalsIgnoreCase("share now")) {

                                            Utils.ShareDialog(activity);
                                        } else if (!pager_list.get(finalName).getCall_action().equalsIgnoreCase("")) {
                                            try {
                                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pager_list.get(finalName).getCall_link()));
                                                activity.startActivity(browserIntent);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                                Log.e("sweta", "" + mDemoSlider.getPagerIndicator());
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("extra", pager_list.get(name).getCourtsy());
                                mDemoSlider.addSlider(textSliderView);
                            }
                            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                            mDemoSlider.setDuration(1500);
                        }
                    })
                    .enableDiff()
                    .attachTo(binding.itemsRecyclerView);
            slimAdapter.updateData(data);
            final int finalCount = DailyCurrent.size() + 2;
            ItemClickSupport.addTo(binding.itemsRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    int pos = position - 2;
                    if (position >= finalCount && finalCount > 2) {
                        pos = position - 3;
                    }
                    if (position != 0 && position != 1 && (position != finalCount || finalCount == 2)) {
                        Intent intent = new Intent(activity, EditorialActivity.class);
                        intent.putExtra("array", arrWord1);
                        intent.putExtra("position", pos);
                        intent.putExtra("date_position", pagerMain.getCurrentItem());
                        activity.startActivity(intent);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLayou(LinearLayout rate_us_popup, RelativeLayout rv_main_layout) {
        rate_us_popup.setVisibility(View.GONE);
        rv_main_layout.setVisibility(View.GONE);
        rv_main_layout.setPadding(0, 0, 0, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void navigateToHome(String DailyDate, ParagraphAdapterInsideBinding binding, String year, String DailyDate1, String response) {
        try {
            JSONObject object = new JSONObject(String.valueOf(response));
            int status = object.getInt("status");
            if (status == 0) {
                binding.comingtxt.setVisibility(View.VISIBLE);
                if (!datee.contains(DailyDate)) {
                    try {
                        datee.add(DailyDate);
                        ((DatePosition) activity).DatePosition(currentItem);
                    } catch (Exception cce) {
                        cce.printStackTrace();
                    }
                }
            }
            if (status == 1) {
                binding.comingtxt.setVisibility(View.GONE);
                try {
                    db.addWebservice("", DailyDate, year, String.valueOf(response), Utills.CAPSULE, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                parseDataWeb(String.valueOf(response), DailyDate1, binding);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}