package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.EditorialAdapterBinding;

import java.util.ArrayList;

import DB.DatabaseHandler;
import DB.SharePrefrence;
import Modal.QuizModel;

public class TestAdapter extends BaseAdapter {
    private ArrayList<QuizModel> Detail;
    private Activity activity;
    DatabaseHandler db;

    public TestAdapter(Activity activity, ArrayList<QuizModel> Detail) {
        this.activity = activity;
        this.Detail = Detail;
        db = new DatabaseHandler(activity);
    }

    @Override
    public int getCount() {
        return Detail.size();
    }

    @Override
    public Object getItem(int i) {
        return Detail.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("WrongViewCast")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder")
        EditorialAdapterBinding binding = DataBindingUtil.inflate(inflater, R.layout.editorial_adapter, parent, false);
        binding.setQuizModel(Detail.get(position));
        String theme = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme) {
            case "default":
                binding.mainLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case "night":
                binding.title.setTextColor(Color.parseColor("#ffffff"));
                break;
        }
        Glide.with(activity).load(Detail.get(position).getImages()).thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.imageView);
        binding.title.setMaxLines(2);
        binding.time.setVisibility(View.GONE);
        return binding.getRoot();
    }
}