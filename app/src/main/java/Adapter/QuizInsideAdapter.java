package Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.QuizPresenter.QuizPresenterImpl;
import com.daily.currentaffairs.QuizPresenter.Quiz_ConfirmView;
import com.daily.currentaffairs.QuizPresenter.Quiz_Presenter;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.TopicResultActivity;
import com.daily.currentaffairs.databinding.QuizInsideAdapterBinding;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;
import Modal.FreeTestItem;
@SuppressWarnings("ALL")
public class QuizInsideAdapter extends RecyclerView.Adapter<QuizInsideAdapter.ViewHolder> implements Quiz_ConfirmView {
    private static final String TAG = QuizInsideAdapter.class.getSimpleName();
    Activity activity;
    ArrayList<DateItem> testitem;
    ArrayList<FreeTestItem> testitem2;
    ViewPager pagerMain;
    DatabaseHandler db;
    String type="0",total_time, total_question, correct_mark, wrong_mark, attempt;
    ViewHolder holder;
    final ProgressDialog dialog;
    String uniqKey ,week_no="";
    String test_name,test_name1;
    ArrayList<String> actualdate;
    ProgressDialog progressDoalog1;
    Handler handle;
    int Count=0,recycler_pos;
    private Quiz_Presenter presenter;
    RecyclerView recyclerView;
    public QuizInsideAdapter(Activity activity, ArrayList<DateItem> testitem, ViewPager pagerMain, ArrayList<String> actualdate, int Count, RecyclerView recyclerView, int position) {
        this.activity = activity;
        this.testitem = testitem;
        this.pagerMain = pagerMain;
        this.actualdate=actualdate;
        db = new DatabaseHandler(activity);
        dialog = new ProgressDialog(activity);
        progressDoalog1 = new ProgressDialog(activity);
        this.Count=Count;
        recycler_pos=position;
        this.recyclerView=recyclerView;
    }
    @Override
    public int getItemViewType (int position){
        return  position;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        String theme2= SharePrefrence.getInstance(activity).getString("Themes");
        if(theme2.equals("night")){
            activity.setTheme(R.style.night);
        }else if(theme2.equals("sepia")){
            activity.setTheme(R.style.sepia);
        }else {
            activity.setTheme(R.style.defaultt);
        }
        QuizInsideAdapterBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.quiz_inside_adapter,parent,false);
        return new ViewHolder(binding.getRoot(),binding);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        presenter = new QuizPresenterImpl(activity, this);
        if(Count == 0){
            MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
        }else{
            MainActivity.activityMainBinding.QuizCount.setVisibility(View.VISIBLE);
            MainActivity.activityMainBinding.QuizCount.setText(""+Count);
        }
        String week="";
        final String idQuiz = testitem.get(position).getId();
        String dateQuiz = testitem.get(position).getDate();
        uniqKey = idQuiz + dateQuiz ;
        test_name = uniqKey;
        boolean flag = false;
        ViewGroup.LayoutParams params = holder.binding.llMainLayout.getLayoutParams();
        holder.binding.testButton.setTextSize(18);
        if(testitem.size() == 1){
            holder.binding.quizIcon.setVisibility(View.VISIBLE);
            holder.binding.viewline.setVisibility(View.GONE);
            holder.binding.titleMain.setPadding(0,20,0,40);
            holder.binding.titleMain.setTextSize(25);
            if(MainActivity.activityMainBinding.continar.getHeight() != 0 &&
                    MainActivity.activityMainBinding.continar.getHeight() < SharePrefrence.getInstance(activity).getInteger("frame_height")){
                params.height =  MainActivity.activityMainBinding.continar.getHeight();
            }else{
                params.height = SharePrefrence.getInstance(activity).getInteger("frame_height");
            }
            holder.binding.llMainLayout.setLayoutParams(params);
            holder.binding.rlQuiz.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            holder.binding.rlQuiz.setBackgroundResource(0);
            RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,0);
            params1.addRule(RelativeLayout.BELOW, R.id.middelLay);
            holder.binding.relativeLayout.setLayoutParams(params1);
            holder.binding.ivQuizicon.setVisibility(View.GONE);
        }else{
            holder.binding.quizIcon.setVisibility(View.GONE);
            if(position == 0) holder.binding.viewline.setVisibility(View.VISIBLE);
            holder.binding.titleMain.setTextSize(20);
            if(MainActivity.activityMainBinding.continar.getHeight() != 0 && MainActivity.activityMainBinding.continar.getHeight() < SharePrefrence.getInstance(activity).getInteger("frame_height")){
                params.height =  MainActivity.activityMainBinding.continar.getHeight()/2;
            }else{
                params.height = SharePrefrence.getInstance(activity).getInteger("frame_height")/2;
            }
            holder.binding.llMainLayout.setLayoutParams(params);
        }
        if(MainActivity.quiz_type.equals("Monthly")){
            test_name = uniqKey;
            holder.binding.titleMain.setText("Monthly Current Affairs Quiz");
            try{
                flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(MainActivity.quiz_type.equals("Weekly")){
            week=testitem.get(position).getWeek_no();
            test_name = uniqKey+"w-"+testitem.get(position).getWeek_no();
            holder.binding.titleMain.setText("Weekly Current Affairs Quiz");
            try{
                flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            test_name1=uniqKey;
            if(position == 1){
                test_name = uniqKey+"1";
                holder.binding.titleMain.setText("Daily GK Quiz");
                try{
                    flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                test_name = uniqKey+"0";
                try{
                    flag = db.CheckIsResultAlreadyInDBorNotReadyStock(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                }catch (Exception e){
                    e.printStackTrace();
                }
                holder.binding.titleMain.setText("Daily Current Affairs Quiz");
            }
        }
        if (flag) {
            holder.binding.testButton.setText("View Result");
            holder.binding.testButton.setTextColor(Color.parseColor("#FF7633"));
            holder.binding.testButton.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.back_quiz_report));
        } else {
            holder.binding.testButton.setText("Start Quiz");
            holder.binding.testButton.setTextColor(Color.parseColor("#ffffff"));
            holder.binding.testButton.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.back_quiz_start));
        }
        if (Utils.hasConnection(activity)) {
            try{
                boolean isData = db.CheckInQuiz(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (isData) {
                    String response = db.getQuizRes(test_name , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    ParseData(response, holder , idQuiz ,dateQuiz ,1 ,"",week);
                } else {
                    if(MainActivity.quiz_type.equals("Monthly")){
                        dateQuiz=testitem.get(position).getId();
                        type="2";
                        week_no="";
                        presenter.validateCredentials(activity ,idQuiz, holder ,dateQuiz ,test_name,type,week_no,holder.binding.testButton,"1", position);
                    }else if(MainActivity.quiz_type.equals("Weekly")){
                        dateQuiz=testitem.get(position).getId();
                        type="1";
                        week_no="w-"+testitem.get(position).getWeek_no();
                        presenter.validateCredentials(activity ,idQuiz, holder ,dateQuiz  , test_name, type, week_no, holder.binding.testButton,"1", position);
                    }else {
                        dateQuiz=testitem.get(position).getId();
                        type="0";
                        week_no="";
                        String quiz_tpe="1";
                        if(position == 1)
                            quiz_tpe="0";
                        presenter.validateCredentials(activity ,idQuiz, holder ,dateQuiz  , test_name, type, week_no, holder.binding.testButton, quiz_tpe,position);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        } else {
            try{
                boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (isData) {
                    String response = db.getQuizRes(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    ParseData(response, holder , idQuiz ,dateQuiz ,1, "", week);
                }else{
                    holder.binding.testButton.setText("Coming Soon");
                    if(MainActivity.quiz_type.equalsIgnoreCase("Daily") && position == 1){
                        testitem.clear();
                        DateItem item = new DateItem();
                        item.setId(MainActivity.actualdate.get(recycler_pos));
                        item.setDate(MainActivity.actualdate.get(recycler_pos));
                        item.setWeek_no(MainActivity.months.get(recycler_pos));
                        testitem.add(item);
                        QuizInsideAdapter quizInsideAdapter = new QuizInsideAdapter(activity, testitem, pagerMain,MainActivity.actualdate,Count,recyclerView, recycler_pos);
                        recyclerView.setAdapter(quizInsideAdapter );
                        quizInsideAdapter.notifyDataSetChanged();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        final String finalDateQuiz = dateQuiz;
        final String finalWeek = week;
        final boolean finalFlag = flag;
        holder.binding.testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("finalWeek",""+position+","+test_name);
                MainActivity.quizdate = finalDateQuiz;
                if(MainActivity.quiz_type.equalsIgnoreCase("Daily")){
                    if(position == 0)
                        test_name=test_name1+"0";
                    else
                        test_name=test_name1+"1";
                }
                if (finalFlag) {
                    Intent intent = new Intent(activity, TopicResultActivity.class);
                    intent.putExtra("testId", "" + test_name);
                    intent.putExtra("Id", "" + testitem.get(position).getId());
                    intent.putExtra("week",""+finalWeek);
                    intent.putExtra("TestName", "Quiz Report");
                    activity.startActivity(intent);
                } else if( holder.binding.testButton.getText().toString().trim().equalsIgnoreCase("Coming soon")) {
                }else{
                    Utils.PROGRESS(activity);
                    holder.binding.testButton.setText("Quiz Fetching...");
                    try{
                        boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                        if (isData) {
                            String response = db.getQuizRes(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                            ParseData(response, holder , idQuiz , finalDateQuiz,2, test_name, finalWeek);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }


        });
    }
    @Override
    public int getItemCount() {
        return testitem.size();
    }
    @Override
    public void navigateToHome(String idQuiz, ViewHolder holder, String dateQuiz, String test_name,
                               String type, String week_no, TextView testButton, String quiz_type, int position, String response) {
        try {
            Log.e("Sweta",""+String.valueOf(response));
            JSONObject obj = new JSONObject(String.valueOf(response));
            int success = obj.getInt("status");
            if (success == 1) {
                correct_mark = obj.getString("right_mark");
                wrong_mark = obj.getString("rowsqsmar");
                total_question = obj.getString("total_question");
                total_time = obj.getString("total_time");
                attempt = obj.getString("attemp");
                boolean isData = db.CheckInQuiz(test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                if (!isData) {
                    db.addQuizRes(test_name, String.valueOf(response) , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    if(MainActivity.quiz_type.equals("Monthly")) {
                        MainActivity.activityMainBinding.monthlyCount.setVisibility(View.GONE);
                        if(Count == 0){
                            MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
                        }else{
                            MainActivity.activityMainBinding.QuizCount.setVisibility(View.VISIBLE);
                            Count=Count-1;
                            MainActivity.activityMainBinding.QuizCount.setText(""+Count);
                        }
                    }
                    if(MainActivity.quiz_type.equals("Weekly")) {
                        pages();
                    }
                }
                Log.e(TAG,"navigateToHome");
                ParseData(String.valueOf(response), holder,idQuiz ,dateQuiz ,1 ,"", week_no);
            }else  if (success == 0){
                /*if(type.equalsIgnoreCase("0")  && testitem.size() > 1){
                    testitem.remove(position);
                    notifyDataSetChanged();
                }*/
                testButton.setText("Coming Soon");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        QuizInsideAdapterBinding binding;
        public ViewHolder(View itemView, QuizInsideAdapterBinding binding) {
            super(itemView);
            this.binding=binding;
        }
    }
    private void ParseData(String response, final ViewHolder holder,String idQuiz, String dateQuiz, int txtLocation, String TestName, String week) {
        testitem2 = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(response);
            int success = obj.getInt("status");
            correct_mark = obj.getString("right_mark");
            wrong_mark = obj.getString("rowsqsmar");
            total_question = obj.getString("total_question");
            total_time = obj.getString("total_time");
            attempt = obj.getString("attemp");
            if (txtLocation == 1) {
                holder.binding.totalQ.setText(String.valueOf(total_question ));
                int time= Integer.parseInt(total_question)/2;
                holder.binding.date.setText(String.valueOf(time));
                holder.binding.positive.setText(String.valueOf(correct_mark));
                holder.binding.negative.setText(String.valueOf(wrong_mark));
            }else {
                if (success == 1) {
                    JSONArray array = obj.getJSONArray("response");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsobj = array.getJSONObject(i);
                        FreeTestItem item = new FreeTestItem();
                        item.setId(jsobj.getString("id"));
                        item.setDirection(jsobj.getString("direction"));
                        item.setQuestionenglish(jsobj.getString("question"));
                        item.setOpt_en_1(jsobj.getString("optiona"));
                        item.setOp_en_2(jsobj.getString("optionb"));
                        item.setOp_en_3(jsobj.getString("optionc"));
                        item.setOp_en_4(jsobj.getString("optiond"));
                        if (jsobj.has("optione"))
                            item.setOp_en_5(jsobj.getString("optione"));
                         else
                            item.setOp_en_5("");
                        item.setAnswer(jsobj.getString("correct_answer"));
                        item.setSolutionenglish(jsobj.getString("explaination"));
                        item.setNoofoption(jsobj.getString("noofoption"));
                        item.setTime(total_time);
                        item.setTotalquestio(total_question);
                        item.setCorrectmarks(correct_mark);
                        item.setWrongmarks(wrong_mark);
                        item.setNo_of_attempt(attempt);
                        testitem2.add(item);
                    }
                    Intent i = new Intent(activity, QuestionQuiz.class);
                    i.putExtra("language", "english");
                    i.putExtra("testitem", testitem2);
                    i.putExtra("catname", "");
                    i.putExtra("testname", TestName);
                    i.putExtra("correctmark", "");
                    i.putExtra("wrongmark", "");
                    i.putExtra("totalque", total_question);
                    i.putExtra("UID", TestName);
                    i.putExtra("RANKUID", idQuiz + "");
                    i.putExtra("time", "");
                    i.putExtra("week",week);
                    i.putExtra("TestName_quiz","Quiz Report");
                    activity.startActivityForResult(i, 100);
                    activity.overridePendingTransition(0, 0);
                } else if (success == 0) {
                    String Message = "Data Not Found";
                    Toast.makeText(activity, "Internal error ocured", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    void pages(){
        MainActivity.activityMainBinding.weeklyCount.setVisibility(View.GONE);
        if(Count<2){
            MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
        }else{
            MainActivity.activityMainBinding.QuizCount.setVisibility(View.VISIBLE);
            Count=Count-1;
            MainActivity.activityMainBinding.QuizCount.setText(""+Count);
        }
    }
}