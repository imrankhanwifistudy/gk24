package Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ServiceAdapterBinding;

import java.util.ArrayList;

import Modal.GkTitleModel;

public class LearnAdapter extends RecyclerView.Adapter<LearnAdapter.ViewHolder> {
    Activity activity;
    private ArrayList<GkTitleModel> arrData;
    private ArrayList<Integer> arrImages;
    private ArrayList<String> colorList;

    public LearnAdapter(Activity activity, ArrayList<GkTitleModel> arrData, ArrayList<Integer> arrImages, ArrayList<String> colorList) {
        this.activity = activity;
        this.arrData = arrData;
        this.arrImages = arrImages;
        this.colorList = colorList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ServiceAdapterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.service_adapter, parent, false);
        Display display = activity.getWindowManager().getDefaultDisplay();
        int height = (display.getHeight()) - (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56, activity.getResources().getDisplayMetrics());
        binding.getRoot().setMinimumHeight(height / 4);
        return new ViewHolder(binding.getRoot(), binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.binding.textview.setText(arrData.get(position).getTitle());
        if (position == 7) {
            SpannableString spannable = new SpannableString(arrData.get(position).getTitle() + "\n (Coming Soon)");
            spannable.setSpan(new RelativeSizeSpan(0.80f), arrData.get(position).getTitle().length(), arrData.get(position).getTitle().length() + 14, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.binding.textview.setText(spannable);
        }
        holder.binding.image.setBackgroundResource(arrImages.get(position));
        holder.binding.cardView.setBackgroundColor(Color.parseColor(colorList.get(position)));
        if (position > 0)
            holder.binding.textview2.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return arrImages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ServiceAdapterBinding binding;

        public ViewHolder(View itemView, ServiceAdapterBinding binding) {
            super(itemView);
            this.binding = binding;
        }
    }
}