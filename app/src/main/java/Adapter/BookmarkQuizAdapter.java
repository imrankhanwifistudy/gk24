package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import ui.AppController;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.StartTestAdapterBinding;
import java.util.List;
import BitmapCache.URLImageParser;
import ClickListener.QuizClickListner;
import Custom.DoubleClickListener;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.FreeTestItem;
@SuppressWarnings("ALL")
public class BookmarkQuizAdapter extends PagerAdapter {
    List<FreeTestItem> testitem;
    Activity activity;
    boolean isclicked = true;
    ViewPager pager;
    FreeTestItem item;
    String posi,Answers,Test_name;
    DatabaseHandler db;
    TextView tvQuestionNumber;
    public BookmarkQuizAdapter(Activity activity,List<FreeTestItem> testitem,ViewPager pager,String Test_name,
                              TextView tvQuestionNumber) {
        this.activity = activity;
        this.testitem = testitem;
        this.pager = pager;
        this.Test_name = Test_name;
        this.tvQuestionNumber = tvQuestionNumber;
        db = new DatabaseHandler(activity);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @SuppressLint("InflateParams")
    @Override
    public Object instantiateItem(View collection, final int position) {
        RadioGroup rg_lang;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final StartTestAdapterBinding binding = DataBindingUtil.inflate(inflater,R.layout.start_test_adapter,null,false);
        item = testitem.get(position);
        binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "").replace("null",""),
                new URLImageParser(binding.tvDirection, activity), null));
        binding.tvDirection.post(new Runnable() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int lineCount = binding.tvDirection.getLineCount();
                        if (lineCount > 5) {
                            Utils.makeTextViewResizable(binding.tvDirection, 5, "View More", true);
                        }
                    }
                });
            }
        });
        String theme= SharePrefrence.getInstance(activity).getString("Themes");
        if (item.getDirection().trim().equalsIgnoreCase("") || item.getDirection().trim().equalsIgnoreCase("null")
                || item.getDirection().trim().length() == 0 ) {
            binding.tvDirection.setVisibility(View.GONE);
        } else {
            binding.tvDirection.setVisibility(View.VISIBLE);
        }
        binding.scrollViewLayout.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {
            }
            @Override
            public void onDoubleClick(View v) {
                try {
                    boolean isAlready = db.CheckIsDataAlreadyInBookMarkQuiz(testitem.get(pager.getCurrentItem()).getUniq(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    if (isAlready) {
                        Toast.makeText(activity, "Bookmark Removed", Toast.LENGTH_SHORT).show();
                        db.deleteBookQuiz(testitem.get(pager.getCurrentItem()).getUniq(), SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                        testitem.remove(pager.getCurrentItem());
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        int p = position + 1;
        posi = String.valueOf(p);
        binding.BtnPrevious.setVisibility(View.GONE);
        if (item.getQuestionenglish().equals("") == true) {
            binding.txtQuestion.setVisibility(View.GONE);
        }
        String Attempt_answer="";
        try {
            Attempt_answer = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
        }catch (Exception e){
            e.printStackTrace();
        }
        Answers = Html.fromHtml(item.getAnswer()).toString();
        if (Attempt_answer != null) {
            if (Attempt_answer.equals("Answers")) {
                if (Answers.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.correct);
                    binding.QueALayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.correct);
                    binding.QueBLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.correct);
                    binding.QueCLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.correct);
                    binding.QueDLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.correct);
                    binding.QueELayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
            } else {
                if (Attempt_answer.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.wrong);
                    binding.QueALayout.setBackgroundColor(Color.parseColor("#FFEBED"));
                }
                if (Attempt_answer.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.wrong);
                    binding.QueBLayout.setBackgroundColor(Color.parseColor("#FFEBED"));
                }
                if (Attempt_answer.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.wrong);
                    binding.QueCLayout.setBackgroundColor(Color.parseColor("#FFEBED"));
                }
                if (Attempt_answer.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.wrong);
                    binding.QueDLayout.setBackgroundColor(Color.parseColor("#FFEBED"));
                }
                if (Attempt_answer.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.wrong);
                    binding.QueELayout.setBackgroundColor(Color.parseColor("#FFEBED"));
                }
                if (Answers.equals("optiona")) {
                    binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDA.setBackgroundResource(R.drawable.correct);
                    binding.QueALayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optionb")) {
                    binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDB.setBackgroundResource(R.drawable.correct);
                    binding.QueBLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optionc")) {
                    binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDC.setBackgroundResource(R.drawable.correct);
                    binding.QueCLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optiond")) {
                    binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDD.setBackgroundResource(R.drawable.correct);
                    binding.QueDLayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
                if (Answers.equals("optione")) {
                    binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                    binding.IDE.setBackgroundResource(R.drawable.correct);
                    binding.QueELayout.setBackgroundColor(Color.parseColor("#E8F6E9"));
                }
            }
        }
        String Options = item.getNoofoption();
        int option = Integer.parseInt(Options);
        if (option < 5) {
            binding.tvOptionE.setVisibility(View.GONE);
            binding.ViewE.setVisibility(View.GONE);
            binding.IDE.setVisibility(View.GONE);
        }
        binding.BtnSubmit.setVisibility(View.GONE);
        binding.BtnNext.setVisibility(View.GONE);
        if( binding.tvOptionE.getText().toString().equals("")){
            binding.tvOptionE.setVisibility(View.GONE);
            binding.IDE.setVisibility(View.GONE);
        }
        if (AppController.sharedPreferences.contains("Language")
                && AppController.sharedPreferences.getString("Hindi", "").equals("Hindi")) {
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionhindi().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            binding.tvOptionA.setText(Html.fromHtml(item.getOp_hi_1().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionA, activity), null));
            binding.tvOptionB.setText(Html.fromHtml(item.getOp_hi2().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionB, activity), null));
            binding.tvOptionC.setText(Html.fromHtml(item.getOp_hi_3().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionC, activity), null));
            binding.tvOptionD.setText(Html.fromHtml(item.getOp_hi_4().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionD, activity), null));
            binding.tvOptionE.setText(Html.fromHtml(item.getOp_hi_5().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionE, activity), null));
            notifyDataSetChanged();
        } else {
            isclicked = true;
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionenglish().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            binding.tvOptionA.setText(Html.fromHtml(item.getOpt_en_1().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionA, activity), null));
            binding.tvOptionB.setText(Html.fromHtml(item.getOp_en_2().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionB, activity), null));
            binding.tvOptionC.setText(Html.fromHtml(item.getOp_en_3().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionC, activity), null));
            binding.tvOptionD.setText(Html.fromHtml(item.getOp_en_4().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionD, activity), null));
            binding.tvOptionE.setText(Html.fromHtml(item.getOp_en_5().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionE, activity), null));
            binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "").replace("null",""), new URLImageParser(binding.tvDirection, activity), null));
        }
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                int p = 1 + arg0;
                String ps = String.valueOf(p);
                tvQuestionNumber.setText("Q.No.: " + ps + "/" + testitem.size());
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
        binding.setClick(new QuizClickListner(activity,binding,pager,testitem,posi,Test_name));
        ((ViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}