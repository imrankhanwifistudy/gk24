package Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ui.AppController;
import com.daily.currentaffairs.EditorialActivity;
import com.daily.currentaffairs.QuestionQuiz;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.TopicResultActivity;
import com.daily.currentaffairs.databinding.StartTestAdapterBinding;
import com.daily.currentaffairs.databinding.TestSubmitBinding;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.List;
import java.util.concurrent.TimeUnit;

import BitmapCache.URLImageParser;
import ClickListener.QuizClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Interface.TimerValuee;
import Modal.FreeTestItem;
@SuppressWarnings("ALL")
public class Start_Test_AdapterEnglish extends PagerAdapter {
    List<FreeTestItem> testitem;
    Activity activity;
    boolean isclicked = true;
    ViewPager pager;
    String theme,CorrectQuestionn = "0",InCorrectQuestionn = "0", Non_AttemptQuestion = "0",Percentagee = "0", totalTime,type, dateQuiz,
            week,posi;
    int total_question_count,position;
    CircularProgressBar cpb;
    FreeTestItem item;
    public static int CorrectQuestion=0, InCorrectQuestion=0,attemp_count = 0;
    DatabaseHandler db;
    TextView tvQuestionNumber, backImage;
    boolean isTimer;
    TimerValuee interfacee;
    private long CurrentTime, EndTime;
    StartTestAdapterBinding binding;
    Drawable drawPlay,drawBack;
    private String Questions, Answers, CorrectMarks, NegativeMarks, Total_Question, Total_Marks, Attempt_Question,
             Test_name, correctM, wrongM, explation, UID, RANKUID, PractiseTestName, TestTitleName;
    public Start_Test_AdapterEnglish(Activity activity, List<FreeTestItem> testitem, ViewPager pager, String Test_name, String correctM,
                                     String wrongM, TextView tvQuestionNumber, TextView backImage, CircularProgressBar circleProgress,
                                     String UID, String RANKUID, boolean isTimer, String totalTime, String PractiseTestName,
                                     String TestTitleName, TimerValuee obj, String week) {
        this.activity = activity;
        this.testitem = testitem;
        this.UID = UID;
        this.RANKUID = RANKUID;
        this.interfacee = obj;
        this.pager = pager;
        this.backImage = backImage;
        this.Test_name = Test_name;
        this.correctM = correctM;
        this.wrongM = wrongM;
        this.totalTime = totalTime;
        this.isTimer = isTimer;
        this.PractiseTestName = PractiseTestName;
        this.TestTitleName = TestTitleName;
        this.tvQuestionNumber = tvQuestionNumber;
        cpb = circleProgress;
        this.week = week;
        db = new DatabaseHandler(activity);
        notifyDataSetChanged();
        attemp_count = 0;
        CorrectQuestion=0;
        InCorrectQuestion=0;
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @SuppressWarnings("deprecation")
    @Override
    public Object instantiateItem(View collection, final int position) {
        theme = SharePrefrence.getInstance(activity).getString("Themes");
        drawPlay= Utils.DrawableChange(activity,R.drawable.ic_play_black_24dp,"#000000");
        drawBack= Utils.DrawableChange(activity,R.drawable.ic_pause_24dp,"#000000");
        if (theme.equals("night")) {
            activity.setTheme(R.style.night);
            drawPlay= Utils.DrawableChange(activity,R.drawable.ic_play_black_24dp,"#ffffff");
            drawBack= Utils.DrawableChange(activity,R.drawable.ic_pause_24dp,"#ffffff");
        } else if (theme.equals("sepia")) {
            activity.setTheme(R.style.sepia);
        } else {
            activity.setTheme(R.style.defaultt);
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding= DataBindingUtil.inflate(inflater,R.layout.start_test_adapter,null,false);
        item = testitem.get(position);
        if (item.getOp_en_5().trim().length() == 0)
            binding.QueELayout.setVisibility(View.GONE);
        int p = position + 1;
        posi = String.valueOf(p);
        binding.BtnPrevious.setVisibility(View.GONE);
        if (item.getDirection().equals("") == false)
            binding.tvDirection.setVisibility(View.VISIBLE);
        if (item.getQuestionenglish().equals("") == true)
            binding.txtQuestion.setVisibility(View.GONE);
        binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "").replace("null", ""), new URLImageParser(binding.tvDirection, activity), null));
        binding.tvDirection.setTextColor(Color.parseColor("#99ffffff"));
        binding.tvDirection.post(new Runnable() {
            @Override
            public void run() {
            int lineCount = binding.tvDirection.getLineCount();
            if (lineCount > 5)
                Utils.makeTextViewResizable(binding.tvDirection, 5, "View More", true);
            }
        });
        if (item.getDirection().trim().equalsIgnoreCase("") || item.getDirection().trim().equalsIgnoreCase("null")
                || item.getDirection().trim().length() == 0) {
            binding.tvDirection.setVisibility(View.GONE);
        } else {
            binding.tvDirection.setVisibility(View.VISIBLE);
        }
        if (item.getQuestionenglish().equals("")) {
            binding.txtQuestion.setVisibility(View.GONE);
        }
        String Attempt_answer = "";
        try {
            Attempt_answer = db.getTestAns(posi, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Answers = Html.fromHtml(item.getAnswer()).toString();
        Questions = Html.fromHtml(item.getQuestionenglish()).toString();
        String Options = item.getNoofoption();
        int option = Integer.parseInt(Options);
        if (option < 5) {
            binding.tvOptionE.setVisibility(View.GONE);
            binding.ViewE.setVisibility(View.GONE);
            binding.IDE.setVisibility(View.GONE);
        }
        if (item.getTotalquestio().equals(posi)) {
            binding.BtnSubmit.setVisibility(View.VISIBLE);
            binding.BtnNext.setVisibility(View.GONE);
        } else {
            binding.BtnSubmit.setVisibility(View.GONE);
            binding.BtnNext.setVisibility(View.GONE);
        }
        CorrectMarks = item.getCorrectmarks();
        NegativeMarks = item.getWrongmarks();
        Total_Question = item.getTotalquestio();
        total_question_count = Integer.parseInt(item.getTotalquestio());
        Listeners(binding, item, posi);
        if (AppController.sharedPreferences.contains("Language")
                && AppController.sharedPreferences.getString("Hindi", "").equals("Hindi")) {
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionhindi().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            binding.tvOptionA.setText(Html.fromHtml(item.getOp_hi_1().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionA, activity), null));
            binding.tvOptionB.setText(Html.fromHtml(item.getOp_hi2().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionB, activity), null));
            binding.tvOptionC.setText(Html.fromHtml(item.getOp_hi_3().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionC, activity), null));
            binding.tvOptionD.setText(Html.fromHtml(item.getOp_hi_4().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionD, activity), null));
            binding.tvOptionE.setText(Html.fromHtml(item.getOp_hi_5().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionE, activity), null));
            binding.tvDirection.setText(Html.fromHtml(item.getDirectionhindi().replaceFirst("<p>", ""), new URLImageParser(binding.tvDirection, activity), null));
            notifyDataSetChanged();
        } else {
            isclicked = true;
            binding.txtQuestion.setText(Html.fromHtml(item.getQuestionenglish().replaceFirst("<p>", ""), new URLImageParser(binding.txtQuestion, activity), null));
            binding.tvOptionA.setText(Html.fromHtml(item.getOpt_en_1().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionA, activity), null));
            binding.tvOptionB.setText(Html.fromHtml(item.getOp_en_2().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionB, activity), null));
            binding.tvOptionC.setText(Html.fromHtml(item.getOp_en_3().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionC, activity), null));
            binding.tvOptionD.setText(Html.fromHtml(item.getOp_en_4().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionD, activity), null));
            binding.tvOptionE.setText(Html.fromHtml(item.getOp_en_5().replaceFirst("<p>", ""), new URLImageParser(binding.tvOptionE, activity), null));
            binding.tvDirection.setText(Html.fromHtml(item.getDirection().replaceFirst("<p>", "")
                    .replace("null", ""), new URLImageParser(binding.tvDirection, activity), null));
        }
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                int p = 1 + arg0;
                String ps = String.valueOf(p);
                String Attempt_answer = "";
                try {
                    Attempt_answer = db.getTestAns(ps, Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Attempt_answer != null) {
                    Log.e("AnswerAttempt", Attempt_answer + "");
                    if (Attempt_answer.equals("optiona")) {
                        binding.IDA.setTextColor(Color.parseColor("#ffffff"));
                        binding.IDA.setBackgroundResource(R.drawable.skipp_correctt);
                        binding.QueALayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                        binding.tvOptionA.setTextColor(Color.parseColor("#2CB8E1"));
                    }
                    if (Attempt_answer.equals("optionb")) {
                        binding.IDB.setTextColor(Color.parseColor("#ffffff"));
                        binding.IDB.setBackgroundResource(R.drawable.skipp_correctt);
                        binding.QueBLayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                        binding.tvOptionB.setTextColor(Color.parseColor("#2CB8E1"));
                    }
                    if (Attempt_answer.equals("optionc")) {
                        binding.IDC.setTextColor(Color.parseColor("#ffffff"));
                        binding.IDC.setBackgroundResource(R.drawable.skipp_correctt);
                        binding.QueCLayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                        binding.tvOptionC.setTextColor(Color.parseColor("#2CB8E1"));
                    }
                    if (Attempt_answer.equals("optiond")) {
                        binding.IDD.setTextColor(Color.parseColor("#ffffff"));
                        binding.IDD.setBackgroundResource(R.drawable.skipp_correctt);
                        binding.QueDLayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                        binding.tvOptionD.setTextColor(Color.parseColor("#2CB8E1"));
                    }
                    if (Attempt_answer.equals("optione")) {
                        binding.IDE.setTextColor(Color.parseColor("#ffffff"));
                        binding.IDE.setBackgroundResource(R.drawable.skipp_correctt);
                        binding.QueELayout.setBackgroundColor(Color.parseColor("#D4F2F9"));
                        binding.tvOptionE.setTextColor(Color.parseColor("#2CB8E1"));
                    }
                    binding.BtnClear.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                int p = 1 + arg0;
                String ps = String.valueOf(p);
                tvQuestionNumber.setText(ps + "/" + item.getTotalquestio());
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        binding.setClick(new QuizClickListner(activity,binding,pager,posi,Test_name,testitem,cpb));
        ((ViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    private void Listeners(final StartTestAdapterBinding binding, final FreeTestItem item, final String posi) {
        binding.BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Action", "Submit Test");
                QuestionQuiz.count.cancel();
                ConfirmDialogeQuizExit("Are you sure you want to Exit ", false);
            }
        });
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    private int getItem(int i) {
        position = i;
        return pager.getCurrentItem() + i;
    }
    public void ConfirmDialogeQuizExit(String message, Boolean Flag) {
        final int Attempts = CorrectQuestion + InCorrectQuestion;
        if (attemp_count == 0) {
            Toast.makeText(activity, "Please Attempt Atleast One Question", Toast.LENGTH_SHORT).show();
        } else {
            backImage.setCompoundDrawablesWithIntrinsicBounds(drawPlay, null, null, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            TestSubmitBinding testBinding=DataBindingUtil.inflate(inflater,R.layout.test_submit,null,false);
            builder.setView(testBinding.getRoot());
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            testBinding.answered.setText(String.valueOf(Attempts));
            testBinding.skippedQueTest.setText(String.valueOf(total_question_count - (CorrectQuestion + InCorrectQuestion)));
            if (!activity.isFinishing() && !Flag) {
                testBinding.submitTest.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        try {
                            db.addreadUnread(Test_name + "Quiz_Submit", "quiz");
                            String value = db.getReadUnread(Test_name + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + "Quiz");
                            if (value.equals("")) {
                                db.addreadUnread(Test_name + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE) + "Quiz", "quiz");
                                Log.e("Visible", Test_name + "Quiz" + "Visible");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        CorrectQuestionn = String.valueOf(CorrectQuestion);
                        InCorrectQuestionn = String.valueOf(InCorrectQuestion);
                        int Attempt_Question = CorrectQuestion + InCorrectQuestion;
                        String Attempt_Questionn = String.valueOf(Attempt_Question);
                        int TotalQue = Integer.parseInt(item.getTotalquestio());
                        int NonAttempt = TotalQue - Attempt_Question;
                        Non_AttemptQuestion = String.valueOf(NonAttempt);
                        float Total_Mark = Integer.parseInt(item.getTotalquestio());
                        Total_Mark = Total_Mark * 4;
                        String Total_Markss = String.valueOf(Total_Mark);
                        long EndTimee = System.currentTimeMillis();
                        EndTime = TimeUnit.MILLISECONDS.toMinutes(EndTimee);
                        long CurrentTimeInSeC = TimeUnit.MINUTES.toSeconds(CurrentTime);
                        long EndTimeInSec = TimeUnit.MINUTES.toMinutes(EndTime);
                        Long TimeTakenInSec = (EndTimeInSec - CurrentTimeInSeC) % 60;
                        long TimeTaken = EndTime - CurrentTime;
                        String TimeTakeninSec = String.valueOf(TimeTakenInSec);
                        String Timetaken = String.valueOf(TimeTaken);
                        CorrectQuestionn = String.valueOf(CorrectQuestion);
                        InCorrectQuestionn = String.valueOf(InCorrectQuestion);
                        int skipContact = total_question_count - (CorrectQuestion + InCorrectQuestion);
                        Non_AttemptQuestion = String.valueOf(skipContact);
                        float Percentage = (float) CorrectQuestion * 100 / (float) Attempts;
                        Log.e("Percentage:--", "" + Percentage);
                        Percentagee = String.format("%.2f", Percentage);
                        try {
                            boolean resultalready = db.CheckIsResultAlreadyInDBorNotReadyStock(Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                            if (resultalready == false) {
                                db.addresult(CorrectQuestionn, InCorrectQuestionn, Non_AttemptQuestion, Percentagee,
                                        Test_name, correctM, wrongM, totalTime, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                            } else {
                                db.updateresult(CorrectQuestionn, InCorrectQuestionn, Non_AttemptQuestion,
                                        Percentagee, Test_name, correctM, wrongM, totalTime, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (alertDialog != null && alertDialog.isShowing() && activity != null) {
                            alertDialog.dismiss();
                        }
                        if (EditorialActivity.quiz_attemp == 1) {
                            EditorialActivity.quiz_attemp = 0;
                            Intent intent = new Intent(activity, TopicResultActivity.class);
                            intent.putExtra("testId", "" + Test_name);
                            intent.putExtra("Id", "" + RANKUID);
                            intent.putExtra("TestName", "Quiz Report");
                            activity.startActivity(intent);
                            activity.finish();
                        } else {
                            if (TestTitleName.equals("")) {
                                TestTitleName = "Quiz Report";
                            }
                            Intent intent = new Intent(activity, TopicResultActivity.class);
                            intent.putExtra("testId", "" + Test_name);
                            intent.putExtra("Id", "" + RANKUID);
                            intent.putExtra("week", "" + week);
                            intent.putExtra("TestName", TestTitleName);
                            intent.putExtra("TestTitleName", TestTitleName);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    }
                });
                testBinding.resumeTest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (alertDialog != null && alertDialog.isShowing() && activity != null) {
                            alertDialog.dismiss();
                        }
                        long millis = SharePrefrence.getInstance(activity).getLong(SharePrefrence.PAUSEBUTTON);
                        interfacee.TimerValue(millis);
                        backImage.setCompoundDrawablesWithIntrinsicBounds(drawBack, null, null, null);
                    }
                });
            } else {
                try {
                    String value = db.getReadUnread(Test_name + "Quiz");
                    if (value.equals("")) {
                        db.addreadUnread(Test_name + "Quiz", "quiz");
                        Log.e("Visible", Test_name + "Quiz" + "Visible");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                CorrectQuestionn = String.valueOf(CorrectQuestion);
                InCorrectQuestionn = String.valueOf(InCorrectQuestion);
                int Attempt_Question = CorrectQuestion + InCorrectQuestion;
                String Attempt_Questionn = String.valueOf(Attempt_Question);
                int TotalQue = Integer.parseInt(item.getTotalquestio());
                int NonAttempt = TotalQue - Attempt_Question;
                Non_AttemptQuestion = String.valueOf(NonAttempt);
                float Total_Mark = Integer.parseInt(item.getTotalquestio());
                Total_Mark = Total_Mark * 4;
                String Total_Markss = String.valueOf(Total_Mark);
                long EndTimee = System.currentTimeMillis();
                EndTime = TimeUnit.MILLISECONDS.toMinutes(EndTimee);
                long CurrentTimeInSeC = TimeUnit.MINUTES.toSeconds(CurrentTime);
                long EndTimeInSec = TimeUnit.MINUTES.toMinutes(EndTime);
                Long TimeTakenInSec = (EndTimeInSec - CurrentTimeInSeC) % 60;
                long TimeTaken = EndTime - CurrentTime;
                String TimeTakeninSec = String.valueOf(TimeTakenInSec);
                String Timetaken = String.valueOf(TimeTaken);
                try {
                    boolean resultalready = db.CheckIsResultAlreadyInDBorNotReadyStock(Test_name, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    if (resultalready == false) {
                        db.addresult(CorrectQuestionn, InCorrectQuestionn, Non_AttemptQuestion, Percentagee,
                                Test_name, correctM, wrongM, totalTime, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    } else {
                        db.updateresult(CorrectQuestionn, InCorrectQuestionn, Non_AttemptQuestion, Percentagee,
                                Test_name, correctM, wrongM, totalTime, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                CorrectQuestionn = String.valueOf(CorrectQuestion);
                InCorrectQuestionn = String.valueOf(InCorrectQuestion);
                int skipContact = total_question_count - (CorrectQuestion + InCorrectQuestion);
                Non_AttemptQuestion = String.valueOf(skipContact);
                float Percentage;
                if (isTimer) {

                    Float CorrectMark = CorrectQuestion * Float.parseFloat(correctM);
                    Float WrongMark = InCorrectQuestion * Float.parseFloat(wrongM);
                    Float totalMark = (float) Attempts * 2;
                    Float getMark = CorrectMark - WrongMark;
                    Percentage = getMark * 100 / totalMark;
                } else {
                    Percentage = CorrectQuestion * 100 / Attempts;
                }
                Percentagee = String.valueOf(Percentage);
                Intent intent = new Intent();
                intent.putExtra("correct", CorrectQuestionn);
                intent.putExtra("incorrect", InCorrectQuestionn);
                intent.putExtra("skipanswer", Non_AttemptQuestion);
                intent.putExtra("acc", Percentagee);
                intent.putExtra("UID", UID);
                intent.putExtra("RANKUID", RANKUID);
                intent.putExtra("PractiseTestName", PractiseTestName);
                intent.putExtra("TestTitleName", TestTitleName);
                intent.putExtra("wrongM", wrongM);
                intent.putExtra("correctM", correctM);
                intent.putExtra("totalTime", totalTime);
                activity.setResult(1000, intent);
                activity.finish();
            }
        }
    }
}