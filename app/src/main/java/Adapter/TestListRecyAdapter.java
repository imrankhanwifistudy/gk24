package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.TopicTestRecyAdapterBinding;

import org.json.JSONArray;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.TopicTestList;

public class TestListRecyAdapter extends RecyclerView.Adapter<TestListRecyAdapter.MyViewHolder> {
    private ArrayList<TopicTestList> arrTestList;
    Activity activity;
    DatabaseHandler db;
    private int mParent;
    private JSONArray mColors;

    public TestListRecyAdapter(Activity activity, ArrayList<TopicTestList> arrTestList) {
        this.arrTestList = arrTestList;
        this.activity = activity;
        db = new DatabaseHandler(activity);
        loadJSONFromAsset(activity);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TopicTestRecyAdapterBinding binding;

        MyViewHolder(View view, TopicTestRecyAdapterBinding binding) {
            super(view);
            this.binding = binding;
        }
    }

    @Override
    public void onViewDetachedFromWindow(final MyViewHolder holder) {
        holder.itemView.clearAnimation();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        String theme2 = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater child = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TopicTestRecyAdapterBinding binding = DataBindingUtil.inflate(child, R.layout.topic_test_recy_adapter, parent, false);
        return new MyViewHolder(binding.getRoot(), binding);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.binding.setTopicTest(arrTestList.get(position));
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        String uniq = year + arrTestList.get(position).getId() + "TopicTest" + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE);
        holder.binding.lockStstus.setImageResource(R.drawable.ic_chevron_right_gray);
        try {
            boolean flag = db.CheckIsResultAlreadyInDBorNotReadyStock(uniq, SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            if (flag) {
                holder.binding.testQuestion.setText("See Result");
                holder.binding.testQuestion.setTextColor(Color.parseColor("#FF0000"));
            } else {
                holder.binding.testQuestion.setText(String.format("%s Ques", arrTestList.get(position).getNoofQuestion()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.binding.colottxt.setText(String.valueOf(position + 1));
        try {
            setColors(position, holder.binding.layoutcolor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.up_from_bottom);
        holder.itemView.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return arrTestList.size();
    }

    private void setColors(int position, View colottxt) {
        try {
            String colorString = "#" + mColors.getJSONArray(mParent % 10).getString(position % 10);
            Drawable drwa = Utils.DrawableChange(activity, R.drawable.wrong, colorString);
            colottxt.setBackgroundDrawable(drwa);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadJSONFromAsset(Context ctx) {
        try {
            InputStream is = ctx.getAssets().open("colors.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String stringJson = new String(buffer, "UTF-8");
            mColors = new JSONArray(stringJson);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}