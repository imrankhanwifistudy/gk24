package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.QuizAdapterBinding;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import Custom.HorizontalViewPager;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DateItem;

public class QuizAdapter extends PagerAdapter {
    Activity activity;
    private ViewPager pagerMain;
    private QuizInsideAdapter quizInsideAdapter;
    private int Count =0,t = 0;
    DatabaseHandler db;
    private ArrayList<String> actualdate;
    public QuizAdapter(Activity activity, ViewPager pagerMain, int size_date, ArrayList<String> actualdate) {
        this.activity = activity;
        this.pagerMain = pagerMain;
        this.actualdate=actualdate;
        db=new DatabaseHandler(activity);
    }
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return actualdate.size()-4;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @SuppressLint("SetTextI18n")
    @Override
    public Object instantiateItem(View collection, final int position) {
        String theme2= SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        QuizAdapterBinding binding= DataBindingUtil.inflate(inflater,R.layout.quiz_adapter,null,false);
        ArrayList<DateItem> arrayList1 = new ArrayList<>();
        DateItem item = new DateItem();
        item.setId(MainActivity.actualdate.get(position));
        item.setDate(MainActivity.actualdate.get(position));
        item.setWeek_no(MainActivity.months.get(position));
        if(MainActivity.quiz_type.equalsIgnoreCase("Daily")) {
            arrayList1.add(item);
            arrayList1.add(item);
        }else{
            arrayList1.add(item);
        }
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        calendar.add(Calendar.MONTH,-1);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat MonthForm = new SimpleDateFormat("yyyy-MM");
        String month=MonthForm.format(calendar.getTime());
        SharePrefrence.getInstance(activity).putString("quiz_lang" , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
        boolean isData1=false;
        if(t == 0){
            t++;
            try{
                isData1 = db.CheckInQuiz(month+month , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            }catch (Exception e){
                e.printStackTrace();
            }
            if(isData1){
                MainActivity.activityMainBinding.monthlyCount.setVisibility(View.GONE);
                if(Count == 0){
                    MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
                }
            }else{
                MainActivity.activityMainBinding.monthlyCount.setVisibility(View.VISIBLE);
                Count=Count+1;
                MainActivity.activityMainBinding.QuizCount.setVisibility(View.VISIBLE);
                MainActivity.activityMainBinding.QuizCount.setText(""+Count);
            }


            boolean isData2=false;
            try{
                isData2 = db.CheckInQuiz(month+month+"w-"+MainActivity.weeks1.get(0) , SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            }catch (Exception e){
                e.printStackTrace();
            }
            if(isData2){
                MainActivity.activityMainBinding.weeklyCount.setVisibility(View.GONE);
                if(Count == 0){
                    MainActivity.activityMainBinding.QuizCount.setVisibility(View.GONE);
                }
            }else{
                MainActivity.activityMainBinding.weeklyCount.setVisibility(View.VISIBLE);
                Count=Count+1;
                MainActivity.activityMainBinding.QuizCount.setVisibility(View.VISIBLE);
                MainActivity.activityMainBinding.QuizCount.setText(""+Count);
            }
        }
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerView.setHasFixedSize(true);
        Display display = activity.getWindowManager().getDefaultDisplay();
        int height = (display.getHeight());
        ViewGroup.LayoutParams params =binding.recyclerView.getLayoutParams();
        params.height = height - 120;
        binding.recyclerView.setLayoutParams(params);
        quizInsideAdapter = new QuizInsideAdapter(activity, arrayList1, pagerMain, MainActivity.actualdate,Count,binding.recyclerView,position);
        binding.recyclerView.setAdapter(quizInsideAdapter );
        ((HorizontalViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((HorizontalViewPager) collection).removeView((View) view);
    }
}