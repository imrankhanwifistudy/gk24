package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;

import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.LayoutPrefAdapterBinding;

import java.util.ArrayList;

import DB.SharePrefrence;
import DB.Utills;

public class DefaultLanguagePrefAdapter extends BaseAdapter {
    private ArrayList<String> languageName;
    private ArrayList<String> languageinLN;
    Activity activity;

    public DefaultLanguagePrefAdapter(Activity activity, ArrayList<String> languageName, ArrayList<String> languageinLN) {
        this.languageName = languageName;
        this.languageinLN = languageinLN;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return languageName.size();
    }

    @Override
    public Object getItem(int i) {
        return languageName.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder")
        LayoutPrefAdapterBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_pref_adapter, viewGroup, false);

        binding.languageCode.setText(languageinLN.get(i));
        binding.languageName.setText(languageName.get(i));
        int selected = SharePrefrence.getInstance(activity).getInteger(Utills.DEFAULT_LANGUAGE_POSITION);
        if (i == selected) {
            binding.ivCorrect.setVisibility(View.VISIBLE);
        } else {
            binding.ivCorrect.setVisibility(View.GONE);
        }
        return binding.getRoot();
    }
}
