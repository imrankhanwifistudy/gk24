package Adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ParagraphAdapterBinding;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ClickListener.ParagraphClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.QuizModel;

public class GkPagerAdapter extends PagerAdapter {
    final Activity activity;
    private ArrayList<QuizModel> testitem;
    private ViewPager pagerMain;
    DatabaseHandler db;
    private SharedPreferences.Editor editor;
    private long lastClickTime = 0;
    private long DOUBLE_CLICK_TIME_DELTA = 300;
    private String user_id;
    private ParagraphAdapterBinding binding;
    private String theme2;

    @SuppressLint("CommitPrefEdits")
    public GkPagerAdapter(Activity activity, ArrayList<QuizModel> testitem, ViewPager pagerMain) {
        SharedPreferences spfd = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = spfd.edit();
        user_id = spfd.getString("uid", "");
        this.activity = activity;
        this.testitem = testitem;
        this.pagerMain = pagerMain;
        db = new DatabaseHandler(activity);
    }

    @Override
    public int getCount() {
        return testitem.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public Object instantiateItem(View collection, final int position) {
        theme2 = SharePrefrence.getInstance(activity).getString("Themes");
        switch (theme2) {
            case "night":
                activity.setTheme(R.style.night);
                break;
            case "sepia":
                activity.setTheme(R.style.sepia);
                break;
            default:
                activity.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(inflater, R.layout.paragraph_adapter, null, false);
        binding.setClick(new ParagraphClickListner(binding, activity, pagerMain));
        binding.totalUpvotes.setVisibility(View.GONE);
        binding.upvote.setVisibility(View.INVISIBLE);
        binding.bookmarkbtn.setVisibility(View.INVISIBLE);
        binding.sharebtn.setVisibility(View.INVISIBLE);
        binding.bookmarkbootm.setVisibility(View.GONE);
        binding.langPref.setVisibility(View.GONE);
        Drawable drawable2 = Utils.DrawableChange(activity, R.drawable.ic_brightness_medium_white_24dp, "#0287c3");
        binding.theme.setCompoundDrawablesWithIntrinsicBounds(drawable2, null, null, null);
        final String id = testitem.get(position).getId();
        final String webDate = testitem.get(position).getDate();
        binding.learnVocab.setVisibility(View.GONE);
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        final String year = YearForm.format(calendar.getTime());
        if (testitem.size() > 1) {
            SpannableString spannable = new SpannableString(position + 1 + "/" + testitem.size());
            StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            spannable.setSpan(bss, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            binding.pageNumber.setText(spannable);
        } else {
            binding.pageNumber.setText("");
        }
        final String uniqid = webDate + id + year;
        binding.back.setVisibility(View.VISIBLE);
        pagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try {
                    db.addreadUnread(testitem.get(position).getId(), "capsule");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageSelected(int position) {
                String webDate = testitem.get(position).getDate();
                Calendar calendar = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
                String year = YearForm.format(calendar.getTime());
                final String uniqid = webDate + testitem.get(position).getCatID() + testitem.get(position).getSubCatID() + year + "GkNoteRead";
                try {
                    String value = db.getReadUnread(uniqid);
                    if (value.equals("")) {
                        db.addreadUnread(uniqid, "NotesRead");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        try {
            parseData(testitem.get(position), binding.paragraph, binding.image);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((ViewPager) collection).addView(binding.getRoot());
        return binding.getRoot();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void parseData(final QuizModel response, final WebView tvParagraph, ImageView image) throws JSONException {
        binding.meaniniTv.setText(Html.fromHtml(response.getTitle()));
        binding.mainWord.setText(Html.fromHtml(response.getHeadings()));
        boolean swImage = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_SWITCH);
        if (swImage)
            Glide.with(activity).load(response.getImages()).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(image);
        Pattern REMOVE_TAGS = Pattern.compile("<.+?>\\/");
        Matcher m = REMOVE_TAGS.matcher(response.getNote().replace("•", "<br><br>● "));
        String ss = m.replaceAll("").replace(".", ". ");
        String str = "<html> <head>  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'> <meta name='apple-mobile-web-app-capable' content='yes'> </head> <body>";
        String Lang_selected = SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE);
        if (Lang_selected.equalsIgnoreCase("ENGLISH")) {
            switch (theme2) {
                case "night":
                    tvParagraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' onclick='wordClicked(this)'>" + ss + "</span> ";
                    break;
                case "sepia":
                    tvParagraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                    break;
                default:
                    tvParagraph.setBackgroundColor(Color.parseColor("#ffffff"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' onclick='wordClicked(this)'>" + ss + "</span> ";
                    break;
            }
        } else {
            switch (theme2) {
                case "night":
                    tvParagraph.setBackgroundColor(Color.parseColor("#2B3E50"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#D4D9DC;' >" + ss + "</span> ";
                    break;
                case "sepia":
                    tvParagraph.setBackgroundColor(Color.parseColor("#F8F2E2"));
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss + "</span> ";
                    break;
                default:
                    tvParagraph.setBackgroundColor(Color.WHITE);
                    str = str + "<span class='words' style='font-size:16px;line-height:1.5em;color:#2B3E50;' >" + ss + "</span> ";
                    break;
            }
        }
        tvParagraph.loadDataWithBaseURL("file:///android_asset/", str + "</body>", "text/html", "UTF-8", null);
        tvParagraph.getSettings().setJavaScriptEnabled(true);
        tvParagraph.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        tvParagraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        tvParagraph.getSettings().setAppCacheEnabled(false);
        tvParagraph.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        tvParagraph.getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        boolean AUTO_IMAGE_SMALLER = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_SMALLER);
        if (AUTO_IMAGE_SMALLER) {
            tvParagraph.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
        }
        boolean AUTO_IMAGE_DEFAULT = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_DEFAULT);
        if (AUTO_IMAGE_DEFAULT) {
            tvParagraph.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        }
        boolean AUTO_IMAGE_LARGE = SharePrefrence.getInstance(activity).getBoolean(Utills.AUTO_IMAGE_LARGE);
        if (AUTO_IMAGE_LARGE) {
            tvParagraph.getSettings().setTextSize(WebSettings.TextSize.LARGER);
        }
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
}