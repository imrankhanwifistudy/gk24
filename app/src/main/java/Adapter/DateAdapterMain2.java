package Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.daily.currentaffairs.MainActivity;
import com.daily.currentaffairs.R;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
@SuppressWarnings("ALL")
public class DateAdapterMain2 extends PagerAdapter {
    Activity context;
    ArrayList<String> testitem;
    int pos ;
    String quiz_type;
    ArrayList<String> year;
    ViewPager pagerDate;
    int count=0;
    DatabaseHandler db;
    public DateAdapterMain2(Activity activity, ArrayList<String> months, ArrayList<String> year, String quiz_type, ViewPager pagerdate, int currentItem) {
        this.context = activity;
        this.testitem = months;
        this.quiz_type=quiz_type;
        this.year=year;
        db = new DatabaseHandler(activity);
        this.pagerDate=pagerdate;
        pos=currentItem;
    }
    @Override
    public int getCount() {
        return testitem.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
    class ViewHolder {
        TextView date, month, bullet;
        RelativeLayout mainLayout;
    }
    @Override
    public float getPageWidth(int position) {
        return .2f;
    }
    @Override
    public Object instantiateItem(View collection, final int position) {
        final ViewHolder holder = new ViewHolder();
        String theme2=SharePrefrence.getInstance(context).getString("Themes");
        switch (theme2) {
            case "night":
                context.setTheme(R.style.night);
                break;
            case "sepia":
                context.setTheme(R.style.sepia);
                break;
            default:
                context.setTheme(R.style.defaultt);
                break;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.date_pager, null);
        holder.date = (TextView) layout.findViewById(R.id.date_home);
        holder.month = (TextView) layout.findViewById(R.id.month);
        holder.mainLayout = (RelativeLayout) layout.findViewById(R.id.mainLayout);
        holder.bullet = (TextView) layout.findViewById(R.id.bullet);
        if(testitem.get(position).equals("")){
            holder.date.setVisibility(View.GONE);
            holder.month.setVisibility(View.GONE);
            holder.bullet.setVisibility(View.GONE);
        }else {
            try{
                holder.date.setVisibility(View.VISIBLE);
                holder.month.setVisibility(View.VISIBLE);
                holder.bullet.setVisibility(View.VISIBLE);
                if(quiz_type.equals("Monthly"))
                {
                    holder.month.setText(year.get(position));
                    holder.date.setText(testitem.get((position)));
                }else  if(quiz_type.equals("Weekly")){
                    holder.month.setText(testitem.get(position));
                    holder.date.setText("W-"+year.get(position));
                }else {
                    holder.month.setText(year.get(position));
                    holder.date.setText(testitem.get(position));
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        int positionPager =0;
        positionPager = SharePrefrence.getInstance(context).getInteger(Utills.POSI_VIEWPAGER_PARA);
        mainTainsize(position, MainActivity.activityMainBinding.pagerdate.getCurrentItem(), holder.date, holder.month, holder.bullet);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pagerDate.setCurrentItem(position);
                pagerDate.setAlpha(1);
                pagerDate.setTranslationX(0);
                pagerDate.setScaleX(1);
                pagerDate.setScaleY(1);

            }
        });
        String id = MainActivity.actualdate.get(position);
        String webDate = MainActivity.actualdate.get(position);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat YearForm = new SimpleDateFormat("yyyy");
        String year = YearForm.format(calendar.getTime());
        String which = SharePrefrence.getInstance(context).getString(Utills.WHICH_SELECTED);
        if (which.equalsIgnoreCase("1")) {
            try{
                int value = db.TodayReadParaGraph(webDate+SharePrefrence.getInstance(context).getString(Utills.DEFAULT_LANGUAGE));
                int valueccount = db.TodayTotalParaGraphCount(webDate);
                if (value >= valueccount && valueccount != 0) {
                    holder.bullet.setVisibility(View.GONE);
                }else {
                    if(testitem.get(position).equals("")){
                        holder.date.setVisibility(View.GONE);
                        holder.month.setVisibility(View.GONE);
                    }else {
                        holder.bullet.setVisibility(View.VISIBLE);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (which.equalsIgnoreCase("2")) {
            final String uniqid = webDate + year + id + "Word";
            String value1="";
            try{
                value1 = db.getReadUnread(uniqid);
            }catch (Exception e){
                e.printStackTrace();
            }
            if (!value1.equals("")) {
                holder.bullet.setVisibility(View.GONE);
            } else {
                if(testitem.get(position).equals("")){
                    holder.date.setVisibility(View.GONE);
                    holder.month.setVisibility(View.GONE);
                    holder.bullet.setVisibility(View.GONE);
                }else {
                    holder.bullet.setVisibility(View.VISIBLE);
                }
            }
        }
        if (which.equalsIgnoreCase("3")) {
            final String uniqid = id + webDate + year +SharePrefrence.getInstance(context).getString(Utills.DEFAULT_LANGUAGE)+ "Quiz";
            String value2="";
            try{
                value2 = db.getReadUnread(uniqid);
            }catch (Exception e){
                e.printStackTrace();
            }
            if (!value2.equals("")) {
                holder.bullet.setVisibility(View.GONE);
            } else {
                if(testitem.get(position).equals("")){
                    holder.date.setVisibility(View.GONE);
                    holder.month.setVisibility(View.GONE);
                    holder.bullet.setVisibility(View.GONE);
                }else {
                    holder.bullet.setVisibility(View.VISIBLE);
                }
            }
        }
        ((ViewPager) collection).addView(layout);
        return layout;
    }
    private void mainTainsize(int position, int positionPager, TextView date, TextView Month, TextView bullet) {
        if (position == positionPager) {
            date.setTextSize(13);
            Month.setTextSize(11);
            bullet.setPadding(0, 3, 0, 0);
            date.setTypeface(null, Typeface.BOLD);
            Month.setTypeface(null, Typeface.BOLD);
        } else if (position == positionPager + 1) {
            date.setTextSize(11);
            date.setPadding(0, 2, 0, 0);
            Month.setTextSize(10);
            Month.setPadding(0, 2, 0, 0);
            date.setAlpha(0.5f);
            bullet.setPadding(0, 2, 0, 0);
            Month.setAlpha(0.5f);
            bullet.setAlpha(0.5f);
            date.setTypeface(null, Typeface.BOLD);
            Month.setTypeface(null, Typeface.BOLD);
        } else if (position == positionPager + 2) {
            date.setTextSize(10);
            date.setPadding(0, 4, 0, 0);
            Month.setTextSize(9);
            Month.setPadding(0, 4, 0, 0);
            date.setAlpha(0.5f);
            bullet.setPadding(0, 4, 0, 0);
            Month.setAlpha(0.5f);
            bullet.setAlpha(0.5f);
            date.setTypeface(null, Typeface.BOLD);
            Month.setTypeface(null, Typeface.BOLD);
        } else if (position == positionPager + 3) {
            date.setTextSize(9);
            date.setPadding(0, 6, 0, 0);
            Month.setTextSize(8);
            Month.setPadding(0, 6, 0, 0);
            date.setAlpha(0.5f);
            bullet.setPadding(0, 6, 0, 0);
            Month.setAlpha(0.5f);
            bullet.setAlpha(0.5f);
            date.setTypeface(null, Typeface.BOLD);
            Month.setTypeface(null, Typeface.BOLD);
        } else {
            date.setTextSize(8);
            date.setPadding(0, 10, 0, 0);
            Month.setTextSize(7);
            Month.setPadding(0, 10, 0, 0);
            date.setAlpha(0.5f);
            bullet.setPadding(0, 10, 0, 0);
            Month.setAlpha(0.5f);
            bullet.setAlpha(0.5f);
            date.setTypeface(null, Typeface.BOLD);
            Month.setTypeface(null, Typeface.BOLD);
        }
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
    public static String getMonth(int month) {
        return new DateFormatSymbols().getShortMonths()[month-1];
    }
}