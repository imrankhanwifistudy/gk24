package Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daily.currentaffairs.R;
import com.daily.currentaffairs.databinding.ItemVideoBinding;

import java.util.ArrayList;

import ClickListener.VideoClickListner;
import Custom.Utils;
import DB.DatabaseHandler;
import DB.SharePrefrence;
import DB.Utills;
import Modal.DataObject;

public class MyRecyclerVideoAdapter extends RecyclerView.Adapter<MyRecyclerVideoAdapter.MyViewHolder> {
    Activity activity;
    private ArrayList<DataObject> list;
    DatabaseHandler db;
    private String bookmark;
    private TextView noTest;

    public MyRecyclerVideoAdapter(Activity activity, ArrayList<DataObject> list, String bookmark, TextView noTest) {
        this.activity = activity;
        this.list = list;
        db = new DatabaseHandler(activity);
        this.bookmark = bookmark;
        this.noTest = noTest;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ItemVideoBinding binding;

        MyViewHolder(View view, ItemVideoBinding binding) {
            super(view);
            this.binding = binding;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemVideoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_video, parent, false);
        return new MyViewHolder(binding.getRoot(), binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.setIsRecyclable(false);
        holder.binding.setDataObject(list.get(position));
        DataObject movie = list.get(position);
        holder.binding.txttitle.setTextSize(14);
        if (movie.getDuration().equalsIgnoreCase("")) {
            holder.binding.duration.setVisibility(View.GONE);
        }
        Glide.with(activity).load(list.get(position).getImageUrl()).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.binding.image);
        String day = Utils.getDays(list.get(position).getDate());
        if (day.equalsIgnoreCase("0")) {
            holder.binding.days.setText("Today");
            String value = "";
            try {
                value = db.getReadUnread(list.get(position).getId() + SharePrefrence.getInstance(activity).getString(Utills.DEFAULT_LANGUAGE));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (value.equalsIgnoreCase("")) {
                holder.binding.bolt.setVisibility(View.VISIBLE);
            } else {
                holder.binding.bolt.setVisibility(View.GONE);
            }
        } else {
            holder.binding.bolt.setVisibility(View.GONE);
            holder.binding.days.setText(day + " Days ago");
        }
        holder.binding.setClick(new VideoClickListner(activity, holder.binding, position, bookmark, list, noTest));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}